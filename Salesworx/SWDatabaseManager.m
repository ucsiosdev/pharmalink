



//
//  SWDatabaseManager.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/7/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWDatabaseManager.h"
///
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "AllUserList.h"
#import "FSRMessagedetails.h"
#import "SurveyQuestionDetails.h"
#import "SurveyResponseDetails.h"
#import "SurveyDetails.h"
#import "PlannedVisitDetails.h"
#import "CustomerShipAddressDetails.h"
#import "AppConstantsList.h"
#import "DataSyncManager.h"
//#import "ILAlertView.h"
#import "SWDefaults.h"
#import "SWFoundation.h"
#import "StatementViewController.h"
#import "SWAppDelegate.h"
#import "MedRepQueries.h"
#import "NSDecimalNumber+SalesWorxDecimalNumber.h"

#define ksampleQuery select Customer_Name from TBL_Customer_Ship_Address;

@implementation SWDatabaseManager

@synthesize target,action;



#pragma cache_size =1

static SWDatabaseManager *sharedSingleton=nil;
//static sqlite3 *db;

+ (SWDatabaseManager *)retrieveManager{
    @synchronized(self)
    {
        if (!sharedSingleton)
        {
            //NSLog(@"DATABASE MANAGER ALLOCS");
            sharedSingleton = [[SWDatabaseManager alloc] init];
        }
        return sharedSingleton;
    }
    
}
+ (void)destroyMySingleton
{
    sharedSingleton = nil;
    // [self performSelector:@selector(cleanObject) withObject:nil afterDelay:0.0];
    
}

-(void)cleanObject
{
    //    resultsArray=nil;
    //    result=nil;
    //    resultSet=nil;
    //    columnName=nil;
    //single=nil;
    
    //Product
    
    //Dashboard
    // actualVisitArray=nil;
    
    //Customer
    //sections=nil;
    
    //Send Order
    //    mainDictionary=nil;
    //    orderDictionary=nil;
    //    itemDictionary =nil;
    //    lotDictionary =nil;
    //    orderArray1=nil;
    //    itemArray=nil;
    //    lotArray1=nil;
    //    mainArray=nil;
    //       id target;
    action=nil;
    //xmlWriter=nil;
    
    //Collection
    
    //Route
    //locationManager=nil;
    latitude=nil;
    longitude=nil;
}
- (id)init{
    if ((self = [super init]))
    {
        
    }
    return self;
}

- (FMDatabase *)getDatabase
{
    
    
    
    
    
    //iOS 8 support
    NSString *documentDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    
    
    //    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentDir = [documentPaths objectAtIndex:0];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    return db;
}


- (BOOL)executeNonQueryOne:(NSString *)sqlQuery
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:sqlQuery];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return success;
}
- (void)executeNonQuery:(NSString *)sql {
    //[self doQuery:sql];
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:sql];
        NSLog(@"insert successfull %hhd", success);
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    //return success;
    
    
}










- (NSMutableArray *)fetchDataForQuery:(NSString *)sql
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSMutableArray *newsItems = [NSMutableArray new];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            [newsItems addObject:[results resultDictionary]];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        
        [db close];
    }
    
    return newsItems;
    
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (NSMutableArray *)GetBonusProductWithDescription:(NSString *)pname
{
    NSMutableArray *GameInfo =[NSMutableArray array];
    NSString *sqlQuery = [NSString stringWithFormat:@"SELECT * FROM TBL_Product where Description = '%@'",pname];
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sqlQuery];
        while ([results next])
        {
            [GameInfo addObject:[results resultDictionary]];
            
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return GameInfo;
}


-(BOOL)databaseExists
{
    BOOL status=NO;
    NSString *documentDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    
    status=[[NSFileManager defaultManager]fileExistsAtPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    return status;
}

-(NSArray*)fetchSurveyforBrandAmbassador
{
    NSString * surveyQry=@"select IFNULL(Survey_ID,'N/A') As Survey_ID, IFNULL(Survey_Title,'N/A') AS Survey_Title, IFNULL(Survey_Type_Code,'N/A') AS Survey_Type_Code,IFNULL(Start_Time,'N/A') AS Start_Time,IFNULL(End_Time,'N/A') AS End_Time from TBL_Survey where  Survey_Type_Code ='B' ";
    NSMutableArray *retVal =[NSMutableArray array];

    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:surveyQry];
        while ([results next])
        {
            int  Survey_ID =[[results stringForColumnIndex:0] intValue];
            NSString * Survey_Title  = [results stringForColumnIndex:1];
            int  Customer_ID=[[results stringForColumnIndex:2] intValue];
            int  Site_Use_ID =[[results stringForColumnIndex:3] intValue];
            int  SalesRep_ID  =[[results stringForColumnIndex:4] intValue];
            
            NSString * Survey_Type_Code  = [results stringForColumnIndex:5];
            NSString * Start_Time  =[results stringForColumnIndex:6];
            NSString *  End_Time  = [results stringForColumnIndex:7];
            
            SurveyDetails *info=[[SurveyDetails alloc] initWithSurveyId:Survey_ID Customer_ID:Customer_ID Site_Use_ID:Site_Use_ID SalesRep_ID:SalesRep_ID Survey_Title:Survey_Title Start_Time:Start_Time End_Time:End_Time Survey_Type_Code:Survey_Type_Code];
            [retVal addObject:info];
            
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    NSArray * sortedSurveyArray;
    
    if (retVal.count>0) {
        
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"Survey_Title" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
        
        sortedSurveyArray=[retVal sortedArrayUsingDescriptors:sortDescriptors];
        return sortedSurveyArray;
        
    }
    else
    {
        return  retVal;
    }
    

    
}

- (NSArray*)selectSurveyWithCustomerID:(NSString *)customerID andSiteUseID:(NSString *)siteID{
    
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM TBL_Survey Where Customer_ID='%@' and Site_Use_ID='%@'",customerID,siteID];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            int  Survey_ID =[[results stringForColumnIndex:0] intValue];
            NSString * Survey_Title  = [results stringForColumnIndex:1];
            int  Customer_ID=[[results stringForColumnIndex:2] intValue];
            int  Site_Use_ID =[[results stringForColumnIndex:3] intValue];
            int  SalesRep_ID  =[[results stringForColumnIndex:4] intValue];
            
            NSString * Survey_Type_Code  = [results stringForColumnIndex:5];
            NSString * Start_Time  =[results stringForColumnIndex:6];
            NSString *  End_Time  = [results stringForColumnIndex:7];
            
            SurveyDetails *info=[[SurveyDetails alloc] initWithSurveyId:Survey_ID Customer_ID:Customer_ID Site_Use_ID:Site_Use_ID SalesRep_ID:SalesRep_ID Survey_Title:Survey_Title Start_Time:Start_Time End_Time:End_Time Survey_Type_Code:Survey_Type_Code];
            [retVal addObject:info];
            
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    NSArray * sortedSurveyArray;
    
    if (retVal.count>0) {
        
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"Survey_Title" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
        
        sortedSurveyArray=[retVal sortedArrayUsingDescriptors:sortDescriptors];
        return sortedSurveyArray;
        
    }
    else
    {
        return  retVal;
    }
    
    
}


-(NSArray*)checkBrandAmbassadorSurveyTaken:(NSString*)surveyID
{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT Survey_ID,Survey_Timestamp FROM TBL_Survey_Cust_Responses Where  and Survey_ID='%@'",surveyID];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSString * Survey_ID =[results stringForColumnIndex:0 ];
            
            NSString * Start_Time  = [results stringForColumnIndex:1];
            
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            
            
            [tempDict setValue:Survey_ID forKey:@"Survey_ID"];
            [tempDict setValue:Start_Time forKey:@"Start_Time"];
            
            
            [retVal addObject:tempDict];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}

- (NSArray*)checkSurveyIsTaken:(NSString *)customerID andSiteUseID:(NSString *)siteID ansSurveyID:(NSString *)surveyID;{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT Survey_ID,Survey_Timestamp FROM TBL_Survey_Cust_Responses Where Customer_ID='%@' and Site_Use_ID='%@' and Survey_ID='%@'",customerID,siteID,surveyID];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSString * Survey_ID =[results stringForColumnIndex:0 ];
            
            NSString * Start_Time  = [results stringForColumnIndex:1];
            
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            
            
            [tempDict setValue:Survey_ID forKey:@"Survey_ID"];
            [tempDict setValue:Start_Time forKey:@"Start_Time"];
            
            
            [retVal addObject:tempDict];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (NSArray*)selectQuestionBySurveyId:(int)SurveyId{
    
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM TBL_Survey_Questions where Survey_ID=%d",SurveyId];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            int Question_ID=[[results stringForColumnIndex:0] intValue];
            
            int Survey_ID=[[results stringForColumnIndex:2] intValue];
            int  Default_Response_ID=[[results stringForColumnIndex:3] intValue];
            NSString *Question_Text = [results stringForColumnIndex:1];
            
            SurveyQuestionDetails *info=[[SurveyQuestionDetails alloc] initWithUniqueQuestionId:Question_ID Survey_ID:Survey_ID Default_Response_ID:Default_Response_ID Question_Text:Question_Text];
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return retVal;
    
}



-(NSArray*)ModifyResponseType:(NSString*)query
{
    NSMutableArray *retVal =[NSMutableArray array];
    FMDatabase *db=[self getDatabase];
    
    [db open];
    @try
    {
        FMResultSet *results = [db executeQuery:query];
        while ([results next])
        {
            int Question_ID=[[results stringForColumnIndex:0] intValue];
            
            int Survey_ID=[[results stringForColumnIndex:2] intValue];
            int  Default_Response_ID=[[results stringForColumnIndex:3] intValue];
            NSString *Question_Text = [results stringForColumnIndex:1];
            
            SurveyQuestionDetails *info=[[SurveyQuestionDetails alloc] initWithUniqueQuestionId:Question_ID Survey_ID:Survey_ID Default_Response_ID:Default_Response_ID Question_Text:Question_Text];
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return retVal;
}



- (NSArray*)selectResponseType:(int)QuestionId{
    
    
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM TBL_Survey_Responses where Question_Id =%d",QuestionId];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            int Question_ID=[[results stringForColumnIndex:2] intValue];
            
            int Response_ID=[[results stringForColumnIndex:0] intValue];
            int Response_Type_ID=[[results stringForColumnIndex:3] intValue];
            NSString *Response_Text = [results stringForColumnIndex:1];
            
            SurveyResponseDetails *info=[[SurveyResponseDetails alloc] initWithUniqueQuestionId:Question_ID Response_ID:Response_ID Response_Type_ID:Response_Type_ID Response_Text:Response_Text];
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    
    return retVal;
}
- (int) dbGetCustomerResponseCount{
    int count = 0;
    NSString *sql = @"SELECT COUNT(*) FROM TBL_Survey_Cust_Responses";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            count = [[results stringForColumnIndex:0] intValue];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return count;
}
- (int) dbGetAuditResponseCount{
    
    int count = 0;
    NSString *sql = @"SELECT COUNT(*) FROM TBL_Survey_Audit_Responses";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            count = [[results stringForColumnIndex:0] intValue];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return count;
}
- (void)InsertdataCustomerResponse:(NSString*)strquery{
    DataSyncManager *appDelegate =[DataSyncManager sharedManager];
    
    NSString *selectSQL =strquery;
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:selectSQL];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    if(appDelegate.alertMessageShown==0)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"InsertSurveyNotification"object:self];
        appDelegate.alertMessageShown=1;
    }
    
}
//Communication Module
- (NSArray*)dbGetFSRMessages{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM TBL_FSR_Messages"];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            int Message_ID=[[results stringForColumnIndex:0] intValue];
            
            int SalesRep_ID=[[results stringForColumnIndex:1] intValue];
            
            NSString *Message_Title = [results stringForColumnIndex:2];
            NSString *Message_Content = [results stringForColumnIndex:3];
            NSString *Message_date = [results stringForColumnIndex:4];
            NSString *Message_Expiry_date = [results stringForColumnIndex:5];
            NSString *Message_Read = [results stringForColumnIndex:6];
            NSString *Message_Reply = [results stringForColumnIndex:7];
            NSString *Emp_Code = [results stringForColumnIndex:8];
            NSString *Reply_Date = [results stringForColumnIndex:9];
            
            
            
            FSRMessagedetails *info=[[FSRMessagedetails alloc] initWithMessage_Id:Message_ID SalesRep_ID:SalesRep_ID Message_Title:Message_Title Message_Content:Message_Content Message_date:Message_date Message_Expiry_date:Message_Expiry_date Message_Read:Message_Read Message_Reply:Message_Reply Emp_Code:Emp_Code Reply_Date:Reply_Date];
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (NSString*)dbGetOrderCount{
    
    NSString *sql = [NSString stringWithFormat:@"SELECT Count(*) FROM  TBL_Order "];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSString *Message_Title;
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            Message_Title =[results stringForColumnIndex:0];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return Message_Title;
    
}
- (NSMutableDictionary *)dbGetBonusItemOfProduct:(NSString *)itemCode{
    
    
    return [[NSDictionary alloc] init];
    
}
- (NSString*)dbGetReturnCount
{
    
    NSString *sql = [NSString stringWithFormat:@"SELECT Count(*) FROM  TBL_RMA "];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSString *Message_Title;
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            Message_Title =[results stringForColumnIndex:0];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return Message_Title;
    
    
    
}
- (NSString*)dbdbGetCollectionCount{
    NSString *sql = [NSString stringWithFormat:@"SELECT Count(*) FROM  TBL_Collection "];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSString *Message_Title;
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            Message_Title =[results stringForColumnIndex:0];
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return Message_Title;
    
}
- (NSArray*)dbGetFSRSentMessages{
    
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT A.Message_Title,A.Message_Content,A.Message_Date , B.Username FROM TBL_Incoming_Messages As A INNER JOIN TBL_Users_All  AS B ON A.Rcpt_User_ID = B.User_ID GROUP BY A.Message_Date "];
    if ([AppControl.retrieveSingleton.USE_EMPNAME_IN_USERLIST isEqualToString:KAppControlsYESCode]) {
        sql = @"SELECT A.Message_Title,A.Message_Content,A.Message_Date , B.Emp_Name as Username FROM TBL_Incoming_Messages As A INNER JOIN TBL_Users_All  AS B ON A.Rcpt_User_ID = B.User_ID GROUP BY A.Message_Date";
    }
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            NSString *Message_Title = [results stringForColumnIndex:0];
            NSString *Message_Content =[results stringForColumnIndex:1];
            NSString *Message_date =[results stringForColumnIndex:2];
            NSString *User_Name = [results stringForColumnIndex:3];
            
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            
            
            [tempDict setValue:Message_Title forKey:@"Message_Title"];
            [tempDict setValue:Message_Content forKey:@"Message_Content"];
            [tempDict setValue:Message_date forKey:@"Message_date"];
            [tempDict setValue:User_Name forKey:@"User_Name"];
            
            [retVal addObject:tempDict];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (NSArray*)dbGetUsersList{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT User_id,Username FROM TBL_Users_ALL"];
    if ([AppControl.retrieveSingleton.USE_EMPNAME_IN_USERLIST isEqualToString:KAppControlsYESCode]) {
        sql = @"SELECT User_id, Emp_Name as Username FROM TBL_Users_ALL order by Username COLLATE nocase ASC";
    }
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            int User_ID=[[results stringForColumnIndex:0] intValue];
            NSString *name = [results stringForColumnIndex:1];
            AllUserList *info =[[AllUserList alloc] initWithUserId:User_ID User:name];
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return retVal;
}
- (void)updateReadMessagestatus:(NSString*)strQuery{
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:strQuery];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"AddResponseNotification"object:self];
    
    
}
- (int) dbGetIncomingMessageCount
{
    int count = 0;
    
    NSString *sql= @"SELECT COUNT(*) FROM TBL_Incoming_Messages";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            count = [[results stringForColumnIndex:0] intValue];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return count;
}
- (NSArray*)dbGetCustomerShipAddressDetailsByCustomer_ID:(NSString *)Customer_ID SiteUse_ID:(NSString *)SiteUse_ID{
    
    
    NSString * sql = [NSString stringWithFormat:@"SELECT * FROM TBL_Customer_Ship_Address where Customer_ID =%@ and Site_Use_ID =%@",Customer_ID,SiteUse_ID];
    //NSLog(@"Query %@",sql);
    
    NSArray *temp = [self fetchDataForQuery:sql];
    return temp;
    
}
- (NSArray*)dbGetCustomerShipAddressDetailsByCustomer_Name:(NSString *)Customer_Name{
    
    
    NSString * sql = [NSString stringWithFormat:@"SELECT * FROM TBL_Customer_Ship_Address where Customer_Name='%@'",Customer_Name];
    //NSLog(@"Query %@",sql);
    
    NSArray *temp = [self fetchDataForQuery:sql];
    return temp;
    
}
- (void)UserAlert:(NSString *)Message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    [alert show];
    
    //    [ILAlertView showWithTitle:NSLocalizedString(@"Message", nil)
    //                       message:Message
    //              closeButtonTitle:NSLocalizedString(@"OK", nil)
    //             secondButtonTitle:nil
    //           tappedButtonAtIndex:nil];
}
- (void)checkUnCompleteVisits{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = @"select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date == ''";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSString * Survey_ID =[results stringForColumnIndex:0];
            [retVal addObject:Survey_ID];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    if([retVal count]!=0)
    {
        for(int is = 0 ; is<[retVal count] ; is++)
        {
            [[SWDatabaseManager retrieveManager] updateEndTimeWithID:[retVal objectAtIndex:is]];
        }
    }
    
}
- (void)updateEndTimeWithID:(NSString *)visitID{
    
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    NSString *selectSQL =[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Visit_End_Date='%@' WHERE Actual_Visit_ID='%@'",dateString,visitID];
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:selectSQL];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    
    formatter=nil;
    usLocale=nil;
    
}
- (NSMutableDictionary*)dbGetPriceOrProductWithID:(NSString *)itemID{
    
    
    NSMutableDictionary *retVal =[NSMutableDictionary dictionary];
    NSString *sql = [NSString stringWithFormat:@"SELECT Unit_Selling_Price,Unit_List_Price from  TBL_Price_List  WHERE  Is_Generic = 'Y' AND Inventory_Item_ID = '%@'",itemID];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            NSString *sellingPrice = [results stringForColumnIndex:0];
            NSString *listPrice = [results stringForColumnIndex:1];
            
            
            [retVal setValue:sellingPrice forKey:@"sellingPrice"];
            [retVal setValue:listPrice forKey:@"listPrice"];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return retVal;
    
}
- (void)deleteManageOrderWithRef:(NSString*)strQuery{
    
    
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM TBL_Proforma_Order WHERE Orig_Sys_Document_Ref = '%@'",strQuery]];
        if(success)
            NSLog(@"Order deleted");
        

    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    [self deleteManageOrderLiteItemsWithRef:strQuery];
    
}
- (void)deleteManageOrderLiteItemsWithRef:(NSString*)strQuery{
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM TBL_Proforma_Order_Line_Items WHERE Orig_Sys_Document_Ref = '%@'",strQuery]];
        if(success)
            NSLog(@"Order line items deleted");
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    [self deleteManageOrderLotWithRef:strQuery];
    
    
}
- (void)deleteManageOrderLotWithRef:(NSString*)strQuery{
    
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM TBL_Proforma_Order_Lots WHERE Orig_Sys_Document_Ref = '%@'",strQuery]];
        if(success)
        NSLog(@"Order line lots deleted");

    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    
}
- (NSArray*)dbGetAppControl{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = @"SELECT Control_Key,Control_Value from TBL_App_Control";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSString * Survey_ID =[results stringForColumnIndex:0];
            NSString * Start_Time  = [results stringForColumnIndex:1];
            [tempDict setValue:Start_Time forKey:Survey_ID];
        }
        [retVal addObject:tempDict];
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (void)deleteTemplateOrderWithRef:(NSString*)strQuery{
    
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM TBL_Order_Template WHERE Order_Template_ID = '%@'",strQuery]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    [self deleteTemplateOrderLiteItemsWithRef:strQuery];
    
}
- (void)deleteTemplateOrderLiteItemsWithRef:(NSString*)strQuery{
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM TBL_Order_Template_Items WHERE Order_Template_ID = '%@'",strQuery]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
}
- (NSArray*)dbGetAllCategory;{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString * sql = @"SELECT DISTINCT Category, Item_No FROM TBL_Product Group BY  Item_No ";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSString * category_Name =[results stringForColumnIndex:1];
            
            NSString * category_Code  = [results stringForColumnIndex:0];
            
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            
            
            [tempDict setValue:category_Name forKey:@"category_Name"];
            [tempDict setValue:category_Code forKey:@"category_Code"];
            
            
            [retVal addObject:tempDict];
            
            
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (NSArray*)dbGetBonusInfoOfProduct:(NSString *)product;{

    
    return [[NSArray alloc] init];
    
}

-(NSString*)dbGetSpecialDiscount:(NSString*)productID
{
    
    
    NSString* specialDiscountQry=[NSString stringWithFormat:@"Select IFNULL(Attrib_Value,'0') AS Attrib_Value FROM TBL_Product_Addl_Info where  Attrib_Name = 'SPECIAL_DISCOUNT' and Inventory_Item_ID ='%@' ",productID];
    
  //  NSLog(@"query for special discount %@", specialDiscountQry);
    
    NSArray* specialDiscountArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:specialDiscountQry];
    
    if (specialDiscountArray.count>0) {
        return [SWDefaults getValidStringValue:[[specialDiscountArray objectAtIndex:0]valueForKey:@"Attrib_Value"]];
    }
    
    else
    {
        return nil;
    }
    
}
- (NSString*)dbGetOnOrderQuantityOfProduct:(NSString *)itemID{
    NSString *sql = [NSString stringWithFormat:@"SELECT IFNULL(Attrib_Value,'0') AS Attrib_Value FROM TBL_Product_Addl_Info where  Attrib_Name = 'ON_ORDER_QTY' AND Inventory_Item_ID = '%@' ",itemID];
    FMDatabase *db = [self getDatabase];
    [db open];
    NSString *Message_Title;
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            Message_Title = [results stringForColumnIndex:0];
            
            
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    
    
    
    return Message_Title;
    
    
}
- (NSArray*)dbGetMultiCurrency{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = @"SELECT * from TBL_Currency";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            NSString * CCode =[results stringForColumnIndex:0];
            NSString * CDescription  = [results stringForColumnIndex:1];
            NSString * CRate =[results stringForColumnIndex:2];
            NSString * CDigits  = [results stringForColumnIndex:3];
            
            [tempDict setValue:CCode forKey:@"CCode"];
            [tempDict setValue:CDescription forKey:@"CDescription"];
            [tempDict setValue:CRate forKey:@"CRate"];
            [tempDict setValue:CDigits forKey:@"CDigits"];
            [retVal addObject:tempDict];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
#pragma Report Functions
- (NSMutableArray*)dbGetDataForReport:(NSString*)sql{
    
    return [[self fetchDataForQuery:sql] mutableCopy];
    
}

/////////////////Product/////////////////////
- (NSArray *)dbGetProductCollection {
    // query = [[SWQuery alloc] init];
    // [query setQuery:kSQLProductList];
    // [query setTag:kQueryTypeCollection];
    // [query setIsSectionedQuery:YES];
    
    NSString *filter = @"";
    
    if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Brand", nil)])
    {
        if ([[SWDefaults productFilterBrand] length] > 0)
        {
            filter = [NSString stringWithFormat:@"WHERE Brand_Code = '%@'", [SWDefaults productFilterBrand]];
        }
    }
    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
    {
        if ([[SWDefaults productFilterProductID] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE Item_Code LIKE '%%%@%%'", [SWDefaults productFilterProductID]];
        }
    }
    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
    {
        if ([[SWDefaults productFilterName] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE Description LIKE '%%%@%%'", [SWDefaults productFilterName]];
        }
    }
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductListNonAlphabet, filter]];
    // query=nil;
    return temp;
}


-(NSArray*)fetchProductDetailswithRanking :(NSString *)itemId organizationId:(NSString *)orgId

{
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductDetailwithRanking, itemId, orgId]];
    
    NSLog(@"product details with ranking are %@", [temp description]);
    
    
    return temp;
    
}
- (NSArray *)dbGetProductDetail:(NSString *)itemId organizationId:(NSString *)orgId {
    
    //check these for MSL
    
    NSLog(@"item id and org id are %@ %@",itemId,orgId);
    
    NSLog(@"product details qry %@", [NSString stringWithFormat:kSQLProductDetail, itemId, orgId]);
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductDetail, itemId, orgId]];
    
    NSLog(@"product details response is  %@", [temp description]);
    
    
    return temp;
}


- (NSArray *)dbGetProductDetailwithUOM:(NSString *)itemId organizationId:(NSString *)orgId {
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductDetail, itemId, orgId]];
    return temp;
}



- (NSArray *)dbGetStockInfo:(NSString *)itemId {
    
    NSLog(@"stock qry %@",[NSString stringWithFormat:kSQLProductStock, itemId]);
    
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductStock, itemId]];
    return temp;
}

- (NSArray *)dbGetStockInfoForManage:(NSString *)itemId {
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Proforma_Order_Lots Where Orig_Sys_Document_Ref = '%@'", itemId]];
    return temp;
}

- (NSArray *)dbGetBonusInfo:(NSString *)itemId {
    
    
    
    NSArray* bonusPlanArray=[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Valid_From,A.Valid_To, IFNULL(C.Is_Default_Plan,'N') AS Is_Default_Plan, A.BNS_Plan_ID, A.Prom_Qty_From, A.Prom_Qty_To, A.Price_Break_Type_Code, A.Get_Item, A.Get_Qty ,A.Get_Add_Per ,B.Description,A.Get_UOM  FROM TBL_BNS_Promotion AS A LEFT JOIN TBL_Product AS B ON A.Get_Item = B.Item_Code LEFT JOIN TBL_BNS_Plan AS C ON A.BNS_Plan_ID=C.BNS_Plan_ID  WHERE   A.Item_Code='%@' and  date(A.Valid_From)<=date('now') and  date(A.Valid_To)>=date('now') ORDER BY A.Prom_Qty_From ASC", itemId]];

    NSPredicate* defualtBonusFilterPredicate=[NSPredicate predicateWithFormat:@"SELF.Is_Default_Plan == 'Y'"];
    NSArray* defaultBnsPlanBonusArray=[bonusPlanArray filteredArrayUsingPredicate:defualtBonusFilterPredicate];
   
    
    NSPredicate* specialBonusFilterPredicate=[NSPredicate predicateWithFormat:@"SELF.Is_Default_Plan == 'N'"];
    NSArray* specialBnsPlanBonusArray=[bonusPlanArray filteredArrayUsingPredicate:specialBonusFilterPredicate];

    
    
    /*
    NSArray *defaultBnsPlanBonusArray = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT  IFNULL(C.Is_Default_Plan,'N') AS Is_Default_Plan, A.BNS_Plan_ID, A.Prom_Qty_From, A.Prom_Qty_To, A.Price_Break_Type_Code, A.Get_Item, A.Get_Qty ,A.Get_Add_Per ,B.Description,A.Get_UOM  FROM TBL_BNS_Promotion AS A LEFT JOIN TBL_Product AS B ON A.Get_Item = B.Item_Code LEFT JOIN TBL_BNS_Plan AS C ON A.BNS_Plan_ID=C.BNS_Plan_ID  WHERE   A.Item_Code='%@' AND C.Is_Default_Plan='Y' ORDER BY A.Prom_Qty_From ASC", itemId]];
    NSArray *specialBnsPlanBonusArray = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT  IFNULL(C.Is_Default_Plan,'N') AS Is_Default_Plan, A.BNS_Plan_ID, A.Prom_Qty_From, A.Prom_Qty_To, A.Price_Break_Type_Code, A.Get_Item, A.Get_Qty ,A.Get_Add_Per ,B.Description,A.Get_UOM  FROM TBL_BNS_Promotion AS A LEFT JOIN TBL_Product AS B ON A.Get_Item = B.Item_Code LEFT JOIN TBL_BNS_Plan AS C ON A.BNS_Plan_ID=C.BNS_Plan_ID  WHERE   A.Item_Code='%@' AND C.Is_Default_Plan='N' ORDER BY A.Prom_Qty_From ASC", itemId]];
     */

    if(specialBnsPlanBonusArray.count>0){
        return specialBnsPlanBonusArray;
    }
    
    return defaultBnsPlanBonusArray;
}

- (NSArray *)dbdbGetTargetInfo:(NSString *)itemId {
    //query = [[SWQuery alloc] init];
    NSDate *date = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSMonthCalendarUnit fromDate:date];
    // [query setQuery:];
    //[query setTag:kQueryTypeTarget];
    
    
    NSLog(@"target qry %@", [NSString stringWithFormat:KSWLProductTarget, itemId, [comps month]]);
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:KSWLProductTarget, itemId, [comps month]]];
    
    NSLog(@"target of this month %@", temp);
    
    //query=nil;
    return temp;
}

- (NSArray *)dbGetFilterByColumn:(NSString *)columnName1 {
    //query = [[SWQuery alloc] init];
    // [query setQuery:];
    // [query setTag:kQueryTypeFilterBrand];
    
    NSArray *temp = [self fetchDataForQuery:[kSQLProductFilterBrand stringByReplacingOccurrencesOfString:@"{0}" withString:columnName1]];
    // query=nil;
    return temp;
}

- (NSArray *)dbGetCategoriesForCustomer:(NSDictionary *)customer {
    //query = [[SWQuery alloc] init];
    // [query setQuery:];
    // [query setTag:kQueryTypeCategory];
    
    NSLog(@"category qry %@",[NSString stringWithFormat:kSQLProductCategories, [customer stringForKey:@"Ship_Customer_ID"], [customer stringForKey:@"Ship_Site_Use_ID"]]);
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductCategories, [customer stringForKey:@"Ship_Customer_ID"], [customer stringForKey:@"Ship_Site_Use_ID"]]];
    //query=nil;
    return temp;
}


-(NSArray*)fetchCategoriesForCustomer:(Customer*)customer
{
    NSLog(@"categories qry %@",[NSString stringWithFormat:kSQLProductCategories, customer.Ship_Customer_ID, customer.Site_Use_ID] );
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductCategories, customer.Ship_Customer_ID, customer.Site_Use_ID]];
    //query=nil;
    return temp;
}

-(NSArray*)fetchProductswithRanking:(NSDictionary*)category
{
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductListfromCategorywithRanking, [category stringForKey:@"Org_ID"], [category stringForKey:@"Category"]]];
    
    NSLog(@"query for products with ranking %@", [NSString stringWithFormat:kSQLProductListfromCategorywithRanking, [category stringForKey:@"Org_ID"], [category stringForKey:@"Category"]]);
    
    
    // NSLog(@"products with ranking are %@", [temp description]);
    
    return temp;
}

- (NSArray *)dbGetProductsOfCategory:(NSDictionary *)category {
    //query = [[SWQuery alloc] init];
    //[query setQuery:nil];
    //[query setParams:category];
    //[query setIsSectionedQuery:YES];
    //[query setTag:kQueryTypeCollectionCat];
    
    NSLog(@"product category received is %@", category);
    
   // NSLog(@"qry for products %@",[NSString stringWithFormat:kSQLProductListFromCategoriesNA, [category stringForKey:@"Org_ID"], [category stringForKey:@"Category"]]);
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductListFromCategoriesNA, [category stringForKey:@"Org_ID"], [category stringForKey:@"Category"]]];
    //query=nil;
    
    // NSLog(@"products of category is %@", [temp description]);
    
    
    return temp;
}

- (NSArray *)checkGenericPriceOfProduct:(NSString *)productID :(NSString*)PriceListID
{
    //query = [[SWQuery alloc] init];
    // [query setQuery:];
    // [query setTag:kQueryTypeCheckPrice];
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Price_List where Inventory_Item_ID='%@' AND  (Price_List_ID='%@' OR Is_Generic='Y') ORDER BY Is_Generic ASC ",productID,PriceListID]];
    
    NSLog(@" price list data is %@", [temp description]);
    
    //query=nil;
    return temp;
}



- (NSArray *)checkGenericPriceOfProductwithUOM:(NSString *)productID UOM:(NSString*)SelectedUOM :(NSString*)priceListID
{
    //query = [[SWQuery alloc] init];
    // [query setQuery:];
    // [query setTag:kQueryTypeCheckPrice];
    
    
    //check this in MO
    
    //    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Price_List where Inventory_Item_ID = '%@' and Item_UOM ='%@'",productID,SelectedUOM]];
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Price_List where Inventory_Item_ID = '%@' and Item_UOM ='%@' and (Price_List_ID='%@' OR Is_Generic='Y')",productID,SelectedUOM,priceListID]];
    
    //query=nil;
    return temp;
}


//- (NSMutableArray *)getProductsOfBonus:(NSString *)bname
//{
//    return [self GetBonusProductWithDescription:bname];
//}

- (NSMutableDictionary *)dbdbGetPriceListProduct:(NSString *)items
{
    return [self dbGetPriceOrProductWithID:items];
}

////////////////Dashboard////////////


-(NSArray *)dbGetTotalOrder{
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    // [query setTag:kQueryTypeTotalOrder];
    
    
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    
    [dateformate setDateFormat:@"YYYY-MM-dd"];
    
    NSString *dateString=[dateformate stringFromDate:[NSDate date] ];
    
    
    NSLog(@"check time stamp %@", dateString);
    
    
    NSString* totalOrderQry=[NSString stringWithFormat:@"SELECT  COUNT(*) AS Total from TBL_Order_History Where  Creation_Date like  '%%%@%%'  and Doc_Type='I'", dateString];
    
    
    
    
    NSArray *temp= [self fetchDataForQuery:totalOrderQry];
    
    formatter=nil;
    usLocale=nil;
    
    return temp;
    
}

-(NSString*)isItemRestrictedForReturn:(NSString*)inventoryItemID
{
    return KAppControlsNOCode;
    /*
    NSString* isRestricted=[[NSString alloc]init];
    NSString* restrictedQry=[NSString stringWithFormat:@"select IFNULL(Attrib_Value,'N/A') AS Attrib_Value from TBL_Product_Addl_Info where Attrib_Name = 'RET_MODE' and Inventory_Item_ID = '%@'",inventoryItemID];
    NSArray* restictedArray=[self fetchDataForQuery:restrictedQry];
    if (restictedArray.count>0) {
        NSString* restrictedString=[NSString getValidStringValue:[[restictedArray objectAtIndex:0]valueForKey:@"Attrib_Value"]];
        isRestricted=[restrictedString isEqualToString:KAppControlsYESCode]?KAppControlsYESCode:KAppControlsNOCode;
    }
    return isRestricted;*/
}
-(NSArray *)dbGetTotalReturn{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    //  [query setTag:kQueryTypeTotalReturn];
    
    
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"YYYY-MM-dd"];
    NSString *dateString=[dateformate stringFromDate:[NSDate date]];
    
    
    NSLog(@"check time stamp %@", dateString);
    
    
    
    
    
    NSArray *temp= [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT  COUNT(*) AS Total from TBL_Order_History Where Creation_Date like  '%%%@%%' and Doc_Type='R'", dateString]];
    formatter=nil;
    usLocale=nil;
    return temp;
}
-(NSArray *)dbGetTotalCollection{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    //[query setTag:kQueryTypeTotalCollection];
    
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"YYYY-MM-dd"];
    [dateformate setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    
    NSString *dateString=[dateformate stringFromDate:[NSDate date]];
    
    
    NSLog(@"date while fetching collection %@",[NSString stringWithFormat:@"SELECT COUNT(*) AS Total  FROM TBL_Collection Where Collected_On like '%%%@%%' ", dateString]);
    
    
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*) AS Total  FROM TBL_Collection Where Collected_On like '%%%@%%' ", dateString]];
    formatter=nil;
    usLocale=nil;
    return temp;
}

-(NSMutableArray*)fetchSalesTrendWithDocType:(NSString*)docType
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    //[query setTag:kQueryTypeSaleTrend];
    
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    
    if ([docType isEqualToString:kCollectionDocType]) {
        
        temp = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT DATE(Collected_On) ,SUM(Amount) As Ammount FROM TBL_Collection Where strftime('%%m', `Collected_On`) = '%@' and strftime('%%Y-%%m-%%d', Collected_On) <= strftime('%%Y-%%m-%%d','now') AND Amount > 0 GROUP BY DATE(Collected_On)", [formatter stringFromDate:[NSDate date]]]];

    }else{
        
        temp = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(Row_ID)  AS Total , DATE(Creation_Date) ,SUM(Transaction_Amt) As Ammount FROM TBL_Order_History Where strftime('%%m', `Creation_Date`) = '%@' AND Doc_Type ='%@' and strftime('%%Y-%%m-%%d', Creation_Date) <= strftime('%%Y-%%m-%%d','now') AND Transaction_Amt>0 GROUP BY DATE(Creation_Date)", [formatter stringFromDate:[NSDate date]],docType]];
    }
    
    formatter=nil;
    usLocale=nil;
    return temp;
}
-(NSArray *)dbGetSaleTrend{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    //[query setTag:kQueryTypeSaleTrend];
    
    NSLog(@"sales trend qry is %@",[NSString stringWithFormat:@"SELECT COUNT(Row_ID)  AS Total , DATE(Creation_Date) ,SUM(Transaction_Amt) As Ammount FROM TBL_Order_History Where strftime('%%m', `Creation_Date`) = '%@' AND Doc_Type ='I' and strftime('%%Y-%%m-%%d', Creation_Date) <= strftime('%%Y-%%m-%%d','now') AND Transaction_Amt>0 GROUP BY DATE(Creation_Date)", [formatter stringFromDate:[NSDate date]]]);
    
    
    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(Row_ID)  AS Total , DATE(Creation_Date) ,SUM(Transaction_Amt) As Ammount FROM TBL_Order_History Where strftime('%%m', `Creation_Date`) = '%@' AND Doc_Type ='I' and strftime('%%Y-%%m-%%d', Creation_Date) <= strftime('%%Y-%%m-%%d','now') AND Transaction_Amt>0 GROUP BY DATE(Creation_Date)", [formatter stringFromDate:[NSDate date]]]];
    
    formatter=nil;
    usLocale=nil;
    return temp;
}
-(NSArray *)dbGetCustomerStatus:(NSString *)customer{
    //[query setTag:kQueryTypeCustomerStatus];
    
    
    
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:@"Select IFNULL(Credit_Hold,'N/A') AS Credit_Hold , IFNULL(Cust_Status,'N/A')AS Cust_Status , IFNULL(Cash_Cust,'N/A') AS Cash_Cust from TBL_Customer where Customer_ID =   '%@'",customer]];
    return temp;
}
-(NSArray *)dbGetCustomerSales:(NSString *)customer{
    
    NSArray *temp= [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT SUM(Transaction_Amt)  AS Total FROM TBL_Sales_History Where Inv_To_Customer_ID =  '%@'",customer]];
    return temp;
}
-(NSArray *)dbGetCustomerOutstanding:(NSString *)customer{
    
    // [query setQuery:];
    // [query setTag:kQueryTypeTotalCustomerOuts];
    
    
    
    NSArray *temp=  [self fetchDataForQuery:[NSString stringWithFormat:@"Select Total_Due AS Total from TBL_Customer_Dues where Customer_ID = '%@'",customer]];
    return temp;
}
-(NSArray *)dbGetListofCustomer{
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    //[query setQuery:];
    //[query setTag:kQueryTypeTotalCustomerList];
    
    
    
    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT B.Customer_Name ,A.Customer_ID , A.Site_Use_ID,A.Start_Time AS Visit_Start_Time  FROM  TBL_FSR_Planned_Visits AS A INNER JOIN TBL_Customer_Ship_Address  AS B ON A.Customer_ID = B.Customer_ID AND A.Site_Use_ID = B.Site_Use_ID   WHERE A.Visit_Date LIKE  '%%%@%%' ORDER BY A.Start_Time ASC , B.Customer_Name ASC", [formatter stringFromDate:[NSDate date]]]];
    formatter=nil;
    usLocale=nil;
    return temp;
}
-(NSArray *)dbGgtDetailofCustomer:(NSString *)customerID andSite:(NSString *)siteID{
    
    
    // [query setQuery:];
    // [query setTag:kQueryTypeGetDetail];
    
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.*,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.* ,A.Customer_Name FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID  WHERE A.Customer_ID = '%@' AND A.Site_Use_ID='%@'",customerID,siteID ]];
    return temp;
}

-(NSMutableArray *)CalculateOutOfRouteVisits:(NSArray *)arrayResult
{
    NSMutableArray *actualVisitArray = [NSMutableArray arrayWithArray:arrayResult];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLRoute, [formatter stringFromDate:[NSDate date]]]];
    formatter=nil;
    usLocale=nil;
    
    int countValue=0;
    if(![actualVisitArray count]==0)
    {
        for (int i=0; i<[temp count]; i++)
        {
            @autoreleasepool{
                NSDictionary *PV = [NSDictionary dictionaryWithDictionary:[temp objectAtIndex:i]];
                for (int j=0; j<[actualVisitArray count]; j++)
                {
                    @autoreleasepool{
                        NSDictionary *AV = [NSDictionary dictionaryWithDictionary:[actualVisitArray objectAtIndex:j]];
                        if([[PV stringForKey:@"Customer_ID"] isEqualToString:[AV stringForKey:@"Customer_ID"]] && [[PV stringForKey:@"Site_Use_ID"] isEqualToString:[AV stringForKey:@"Site_Use_ID"]])
                        {
                            countValue++;
                        }
                    }
                }
            }
        }
        int resultCount = [actualVisitArray count]-countValue;
        [actualVisitArray removeAllObjects];
        [actualVisitArray addObject:[NSString stringWithFormat:@"%d",resultCount]];
        return actualVisitArray;
    }
    else
    {
        return actualVisitArray;
    }
}

///////////////////////Session/////////////////
- (NSString *)verifyLogin:(NSString *)login andPassword:(NSString *)password
{
    NSString *shaPass = [self digest:password];
    NSLog(@" user : %@ SHA : %@",login,[shaPass uppercaseString]);
    NSString *query = [NSString stringWithFormat:kSQLLogin, login, [shaPass uppercaseString]];
    
    NSArray *temp =[self fetchDataForQuery:query];
    if (temp.count == 0)
    {
        return NSLocalizedString(@"Invalid username or password", nil);
        //return @"Done";
        
    }
    else
    {
        [SWDefaults setUserProfile:[temp objectAtIndex:0]];
        return @"Done";
    }
}

-(NSString *) encrypt:(NSString *)plaintext andKey:(NSString *)keytext
{
    NSData *data;
    NSData *key;
    
    int BLOCK_SIZE = 6 ;
    int data_lenght,key_lenght ;
    
    data = [plaintext dataUsingEncoding:NSUTF8StringEncoding];
    key = [keytext dataUsingEncoding:NSUTF8StringEncoding];
    
    data_lenght = [data length];
    key_lenght = [key length];
    
    NSMutableData *Mdata = [data mutableCopy];
    NSMutableData *Mkey = [key mutableCopy];
    
    char *dataBytes = (char *) [Mdata mutableBytes];
    char *keyBytes = (char *) [Mkey mutableBytes];
    
    int y =0;
    int i = 0 ;
    
    
    while (i <= data_lenght) {
        for (int x=0 ; x <= BLOCK_SIZE; x++)
        {
            if (i < data_lenght) {
                dataBytes[i] =  dataBytes[i] ^ keyBytes[y];
            }
            i+=1;
        }
        
        y+=1;
        if (y== key_lenght)
        {
            y=0;
        }
        
    }
    
    NSData *objData = [NSData dataWithBytes:dataBytes length:data_lenght];
    return [objData base64EncodedString];
}
-(NSString *) decrypt:(NSString *)encryptedtext andKey:(NSString *)keytext
{
    NSData *data;
    NSData *key;
    
    int BLOCK_SIZE = 6 ;
    int data_lenght,key_lenght ;
    
    key = [keytext dataUsingEncoding:NSUTF8StringEncoding];
    key_lenght = [key length];
    NSMutableData *Mkey = [key mutableCopy];
    char *keyBytes = (char *) [Mkey mutableBytes];
    
    data = [NSData dataFromBase64String:encryptedtext];
    data_lenght = [data length];
    NSMutableData *Mdata = [data mutableCopy];
    char *dataBytes = (char *) [Mdata mutableBytes];
    
    int y =0;
    int i = 0 ;
    
    while (i <= data_lenght) {
        for (int x=0 ; x <= BLOCK_SIZE; x++)
        {
            if (i < data_lenght) {
                dataBytes[i] =  dataBytes[i] ^ keyBytes[y];
            }
            i+=1;
        }
        
        y+=1;
        if (y== key_lenght)
        {
            y=0;
        }
        
    }
    NSData *objData = [NSData dataWithBytes:dataBytes length:data_lenght];
    NSString* newStr = [[NSString alloc] initWithData:objData encoding:NSUTF8StringEncoding];
    return newStr;
}

-(NSString*) digest:(NSString*)input
{
    NSString *hashkey = input;
    // PHP uses ASCII encoding, not UTF
    const char *s = [hashkey cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData;
    if (s != NULL) {
        keyData = [NSData dataWithBytes:s length:strlen(s)];
    } else {
        return @"";
    }
    
    // This is the destination
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    // This one function does an unkeyed SHA1 hash of your hash data
    CC_SHA1(keyData.bytes, keyData.length, digest);
    
    // Now convert to NSData structure to make it usable again
    NSData *out = [NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH];
    // description converts to hex but puts <> around it and spaces every 4 bytes
    NSString *hash = [out debugDescription];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    [hash uppercaseString];
    return hash;
}

-(void)writeXML
{
    float x = 0.0, y = 1.0, z = 2.0;
    NSString *name = @"Ansasri";
    
    NSMutableData *data = [NSMutableData data];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver setOutputFormat:NSPropertyListXMLFormat_v1_0];
    [archiver encodeFloat:x forKey:@"x"];
    [archiver encodeFloat:y forKey:@"y"];
    [archiver encodeFloat:z forKey:@"z"];
    [archiver encodeObject:name forKey:@"name"];
    [archiver finishEncoding];
    //BOOL result = [data writeToFile:@"MyFile" atomically:YES];
    
    
    
    
    
    
    
    
    //iOS 8 support
    NSString *storePath;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        storePath=[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"sample.xml"];
    }
    
    else
    {
        NSString *applicationDocumentsDir =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        storePath = [applicationDocumentsDir stringByAppendingPathComponent:@"sample.xml"];
    }
    
    
    
    
    
    
    
    // write to file atomically (using temp file)
    [data writeToFile:storePath atomically:TRUE];
    //[archiver release];
}

///////Customer//////////////////////

- (void)cancel
{
    shouldStop=YES;
}

- (NSArray *)dbGetRecentOrders:(NSString *)customerId
{
    
    NSLog(@"order history qry is %@",[NSString stringWithFormat:kSQLCustomerOrderHistory, customerId] );
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerOrderHistory, customerId]];
    return temp;
}
- (NSArray *)dbGetRecentOrdersForCustomer:(Customer *)customer
{
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLOrderHistoryForCustomer, customer.Customer_No,customer.Ship_Site_Use_ID]];
    
    for (NSMutableDictionary *dicObj in temp) {
        if ([NSString getValidStringValue:[dicObj valueForKey:@"LPO_NO"]].length == 0) {
            [dicObj setValue:@"N/A" forKey:@"LPO_NO"];
        }
    }

    return temp;
}


- (NSArray *)dbGetCashCutomerList
{
    
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:kSQLCashCustomerListAll]];
    return  temp;
}
#define KSQLSumOfOrderAmount @"SELECT SUM(Order_Amt) AS OrderAmount FROM TBL_Order  WHERE Inv_To_Customer_ID='{0}'  "

- (NSArray *)dbGetOrderAmountForAvl_Balance:(NSString *)customerId
{
    
    NSArray *temp = [self fetchDataForQuery:[KSQLSumOfOrderAmount stringByReplacingOccurrencesOfString:@"{0}" withString:customerId]];
    return  temp;
}

- (NSArray *)dbGetPriceList:(NSString *)customerId
{
    NSString *filter = @"";
    
    if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Brand", nil)])
    {
        if ([[SWDefaults productFilterBrand] length] > 0)
        {
            filter = [NSString stringWithFormat:@"AND Brand_Code = '%@'", [SWDefaults productFilterBrand]];
        }
    }
    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
    {
        if ([[SWDefaults productFilterProductID] length] > 0) {
            filter = [NSString stringWithFormat:@"AND Item_Code LIKE '%%%@%%'", [SWDefaults productFilterProductID]];
        }
    }
    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
    {
        if ([[SWDefaults productFilterName] length] > 0) {
            filter = [NSString stringWithFormat:@"AND Description LIKE '%%%@%%'", [SWDefaults productFilterName]];
        }
    }
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerPriceList, customerId,filter]];
    return temp;
}

//- (NSArray *)dbGetPriceListGeneric:(NSString *)customerId
//{
//    NSString *filter = @"";
//    
//    if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Brand", nil)])
//    {
//        if ([[SWDefaults productFilterBrand] length] > 0)
//        {
//            filter = [NSString stringWithFormat:@"AND Brand_Code = '%@'", [SWDefaults productFilterBrand]];
//        }
//    }
//    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
//    {
//        if ([[SWDefaults productFilterProductID] length] > 0) {
//            filter = [NSString stringWithFormat:@"AND Item_Code LIKE '%%%@%%'", [SWDefaults productFilterProductID]];
//        }
//    }
//    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
//    {
//        if ([[SWDefaults productFilterName] length] > 0) {
//            filter = [NSString stringWithFormat:@"AND Description LIKE '%%%@%%'", [SWDefaults productFilterName]];
//        }
//    }
//    
//    
//    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerPriceListGeneric, customerId,filter]];
//    return temp;
//}

- (NSArray *)dbGetPriceListGeneric:(Customer *)customer
{
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerPriceListGeneric, customer.Customer_No, customer.Customer_No, customer.Customer_ID, customer.Site_Use_ID]];
    return temp;
}


- (NSArray *)dbGetCollectionFromReports {
    
    NSString *filter = @"";
    NSString *tableName = @"TBL_Customer_Ship_Address";
    
    if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Name", nil)])
    {
        
        if ([[SWDefaults nameFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.Customer_Name LIKE '%%%@%%'", [SWDefaults nameFilterForCustomerList]];
            
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Code", nil)])
    {
        
        if ([[SWDefaults codeFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.Customer_No LIKE '%%%@%%'", [SWDefaults codeFilterForCustomerList]];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:@"BarCode"])
    {
        
        if ([[SWDefaults barCodeFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.Customer_Barcode = '%@'", [SWDefaults barCodeFilterForCustomerList]];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Location", nil)])
    {
        
        if ([[SWDefaults locationFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.City = '%@'", [SWDefaults locationFilterForCustomerList]];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Status", nil)])
    {
        if ([[SWDefaults statusFilterForCustomerList] length] > 0) {
            NSString *queryParam = @"";
            if([[SWDefaults statusFilterForCustomerList] isEqualToString:@"Open"])
            {
                queryParam = @"Y";
            }
            else if([[SWDefaults statusFilterForCustomerList] isEqualToString:@"Blocked"])
            {
                queryParam = @"N";
            }
            filter = [NSString stringWithFormat:@"WHERE A.Cust_Status = '%@'",queryParam];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Payment", nil)])
    {
        
        if ([[SWDefaults paymentFilterForCustomerList] length] > 0)
        {
            NSString *queryParam = @"" ;
            if([[SWDefaults paymentFilterForCustomerList] isEqualToString:@"Cash"])
            {
                queryParam = @"Y";
            }
            else if([[SWDefaults paymentFilterForCustomerList] isEqualToString:@"Credit"])
            {
                queryParam = @"N";
            }
            filter = [NSString stringWithFormat:@"WHERE B.Cash_Cust = '%@'",queryParam];
            NSLog(@"filter is %@", filter);
        }
    }
    
    
//    NSString * queryforCustomers=@" SELECT A.Sales_District_ID,A.Postal_Code ,A.Location,A.Dept,A.Customer_Segment_ID,B.Customer_NO,B.Customer_barCode, A.Cust_lat,A.Cust_long, A.City,A.Address,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, IFNULL(B.Phone,'N/A') AS Phone,IFNULL(B.Credit_Limit,'0')AS Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, IFNULL(B.Avail_Bal,'0') AS Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID   ORDER BY A.Customer_Name";
    
    
    // NSString* queryforCustomers=@"SELECT C.Attrib_Value AS CUST_LOC_RNG, CASE WHEN C.Attrib_NAME='CUST_LOC_RNG Then 'Y' END As IS_LOC_RNG,A.Sales_District_ID,A.Postal_Code ,A.Location,A.Dept,A.Customer_Segment_ID,B.Customer_NO,B.Customer_barCode, A.Cust_lat,A.Cust_long, A.City,A.Address,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, IFNULL(B.Phone,'N/A') AS Phone,IFNULL(B.Credit_Limit,'0')AS Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, IFNULL(B.Avail_Bal,'0') AS Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID left join TBL_Customer_Addl_Info AS C on A.Customer_ID=C.Customer_ID and A.Site_Use_ID=C.Site_Use_ID  ORDER BY A.Customer_Name";
    
    
//    NSArray *temp=[self fetchDataForQuery:queryforCustomers];
    
    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerListNonAlpha,tableName ,filter]];
    return temp;
}
-(NSArray *)fetchCutomerDetailsForBarCode:(NSString *)barcode
{
    NSString *query=  [NSString stringWithFormat:@"SELECT A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, B.Phone,IFNULL(B.Credit_Limit,'0')AS Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, IFNULL(B.Avail_Bal,'0') AS Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID  WHERE A.Customer_Barcode = '%@' ORDER BY A.Customer_Name",barcode];
    
    NSArray *temp=[self fetchDataForQuery:query];
    return temp;
}
- (NSArray *)dbGetCollection {
    
    NSString *filter = @"";
    NSString *tableName = @"TBL_Customer_Ship_Address";
    
    if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Name", nil)])
    {
        
        if ([[SWDefaults nameFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.Customer_Name LIKE '%%%@%%'", [SWDefaults nameFilterForCustomerList]];
            
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Code", nil)])
    {
        
        if ([[SWDefaults codeFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.Customer_No LIKE '%%%@%%'", [SWDefaults codeFilterForCustomerList]];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:@"BarCode"])
    {
        
        if ([[SWDefaults barCodeFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.Customer_Barcode = '%@'", [SWDefaults barCodeFilterForCustomerList]];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Location", nil)])
    {
        
        if ([[SWDefaults locationFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.City = '%@'", [SWDefaults locationFilterForCustomerList]];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Status", nil)])
    {
        if ([[SWDefaults statusFilterForCustomerList] length] > 0) {
            NSString *queryParam = @"";
            if([[SWDefaults statusFilterForCustomerList] isEqualToString:@"Open"])
            {
                queryParam = @"Y";
            }
            else if([[SWDefaults statusFilterForCustomerList] isEqualToString:@"Blocked"])
            {
                queryParam = @"N";
            }
            filter = [NSString stringWithFormat:@"WHERE A.Cust_Status = '%@'",queryParam];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Payment", nil)])
    {
        
        if ([[SWDefaults paymentFilterForCustomerList] length] > 0)
        {
            NSString *queryParam = @"" ;
            if([[SWDefaults paymentFilterForCustomerList] isEqualToString:@"Cash"])
            {
                queryParam = @"Y";
            }
            else if([[SWDefaults paymentFilterForCustomerList] isEqualToString:@"Credit"])
            {
                queryParam = @"N";
            }
            filter = [NSString stringWithFormat:@"WHERE B.Cash_Cust = '%@'",queryParam];
            NSLog(@"filter is %@", filter);
        }
    }
    
    NSString * queryforCustomers ;
    if([[AppControl retrieveSingleton].SHOW_ALERT_FOR_TRD_LIC_EXP isEqualToString:KAppControlsYESCode]){
        queryforCustomers = @"Select IFNULL(A.Sales_District_ID,'N/A') AS Sales_District_ID,IFNULL(A.Postal_Code,'N/A') AS Postal_Code ,IFNULL(A.Location,'N/A')AS Location,IFNULL(A.Dept,'N/A') AS Dept,IFNULL(A.Customer_Segment_ID,'N/A') AS Customer_Segment_ID,IFNULL(B.Customer_No,'N/A') AS Customer_No,IFNULL(B.Customer_barCode,'N/A')AS Customer_barCode, IFNULL(A.Cust_lat,'N/A')AS Cust_Lat,IFNULL(A.Cust_long,'N/A') AS Cust_Long, IFNULL(A.City,'N/A') AS City,IFNULL(A.Address, 'N/A') AS Address,IFNULL(A.Customer_ID,'N/A') AS [Ship_Customer_ID] , IFNULL(A.Site_Use_ID,'N/A') AS [Ship_Site_Use_ID], IFNULL(B.Customer_ID,'N/A')AS Customer_ID, IFNULL(B.Site_Use_ID,'N/A') AS Site_Use_ID, IFNULL(B.Contact,'N/A') AS Contact, IFNULL(B.Phone,'N/A') AS Phone,IFNULL(B.Credit_Limit,'0')AS Credit_Limit, IFNULL(B.Credit_Hold,'N/A') AS Credit_Hold, IFNULL(B.Customer_Type,'N/A') AS Customer_Type, IFNULL(B.Customer_Class,'N/A') AS Customer_Class, IFNULL(B.Trade_Classification,'N/A') AS Trade_Classification,IFNULL(B.Trade_License_Expiry,'N/A') AS Trade_License_Expiry,IFNULL(B.Chain_Customer_Code,'N/A') AS Chain_Customer_Code, IFNULL(B.Cust_Status,'N/A') AS Cust_Status, IFNULL(B.Customer_OD_Status,'N/A') AS Customer_OD_Status, IFNULL(B.Cash_Cust,'N') AS Cash_Cust, IFNULL(B.Price_List_ID,'N/A') AS Price_List_ID, IFNULL(B.Avail_Bal,'0') AS Avail_Bal, IFNULL(B.Bill_Credit_Period,'N/A') AS Bill_Credit_Period, IFNULL(B.Allow_FOC,'N/A') AS Allow_FOC, IFNULL(B.Creation_Date,'N/A') AS Creation_Date,IFNULL(A.Customer_Name,'N/A') AS  Customer_Name,IFNULL(C.isDelegatedCustomer,'N') AS isDelegatedCustomer,IFNULL(B.Area,'N/A') AS Area FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID LEFT JOIN (select IFNULL(Attrib_Value,'N')AS isDelegatedCustomer,Customer_ID AS Delegated_Customer_ID,Site_Use_ID AS Delegated_Customer_Site_Use_ID from TBL_Customer_Addl_Info where Attrib_Name='Is_Delegated') AS C ON B.Customer_ID=C.Delegated_Customer_ID AND B.Site_Use_ID=C.Delegated_Customer_Site_Use_ID  ORDER BY A.Customer_Name";
    }
    else{
        
        queryforCustomers = @"Select IFNULL(A.Sales_District_ID,'N/A') AS Sales_District_ID,IFNULL(A.Postal_Code,'N/A') AS Postal_Code ,IFNULL(A.Location,'N/A')AS Location,IFNULL(A.Dept,'N/A') AS Dept,IFNULL(A.Customer_Segment_ID,'N/A') AS Customer_Segment_ID,IFNULL(B.Customer_No,'N/A') AS Customer_No,IFNULL(B.Customer_barCode,'N/A')AS Customer_barCode, IFNULL(A.Cust_lat,'N/A')AS Cust_Lat,IFNULL(A.Cust_long,'N/A') AS Cust_Long, IFNULL(A.City,'N/A') AS City,IFNULL(A.Address, 'N/A') AS Address,IFNULL(A.Customer_ID,'N/A') AS [Ship_Customer_ID] , IFNULL(A.Site_Use_ID,'N/A') AS [Ship_Site_Use_ID], IFNULL(B.Customer_ID,'N/A')AS Customer_ID, IFNULL(B.Site_Use_ID,'N/A') AS Site_Use_ID, IFNULL(B.Contact,'N/A') AS Contact, IFNULL(B.Phone,'N/A') AS Phone,IFNULL(B.Credit_Limit,'0')AS Credit_Limit, IFNULL(B.Credit_Hold,'N/A') AS Credit_Hold, IFNULL(B.Customer_Type,'N/A') AS Customer_Type, IFNULL(B.Customer_Class,'N/A') AS Customer_Class, IFNULL(B.Trade_Classification,'N/A') AS Trade_Classification,IFNULL(B.Chain_Customer_Code,'N/A') AS Chain_Customer_Code, IFNULL(B.Cust_Status,'N/A') AS Cust_Status, IFNULL(B.Customer_OD_Status,'N/A') AS Customer_OD_Status, IFNULL(B.Cash_Cust,'N') AS Cash_Cust, IFNULL(B.Price_List_ID,'N/A') AS Price_List_ID, IFNULL(B.Avail_Bal,'0') AS Avail_Bal, IFNULL(B.Bill_Credit_Period,'N/A') AS Bill_Credit_Period, IFNULL(B.Allow_FOC,'N/A') AS Allow_FOC, IFNULL(B.Creation_Date,'N/A') AS Creation_Date,IFNULL(A.Customer_Name,'N/A') AS  Customer_Name,IFNULL(C.isDelegatedCustomer,'N') AS isDelegatedCustomer,IFNULL(B.Area,'N/A') AS Area FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID LEFT JOIN (select IFNULL(Attrib_Value,'N')AS isDelegatedCustomer,Customer_ID AS Delegated_Customer_ID,Site_Use_ID AS Delegated_Customer_Site_Use_ID from TBL_Customer_Addl_Info where Attrib_Name='Is_Delegated') AS C ON B.Customer_ID=C.Delegated_Customer_ID AND B.Site_Use_ID=C.Delegated_Customer_Site_Use_ID  ORDER BY A.Customer_Name";
    }
    
    
//    NSString * queryforCustomers=@"Select A.Sales_District_ID,A.Postal_Code ,A.Location,A.Dept,A.Customer_Segment_ID,B.Customer_NO,B.Customer_barCode, A.Cust_lat,A.Cust_long, A.City,A.Address,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, IFNULL(B.Phone,'N/A') AS Phone,IFNULL(B.Credit_Limit,'0')AS Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, IFNULL(B.Avail_Bal,'0') AS Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID   ORDER BY A.Customer_Name";
    
    NSLog(@"customers query is %@",queryforCustomers);
    
    
   // NSString* queryforCustomers=@"SELECT C.Attrib_Value AS CUST_LOC_RNG, CASE WHEN C.Attrib_NAME='CUST_LOC_RNG Then 'Y' END As IS_LOC_RNG,A.Sales_District_ID,A.Postal_Code ,A.Location,A.Dept,A.Customer_Segment_ID,B.Customer_NO,B.Customer_barCode, A.Cust_lat,A.Cust_long, A.City,A.Address,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, IFNULL(B.Phone,'N/A') AS Phone,IFNULL(B.Credit_Limit,'0')AS Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, IFNULL(B.Avail_Bal,'0') AS Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID left join TBL_Customer_Addl_Info AS C on A.Customer_ID=C.Customer_ID and A.Site_Use_ID=C.Site_Use_ID  ORDER BY A.Customer_Name";
    
    
    NSArray *temp=[self fetchDataForQuery:queryforCustomers];

    
//    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerListNonAlpha,tableName ,filter]];
    return temp;
}

- (NSArray *)dbGetFilterByColumnCustomer:(NSString *)columnName {
    
    NSMutableArray *temp = [self fetchDataForQuery:kSQLCUstomerLocationFilter];
    
    
    return  temp;
}

- (NSArray *)dbGetTarget:(NSString *)customerCode {
    NSDate *date = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSMonthCalendarUnit fromDate:date];
    NSArray *temp;
    
    
    NSArray* appCtrlFlag=[SWDefaults appControl];
    NSLog(@"check app control flag for custmer target %@",[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"]);
    
    
    
    NSString* tgtStr=[[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"] objectAtIndex:0];
    
    
    if (tgtStr==(id)[NSNull null]|| tgtStr.length==0) {
        
        
        
    }
    
    
    
    else if ([[[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"] objectAtIndex:0] isEqualToString:@"Y"] )
        
    {
        
        NSLog(@"This is Al seer");
        
        
        NSLog(@"check query %@",[NSString stringWithFormat:kSQLCustomerTargetAlseer, customerCode, [comps month]] );
        
        
        temp=[self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerTargetAlseer, customerCode, [comps month]]];
        
    }
    
    else
    {
        temp=[self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerTargetGeneral, customerCode, [comps month]]];
        
    }
    
    return temp;
}

- (NSArray *)dbGetCustomerProductSalesWithCustomerID:(NSString *)custID andProductId:(NSString *)productID
{
    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Customer_Sales_Data where Cust_ID='%@' and Item_ID='%@'", custID,productID]];
    return temp;
}
- (NSArray *)dbGetCustomerTotalSalesWithCustomerID:(NSString *)custID
{
    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"Select sum(Bk0_Amt) as Bk0_Amt from TBL_Customer_Sales_Data where Cust_ID='%@'", custID]];
    return temp;
}

//////////////////////Distribution/////////////
//#define kSQLDictributionChecktInfo @"INSERT INTO TBL_Distribution_Check (DistributionCheck_ID,Customer_ID, Site_Use_ID,SalesRep_ID,Emp_Code,Visit_ID,Status) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')"

#define kSQLDictributionChecktInfo @"INSERT INTO TBL_Distribution_Check (DistributionCheck_ID,Customer_ID, Site_Use_ID,SalesRep_ID,Emp_Code,Visit_ID,Status,Checked_On) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}','{7}')"

//#define kSQLDictributionItemInfo @"INSERT INTO TBL_Distribution_Check_Items(DistributionCheckLine_ID, DistributionCheck_ID, Inventory_Item_ID, Is_Available, Qty, Expiry_Dt, Display_UOM) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}','{5}', '{6}')"

#define kSQLDictributionItemInfo @"INSERT INTO TBL_Distribution_Check_Items(DistributionCheckLine_ID, DistributionCheck_ID, Inventory_Item_ID, Is_Available, Qty, Expiry_Dt, Display_UOM, Custom_Attribute_1, Custom_Attribute_2, Custom_Attribute_3, Reorder) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}','{5}', '{6}', '{7}', '{8}', '{9}', '{10}')"

- (NSArray *)dbGetDistributionCollection {
    //[query setQuery:kSQLDistributionCollection];
    //[query setTag:kQueryTypeCollection];
    
    NSArray *temp = [self fetchDataForQuery:kSQLDistributionCollection];
    
    NSMutableArray *results = [NSMutableArray array];
    
    for (NSDictionary *row in temp) {
        @autoreleasepool{
            NSMutableDictionary *newROW = [NSMutableDictionary dictionaryWithDictionary:row];
            [newROW setValue:@"" forKey:@"Qty"];
            [newROW setValue:@"" forKey:@"Avl"];
            [newROW setValue:@"" forKey:@"ExpDate"];
            [results addObject:newROW];
        }
    }
    return temp;
}
#define kSQLcheckCurrentVisitID @"SELECT DistributionCheck_ID FROM TBL_Distribution_Check WHERE Visit_ID = '%@'"
- (NSArray *)checkCurrentVisitInDC:(NSString *)visitID{
    
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:kSQLcheckCurrentVisitID,visitID]];
    return temp;
}

- (NSString *)saveDistributionCheckWithCustomerInfo:(Customer *)customerInfo andDistChectItemInfo:(NSMutableArray *)fetchDistriButionCheckLocations
{
    NSString *DistributionCheckID = [NSString createGuid];
    
    NSDictionary *FSR = [SWDefaults userProfile];
    
    NSString *temp = kSQLDictributionChecktInfo;
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:DistributionCheckID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:customerInfo.Ship_Customer_ID];//[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:customerInfo.Ship_Site_Use_ID];//[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[FSR stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[FSR stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[SWDefaults currentVisitID]];
    /*pavan:24-02-2015 status should be always "N" while saving the data(//test/backoffice)*/
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]];
    
    
    [self executeNonQuery:temp];

    for (DistriButionCheckLocation *DCLocation in fetchDistriButionCheckLocations) {
        for (DistriButionCheckItem *DCItem in DCLocation.dcItemsArray)
        {
            for (int i =0; i<[DCItem.dcItemLots count]; i++) {
                DistriButionCheckItemLot *DCItemLot = [DCItem.dcItemLots objectAtIndex:i];
                
                if (DCItem.imageName.length >0 && i == 0) {
                    DCItemLot.DistriButionCheckItemLotId = DCItem.imageName;
                }
                else{
                    DCItemLot.DistriButionCheckItemLotId = [NSString createGuid];
                }
                
                NSString *sql = kSQLDictributionItemInfo;
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:DCItemLot.DistriButionCheckItemLotId];
                sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:DistributionCheckID];
                sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[SWDefaults getValidStringValue:DCItem.DcProduct.Inventory_Item_ID]];
                
                if ([[AppControl retrieveSingleton].ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
                {
                    if ([[AppControl retrieveSingleton].FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
                        sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[SWDefaults getValidStringValue:DCItemLot.itemAvailability]];
                    } else {
                        if (DCItemLot.Quntity.length > 0) {
                            sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:@"Y"];
                        }
                        else{
                            sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:@"N"];
                        }
                    }
                }
                else{
                    if ([[AppControl retrieveSingleton].FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
                        sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[SWDefaults getValidStringValue:DCItemLot.itemAvailability]];
                    } else {
                        sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[SWDefaults getValidStringValue:DCItem.itemAvailability]];
                    }
                }


                if ([[AppControl retrieveSingleton].DEFAULT_AVAILABILITY_IN_DC isEqualToString:KAppControlsYESCode]) {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[SWDefaults getValidStringValue:DCItem.Quntity]];
                }else{
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[SWDefaults getValidStringValue:DCItemLot.Quntity]];
                }
            
//                NSString *refinedExpiryDate = [MedRepDefaults refineDateFormat:kDateFormatWithoutTime destFormat:kDatabseDefaultDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:DCItemLot.expiryDate]];
                NSString *refinedExpiryDate;
                if ([[AppControl retrieveSingleton].FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
                    refinedExpiryDate = [MedRepDefaults refineDateFormat:kDateFormatWithoutTime destFormat:kDatabseDefaultDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:DCItemLot.expiryDate]];
                    
                }else{
                    if ([[AppControl retrieveSingleton].DEFAULT_AVAILABILITY_IN_DC isEqualToString:KAppControlsYESCode]){
                        refinedExpiryDate = DCItem.expiryDate;
                    }else{
                        refinedExpiryDate = DCItemLot.expiryDate;
                    }
                }
            
                sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[SWDefaults getValidStringValue:refinedExpiryDate]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[SWDefaults getValidStringValue:DCItem.DcProduct.primaryUOM]];

                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[SWDefaults getValidStringValue:DCItemLot.LotNumber]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:DCLocation.LocationID];
                
                if (DCItem.imageName.length >0) {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:@"Y"];
                } else {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:@"N"];
                }
                
                if ([[AppControl retrieveSingleton].FS_SHOW_DC_RO isEqualToString:KAppControlsYESCode]) {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[SWDefaults getValidStringValue:DCItemLot.reOrder]];
                } else {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:@"N"];
                }
                
                [self executeNonQuery:sql];
            }
        }
    }

    return DistributionCheckID;
}

//- (NSString *)saveDistributionCheckWithCustomerInfo:(NSDictionary *)customerInfo andDistChectItemInfo:(NSArray *)invoices{
//    NSString *DistributionCheckID = [NSString createGuid];
//    
//    // customerInfo = [customerInfo mutableCopy];
//    NSDictionary *FSR = [SWDefaults userProfile];
//    
//    NSString *temp = kSQLDictributionChecktInfo;
//    
//    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:DistributionCheckID];
//    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
//    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
//    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[FSR stringForKey:@"SalesRep_ID"]];
//    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[FSR stringForKey:@"Emp_Code"]];
//    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[SWDefaults currentVisitID]];
//    /*pavan:24-02-2015 status should be always "N" while saving the data(//test/backoffice)*/
//    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:@"N"];
//    
//    
//    
//    [self executeNonQuery:temp];
//    
//    
//    
//    for (int i = 0; i < invoices.count; i++) {
//        @autoreleasepool{
//            NSDictionary *row = [invoices objectAtIndex:i];
//            
//            NSString *sql = kSQLDictributionItemInfo;
//            
//            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
//            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:DistributionCheckID];
//            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[row stringForKey:@"Inventory_Item_ID"]];
//            sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Avl"]];
//            sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Qty"]];
//            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"ExpDate"]];
//            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Primary_UOM_Code"]];
//            
//            [self executeNonQuery:sql];
//        }
//    }
//    return DistributionCheckID;
//    
//}
////////////////////SalesOrder/////////////////
#define kSQLSaveOrder @"INSERT INTO TBL_Order (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, Packing_Instructions, Request_Date, Schedule_Ship_Date, Order_Status, Start_Time, End_Time, Signee_Name, Custom_Attribute_1, Emp_Code, Order_Amt, Visit_ID , System_Order_Date,Credit_Customer_ID,Credit_Site_ID,Last_Update_Date ,Last_Updated_By,Custom_Attribute_2,Custom_Attribute_3,Discount_Amt,Customer_PO_Number) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}', '{10}', '{11}' , '{12}', '{13}', '{15}', '{16}' , '{17}', '{18}' , '{19}','{20}','{21}','{22}' ,'{23}','{24}','{25}' ,'{26}','{27}','{28}')"


#define kSQLSaveOrderwithVisitType @"INSERT INTO TBL_Order (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, Packing_Instructions, Request_Date, Schedule_Ship_Date, Order_Status, Start_Time, End_Time, Signee_Name, Custom_Attribute_1, Emp_Code, Order_Amt, Visit_ID , System_Order_Date,Credit_Customer_ID,Credit_Site_ID,Last_Update_Date ,Last_Updated_By,Custom_Attribute_2,Custom_Attribute_3,Discount_Amt,Customer_PO_Number,Custom_Attribute_4) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}', '{10}', '{11}' , '{12}', '{13}', '{15}', '{16}' , '{17}', '{18}' , '{19}','{20}','{21}','{22}' ,'{23}','{24}','{25}' ,'{26}','{27}','{28}','{29}')"

#define kSQLSaveOrderLineItem @"INSERT INTO TBL_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID, Unit_Selling_Price, Def_Bonus,Calc_Price_Flag, Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}','{9}' ,'{10}','{11}','{12}')"

#define kSQLSaveOrderLotItem @"INSERT INTO TBL_Order_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Ordered_Quantity, Is_FIFO, Org_ID) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')"

#define kSQLSavePerformaOrder @"INSERT INTO TBL_Proforma_Order (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Start_Time, Emp_Code,Order_Amt,Visit_ID , Custom_Attribute_2,Credit_Customer_ID,Credit_Site_ID,Custom_Attribute_3,Last_Update_Date,Last_Updated_By) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}' ,'{10}','{11}' ,'{12}','{13}','{14}','{15}')"

//TODO: change this also. OLA!!
#define kSQLSavePerfomrmaLineItem @"INSERT INTO TBL_Proforma_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID,Unit_Selling_Price,Def_Bonus,Calc_Price_Flag,Custom_Attribute_3,Custom_Attribute_2) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}' , '{9}', '{10}','{11}' )"

#define kSQLSavePerfomrmaLotItem @"INSERT INTO TBL_Proforma_Order_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Ordered_Quantity, Is_FIFO, Org_ID) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')"

#define kSQLSaveOrderHistory @"INSERT INTO TBL_Order_History (Row_ID, Orig_Sys_Document_Ref, Doc_Type, Creation_Date  , Created_By, System_Order_Date, Request_Date, Schedule_Ship_Date, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, BO_Status, Start_Time, End_Time, Packing_Instructions, Emp_Code,ERP_Status, Transaction_Amt,Due_Date,Credit_Customer_ID,Credit_Site_ID, Visit_ID,Sync_Timestamp) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}', '{10}', '{11}' , '{12}', '{13}', '{14}', '{15}', '{16}' , '{17}', '{18}' , '{19}','{20}','{21}','{22}','{23}' )"

#define kSQLNewSaveUnconfirmedOrderHistory @"INSERT INTO TBL_Order_History (Row_ID, Orig_Sys_Document_Ref, Doc_Type, Creation_Date  , Created_By, System_Order_Date, Request_Date, Schedule_Ship_Date, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, BO_Status, Start_Time, End_Time, Packing_Instructions, Emp_Code,ERP_Status, Transaction_Amt,Due_Date,Credit_Customer_ID,Credit_Site_ID, Visit_ID,Sync_Timestamp, Custom_Attribute_2,Custom_Attribute_3,Custom_Attribute_6) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}', '{10}', '{11}' , '{12}', '{13}', '{14}', '{15}', '{16}' , '{17}', '{18}' , '{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}' )"



//#define kSQLSaveOrderHistoryLineItem @"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}' ,'{8}')"

/*modified on 25-02-2015 : adding lot no and expiry date*/
#define kSQLSaveOrderHistoryLineItem @"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value,Lot_No,Expiry_Date) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}' ,'{8}','{9}' ,'{10}')"

#define kSQLNewSaveUnconfirmedHistoryLineItem @"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value,Lot_No,Expiry_Date,Custom_Attribute_1,Custom_Attribute_2,Custom_Attribute_3) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}' ,'{8}','{9}' ,'{10}','{11}','{12}','{13}')"




#define kSQLdbGetTemplateOrder @"Select * from TBL_Order_Template"
#define kSQLdbGetTemplateOrderItems @"SELECT A.*, B.* FROM TBL_Order_Template_Items As A INNER JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Order_Template_ID='%@'"
#define kSQLDeletePerformaOrder @"DELETE FROM TBL_Proforma_Order WHERE Orig_Sys_Document_Ref='{0}'"
#define kSQLDeletePerformaOrderItems @"DELETE FROM TBL_Proforma_Order_Line_Items WHERE Orig_Sys_Document_Ref='{0}'"
#define kSQLDeletePerformaOrderLots @"DELETE FROM TBL_Proforma_Order_Lots WHERE Orig_Sys_Document_Ref='{0}'"
#define kSQLdbGetConfirmOrder @"Select * from TBL_Order where Ship_To_Customer_ID = '%@' AND Ship_To_Site_ID = '%@'"

// TODO: change this. OLA!!
#define kSQLPerformaOrderItems @"SELECT A.*, B.* FROM TBL_Proforma_Order_Line_Items As A INNER JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Orig_Sys_Document_Ref='%@' ORDER BY A.Line_Number ASC"

#define kSQLConfirmOrderItems @"SELECT A.*, B.* FROM TBL_Order_Line_Items As A INNER JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Orig_Sys_Document_Ref='%@' ORDER BY A.Line_Number ASC"

#define kSQLInsertTemplateOrder  @"INSERT INTO TBL_Order_Template (Order_Template_ID, Template_Name, SalesRep_ID, Custom_Attribute_1, Custom_Attribute_2) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')"

#define kSQLInsertTemplateOrderItem @"INSERT INTO TBL_Order_Template_Items (Template_Line_ID, Order_Template_ID, Inventory_Item_ID, Organization_ID, Quantity) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')"

#define kSQLSaveReturnOrder @"INSERT INTO TBL_RMA (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID,Internal_Notes, Customer_Ref_No, Order_Status, Start_Time, End_Time,Last_Update_Date ,Last_Updated_By, Signee_Name, Order_Amt, Visit_ID ,Emp_Code,Credit_Customer_ID , Credit_Site_ID ,Invoice_Ref_No ,Reason_Code , Custom_Attribute_2,Custom_Attribute_4,Custom_Attribute_1,Custom_Attribute_3) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}', '{10}', '{11}' , '{12}', '{14}', '{15}', '{16}' , '{17}', '{18}' , '{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}')"

#define kSQLSaveReturnLineItem @"INSERT INTO TBL_RMA_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number,Return_Quantity_UOM, Display_UOM, Returned_Quantity, Inventory_Item_ID,Unit_Selling_Price,Custom_Attribute_2,Custom_Attribute_1) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}','{8}','{9}')"

#define kSQLSaveReturnLotItem @"INSERT INTO TBL_RMA_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Inventory_Item_ID,Returned_Quantity , Expiry_Dt ,Lot_Type) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}' )"

#define kSQLdbGetPerformaOrder @"Select * from TBL_Proforma_Order where Ship_To_Customer_ID = '%@' AND Ship_To_Site_ID = '%@'"
#define kSQLGetAllPerformaOrder @"Select A.*,B.Customer_Name,B.Customer_ID,B.Site_Use_ID from TBL_Proforma_Order AS A INNER JOIN TBL_Customer As B ON A.Ship_To_Customer_ID=B.Customer_ID"

#define KSQLdbGetReorderItems @"SELECT A.*, B.* FROM TBL_Order_History_Line_Items As A INNER JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Orig_Sys_Document_Ref='%@' ORDER BY A.Line_Number ASC"




static NSString *BonusCA1GUID;
- (NSArray *)dbGetSalesOrderItems:(NSString *)refId {
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypeItems];
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLOrderInvoiceItems, refId]];
    return temp;
}

- (NSArray *)dbGetSalesOrderInvoice:(NSString *)refId {
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypeInvoice];
    
    NSLog(@"onnvoice qry %@",[NSString stringWithFormat:kSQLOrderInvoice, refId] );
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLOrderInvoice, refId]];
    
    return temp;
}

- (NSArray *)dbGetInvoicesOfCustomer:(NSString *)customerId withSiteId:(NSString *)siteId andShipCustID:(NSString *)sCusId withShipSiteId:(NSString *)sSiteId andIsStatement:(BOOL)isStatement {
    
    /**select A.Invoice_Ref_No AS Invoice_Ref_No,A.Pending_Amount AS NetAmount,IFNULL(C.Amount,'0') AS PaidAmt,'0' AS Amount,A.Invoice_Date AS InvDate, A.Due_Date as DueDate from tbl_open_Invoices AS A
     LEFT JOIN (select * from TBL_Collection group by Customer_ID,Site_Use_ID) AS B  ON A.Customer_ID = B.Customer_ID AND A.Site_Use_ID = B.Site_Use_ID
     LEFT JOIN (select SUM(Amount)AS Amount,Invoice_No  from TBL_Collection_Invoices Where ERP_Status!='C' group by Invoice_No) AS C ON A.Invoice_Ref_No = C.Invoice_No
     WHERE A.Customer_ID='2426' AND A.Site_Use_ID='1001'
     
     UNION ALL
     select A.Orig_Sys_Document_Ref AS Invoice_Ref_No,IFNULL(A.Transaction_Amt,'0') As NetAmount,IFNULL(C.Amount,'0') AS PaidAmt,'0' AS Amount,A.Creation_Date AS InvDate,A.Due_Date as DueDate from tbl_Sales_History AS A
     LEFT JOIN (select * from TBL_Collection group by Customer_ID,Site_Use_ID) AS B  ON A.Ship_To_Customer_ID = B.Customer_ID AND A.Ship_To_Site_ID = B.Site_Use_ID
     LEFT JOIN (select SUM(Amount)AS Amount,Invoice_No  from TBL_Collection_Invoices Where ERP_Status!='C' group by Invoice_No) AS C ON A.Orig_Sys_Document_Ref = C.Invoice_No
     WHERE (A.ERP_Status = 'X') AND (A.Inv_To_Customer_ID='2426') AND (A.Inv_To_Site_ID='1001') AND A.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices WHERE Customer_ID='2426' AND Site_Use_ID='1001') ORDER BY InvDate ASC*/
    
    NSString *q = [NSString stringWithFormat:@"select (IFNULL(A.Pending_Amount,'0')-IFNULL(C.Amount,'0')) AS DueAmount,A.Invoice_Ref_No AS Invoice_Ref_No,A.Pending_Amount AS NetAmount,IFNULL(C.Amount,'0') AS PaidAmt,'0' AS Amount,A.Invoice_Date AS InvDate, A.Due_Date as DueDate from tbl_open_Invoices AS A LEFT JOIN (select * from TBL_Collection group by Customer_ID,Site_Use_ID) AS B  ON A.Customer_ID = B.Customer_ID AND A.Site_Use_ID = B.Site_Use_ID  LEFT JOIN (select SUM(Amount)AS Amount,Invoice_No  from TBL_Collection_Invoices Where ERP_Status!='C' group by Invoice_No) AS C ON A.Invoice_Ref_No = C.Invoice_No WHERE A.Customer_ID='%@' AND A.Site_Use_ID='%@' UNION ALL select (IFNULL(A.Transaction_Amt,'0')-IFNULL(C.Amount,'0')) AS DueAmount,A.Orig_Sys_Document_Ref AS Invoice_Ref_No,IFNULL(A.Transaction_Amt,'0') As NetAmount,IFNULL(C.Amount,'0') AS PaidAmt,'0' AS Amount,A.Creation_Date AS InvDate,A.Due_Date as DueDate from tbl_Sales_History AS A LEFT JOIN (select * from TBL_Collection group by Customer_ID,Site_Use_ID) AS B  ON A.Ship_To_Customer_ID = B.Customer_ID AND A.Ship_To_Site_ID = B.Site_Use_ID   LEFT JOIN (select SUM(Amount)AS Amount,Invoice_No  from TBL_Collection_Invoices Where ERP_Status!='C' group by Invoice_No) AS C ON A.Orig_Sys_Document_Ref = C.Invoice_No  WHERE (A.ERP_Status = 'X') AND (A.Inv_To_Customer_ID='%@') AND (A.Inv_To_Site_ID='%@') AND A.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices WHERE Customer_ID='%@' AND Site_Use_ID='%@') ORDER BY InvDate ASC",customerId,siteId,customerId,siteId,customerId,siteId];
    
    NSLog(@"Invoices Query %@",q);
    
    NSArray *tempInvoicesArray = [[self fetchDataForQuery:q] mutableCopy];
   // NSLog(@"Invoices array %@",tempInvoicesArray);

    return tempInvoicesArray;
}


- (NSArray *)dbGetPerformaOrder:(NSString *)customerId withSiteId:(NSString *)siteId
{
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypedbGetPerformaOrder];
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLdbGetPerformaOrder, customerId , siteId]];
    return temp;
}
- (NSArray *)dbGetPerformaOrder{
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypedbGetPerformaOrder];
    
    NSArray *temp = [self fetchDataForQuery:kSQLGetAllPerformaOrder];
    return temp;
}

- (NSArray *)dbGetConfirmOrder:(NSString *)customerId withSiteId:(NSString *)siteId
{
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypedbGetConfirmOrder];
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLdbGetConfirmOrder, customerId , siteId]];
    return temp;
}


- (NSArray *)dbGetSalesConfirmOrderItems:(NSString *)refId
{
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypedbGetConfirmOrderItems];
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLConfirmOrderItems, refId]];
    
    return temp;
}
- (NSArray *)dbGetSalesPerformaOrderItems:(NSString *)refId
{
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypedbGetPerformaOrderItems];
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLPerformaOrderItems, refId]];
    //NSLog(@"OLA!!******* ref ID %@, fetched items %@", refId, temp);
    
    return temp;
}
- (NSArray *)dbGetSalesReOrderItems:(NSString *)refId
{
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:KSQLdbGetReorderItems, refId]];
    return temp;
}
- (NSArray *)dbGetReturnsType
{
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypeReturns];
    
    NSArray *temp = [self fetchDataForQuery:@"select * from TBL_RMA_Lot_Types"];
    return temp;
}

- (NSArray *)dbGetReasonCode
{
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypeReason];
    
    NSArray *temp = [self fetchDataForQuery:@"select * from TBL_Reason_Codes where Purpose = 'Returns'"];
    return temp;
}

- (NSArray *)dbGetTemplateOrder
{
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypeGetTemplate];
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLdbGetTemplateOrder]];
    return temp;
}
- (NSArray *)dbGetTemplateOrderItem:(NSString *)refId
{
    //SWQuery *query = [[SWQuery alloc] init];
    //[query setQuery:];
    //[query setTag:kQueryTypeGetTemplateItems];
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLdbGetTemplateOrderItems, refId]];
    NSLog(@"template order item response %@", temp);
    return temp;
}

- (void)filterInvoices:(NSArray *)result {
}


- (NSString *)saveOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID{
    NSString *orderID = [NSString createGuid];
    
    Singleton *single = [Singleton retrieveSingleton];
    
    NSString *nUserId = [userInfo stringForKey:@"Emp_Code"];
    if(nUserId.length == 1)
    {nUserId = [NSString stringWithFormat:@"00%@",nUserId];}
    else if(nUserId.length == 2)
    {nUserId = [NSString stringWithFormat:@"0%@",nUserId];}
    
    NSString *lastOrderReference = [SWDefaults lastOrderReference];
    unsigned long long lastRefId = [[lastOrderReference substringFromIndex: [lastOrderReference length] - 10] longLongValue];
    lastRefId = lastRefId + 1;
    lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
    
    if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
    {
        [SWDefaults setLastOrderReference:lastOrderReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
        if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
        {
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
    }
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *dateTimeString =  [formatterTime stringFromDate:[NSDate date]];
    
    NSString *startTime = [formatterTime stringFromDate:single.orderStartDate];
    
    NSArray *orderArray = [NSArray arrayWithArray:[orderInfo objectForKey:@"OrderItems"]] ;
    NSDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[orderInfo objectForKey:@"info"]] ;
    //NSArray *lotArray = [NSArray aray] ;
    NSDictionary *extraInfo = [NSMutableDictionary dictionaryWithDictionary:[infoOrder objectForKey:@"extraInfo"]];
    
    
    
    
    //save the order based on app control flag
    NSString *temp;

    AppControl * appControl=[AppControl retrieveSingleton];
    
    if ([appControl.ENABLE_VISIT_TYPE isEqualToString:@"Y"]) {
        
        temp= kSQLSaveOrderwithVisitType;
    }
    else
    {
        temp = kSQLSaveOrder;
    }
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}"  withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{23}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}"  withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{24}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[customerInfo stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[customerInfo stringForKey:@"Site_Use_ID"]];
    NSString *stringComments = [extraInfo stringForKey:@"comments"];
    
    NSLog(@"sting comments before replacing %@", stringComments);

    
    stringComments = [stringComments stringByReplacingOccurrencesOfString:@"'" withString:@""];
    
    NSLog(@"sting comments after replacing %@", stringComments);
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:stringComments];
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:dateString];
    
    
    NSLog(@"query after packing instructions %@", temp);
    
    //check ship date here
    
    NSLog(@"check ship date here %@", [extraInfo objectForKey:@"ship_date"]);
    
    
    if ([[extraInfo objectForKey:@"ship_date"] isKindOfClass:[NSDate class]]) {
        
        NSLog(@"ship date is NSDate");

    }
    else if ([[extraInfo objectForKey:@"ship_date"] isKindOfClass:[NSString class]])
        
    {
        NSLog(@"ship date is nssting");
    }
    


    
    
    if ([extraInfo objectForKey:@"ship_date"]) {
        temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[extraInfo objectForKey:@"ship_date"]];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:dateString];
    }
    
    
    //saving the order type, onsite/telephone in shipping instructions of tbl_order
    
    
    
    if ([appControl.ENABLE_VISIT_TYPE isEqualToString:@"Y"]) {
       
        BOOL isOnsite=[SWDefaults fetchOnsiteVisitStatus];
        
        
        
        if (isOnsite==YES) {
            temp = [temp stringByReplacingOccurrencesOfString:@"{29}" withString:kOnsiteVisitDatabaseTitle];
        }
        
        else
        {
            temp = [temp stringByReplacingOccurrencesOfString:@"{29}" withString:kTelephonicVisitDatabaseTitle];
        }
    }
    
    else
    {
        
    }
    
    
    
  

    
    
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:dateTimeString];
    if([extraInfo objectForKey:@"signature"])
    {temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@"[SIGN:Y]"];}
    else
    {temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@"[SIGN:N]"];}
    //temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:[extraInfo stringForKey:@"signature"]];
    if([extraInfo objectForKey:@"name"])
    {
        NSString *stringName = [extraInfo stringForKey:@"name"];
        stringName = [stringName stringByReplacingOccurrencesOfString:@"'" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:stringName];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
    }
    temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:[userInfo stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:[infoOrder stringForKey:@"orderAmt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{25}" withString:[infoOrder stringForKey:@"productCategory"]];
    
    
    NSString* warehouseString=[[SWDefaults productCategory] stringForKey:@"Org_ID"];
    NSLog(@"check ware house  %@",warehouseString);
    
    if ([NSString isEmpty:warehouseString]) {
        warehouseString=single.selectedWarehouse;
        NSLog(@"warehouse from singleton %@",warehouseString);
    }
    
    NSString* warehouseLogString;
    
    warehouseLogString =[NSString stringWithFormat:@"Creating SalesOrder with warehouse:%@ \n ", warehouseString];
    
    [SWDefaults writeLogs:[NSString stringWithFormat:@"Product category in sales order %@", [SWDefaults productCategory]]];
    
    
    [SWDefaults writeLogs:warehouseLogString];
    
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{26}" withString:warehouseString];
    
    
    //temp = [temp stringByReplacingOccurrencesOfString:@"{26}" withString:[[SWDefaults productCategory] stringForKey:@"Org_ID"]];
    
    //why is discount harded coded as 0.0
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{27}" withString:@"0.0"]; // discount amt
    
    
    NSString *stringReference = [extraInfo stringForKey:@"reference"];
    stringReference = [stringReference stringByReplacingOccurrencesOfString:@"'" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{28}" withString:stringReference];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary] ;
    
    if([single.isCashCustomer isEqualToString:@"N"] || [dict objectForKey:@"cash_Name"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{22}" withString:@""];
    }
    else{
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:[dict stringForKey:@"Customer_ID"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"{22}" withString:[dict stringForKey:@"Site_Use_ID"]];
    }
    
    NSLog(@"order query %@", temp);
    
    
    [self executeNonQuery:temp];
    int line;
    for (int i = 0; i < orderArray.count; i++)
    {
        @autoreleasepool{
            NSDictionary *row = [orderArray objectAtIndex:i];
            //NSLog(@"TEsting %@",row);
            NSArray *lotArray = [NSArray arrayWithArray:[row objectForKey:@"StockAllocation"]] ;
            
            //lotArray = [row objectForKey:@"StockAllocation"];
            line=i+1;
            
            
            NSString *sql = kSQLSaveOrderLineItem;
            
            
            
            
            
            
            
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Qty"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"ItemID"]];
            
            
            NSLog(@"check line items data %@", row);
            
            
            
            if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {
                
                //UOM based on UOM picker al Seer
                
                
                NSString* uomStr=[row stringForKey:@"SelectedUOM"];
                
                if ([[row stringForKey:@"SelectedUOM"]isEqual:[NSNull null]]|| uomStr.length==0) {
                    
                    sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Order_Quantity_UOM"]];
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                    
                    NSLog(@"inserted uom qry with order quantity uom %@", sql);
                    
                    
                }
                else
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"SelectedUOM"]];
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
                    NSLog(@"inserted uom qry %@", sql);
                    
                }
            }
            
            
            else
            {
                
                
                NSString * primaryuomCode=[row objectForKey:@"Primary_UOM_Code"];

                if (primaryuomCode == (id)[NSNull null] || primaryuomCode.length == 0 )
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"UOM"]];
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
                    
                }
                else
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                }
                
                NSLog(@"query in save customer order with info %@",sql);
                
                
            }
            if([[row stringForKey:@"priceFlag"] isEqualToString:@"N"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Net_Price"]];
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Price"]];
            }
            
            NSString *zeroString=@"0";
            if([[row stringForKey:@"Price"] isEqualToString:@"0"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Def_Qty"]];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:zeroString];
            }
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"priceFlag"]];
            
            NSString *stringPercent = [[row stringForKey:@"DiscountPercent"]stringByReplacingOccurrencesOfString:@"%" withString:@""];
            double discountPercent = 0.0;
            discountPercent = [stringPercent doubleValue] /(double)100.0;
            sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[NSString stringWithFormat:@"%.02f",discountPercent]];
            
            if([row objectForKey:@"ChildGuid"])
            {
                BonusCA1GUID=[NSString createGuid];
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:BonusCA1GUID];
            }
            else if([row objectForKey:@"ParentGuid"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:BonusCA1GUID];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:[NSString createGuid]];
            }
            
            
            
            
            //adding line item notes(text view comments while capturing order)
            
            
            NSLog(@"add line item notes %@", [row valueForKey:@"lineItemNotes"]);
            
            
            if ([row valueForKey:@"lineItemNotes"]==nil) {
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{12}" withString:@""];
                
                
            }
            
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{12}" withString:[row valueForKey:@"lineItemNotes"]];
            }
            
            
            NSLog(@"check for notes %@", sql);
            
            
            [self executeNonQuery:sql];
            
            //            BOOL checkStatus=[self executeNonQueryOne:sql];
            //
            //            if (checkStatus==YES) {
            //
            //                NSLog(@"db success");
            //
            //
            //            }
            
            
            for (int i = 0; i < lotArray.count; i++)
            {
                @autoreleasepool{
                    NSDictionary *row = [lotArray objectAtIndex:i];
                    if([row objectForKey:@"Allocated"])
                    {
                        NSString *temp = kSQLSaveOrderLotItem;
                        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Lot_No"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Allocated"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:@"N"];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Org_ID"]];
                        
                        
                        
                        [self executeNonQuery:temp];
                    }
                }
            }
            lotArray =nil;
        }
    }
    
    [self saveOrderHistoryWithCustomerInfo:customerInfo andOrderInfo:orderInfo andFSRInfo:userInfo andVisitID:visitID andOrderID:orderID andOrderRef:lastOrderReference];
    formatter=nil;
    usLocale=nil;
    formatterTime=nil;
    usLocaleq=nil;
    extraInfo=nil;
    infoOrder=nil;
    orderArray=nil;
    dict=nil;
    return lastOrderReference;
}
- (void)saveOrderHistoryWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID andOrderID:(NSString *)orderID andOrderRef:(NSString *)lastOrderReference{
    
    Singleton *single = [Singleton retrieveSingleton];
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString * dummyTimeStr=@" 00:00:00";
    
    NSString *dateString =  [formatter stringFromDate:[NSDate date]] ;
    dateString=[dateString stringByAppendingString:dummyTimeStr];
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setLocale:usLocaleq];
    
    NSString *dateTimeString =  [formatterTime stringFromDate:[NSDate date]];
    NSString *startTime =  [formatterTime stringFromDate:single.orderStartDate];
    
    NSCalendar *gregCalendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    
    NSDateComponents *components=[gregCalendar components:NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger month=[components month];
    NSInteger year=[components year];
    
    if (month==12) {
        [components setYear:year+1];
        [components setMonth:1];
    }
    else {
        [components setMonth:month+1];
    }
    [components setDay:1];
    
    
    NSDate *lastDateofMonth= [[gregCalendar dateFromComponents:components] dateByAddingTimeInterval:0];
    NSString *lastDateMonthString =  [formatter stringFromDate:lastDateofMonth];
    NSArray *orderArray = [NSArray arrayWithArray:[orderInfo objectForKey:@"OrderItems"]] ;
    NSDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[orderInfo objectForKey:@"info"]] ;
    // NSArray *lotArray = [NSArray aray] ;
    NSMutableDictionary *extraInfo = [NSMutableDictionary dictionaryWithDictionary:[infoOrder objectForKey:@"extraInfo"]];
    
    if ([extraInfo objectForKey:@"ship_date"])
    {
        //NSLog(@"ShipDate");
    }
    else
    {
        [extraInfo setObject:[NSDate date] forKey:@"ship_date"];
        //NSLog(@"NO Shipdte");
    }
    NSString *temp = kSQLSaveOrderHistory;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[infoOrder stringForKey:@"order_Status"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:dateString];
    
    
    if ([extraInfo objectForKey:@"ship_date"]) {
        
    }
    
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[extraInfo objectForKey:@"ship_date"]];
        
    }
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[customerInfo stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:[customerInfo stringForKey:@"Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:dateTimeString];
    NSString *stringComments = [extraInfo stringForKey:@"comments"];
    stringComments = [stringComments stringByReplacingOccurrencesOfString:@"'" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:stringComments];
    temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:[userInfo stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:[infoOrder stringForKey:@"orderAmt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:lastDateMonthString];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary] ;
    
    if([single.isCashCustomer isEqualToString:@"N"] || [dict objectForKey:@"cash_Name"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:@""];
    }
    else{
        temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:[dict stringForKey:@"Customer_ID"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:[dict stringForKey:@"Site_Use_ID"]];
    }
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{22}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{23}" withString:dateString];
    
    
    //NSLog(@"Return %@",temp);
    [self executeNonQuery:temp];
    
    int line;
    for (int i = 0; i < orderArray.count; i++) {
        @autoreleasepool{
            NSDictionary *row = [orderArray objectAtIndex:i];
            
            line=i+1;
            
            NSString *sql = kSQLSaveOrderHistoryLineItem;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            
            
            if([row stringForKey:@"lot"]==nil||[row stringForKey:@"lot"].length==0) /**return items*/
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:@""];
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"lot"]];
                
            }
            
            NSLog(@"%@",[row stringForKey:@"EXPDate"]);
            if([row stringForKey:@"EXPDate"]==nil||[row stringForKey:@"EXPDate"].length==0) /**return items*/
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:@""];
                
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"EXPDate"]];
                
            }
            
            
            if([row stringForKey:@"ItemID"]==nil||[row stringForKey:@"ItemID"].length==0) /**return items*/
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Inventory_Item_ID"]];
                
            }
            else /**sales order or manage order items*/
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"ItemID"]];
                
            }
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Qty"]];
            
            
            
            if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {
                
                //save uom based on uom selection Al seer
                
                NSString* uomStr=[row stringForKey:@"SelectedUOM"];
                
                if ([[row stringForKey:@"SelectedUOM"]isEqual:[NSNull null]]|| uomStr.length==0) {
                    
                    
                    NSString* orderQtyUOM=[row stringForKey:@"Order_Quantity_UOM"];
                    
                    if (orderQtyUOM.length==0|| [[row stringForKey:@"Order_Quantity_UOM"]isEqual:[NSNull null]]) {
                        
                         sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
                    }
                    
                    else
                    {
                    
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Order_Quantity_UOM"]];
                    }
                }
                
                else
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"SelectedUOM"]];
                }
                
            }
            
            else
            {
                
                
                
                
                
                NSString * primaryuomCode=[row objectForKey:@"Primary_UOM_Code"];
                
                NSLog(@"row in save history with customer info %@",row);

                
                if (primaryuomCode == (id)[NSNull null] || primaryuomCode.length == 0 )
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
                    
                }
                
               else
               {
                   sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
               }
               

                              NSLog(@"uom inserted here %@", sql);
                
            }
            if([[row stringForKey:@"Price"] isEqualToString:@"0"])
            {
                sql =  [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Price"]];
                double iTemValue = [[row stringForKey:@"Qty"] doubleValue] * [[row stringForKey:@"Price"] doubleValue];
                
                NSLog(@"check item val in 1st item %f", iTemValue);
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[NSString stringWithFormat:@"%.2f",iTemValue]];
                
                
                
                NSLog(@"check discounted price here before inserting %@", [row stringForKey:@"Discounted_Price"]);
                
                
                //adding discounted price to item_value which was not added earlier
                
                
                // sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Discounted_Price" ]];
                
                
                NSLog(@"check the item value %@", sql);
                
                
                if ([row objectForKey:@"priceFlag"])
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"priceFlag"]];
                }
                else
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:@"F"];
                }
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Net_Price"]];
                
                
                double iTemValue = [[row stringForKey:@"Qty"] doubleValue] * [[row stringForKey:@"Net_Price"] doubleValue];
                NSLog(@"check the item value %@", sql);
                
                
                //taking discount into consideration
                
                //                double discount=[[row stringForKey:@"Discounted_Price"]doubleValue];
                //
                //                double totalVal=iTemValue-discount;
                double qty=[[row stringForKey:@"Qty"] doubleValue];
                
                double usp=[[row stringForKey:@"Net_Price"] doubleValue];
                
                double discountPrice=[[row stringForKey:@"DiscountPercent"] doubleValue];
                NSLog(@"check discount percent %f", discountPrice);
                
                
                
                double tempTotal= qty*usp;
                
                discountPrice=discountPrice/100;
                
                double totalMO=tempTotal*(1-discountPrice);
                
                NSLog(@"check total val %f", totalMO);
                
                
                //adding discounted price to item_value which was not added earlier
                
                NSLog(@"check discounted price here before inserting %@", [row stringForKey:@"Discounted_Price"]);
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[NSString stringWithFormat:@"%.1f",totalMO]];
                //                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Discounted_Price"] ];
                
                if ([row objectForKey:@"priceFlag"])
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"priceFlag"]];
                }
                else
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:@"N"];
                }
                
            }
            
            
            NSLog(@"check query %@", sql);
            
            
            [self executeNonQuery:sql];
        }
    }
    formatter=nil;
    usLocale=nil;
    formatterTime=nil;
    usLocaleq=nil;
    gregCalendar=nil;
    components=nil;
    infoOrder=nil;
    extraInfo=nil;
    orderArray=nil;
    dict=nil;
}
- (void)saveTemplateWithName:(NSString *)name andOrderInfo:(NSMutableArray *)orderInfo{
    NSString *orderID = [NSString createGuid];
    
    NSLog(@"category being passed to template and order info %@  %@", [SWDefaults productCategory], orderInfo);
    
    
    NSString *temp = kSQLInsertTemplateOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:name];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[[SWDefaults productCategory] stringForKey:@"Category"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[[SWDefaults productCategory] stringForKey:@"Custom_Attribute_2"]];
    NSLog(@"qry for saving template %@", temp);
    
    
    [self executeNonQuery:temp];
    
    for (int i = 0; i < orderInfo.count; i++)
    {
        @autoreleasepool
        {
            NSString *itemID = [NSString createGuid];
            NSDictionary *row = [orderInfo objectAtIndex:i];
            NSString *temp = kSQLInsertTemplateOrderItem;
            temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:itemID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:orderID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[row stringForKey:@"ItemID"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[[SWDefaults productCategory]stringForKey:@"Org_ID"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Qty"]];
            
            
            [self executeNonQuery:temp];
        }
    }
}
- (void)deleteSalesPerformaOrderItems:(NSString *)refId{
    NSString *temp = kSQLDeletePerformaOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:refId];
    
    
    [self executeNonQuery:temp];
    
    NSString *temp1 = kSQLDeletePerformaOrderItems;
    temp1 = [temp1 stringByReplacingOccurrencesOfString:@"{0}" withString:refId];
    
    
    [self executeNonQuery:temp1];
    
    NSString *temp2 = kSQLDeletePerformaOrderLots;
    temp2 = [temp2 stringByReplacingOccurrencesOfString:@"{0}" withString:refId];
    
    
    [self executeNonQuery:temp2];
    
}
- (void)kSQLDeletePerformaOrderItemsFunction:(NSString *)refId{
    
    NSString *temp1 = kSQLDeletePerformaOrderItems;
    temp1 = [temp1 stringByReplacingOccurrencesOfString:@"{0}" withString:refId];
    
    
    [self executeNonQuery:temp1];
    
    [self performSelector:@selector(kSQLDeletePerformaOrderLotsFunction:) withObject:refId afterDelay:1.0f];
    
}
- (void)kSQLDeletePerformaOrderLotsFunction:(NSString *)refId{
    NSString *temp2 = kSQLDeletePerformaOrderLots;
    temp2 = [temp2 stringByReplacingOccurrencesOfString:@"{0}" withString:refId];
    
    
    [self executeNonQuery:temp2];
}
- (NSString *)savePerformaOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID{
    
    NSString *orderID = [NSString createGuid];
    
    NSString *lastPerformaOrderReference = [SWDefaults lastPerformaOrderReference];
    NSString *nUserId = [userInfo stringForKey:@"Emp_Code"];
    
    unsigned long long lastRefId = [[lastPerformaOrderReference substringFromIndex: [lastPerformaOrderReference length] - 10] longLongValue];
    lastRefId = lastRefId + 1;
    
    
    if(nUserId.length == 1)
    {
        nUserId = [NSString stringWithFormat:@"00%@",nUserId];
    }
    else if(nUserId.length == 2)
    {
        nUserId = [NSString stringWithFormat:@"0%@",nUserId];
    }
    lastPerformaOrderReference = [NSString stringWithFormat:@"M%@D%010lld", nUserId, lastRefId];
    if (![lastPerformaOrderReference isEqualToString:[SWDefaults lastPerformaOrderReference]])
    {
        [SWDefaults setLastPerformaOrderReference:lastPerformaOrderReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastPerformaOrderReference = [NSString stringWithFormat:@"M%@D%010lld", nUserId, lastRefId];
        if (![lastPerformaOrderReference isEqualToString:[SWDefaults lastPerformaOrderReference]])
        {
            [SWDefaults setLastPerformaOrderReference:lastPerformaOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastPerformaOrderReference = [NSString stringWithFormat:@"M%@D%010lld", nUserId, lastRefId];
            [SWDefaults setLastPerformaOrderReference:lastPerformaOrderReference];
        }
    }
    
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    Singleton *single = [Singleton retrieveSingleton];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocale];
    
    //to be done, this was single.orderstartdate chnaged it to nsdate
    NSString *startTime = [formatterTime stringFromDate:[NSDate date]];
    
    NSArray *orderArray = [NSArray arrayWithArray:[orderInfo objectForKey:@"OrderItems"]] ;
    NSDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[orderInfo objectForKey:@"info"]] ;
    
    
    NSString *temp = kSQLSavePerformaOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[userInfo stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[infoOrder stringForKey:@"orderAmt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[infoOrder stringForKey:@"productCategory"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:[[SWDefaults productCategory] stringForKey:@"Org_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:startTime];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary] ;
    
    if([single.isCashCustomer isEqualToString:@"N"] || [dict objectForKey:@"cash_Name"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:@"0"];
    }
    else{
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:[dict stringForKey:@"Customer_ID"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:[dict stringForKey:@"Site_Use_ID"]];
    }
    
    
    
    [self executeNonQuery:temp];
    
    int line;
    //NSLog(@"OLA!!******* inserting in DB : orderItems %@", orderArray);
    
    for (int i = 0; i < orderArray.count; i++)
    {
        @autoreleasepool
        {
            NSDictionary *row = [orderArray objectAtIndex:i];
            //NSLog(@"TET %@",row);
            NSArray *lotArray = [NSArray arrayWithArray:[row objectForKey:@"StockAllocation"]] ;
            line=i+1;
            NSString *sql = kSQLSavePerfomrmaLineItem;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Qty"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"ItemID"]];
            if([[row stringForKey:@"priceFlag"] isEqualToString:@"N"])
            {sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Net_Price"]];}
            else{sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Price"]];}
            
            NSString *zeroString=@"0";
            
            
            //adding selected UOM in tbl proforma order
            
            
            //check for multi uom
            
            
            if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {
                
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"SelectedUOM"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
                
                
                NSLog(@"uom inserted to proforma order %@", sql);
                
                
                
            }
            
            
            else  if ([row objectForKey:@"Primary_UOM_Code"]) {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"UOM"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
            }
            
            if([[row stringForKey:@"Price"] isEqualToString:@"0"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Def_Qty"]];
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:zeroString];
                
            }
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"priceFlag"]];
//            sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"DiscountPercent"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"Discount"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:[row stringForKey:@"lineItemNotes"]];
            
            
            
            [self executeNonQuery:sql];
            
            for (int i = 0; i < lotArray.count; i++)
            {
                @autoreleasepool
                {
                    NSDictionary *row = [lotArray objectAtIndex:i];
                    if([row objectForKey:@"Allocated"])
                    {
                        NSString *temp = kSQLSavePerfomrmaLotItem;
                        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Lot_No"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Allocated"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:@"N"];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Org_ID"]];
                        
                        [self executeNonQuery:temp];
                    }
                }
            }
            lotArray=nil;
        }
    }
    
    formatter=nil;
    usLocale=nil;
    formatterTime=nil;
    orderArray=nil;
    infoOrder=nil;
    dict=nil;
    return lastPerformaOrderReference;
}
- (NSString *)updatePerformaOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID andOrderRef:(NSString *)orderRef{
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    
    NSArray *orderArray = [NSArray arrayWithArray:[orderInfo objectForKey:@"OrderItems"]] ;
    NSDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[orderInfo objectForKey:@"info"]] ;
    
    
    Singleton *single = [Singleton retrieveSingleton];
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale1 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setLocale:usLocale1];
    NSString *startTime = [formatterTime stringFromDate:single.orderStartDate];
    NSString *temp = kSQLSavePerformaOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:orderRef];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[userInfo stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[infoOrder stringForKey:@"orderAmt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[infoOrder stringForKey:@"productCategory"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:[infoOrder stringForKey:@"Org_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:startTime];
    
    //Singleton *single = [Singleton retrieveSingleton];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary] ;
    
    if([single.isCashCustomer isEqualToString:@"N"] || [dict objectForKey:@"cash_Name"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:@"0"];
    }
    else{
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:[dict stringForKey:@"Customer_ID"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:[dict stringForKey:@"Site_Use_ID"]];
    }
    
    
    [self executeNonQuery:temp];
    
    int line;
    for (int i = 0; i < orderArray.count; i++) {
        @autoreleasepool{
            NSDictionary *row = [orderArray objectAtIndex:i];
            NSArray *lotArray = [row objectForKey:@"StockAllocation"];
            
            line=i+1;
            
            NSString *sql = kSQLSavePerfomrmaLineItem;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:orderRef];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Qty"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"ItemID"]];
            
            if ([row objectForKey:@"Primary_UOM_Code"]) {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"UOM"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
            }
            
            if([[row stringForKey:@"priceFlag"] isEqualToString:@"N"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Net_Price"]];
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Price"]];
                
            }
            NSString *zeroString=@"0";
            if([[row stringForKey:@"Price"] isEqualToString:@"0"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Def_Qty"]];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:zeroString];
            }
            sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"priceFlag"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"DiscountPercent"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:[row stringForKey:@"lineItemNotes"]];
            
            
            
            [self executeNonQuery:sql];
            
            for (int i = 0; i < lotArray.count; i++)
            {
                @autoreleasepool{
                    NSDictionary *row = [lotArray objectAtIndex:i];
                    if([row objectForKey:@"Allocated"])
                    {
                        NSString *temp = kSQLSavePerfomrmaLotItem;
                        
                        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:orderRef];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Lot_No"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Allocated"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:@"N"];
                        
                        temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Org_ID"]];
                        
                        
                        [self executeNonQuery:temp];
                    }
                }
            }
        }
    }
    formatter=nil;
    usLocale=nil;
    formatterTime=nil;
    usLocale1=nil;
    orderArray=nil;
    infoOrder=nil;
    // lotArray=nil;
    dict=nil;
    return orderRef;
}
- (NSString *)saveReturnOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID{
    
    
    
    NSLog(@"data for saving return %@ %@  %@ %@ ", [orderInfo description], [userInfo description],[customerInfo description],visitID);
    
    
    
    NSString *orderID = [NSString createGuid];
    NSString *lastPerformaOrderReference = [SWDefaults lastReturnOrderReference];
    NSString *nUserId = [userInfo stringForKey:@"Emp_Code"];
    
    unsigned long long lastRefId = [[lastPerformaOrderReference substringFromIndex: [lastPerformaOrderReference length] - 10] longLongValue];
    lastRefId = lastRefId + 1;
    
    
    if(nUserId.length == 1)
    {
        nUserId = [NSString stringWithFormat:@"00%@",nUserId];
    }
    else if(nUserId.length == 2)
    {
        nUserId = [NSString stringWithFormat:@"0%@",nUserId];
    }
    Singleton *single = [Singleton retrieveSingleton];
    
    
    
    
    lastPerformaOrderReference = [NSString stringWithFormat:@"M%@R%010lld", nUserId, lastRefId];
    //[SWDefaults setLastReturnOrderReference:lastPerformaOrderReference];
    
    if (![lastPerformaOrderReference isEqualToString:[SWDefaults lastReturnOrderReference]])
    {
        [SWDefaults setLastReturnOrderReference:lastPerformaOrderReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastPerformaOrderReference = [NSString stringWithFormat:@"M%@R%010lld", nUserId, lastRefId];
        if (![lastPerformaOrderReference isEqualToString:[SWDefaults lastReturnOrderReference]])
        {
            [SWDefaults setLastReturnOrderReference:lastPerformaOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastPerformaOrderReference = [NSString stringWithFormat:@"M%@R%010lld", nUserId, lastRefId];
            [SWDefaults setLastReturnOrderReference:lastPerformaOrderReference];
        }
    }
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale1 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setLocale:usLocale1];
    NSString *dateTimeString =  [formatterTime stringFromDate:[NSDate date]];
    
    NSString *startTime = [formatterTime stringFromDate:single.orderStartDate];
    
    NSArray *orderArray = [NSArray arrayWithArray:[orderInfo objectForKey:@"OrderItems"]] ;
    NSDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[orderInfo objectForKey:@"info"]] ;
    NSDictionary *extraInfo = [NSMutableDictionary dictionaryWithDictionary:[infoOrder objectForKey:@"extraInfo"]];
    
    
    NSString *temp = kSQLSaveReturnOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[customerInfo stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[customerInfo stringForKey:@"Site_Use_ID"]];
    NSString *stringComments = [extraInfo stringForKey:@"comments"];
    stringComments = [stringComments stringByReplacingOccurrencesOfString:@"'" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:stringComments];
    NSString *stringReference = [extraInfo stringForKey:@"reference"];
    stringReference = [stringReference stringByReplacingOccurrencesOfString:@"'" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:stringReference];
    //  temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:@"Y"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:dateTimeString];
    //  temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:[extraInfo stringForKey:@"signature"]];
    if([extraInfo objectForKey:@"signature"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{26}" withString:@"[SIGN:Y]"];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{26}" withString:@"[SIGN:N]"];
    }
    if([extraInfo objectForKey:@"name"])
    {
        NSString *stringName = [extraInfo stringForKey:@"name"];
        stringName = [stringName stringByReplacingOccurrencesOfString:@"'" withString:@""];
        
        temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:stringName];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@""];
    }
    temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:[infoOrder stringForKey:@"orderAmt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:[userInfo stringForKey:@"Emp_Code"]];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary] ;
    
    if([single.isCashCustomer isEqualToString:@"N"] || [dict objectForKey:@"cash_Name"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:@""];
    }
    else{
        temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:[dict stringForKey:@"Customer_ID"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:[dict stringForKey:@"Site_Use_ID"]];
    }
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{22}" withString:[extraInfo stringForKey:@"invoice_no"]];
    
    
    //this should not be description diretcly it should be from TBL_Reason_Codes
    
    NSString *stringReturns=[extraInfo stringForKey:@"returnType"];
    
    
    NSString* reasonCodeStr=stringReturns;
    
    
    NSMutableString* reasonCodeQry=[NSMutableString stringWithFormat:@"select IFNULL(Reason_Code,'N/A') AS Reason_Code,IFNULL(Description,'N/A') AS Description,IFNULL(Purpose,'N/A')  AS Purpose from TBL_Reason_Codes  where Purpose ='Returns' and Description ='%@'",reasonCodeStr ];
    
    NSLog(@"reasoncode qry is %@", reasonCodeQry);
    
    
    NSMutableArray *reasonCodesArray=[self fetchDataForQuery:reasonCodeQry];
    
    if (reasonCodesArray.count>0) {
        
        
        NSLog(@"reason codes are %@", [[reasonCodesArray objectAtIndex:0]valueForKey:@"Reason_Code"]);
        
        
        NSString* stringReturnsCode=[NSString stringWithFormat:@"%@",[[reasonCodesArray objectAtIndex:0]valueForKey:@"Reason_Code"]];
        
        temp = [temp stringByReplacingOccurrencesOfString:@"{23}" withString:stringReturnsCode];
        
        
    }
    
    
    else
    {
        stringReturns =[extraInfo stringForKey:@"returnType"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{23}" withString:stringReturns];
        
        
    }
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{24}" withString:[infoOrder stringForKey:@"productCategory"]];
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{25}" withString:[extraInfo stringForKey:@"goodCollected"]];
    
    
    
    
    NSString* warehouseLogString;
    
    NSString* warehouseString=[[SWDefaults productCategory] stringForKey:@"Org_ID"];
    
    
    if ([NSString isEmpty:warehouseString]) {
        
        warehouseString=single.selectedWarehouse;
        NSLog(@"warehouse from singleton in rma %@",warehouseString);
        warehouseLogString=[NSString stringWithFormat:@"Creating RMA with warehouse from singleton %@", warehouseString];
        
    }
    warehouseLogString =[NSString stringWithFormat:@"Creating RMA with warehouse:%@", warehouseString];
    [SWDefaults writeLogs:warehouseLogString];
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{27}" withString:warehouseString];

    
    
   // temp = [temp stringByReplacingOccurrencesOfString:@"{27}" withString:[[SWDefaults productCategory] stringForKey:@"Org_ID"]];
    
    
    
    [self executeNonQuery:temp];
    
    int line;
    for (int i = 0; i < orderArray.count; i++) {
        @autoreleasepool{
            NSDictionary *row = [orderArray objectAtIndex:i];
            
            line=i+1;
            
            NSString *sql = kSQLSaveReturnLineItem;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Qty"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Inventory_Item_ID"]];
            
            
            /*pavan 26-02-2015:multiple UOM implementation */
            
            
            if ([[[[SWDefaults appControl] objectAtIndex:0] valueForKey:@"ENABLE_MULTI_UOM"]isEqualToString:@"Y"]) {
                
                
                if([row objectForKey:@"SelectedUOM"])
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"SelectedUOM"]];
                    
                }
                else
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@""]];
                    
                }
                
            }
            
            else
            {
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"UOM"]];

                
            }
            
            
            
            if ([row objectForKey:@"Primary_UOM_Code"]) {
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
            }
            
            if([[row stringForKey:@"Price"] isEqualToString:@"0"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:@"F"];
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:@"0"];
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:@"N"];
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Net_Price"]];
                
            }
            
            if([row objectForKey:@"ChildGuid"])
            {
                BonusCA1GUID=[NSString createGuid];
                sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:BonusCA1GUID];
            }
            else if([row objectForKey:@"ParentGuid"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:BonusCA1GUID];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[NSString createGuid]];
            }
            
            
            [self executeNonQuery:sql];
            //for (int i = 0; i < orderArray.count; i++)
            //{
            //  @autoreleasepool{
            NSDictionary *row1 = [orderArray objectAtIndex:i];
            NSString *temp = kSQLSaveReturnLotItem;
            
            NSDateFormatter *expformatter = [NSDateFormatter new] ;
            [expformatter setDateFormat:@"MMM dd, yyyy"];
            NSLocale *expLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [expformatter setLocale:expLocale];
            
            
            //echeck exp date here
            NSDate *expDate = [expformatter dateFromString:[row1 stringForKey:@"EXPDate"]];
            NSString *expStr = [formatter stringFromDate:expDate];
            if (expStr==nil) {
                //check what if date is nill
                
                expStr=[formatter stringFromDate:[NSDate date]];
            }
            
            temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
            temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row1 stringForKey:@"lot"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row1 stringForKey:@"ItemID"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[row1 stringForKey:@"Qty"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:expStr];
            NSString *stringReturns =[row1 stringForKey:@"RMA_Lot_Type"];
            temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:stringReturns];
            
            [self executeNonQuery:temp];
            
            expformatter=nil;
            expLocale=nil;
            //   }
            //}
        }
    }
    
    [self saveOrderHistoryWithCustomerInfo:customerInfo andOrderInfo:orderInfo andFSRInfo:userInfo andVisitID:visitID andOrderID:orderID andOrderRef:lastPerformaOrderReference];
    formatter=nil;
    usLocale=nil;
    formatterTime=nil;
    usLocale1=nil;
    orderArray=nil;
    infoOrder=nil;
    extraInfo=nil;
    dict=nil;
    
    return lastPerformaOrderReference;
}
- (void)saveDocAdditionalInfoWithDoc:(NSString *)docNumber andDocType:(NSString *)docType andAttName:(NSString *)attName andAttValue:(NSString *)attVale andCust_Att_5:(NSString *)cust_Att_5{
    NSString *temp = @"INSERT INTO TBL_Doc_Addl_Info (Row_ID, Doc_No, Doc_Type, Attrib_Name, Attrib_Value,Custom_Attribute_5) VALUES ('{0}', '{1}', '{2}', '{3}','{4}','{5}')";
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:docNumber];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:docType];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:attName];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:attVale];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:cust_Att_5];
    
    
    [self executeNonQuery:temp];
}
///////////////Send Orders
#define kSQLdbGetConfirmOrderS @"Select * from TBL_Order"

#define kSQLdbGetConfirmOrderItems @"SELECT * FROM TBL_Order_Line_Items  ORDER BY Orig_Sys_Document_Ref ASC"

#define kSQLdbGetConfirmOrderLots @"SELECT * FROM TBL_Order_Lots  ORDER BY Orig_Sys_Document_Ref ASC"

- (void)dbGetConfirmOrder
{
    
    //    mainDictionary = [NSMutableDictionary dictionary];
    //    mainArray=[NSMutableArray array];
    //   // orderArray1 = [NSMutableArray array];
    //    lotArray1 = [NSMutableArray array];
    //    itemArray = [NSMutableArray array];
    
    
    NSArray *temp = [self fetchDataForQuery:kSQLdbGetConfirmOrderS];
    
    if (orderArray1)
    {
        orderArray1=nil;
    }
    orderArray1 = [NSMutableArray arrayWithArray:temp];
    if([orderArray1 count]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please take order first." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        //        [ILAlertView showWithTitle:@"Error"
        //                           message:@"Please take order first."
        //                  closeButtonTitle:@"OK"
        //                 secondButtonTitle:nil
        //               tappedButtonAtIndex:nil];
        
        [[DataSyncManager sharedManager]hideCustomIndicator];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
        
    }
    else
    {
        [self dbGetConfirmOrderItems];
    }
}
- (void)dbGetConfirmOrderItems
{
    //2
    
    NSArray *temp = [self fetchDataForQuery:kSQLdbGetConfirmOrderItems];
    if (itemArray)
    {
        itemArray=nil;
    }
    itemArray = [NSMutableArray arrayWithArray:temp];
    
    [self dbGetConfirmOrderLots];
}
- (void)dbGetConfirmOrderLots
{
    //3
    
    NSArray *temp = [self fetchDataForQuery:kSQLdbGetConfirmOrderLots];
    if (lotArray1) {
        lotArray1=nil;
    }
    lotArray1 = [NSMutableArray arrayWithArray:temp];
    [self writeXMLString];
}

#define kSQLDeletePerformaOrder1 @"DELETE FROM TBL_Order "
#define kSQLDeletePerformaOrderItems1 @"DELETE FROM TBL_Order_Line_Items"
#define kSQLDeletePerformaOrderLots1 @"DELETE FROM TBL_Order_Lots"

- (void)deleteSalesOrder
{
    NSString *temp = kSQLDeletePerformaOrder1;
    
    [self executeNonQuery:temp];
    
    NSString *temp1 = kSQLDeletePerformaOrderItems1;
    
    [self executeNonQuery:temp1];
    
    NSString *temp2 = kSQLDeletePerformaOrderLots1;
    
    [self executeNonQuery:temp2];
}


-(void)writeXMLString{
    //    if (xmlWriter) {
    //        xmlWriter=nil;
    //    }
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    
    if(![orderArray1 count]==0)
    {
        [xmlWriter writeStartElement:@"Orders"];
        
        for (int o=0; o<[orderArray1 count]; o++)
        {
            if (orderDictionary) {
                orderDictionary=nil;
            }
            orderDictionary = [NSMutableDictionary dictionaryWithDictionary:[orderArray1 objectAtIndex:o]];
            NSString *orderRef = [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"];
            
            [xmlWriter writeStartElement:@"Order"];
            
            if (![[orderDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Creation_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Creation_Date"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Creation_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"System_Order_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"System_Order_Date"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"System_Order_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Shipping_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Shipping_Instructions"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Shipping_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Packing_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Packing_Instructions"].length !=0 && [orderDictionary stringForKey:@"Packing_Instructions"].length !=0 ){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Packing_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Customer_PO_Number"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Customer_PO_Number"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Customer_PO_Number"]];
                [xmlWriter writeEndElement];}
            if (![[orderDictionary stringForKey:@"Request_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Request_Date"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Request_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Schedule_Ship_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Schedule_Ship_Date"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Schedule_Ship_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Order_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Order_Status"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Order_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Start_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Start_Time"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Start_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"End_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"End_Time"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"End_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Signee_Name"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Signee_Name"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Signee_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Last_Update_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Last_Update_Date"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Last_Update_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Discount_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Discount_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Discount_Amt"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Print_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Print_Status"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Print_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Emp_Code"].length !=0){
                [xmlWriter writeStartElement:@"C27"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Order_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Order_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C28"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Order_Amt"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C29"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C30"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C31"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C32"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C33"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C34"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"LineItems"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    if (itemDictionary) {
                        itemDictionary=nil;
                    }
                    itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    NSString *itemLine = [itemDictionary stringForKey:@"Line_Number"];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]])
                    {
                        [xmlWriter writeStartElement:@"LineItem"];
                        if (![[itemDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Row_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Row_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Line_Number"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Line_Number"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Line_Number"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Order_Quantity_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Order_Quantity_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Order_Quantity_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Display_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Display_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Display_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Ordered_Quantity"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Ordered_Quantity"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Ordered_Quantity"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Inventory_Item_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Inventory_Item_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Inventory_Item_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Calc_Price_Flag"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Calc_Price_Flag"].length !=0){
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Calc_Price_Flag"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Unit_Selling_Price"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Unit_Selling_Price"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C9"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Unit_Selling_Price"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Def_Bonus"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Def_Bonus"].length !=0){
                            //NSLog(@"Def_Bonus");
                            [xmlWriter writeStartElement:@"C10"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Def_Bonus"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                            [xmlWriter writeStartElement:@"C11"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_1"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                            [xmlWriter writeStartElement:@"C12"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_2"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                            [xmlWriter writeStartElement:@"C13"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_3"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Assortment_Plan_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Assortment_Plan_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C14"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Assortment_Plan_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Amount"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Amount"].length !=0){
                            [xmlWriter writeStartElement:@"C15"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Amount"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Percentage"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Percentage"].length !=0){
                            [xmlWriter writeStartElement:@"C16"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Percentage"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Level"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Level"].length !=0){
                            [xmlWriter writeStartElement:@"C17"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Level"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"VAT_Code"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"VAT_Code"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C18"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"VAT_Code"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                            [xmlWriter writeStartElement:@"C19"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_4"]];
                            [xmlWriter writeEndElement];
                        }
                        
                        if([lotArray1 count]!=0)
                        {
                            [xmlWriter writeStartElement:@"Lots"];
                            for (int l=0 ; l<[lotArray1 count] ; l++)
                            {
                                if (lotDictionary) {
                                    lotDictionary=nil;
                                }
                                lotDictionary = [NSMutableDictionary dictionaryWithDictionary:[lotArray1 objectAtIndex:l]];
                                if([orderRef isEqualToString:[lotDictionary stringForKey:@"Orig_Sys_Document_Ref"]] && [itemLine isEqualToString:[lotDictionary stringForKey:@"Line_Number"]]){
                                    [xmlWriter writeStartElement:@"Lot"];
                                    
                                    [xmlWriter writeStartElement:@"C1"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Row_ID"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C2"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C3"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Line_Number"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C4"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Lot_Number"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C5"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Ordered_Quantity"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C6"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Is_FIFO"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C7"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Org_ID"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeEndElement];//lot
                                }
                            }
                            [xmlWriter writeEndElement];//lots
                        }
                        [xmlWriter writeEndElement];//lineItem
                    }
                }
                [xmlWriter writeEndElement];//lineItems
            }
            
            [xmlWriter writeEndElement];//Orders
            
        }
        [xmlWriter writeEndElement];//Order
    }
    Singleton *single = [Singleton retrieveSingleton];
    single.xmlParsedString = [xmlWriter toString];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:@"done-01"];
#pragma clang diagnostic pop
    });
    
}


-(NSString*)getcodeforkeys:(NSString*)str_code
{
    str_code = [str_code stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    str_code = [str_code stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];
    str_code = [str_code stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
    str_code = [str_code stringByReplacingOccurrencesOfString:@"'" withString:@"&apos;"];
    str_code = [str_code stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];
    return str_code;
}

///////////Collection///////////////
/*- (NSString *)saveCollection:(NSDictionary *)collectionInfo withCustomerInfo:(NSDictionary *)customerInfo andInvoices:(NSArray *)invoices {
    // Create CollectionId
    NSString *collectionId = [NSString createGuid];
    
    // setup date formatter
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    // Collection date and time
    NSDate *dateOfCollection = [NSDate date];
    
    // Make new Last Collection Reference and save it to NSDefaults
    NSString *lastCollectionReference = [SWDefaults lastCollectionReference];
    NSString *nUserId = [[SWDefaults userProfile] stringForKey:@"Emp_Code"];
    
    unsigned long long lastRefId = [[lastCollectionReference substringFromIndex: [lastCollectionReference length] - 10] longLongValue];
    lastRefId = lastRefId + 1;
    if(nUserId.length == 1)
    {
        nUserId = [NSString stringWithFormat:@"00%@",nUserId];
    }
    else if(nUserId.length == 2)
    {
        nUserId = [NSString stringWithFormat:@"0%@",nUserId];
    }
    
    lastCollectionReference = [NSString stringWithFormat:@"M%@C%010lld", nUserId, lastRefId];
    if (![lastCollectionReference isEqualToString:[SWDefaults lastCollectionReference]])
    {
        [SWDefaults setLastCollectionReference:lastCollectionReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastCollectionReference = [NSString stringWithFormat:@"M%@C%010lld", nUserId, lastRefId];
        if (![lastCollectionReference isEqualToString:[SWDefaults lastCollectionReference]])
        {
            [SWDefaults setLastCollectionReference:lastCollectionReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastCollectionReference = [NSString stringWithFormat:@"M%@C%010lld", nUserId, lastRefId];
            [SWDefaults setLastCollectionReference:lastCollectionReference];
        }
    }
    //[SWDefaults setLastCollectionReference:lastCollectionReference];
    
    // setup collection type
    NSString *collectionType = @"CASH";
    
    // check date
    NSString *collectionChqDate = @"";
    
    if ([[collectionInfo objectForKey:@"CollectionType"] isEqual:@"PDC"]) {
//        collectionType = @"POST-CHQ";
        collectionType = @"PDC";

        NSDate *tempDate = [collectionInfo objectForKey:@"ChqDate"];
        collectionChqDate = [dateFormatter stringFromDate:tempDate];
    } else if ([[collectionInfo objectForKey:@"CollectionType"] isEqual:@"CURRENT CHEQUE"]) {
        collectionType = @"CURR-CHQ";
        NSDate *tempDate = [collectionInfo objectForKey:@"ChqDate"];
        collectionChqDate = [dateFormatter stringFromDate:tempDate];
    }
    
    // Construct SQL Statement
    NSString *temp = kSQLCollectionInsertInfo;
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:collectionId];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastCollectionReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[dateFormatter stringFromDate:dateOfCollection]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:collectionType];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[customerInfo stringForKey:@"Site_Use_ID"]];
    if ([[AppControl retrieveSingleton].ALLOW_MULTI_CURRENCY isEqualToString:@"Y"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[collectionInfo stringForKey:@"selectedAmount"]];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[collectionInfo stringForKey:@"Amount"]];
    }
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[collectionInfo stringForKey:@"ChqNumber"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:collectionChqDate];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[collectionInfo stringForKey:@"Bank"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:[collectionInfo stringForKey:@"Branch"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:[[SWDefaults userProfile] stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:[dateFormatter stringFromDate:dateOfCollection]];
    
    
    [self executeNonQuery:temp];
    
    for (int i = 0; i < invoices.count; i++) {
        @autoreleasepool {
            NSDictionary *row = [invoices objectAtIndex:i];
            
            NSString *sql = kSQLCollectionInsertInvoice;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:collectionId];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:[row stringForKey:@"Invoice_Ref_No"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[row stringForKey:@"InvDate"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Paid"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:@"N"];
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[dateFormatter stringFromDate:dateOfCollection]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[NSString createGuid]];
            
            [self executeNonQuery:sql];
        }
    }
    
    dateFormatter=nil;
    usLocale=nil;
    return lastCollectionReference;
}
*/
//////////////////////////Route//////////////
#pragma  Route Service
- (NSArray *)dbGetCollectionFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    
    
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:kSQLRoute, [formatter stringFromDate:date]]];
    
    
    
    
    NSString* strDate=[formatter stringFromDate:date];
    
    NSLog(@"test string %@", strDate);
    formatter=nil;
    usLocale=nil;
    return temp;
}

#pragma  Visit Service

#define kSQLOpenVisit @"SELECT A.Actual_Visit_ID,A.Customer_ID,A.Visit_Start_Date,A.Visit_End_Date FROM  TBL_FSR_Actual_Visits AS A  WHERE  A.Customer_ID = '%@' AND A.Site_Use_ID = '%@' AND A.Visit_End_Date =''"

- (NSArray *)dbGetOpenVisiteFromCustomerID:(NSString *)customer_ID andSiteID:(NSString *)siteID
{
    
    NSLog(@"actual visits qry %@",[NSString stringWithFormat:kSQLOpenVisit,customer_ID,siteID] );
    
    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:kSQLOpenVisit,customer_ID,siteID]];
    
    return temp;
}

#define kSQLSaveNewVisit @"INSERT OR REPLACE INTO TBL_FSR_Actual_Visits (Actual_Visit_ID,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Latitude,Longitude,Visit_End_Date ,Cash_Customer_ID , Cash_Site_ID ,CC_Name ,CC_TelNo,Visit_Start_Date,FSR_Plan_Detail_ID,Custom_Attribute_1,Custom_Attribute_2,Custom_Attribute_3) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}' ,'{8}', '{9}', '{10}' , '{11}','{12}','{14}','{15}','{16}','{17}')"


#define kSQLSaveNewVisitwithVisitType @"INSERT OR REPLACE INTO TBL_FSR_Actual_Visits (Actual_Visit_ID,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Latitude,Longitude,Visit_End_Date ,Cash_Customer_ID , Cash_Site_ID ,CC_Name ,CC_TelNo,Visit_Start_Date,FSR_Plan_Detail_ID,Custom_Attribute_1,Custom_Attribute_2,Custom_Attribute_3,Custom_Attribute_4,Custom_Attribute_5) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}' ,'{8}', '{9}', '{10}' , '{11}','{12}','{14}','{15}','{16}','{17}','{18}','{19}')"



-(BOOL)saveVisitContactDetails:(SalesWorxVisit*)currentVisit
{
    
    BOOL status=NO;
    FMDatabase *database;
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        documentDir = [filePath path] ;
    }
    
    database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];

    NSString* rowID=[NSString createGuid];
    NSString* attributeName=@"CUST_CONTACT";
    NSString * attributeValue=[[[SWDefaults getValidStringValue:currentVisit.visitOptions.contact.contactName] stringByAppendingString:@","] stringByAppendingString:[NSString getValidStringValue:currentVisit.visitOptions.contact.contactNumber]];
    NSString *createdAt =  [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];
    NSString *customerID=[NSString getValidStringValue:currentVisit.visitOptions.customer.Customer_ID];

    NSString *siteUseID=[NSString getValidStringValue:currentVisit.visitOptions.customer.Ship_Site_Use_ID];

    [database open];
       status =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Custom_Attribute_1,Custom_Attribute_2,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?,?)",rowID,currentVisit.Visit_ID,attributeName,attributeValue,customerID,siteUseID,createdAt,createdBy, nil];
    if (status) {
        [database commit];
        NSLog(@"visit additinal info updated");
    }
    
    [database close];
    return status;
}
-(BOOL)saveVisitDetails:(SalesWorxVisit*)curentVisit
{
    BOOL status;
    
    BOOL isOnsite;
    
    SWAppDelegate* appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    latitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude];
    longitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude];
    if (latitude==nil) {
        latitude=@"0.0";
        longitude=@"0.0";
    }
    AppControl *appCtrl=[AppControl retrieveSingleton];
    NSString *temp,*planDetailID;
    FMDatabase *database;
    NSString *documentDir;

    if ([appCtrl.ENABLE_VISIT_TYPE isEqualToString:@"Y"]) {
        temp=  kSQLSaveNewVisitwithVisitType;
        
         isOnsite=[SWDefaults fetchOnsiteVisitStatus];

        NSLog(@"accompanied by data %@",curentVisit.Accompanied_By_User_ID);
        
        NSString * accompaniedByUserID=[SWDefaults getValidStringValue:curentVisit.Accompanied_By_User_ID];
        

        NSLog(@"accompanied by user id is %@", accompaniedByUserID);
        
        
        if ([curentVisit.Visit_Type isEqualToString:@"Onsite"] && [NSString isEmpty:accompaniedByUserID]==NO && [[[AppControl retrieveSingleton] ENABLE_FS_ACCOMPANIED_BY] isEqualToString:KAppControlsYESCode]) {
            
        if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            documentDir = [paths objectAtIndex:0];
            
        }
        else
        {
            NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
            documentDir = [filePath path] ;
        }
        
        
        
        
        database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
        

        NSString* rowID=[NSString createGuid];
        NSString* attributeName=@"FS_Accompanied_By";
        NSString * attributeValue=[SWDefaults getValidStringValue:curentVisit.Accompanied_By_User_ID];
        NSDateFormatter *formatterTime = [NSDateFormatter new] ;
        NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatterTime setLocale:usLocaleq];
        NSString *createdAt =  [formatterTime stringFromDate:[NSDate date]];
        NSString* createdBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];

        [database open];
     BOOL   insertIntoVisitAdditionalInfo =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,curentVisit.Visit_ID,attributeName,attributeValue,createdAt,createdBy, nil];
        
        
        if (insertIntoVisitAdditionalInfo) {
            [database commit];

            NSLog(@"visit additinal info updated");
        }

        [database close];

        }
        
        if (isOnsite==YES && curentVisit.isFSRinCustomerLocation==NO && [NSString isEmpty:curentVisit.onSiteExceptionReason]==NO) {
            temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:curentVisit.onSiteExceptionReason];
        }
        else{
            temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:@""];
        }
        
        
        
        //inserting beacon data
        
        if (isOnsite==YES && curentVisit.beaconValidationSuccessfull==YES && [[AppControl retrieveSingleton].VALIDATE_CUST_IBEACON isEqualToString:KAppControlsYESCode]) {
            
      
        NSString *beaconDataCreatedAt =  [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        NSString* beaconDataCreatedBy=[[SWDefaults userProfile] stringForKey:@"User_ID"];

        NSString * beaconAttribName= @"iBeacon";
        NSString * beaconAttributeValue = [SWDefaults getValidStringValue:curentVisit.detectedBeacon.Beacon_UUID];
        
           FMDatabase *fmdatabase = [FMDatabase databaseWithPath:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];

            [fmdatabase open];
        BOOL   insertBeaconDataIntoVisitAdditionalInfo =  [fmdatabase executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Custom_Attribute_1,Custom_Attribute_2,Created_At,Created_By)  VALUES (?,?,?,?,?,?,?,?)",[NSString createGuid],curentVisit.Visit_ID,beaconAttribName,beaconAttributeValue,[SWDefaults getValidStringValue:curentVisit.detectedBeacon.Beacon_Major],[SWDefaults getValidStringValue:curentVisit.detectedBeacon.Beacon_Minor],beaconDataCreatedAt,beaconDataCreatedBy, nil];
        
            if (insertBeaconDataIntoVisitAdditionalInfo==YES) {
                
                [fmdatabase commit];
            }
            
            [fmdatabase close];

            NSLog(@"inserted beacon data successful %hhd",insertBeaconDataIntoVisitAdditionalInfo);
            
        }
        

        
        
        
        
    }
    else{
        temp=kSQLSaveNewVisit;
    }
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    NSDictionary *FSR = [NSMutableDictionary dictionaryWithDictionary:[SWDefaults userProfile]  ];
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:curentVisit.Visit_ID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:curentVisit.visitOptions.customer.Ship_Customer_ID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:curentVisit.visitOptions.customer.Ship_Site_Use_ID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[FSR stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[FSR stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:latitude];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:longitude];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:dateString];
  //  temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:dateString]; /** odoreading commented */
    if (!curentVisit.FSR_Plan_Detail_ID) {
        planDetailID=@"0";
    }
    else{
        planDetailID=[NSString stringWithFormat:@"%@",curentVisit.FSR_Plan_Detail_ID];
    }
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:planDetailID];
    
    
    if ([curentVisit.visitOptions.customer.Cash_Cust isEqualToString:@"Y"]) {
        
        temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:@"0"];
        
        //only name and phone are mandatory, they may be chance of no data in contact, address and fax
        
        temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:curentVisit.visitOptions.customer.cash_Name];
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:curentVisit.visitOptions.customer.cash_Phone];
        
        if ([NSString isEmpty:curentVisit.visitOptions.customer.cash_Contact ]==NO) {
           temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:curentVisit.visitOptions.customer.cash_Contact];
        }
        else
        {
            temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
        }
        
        if ([NSString isEmpty:curentVisit.visitOptions.customer.cash_Address]==NO) {
           temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:curentVisit.visitOptions.customer.cash_Address];
        }
        else{
            temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@""];
        }
        
        if ([NSString isEmpty:curentVisit.visitOptions.customer.cash_Fax]==NO) {
            temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:curentVisit.visitOptions.customer.cash_Fax];
        }
        else{
            temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:@""];
        }
        
        
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:@""];
    }
    NSString* isMerchandisingVisit = [NSString getValidStringValue:curentVisit.Visit_Type];
    
    NSString* visitTypeString = [SWDefaults getValidStringValue:curentVisit.visitOptions.customer.Visit_Type];

    if ([visitTypeString isEqualToString:kTelephonicVisitDatabaseTitle] || [visitTypeString isEqualToString:kTelephonicVisitTitle]) {
        visitTypeString=kTelephonicVisitDatabaseTitle;
    }else{
        visitTypeString=kOnsiteVisitDatabaseTitle;
    }
    
    NSString* visitType = visitTypeString;
    
    if ([appCtrl.ENABLE_VISIT_TYPE isEqualToString:@"Y"]) {
        if (isOnsite==YES) {
            temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:visitType];
        }
        else if ([isMerchandisingVisit isEqualToString:kMerchandisingVisitType])
        {
            temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:@"MERCHANDISING"];
        }
        else
        {
            temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:visitType];
        }
    }
    
    
    NSLog(@"query being executed to save visit is %@", temp);
    @try {
        status=[self executeNonQueryOne:temp];
    }
    @catch (NSException *exception) {
        NSLog(@"failed to save visit info %@", exception.debugDescription);
    }
    @finally {
        
    }
    NSLog(@"status of save visit %hhd", status);
    return status;
}

- (void)saveVistWithCustomerInfo:(NSMutableDictionary *)customerInfo andVisitID:(NSString *)visitID
{
    
    // [self currentLocation];
    
    NSLog(@"visit id in save visit %@", visitID);
    
    NSLog(@"save visit with customer info called");
    
    
    NSLog(@"customer info in save visit %@", customerInfo);
    
    NSLog(@"lat while saving visit info is %@", latitude);
    NSLog(@"long while saving visit info is %@", longitude);
    
    
    
    SWAppDelegate * appDel =(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    latitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude];
    longitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude];
    
    if (latitude==nil) {
        latitude=@"0.0";
        longitude=@"0.0";
    }

    
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    Singleton *single = [Singleton retrieveSingleton];
    
    NSDictionary *tempCust = [NSMutableDictionary dictionaryWithDictionary:customerInfo];
    NSDictionary *FSR = [NSMutableDictionary dictionaryWithDictionary:[SWDefaults userProfile]  ];
    
    NSString *temp;
    
    AppControl *appCtrl=[AppControl retrieveSingleton];
    if ([appCtrl.ENABLE_VISIT_TYPE isEqualToString:@"Y"]) {
        
        temp=  kSQLSaveNewVisitwithVisitType;
    }
    else{
        
        temp=kSQLSaveNewVisit;
    }
    
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:[tempCust stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[tempCust stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[FSR stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[FSR stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:latitude];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:longitude];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:dateString];
   // temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:dateString]; /* ODO_reading commented*/
    
    if (!single.planDetailId) {
        single.planDetailId=@"0";
    }
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:single.planDetailId];
    
    

    
    NSLog(@"visit coordinates while saving %@", temp);
    
    //saving the order type, onsite/telephone in shipping instructions of tbl_order
    
    
  
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary];
    NSLog(@"single ton dict is %@", dict);
    
    formatter=nil;
    usLocale=nil;
    
    if([single.isCashCustomer isEqualToString:@"Y"] )
    {
        if([dict objectForKey:@"cash_Name"])
        {
            temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:@""];
            temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:@""];
            temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[dict stringForKey:@"cash_Name"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:[dict stringForKey:@"cash_Phone"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:[dict stringForKey:@"cash_Contact"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:[dict stringForKey:@"cash_Address"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:[dict stringForKey:@"cash_Fax"]];
            
        }
        else
        {
            temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[dict stringForKey:@"Customer_ID"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:[dict stringForKey:@"Site_Use_ID"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:@""];
            temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@""];
            temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
            temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@""];
            temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:@""];
        }
        
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:@""];
    }
    
    
    
    //the onsite or telephonic should be based on app control flag.

    if ([appCtrl.ENABLE_VISIT_TYPE isEqualToString:@"Y"]) {
        
        BOOL isOnsite=[SWDefaults fetchOnsiteVisitStatus];
        
        NSLog(@"ON SITE VISIT STATUS IN FSR ACTUAL VISITS %hhd", isOnsite);
        
        if (isOnsite==YES) {
            temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:kOnsiteVisitTitle];
            
        }
        else
        {
            temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:kTelephonicVisitTitle];
            
        }

    }
    else
    {
        
    }
    
    
    NSLog(@"temp in fsr actual visits %@", temp);

    
    [self executeNonQuery:temp];
    tempCust=nil;
    FSR=nil;
    dict=nil;
}
#define kSQLUpdateEndDate @"UPDATE TBL_FSR_Actual_Visits SET Visit_End_Date='{0}' WHERE Actual_Visit_ID='{1}'; "

- (void)saveVisitEndDateWithVisiteID:(NSString *)visitID
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    NSString *temp = kSQLUpdateEndDate;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:visitID];
    
    [self executeNonQuery:temp];
    formatter=nil;
    usLocale=nil;
}

#define kSQLUpdatePlannedVisitStatus @"UPDATE TBL_FSR_Planned_Visits SET Visit_Status='{0}' WHERE Planned_Visit_ID='{1}'; "

- (void)saveVisitStatusWithVisiteID:(NSString *)visitID
{
    NSString *temp = kSQLUpdatePlannedVisitStatus;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:@"Y"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:visitID];
    
    [self executeNonQuery:temp];
}


-(NSString*)fetchuserNamefromDB
{
    
    NSMutableArray*    tblUserArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Username from tbl_user"];
    if (tblUserArray.count>0) {
        
        return [[tblUserArray objectAtIndex:0] valueForKey:@"Username"];
    }
    else
    {
        return nil;
    }
    

}

#define kSQLDuesStatics @"SELECT Customer_ID, M0_Due ,M1_Due ,M2_Due ,M3_Due ,M4_Due ,Prior_Due ,Total_Due ,PDC_Due FROM TBL_Customer_Dues WHERE Customer_ID = '%@'"

-(NSArray *)dbGetDuesStaticWithCustomerID:(NSString *)customerID
{
    
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:kSQLDuesStatics,customerID]];
    return  temp;
}

- (void)currentLocation
{
    for (int i= 0; i <=3; i++) {
        //NSLog(@"Location captureing ...ellloooo!!!!");
        
        locationManager = [[CLLocationManager alloc] init];
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        //locationManager.delegate=self;
        
        [locationManager startUpdatingLocation];
        CLLocation *location = [locationManager location];
        // Configure the new event with information from the location
        CLLocationCoordinate2D coordinate = [location coordinate];
        latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
        //locationManager=nil;
        [locationManager stopUpdatingLocation];
        if ([latitude isEqualToString:@"0"] || [longitude isEqualToString:@"0"]) {
        }
        else
        {
            break;
        }
    }
    //  Latitude = "25.304524";
    //    Longitude = "55.373513";
    //    NSLog(@"latitude %@",latitude);
    //    NSLog(@"longitude %@",longitude);
}

//-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//
//{
//    [manager stopUpdatingLocation];
//    NSString *lati = [[NSString alloc] initWithFormat:@"%+.6f", newLocation.coordinate.latitude];
//    NSLog(@"address:%@",lati);
//
//    NSString *longi = [[NSString alloc] initWithFormat:@"%+.6f", newLocation.coordinate.longitude];
//    NSLog(@"address:%@",longi);
//
//    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
//
//    [geoCoder reverseGeocodeLocation: newLocation completionHandler: ^(NSArray *placemarks, NSError *error)
//     {
//         //Get nearby address
//         CLPlacemark *placemark = [placemarks objectAtIndex:0];
//
//         //String to hold address
//         NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//
//         //Print the location to console
//         NSLog(@"I am currently at %@",locatedAt);
//
//         NSString *address = [[NSString alloc]initWithString:locatedAt];
//         NSLog(@"address:%@",address);
//     }];
//
//}
-(void)saveConfirmedOrder : (NSMutableDictionary *)order
{
    //kSQLSaveOrder
    //NSLog(@"Orer %@",order);x
    
    
    NSString *orderID = [NSString createGuid];
    NSString *lastOrderReference = [SWDefaults lastOrderReference];
    NSString *nUserId = [order stringForKey:@"Emp_Code"];
    unsigned long long lastRefId = [[lastOrderReference substringFromIndex: [lastOrderReference length] - 10] longLongValue];
    lastRefId = lastRefId + 1;
    if(nUserId.length == 1)
    {
        nUserId = [NSString stringWithFormat:@"00%@",nUserId];
    }
    else if(nUserId.length == 2)
    {
        nUserId = [NSString stringWithFormat:@"0%@",nUserId];
    }
    lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
    
    if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
    {
        [SWDefaults setLastOrderReference:lastOrderReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
        if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
        {
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastOrderReference = [NSString stringWithFormat:@"M%@S%010lld", nUserId, lastRefId];
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
    }
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *startTime =  [formatterTime stringFromDate:[NSDate date]];
    
    
    NSString *temp =@"INSERT INTO TBL_Order ( Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, Packing_Instructions, Request_Date, Schedule_Ship_Date, Order_Status, Start_Time, End_Time, Signee_Name, Custom_Attribute_1, Emp_Code, Order_Amt, Visit_ID , System_Order_Date,Credit_Customer_ID,Credit_Site_ID,Last_Update_Date ,Last_Updated_By,Custom_Attribute_2,Custom_Attribute_3,Discount_Amt,Customer_PO_Number,Shipping_Instructions) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}', '{10}', '{11}' , '{12}', '{13}', '{15}', '{16}' , '{17}', '{18}' , '{19}','{20}','{21}','{22}' ,'{23}','{24}','{25}' ,'{26}','{27}','{28}','{29}')";
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}"  withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}"  withString:lastOrderReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}"  withString:[order stringForKey:@"Creation_Date"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}"  withString:[order stringForKey:@"Created_By"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}"  withString:[order stringForKey:@"Ship_To_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}"  withString:[order stringForKey:@"Ship_To_Site_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}"  withString:[order stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}"  withString:[order stringForKey:@"Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}"  withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}"  withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:[order stringForKey:@"Start_Time"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@"[SIGN:N]"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:[order stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:[order stringForKey:@"Order_Amt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:[order stringForKey:@"Visit_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:[order stringForKey:@"Credit_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{22}" withString:[order stringForKey:@"Credit_Site_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{23}" withString:[order stringForKey:@"Last_Update_Date"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{24}" withString:[order stringForKey:@"Last_Updated_By"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{25}" withString:[order stringForKey:@"Custom_Attribute_2"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{26}" withString:[order stringForKey:@"Custom_Attribute_3"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{27}" withString:@"0.0"]; // discount amt
    temp = [temp stringByReplacingOccurrencesOfString:@"{28}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{29}" withString:[order stringForKey:@"Custom_Attribute_1"]];
    

    [self executeNonQuery:temp];
    
    //#define kSQLSaveOrderHistory Row_ID, Orig_Sys_Document_Ref, Doc_Type, Creation_Date  , Created_By, System_Order_Date, Request_Date, Schedule_Ship_Date, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, BO_Status, Start_Time, End_Time, Packing_Instructions, Emp_Code,ERP_Status, Transaction_Amt,Due_Date,Credit_Customer_ID,Credit_Site_ID, Visit_ID,Sync_Timestamp
    NSString *dateTimeStr=@" 00:00:00";
   NSString* dateStringWithTime=[dateString stringByAppendingString:dateTimeStr];
    NSString *tempHistory = [[AppControl retrieveSingleton].ENABLE_REORDER_OPTION isEqualToString:KAppControlsYESCode]?kSQLNewSaveUnconfirmedOrderHistory:kSQLSaveOrderHistory;
    NSCalendar *gregCalendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    
    NSDateComponents *components=[gregCalendar components:NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger month=[components month];
    NSInteger year=[components year];
    
    if (month==12) {
        [components setYear:year+1];
        [components setMonth:1];
    }
    else {
        [components setMonth:month+1];
    }
    [components setDay:1];
    
    
    NSDate *lastDateofMonth= [[gregCalendar dateFromComponents:components] dateByAddingTimeInterval:0];
    NSString *lastDateMonthString =  [formatter stringFromDate:lastDateofMonth];
    
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{2}" withString:@"I"];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{3}" withString:dateStringWithTime];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{4}" withString:[order stringForKey:@"Created_By"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{5}" withString:dateStringWithTime];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{6}" withString:dateStringWithTime];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{7}" withString:dateString];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{8}" withString:[order stringForKey:@"Ship_To_Customer_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{9}" withString:[order stringForKey:@"Ship_To_Site_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{10}" withString:[order stringForKey:@"Customer_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{11}" withString:[order stringForKey:@"Site_Use_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{12}" withString:@"N"];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{13}" withString:[order stringForKey:@"Start_Time"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{14}" withString:startTime];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{16}" withString:[order stringForKey:@"Emp_Code"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{17}" withString:@"N"];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{18}" withString:[order stringForKey:@"Order_Amt"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{19}" withString:lastDateMonthString];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{20}" withString:[order stringForKey:@"Credit_Customer_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{21}" withString:[order stringForKey:@"Credit_Site_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{22}" withString:[order stringForKey:@"Visit_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{23}" withString:dateString];
    
    
    if([[AppControl retrieveSingleton].ENABLE_REORDER_OPTION isEqualToString:KAppControlsYESCode])
    {
        tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{24}" withString:[order stringForKey:@"Custom_Attribute_2"]];
        tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{25}" withString:[order stringForKey:@"Custom_Attribute_3"]];
        tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{26}" withString:[order stringForKey:@"Custom_Attribute_1"]];

    }
    
    [self executeNonQuery:tempHistory];
    
    
    
    
    [self saveConfirmedOrderLineItems:[order stringForKey:@"Orig_Sys_Document_Ref"] andConfirmedOrderRef:lastOrderReference OrderType:[order stringForKey:@"Custom_Attribute_1"]];
    
}

-(void)saveConfirmedOrderLineItems : (NSString *)orderRef andConfirmedOrderRef:(NSString *)cOrderRef OrderType:(NSString *)orderType
{
    NSMutableArray *tempArray =   [[self fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Proforma_Order_Line_Items where Orig_Sys_Document_Ref='%@'",orderRef]] mutableCopy];
    
    int line;
    
    for (int i = 0; i < tempArray.count; i++)
    {
        @autoreleasepool{
            NSDictionary *row = [tempArray objectAtIndex:i];
            NSString *sql = kSQLSaveOrderLineItem;
            line=i+1;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:cOrderRef];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Order_Quantity_UOM"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Display_UOM"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Ordered_Quantity"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Inventory_Item_ID"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Unit_Selling_Price"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Def_Bonus"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"Calc_Price_Flag"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"Custom_Attribute_3"]];
            if([[row objectForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
            {
                BonusCA1GUID=[NSString createGuid];
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:BonusCA1GUID];
            }
            else if([[row objectForKey:@"Calc_Price_Flag"] isEqualToString:@"F"] || [[row objectForKey:@"Calc_Price_Flag"] isEqualToString:@"M"])
            {
                if([orderType isEqualToString:@"F"])
                {
                    BonusCA1GUID=[NSString createGuid];
                }
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:BonusCA1GUID];
            }
            
            if ([row valueForKey:@"lineItemNotes"]==nil) {
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{12}" withString:@""];
                
                
            }
            
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{12}" withString:[row valueForKey:@"lineItemNotes"]];
            }
            
            
            [self executeNonQuery:sql];
            
            
            //Order HISTORY
            NSString *sql1 = [[AppControl retrieveSingleton].ENABLE_REORDER_OPTION isEqualToString:KAppControlsYESCode]?kSQLNewSaveUnconfirmedHistoryLineItem:kSQLSaveOrderHistoryLineItem;

            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{1}" withString:cOrderRef];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Inventory_Item_ID"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Order_Quantity_UOM"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Ordered_Quantity"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Calc_Price_Flag"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Unit_Selling_Price"]];
            double iTemValue = [[row stringForKey:@"Ordered_Quantity"] doubleValue] * [[row stringForKey:@"Unit_Selling_Price"] doubleValue];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{8}" withString:[NSString stringWithFormat:@"%.2f",iTemValue]];
            //NSLog(@"Query %@",sql1);
            
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@""]];

            if([[AppControl retrieveSingleton].ENABLE_REORDER_OPTION isEqualToString:KAppControlsYESCode])
            {
                sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{11}" withString:[row stringForKey:@"Custom_Attribute_1"]];
                sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{12}" withString:[row stringForKey:@"Custom_Attribute_2"]];
                sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{13}" withString:[row stringForKey:@"Custom_Attribute_3"]];

                
            }
            
            [self executeNonQuery:sql1];
            
        }
    }
    
    [self saveConfirmedOrderLots:orderRef andConfirmedOrderRef:cOrderRef];
    
}

-(void)saveConfirmedOrderLineItems : (NSString *)orderRef andConfirmedOrderRef:(NSString *)cOrderRef
{
    NSMutableArray *tempArray =   [[self fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Proforma_Order_Line_Items where Orig_Sys_Document_Ref='%@'",orderRef]] mutableCopy];
    
    int line;
    
    for (int i = 0; i < tempArray.count; i++)
    {
        @autoreleasepool{
            NSDictionary *row = [tempArray objectAtIndex:i];
            NSString *sql = kSQLSaveOrderLineItem;
            line=i+1;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:cOrderRef];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Order_Quantity_UOM"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Display_UOM"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Ordered_Quantity"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Inventory_Item_ID"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Unit_Selling_Price"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Def_Bonus"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"Calc_Price_Flag"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"Custom_Attribute_3"]];
            if([[row objectForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
            {
                BonusCA1GUID=[NSString createGuid];
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:BonusCA1GUID];
            }
            else if([[row objectForKey:@"Calc_Price_Flag"] isEqualToString:@"F"] || [[row objectForKey:@"Calc_Price_Flag"] isEqualToString:@"M"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:BonusCA1GUID];
            }
            
            if ([row valueForKey:@"lineItemNotes"]==nil) {
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{12}" withString:@""];
                
                
            }
            
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{12}" withString:[row valueForKey:@"lineItemNotes"]];
            }
            
            
            [self executeNonQuery:sql];
            
            
            //Order HISTORY
            NSString *sql1 = kSQLSaveOrderHistoryLineItem;
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{1}" withString:cOrderRef];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Inventory_Item_ID"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Order_Quantity_UOM"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Ordered_Quantity"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Calc_Price_Flag"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Unit_Selling_Price"]];
            double iTemValue = [[row stringForKey:@"Ordered_Quantity"] doubleValue] * [[row stringForKey:@"Unit_Selling_Price"] doubleValue];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{8}" withString:[NSString stringWithFormat:@"%.2f",iTemValue]];
            //NSLog(@"Query %@",sql1);
            [self executeNonQuery:sql1];
            
        }
    }
    
    [self saveConfirmedOrderLots:orderRef andConfirmedOrderRef:cOrderRef];
    
}
-(void)saveConfirmedOrderLots : (NSString *)orderRef andConfirmedOrderRef:(NSString *)cOrderRef
{
    NSMutableArray *tempArray =   [[self fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Proforma_Order_Lots where Orig_Sys_Document_Ref='%@'",orderRef]] mutableCopy];
    //NSLog(@"Temsp %@",tempArray);
    
    int line;
    
    for (int i = 0; i < tempArray.count; i++)
    {
        @autoreleasepool
        {
            NSDictionary *row = [tempArray objectAtIndex:i];
            line=i+1;
            //#define kSQLSaveOrderLotItem @"INSERT   INTO TBL_Order_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Ordered_Quantity, Is_FIFO, Org_ID) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')"
            
            NSString *temp = kSQLSaveOrderLotItem;
            temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:cOrderRef];
            temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Lot_Number"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Ordered_Quantity"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Is_FIFO"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Org_ID"]];
            
            [self executeNonQuery:temp];
        }
    }
}

-(void)insertProductAddInfo:(NSDictionary *)row
{
    NSString *temp= @"INSERT INTO TBL_Product_Addl_Info (Attrib_Name, Attrib_Value, Custom_Attribute_1, Custom_Attribute_2, Custom_Attribute_3, Custom_Attribute_4, Custom_Attribute_5,Inventory_Item_ID,Organization_ID,Sync_Timestamp) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}')";
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[row stringForKey:@"Attrib_Name"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:[row stringForKey:@"Attrib_Value"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[row stringForKey:@"Custom_Attribute_1"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Custom_Attribute_2"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Custom_Attribute_3"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Custom_Attribute_4"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Custom_Attribute_5"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Inventory_Item_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Organization_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"Sync_Timestamp"]];
    NSLog(@"Hello %@",temp);
    [self executeNonQuery:temp];
    
}

-(NSMutableArray*)fetchAgencies
{
    
    //    NSString* agencyQuery=@"SELECT Classification_1 FROM tbl_sales_Target_Items";
    
    NSString* agencyQuery=@"SELECT Classification_1 As [Agency],SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items GROUP BY Classification_1 ORDER BY Classification_1";
    
    
    NSMutableArray* agencies=[self fetchDataForQuery:agencyQuery];
    
    return agencies;
    
    
    
    
}

-(NSMutableArray*)fetchUOMforItem:(NSString*)itemCode

{
    NSString* UOMQuery=[NSString stringWithFormat:@"SELECT Item_UOM FROM TBL_Item_UOM where Item_Code ='%@'",itemCode];
    
    
    
    NSMutableArray* UOM=[self fetchDataForQuery:UOMQuery];
    
    return UOM;
    
    
}

-(NSMutableArray*)fetchCustomerswithOpenInvoices

{
    /*
    NSString* openinvoicesQry=@"SELECT DISTINCT C.Customer_ID ,C.Site_Use_ID  ,V.Customer_No ,V.Customer_Name FROM TBL_Open_Invoices AS C  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID LEFT JOIN  TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No INNER JOIN TBL_Customer AS V On V.Customer_ID =C.Customer_ID AND V.Site_Use_ID =C.Site_Use_ID UNION ALL  SELECT DISTINCT B.Ship_To_Customer_ID AS Customer_ID,B.Inv_To_Site_ID ,V.Customer_No ,V.Customer_Name FROM TBL_Sales_History AS B  LEFT JOIN TBL_Collection AS N ON N.Customer_ID = B.Ship_To_Customer_ID AND  B.Ship_To_Site_ID= N.Site_Use_ID  LEFT JOIN  TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID AND B.Orig_Sys_Document_Ref = D.Invoice_No INNER JOIN TBL_Customer AS V On V.Customer_ID =B.Ship_To_Customer_ID AND V.Site_Use_ID =B.Inv_To_Site_ID WHERE (B.ERP_Status = 'X')";*/

    NSString* openinvoicesQry=@"SELECT DISTINCT IFNULL(V.Cust_Status,'N') AS Cust_Status, IFNULL(V.Avail_Bal,'0') AS Avail_Bal, C.Customer_ID ,C.Site_Use_ID  ,V.Customer_No ,V.Customer_Name,V.Cash_Cust  FROM TBL_Open_Invoices AS C  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID LEFT JOIN  TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No INNER JOIN TBL_Customer AS V On V.Customer_ID =C.Customer_ID AND V.Site_Use_ID =C.Site_Use_ID UNION ALL  SELECT DISTINCT IFNULL(V.Cust_Status,'N') AS Cust_Status,IFNULL(V.Avail_Bal,'0') AS Avail_Bal,B.Ship_To_Customer_ID AS Customer_ID,B.Inv_To_Site_ID ,V.Customer_No ,V.Customer_Name,V.Cash_Cust FROM TBL_Sales_History AS B  LEFT JOIN TBL_Collection AS N ON N.Customer_ID = B.Ship_To_Customer_ID AND  B.Ship_To_Site_ID= N.Site_Use_ID  LEFT JOIN  TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID AND B.Orig_Sys_Document_Ref = D.Invoice_No INNER JOIN TBL_Customer AS V On V.Customer_ID =B.Ship_To_Customer_ID AND V.Site_Use_ID =B.Inv_To_Site_ID WHERE (B.ERP_Status = 'X')";

    NSLog(@"open inv qry %@", openinvoicesQry);

    NSArray *invoiceCustomersArray = [self fetchDataForQuery:openinvoicesQry];

    NSLog(@"inv data %@", invoiceCustomersArray);
    
    if (invoiceCustomersArray>0) {

        return [invoiceCustomersArray mutableCopy];
    }

    else
    {
        return nil;
    }


}

///////////Collection///////////////
- (BOOL)saveCollection:(CustomerPaymentClass *)customerPaymentDetails withCustomerInfo:(NSDictionary *)customerInfo :(UIImage *)signImage :(NSMutableArray *)arrCollectionImages{
    // Create CollectionId
    
    
    
    
    NSString *collectionId = [NSString createGuid];
    
    NSString * collectionDateStr=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    
    // Make new Last Collection Reference and save it to NSDefaults
    NSString *lastCollectionReference = [SWDefaults lastCollectionReference];
    NSString *nUserId = [[SWDefaults userProfile] stringForKey:@"Emp_Code"];
    
    unsigned long long lastRefId = [[lastCollectionReference substringFromIndex: [lastCollectionReference length] - 10] longLongValue];
    lastRefId = lastRefId + 1;
    if(nUserId.length == 1)
    {
        nUserId = [NSString stringWithFormat:@"00%@",nUserId];
    }
    else if(nUserId.length == 2)
    {
        nUserId = [NSString stringWithFormat:@"0%@",nUserId];
    }
    
    lastCollectionReference = [NSString stringWithFormat:@"M%@C%010lld", nUserId, lastRefId];
    if (![lastCollectionReference isEqualToString:[SWDefaults lastCollectionReference]])
    {
        [SWDefaults setLastCollectionReference:lastCollectionReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastCollectionReference = [NSString stringWithFormat:@"M%@C%010lld", nUserId, lastRefId];
        if (![lastCollectionReference isEqualToString:[SWDefaults lastCollectionReference]])
        {
            [SWDefaults setLastCollectionReference:lastCollectionReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastCollectionReference = [NSString stringWithFormat:@"M%@C%010lld", nUserId, lastRefId];
            [SWDefaults setLastCollectionReference:lastCollectionReference];
        }
    }
    //[SWDefaults setLastCollectionReference:lastCollectionReference];
    
    // setup collection type
    NSString *collectionType;
    
    // check date
    NSString *collectionChqDate = @"";
    
    
    
    
    
    if([customerPaymentDetails.paymentMethod.lowercaseString isEqualToString:@"pdc"]){
        collectionType = @"PDC";
        collectionChqDate = customerPaymentDetails.chequeDate;
    }
    else if([customerPaymentDetails.paymentMethod.lowercaseString isEqualToString:@"current cheque"]){
        collectionType = @"CURR-CHQ";
        collectionChqDate = customerPaymentDetails.chequeDate;
    }
    else{
        collectionType=@"CASH";
    }
    
    [SWDefaults signatureSaveImage:signImage withName:[NSString stringWithFormat:@"%@.jpg",lastCollectionReference]];
    
    // Construct SQL Statement
    NSString *temp = kSQLCollectionInsertInfo;
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:collectionId];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastCollectionReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:collectionDateStr];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:collectionType];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[customerInfo stringForKey:@"Site_Use_ID"]];
    if ([[AppControl retrieveSingleton].ALLOW_MULTI_CURRENCY isEqualToString:@"Y"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:customerPaymentDetails.customerPaidAmountAfterConversion.stringValue];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:customerPaymentDetails.customerPaidAmountAfterConversion.stringValue];
    }
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:customerPaymentDetails.chequeNumber];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:collectionChqDate];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:customerPaymentDetails.bankName];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:customerPaymentDetails.bankBranchName];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:[[SWDefaults userProfile] stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:collectionDateStr];
    temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:customerPaymentDetails.remarks];
    
    
    BOOL tblCollectionInsertSuccess=NO;
    
    BOOL tblCollectionInvoicesSuccess=NO;
    
    BOOL docAddlInfoInsertSuccess=NO;
    
    BOOL  insertDocInfoStatus = YES;
    
    BOOL status=NO;
    
    
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
        
    }
    
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
        
        
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    
    @try {
        
        tblCollectionInsertSuccess =  [database executeUpdate:temp];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        //[database close];
    }
    
    //[self executeNonQuery:temp];
    
    AppControl *appCtrl = [AppControl retrieveSingleton];
    if ([appCtrl.DISABLE_COLL_INV_SETTLEMENT isEqualToString:KAppControlsNOCode])
    {
        if (tblCollectionInsertSuccess==YES) {
            
            for (int i = 0; i < customerPaymentDetails.invoicesArray.count; i++) {
                //NSDictionary *row = [invoices objectAtIndex:i];
                
                CustomerPaymentInvoiceClass *invoice=[customerPaymentDetails.invoicesArray objectAtIndexedSubscript:i];
                
                if ([appCtrl.COLL_INV_SETTLEMENT_MANDATORY isEqualToString:KAppControlsNOCode] && !([invoice.settledAmountForInvoice floatValue] > 0)) {
                    
                    @try
                    {
                        //tblCollectionInvoicesSuccess =  @"YES";
                        tblCollectionInvoicesSuccess =  YES;
                    }@catch (NSException *exception){
                        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                    }
                    @finally
                    {
                        //[database close];
                    }
                } else if ([appCtrl.ALLOW_EXCESS_CASH_COLLECTION isEqualToString:KAppControlsYESCode] && !([invoice.settledAmountForInvoice floatValue] > 0)){
                    //tblCollectionInvoicesSuccess =  @"YES";
                    tblCollectionInvoicesSuccess =  YES;
                }
                else{
                    if ([invoice.settledAmountForInvoice floatValue] > 0)
                    {
                        NSString *sql = kSQLCollectionInsertInvoice;
                        NSString* amtString=invoice.settledAmountForInvoice.stringValue;
                        
                        NSLog(@"amount string being inserted %@", [amtString description]);
                        if (amtString.length==0)
                        {
                            amtString=@"0";
                        }
                        
                        sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:collectionId];
                        sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:invoice.invoiceNumber];
                        sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:invoice.InvoiceDate];
                        sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:amtString];
                        sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:@"N"];
                        sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:collectionDateStr];
                        sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[NSString createGuid]];
                        
                        
                        NSLog(@"invoice being inserted %@", sql);
                        
                        
                        @try
                        {
                            tblCollectionInvoicesSuccess =  [database executeUpdate:sql];
                            NSLog(@"is tblCollectionInvoicesSuccess ? %d", tblCollectionInvoicesSuccess);
                        }@catch (NSException *exception){
                            NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                        }
                        @finally
                        {
                            //[database close];
                        }
                    }
                }
            }
            
            /** If user paid amount with out having the invoices*/
            if(customerPaymentDetails.invoicesArray.count==0){
                tblCollectionInvoicesSuccess=YES;
            }
            
            if (tblCollectionInvoicesSuccess==YES && [appCtrl.ALLOW_MULTI_CURRENCY isEqualToString:@"Y"] )
            {
                NSString *temp = @"INSERT INTO TBL_Doc_Addl_Info (Row_ID, Doc_No, Doc_Type, Attrib_Name, Attrib_Value,Custom_Attribute_5) VALUES ('{0}', '{1}', '{2}', '{3}','{4}','{5}')";
                temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
                temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastCollectionReference];
                temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:@"C"];
                temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:@"CURR"];
                temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:customerPaymentDetails.currencyType];
                temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:customerPaymentDetails.currencyRate];
                
                NSLog(@"TBL_Doc_Addl_Info being inserted %@", temp);
                
                @try {
                    
                    docAddlInfoInsertSuccess =  [database executeUpdate:temp];
                    NSLog(@"is docAddlInfoInsertSuccess ? %d", docAddlInfoInsertSuccess);
                    
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    //[database close];
                }
            }
            else if (tblCollectionInvoicesSuccess==YES)
            {
                //content inserted in doc additional info only for multi currency
                docAddlInfoInsertSuccess=YES;
            }
            
            if(arrCollectionImages.count>0)
            {
                int i = 1;
                NSString *InsertDocAdditionalInfoQuery = @"INSERT INTO TBL_Doc_Addl_Info (Row_ID, Doc_No, Doc_Type, Attrib_Name, Attrib_Value,Custom_Attribute_5) VALUES (?,?,?,?,?,?)";
                
                for (PaymentImage *imgObj in arrCollectionImages) {
                    
                    NSString *collectionImageName = [NSString stringWithFormat:@"%@_%d",lastCollectionReference,i];
                    
                    if ([imgObj.imageType isEqualToString:kReceiptImageType] && ![NSString isEmpty:imgObj.imageID]) {
                        
                        NSMutableArray *InsertDocAdditionalInfoParameters = [[NSMutableArray alloc]initWithObjects: [NSString createGuid], lastCollectionReference, @"C", @"RECEIPT_IMAGE", collectionImageName, @"", nil];
                        insertDocInfoStatus= [database executeUpdate:InsertDocAdditionalInfoQuery withArgumentsInArray:InsertDocAdditionalInfoParameters];
                        i++;
                        NSLog(@"TBL_Doc_Addl_Info being inserted Query %@", InsertDocAdditionalInfoQuery);
                        NSLog(@"TBL_Doc_Addl_Info being inserted Param %@", InsertDocAdditionalInfoParameters);
                    }
                    else if ([imgObj.imageType isEqualToString:kChequeImageType] && ![NSString isEmpty:imgObj.imageID])
                    {
                        NSMutableArray *InsertDocAdditionalInfoParameters = [[NSMutableArray alloc]initWithObjects: [NSString createGuid], lastCollectionReference, @"C", @"COL_IMG", collectionImageName, @"", nil];
                        insertDocInfoStatus= [database executeUpdate:InsertDocAdditionalInfoQuery withArgumentsInArray:InsertDocAdditionalInfoParameters];
                        i++;
                        NSLog(@"TBL_Doc_Addl_Info being inserted Query %@", InsertDocAdditionalInfoQuery);
                        NSLog(@"TBL_Doc_Addl_Info being inserted Param %@", InsertDocAdditionalInfoParameters);
                    }
                    NSLog(@"is insertDocInfoStatus ? %d", insertDocInfoStatus);
                }
            }
            
            if (!tblCollectionInvoicesSuccess || !tblCollectionInsertSuccess || !docAddlInfoInsertSuccess || !insertDocInfoStatus)
            {
                [database rollback];
                [database close];
                status=NO;
            }
            
            else
            {
                NSLog(@"INSERT INTO COLLECTION & INVOICES SUCCESSFULL");
                
                [database commit];
                [database close];
                status=YES;
                
            }
            
            //[self executeNonQuery:sql];
        }
    }
    else
    {
        if (tblCollectionInsertSuccess==YES)
        {
            if ([appCtrl.ALLOW_MULTI_CURRENCY isEqualToString:@"Y"])
            {
                NSString *temp = @"INSERT INTO TBL_Doc_Addl_Info (Row_ID, Doc_No, Doc_Type, Attrib_Name, Attrib_Value,Custom_Attribute_5) VALUES ('{0}', '{1}', '{2}', '{3}','{4}','{5}')";
                temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
                temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastCollectionReference];
                temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:@"C"];
                temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:@"CURR"];
                temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:customerPaymentDetails.currencyType];
                temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:customerPaymentDetails.currencyRate];
                
                @try
                {
                    docAddlInfoInsertSuccess =  [database executeUpdate:temp];
                    
                }@catch (NSException *exception)
                {
                    NSLog(@"Exception while obtaining data from database: %@", exception.reason);
                }
                @finally
                {
                    //[database close];
                }
            }
            if(arrCollectionImages.count>0)
            {
                int i = 1;
                NSString *InsertDocAdditionalInfoQuery = @"INSERT INTO TBL_Doc_Addl_Info (Row_ID, Doc_No, Doc_Type, Attrib_Name, Attrib_Value,Custom_Attribute_5) VALUES (?,?,?,?,?,?)";
                
                for (PaymentImage *imgObj in arrCollectionImages) {
                    
                    NSString *collectionImageName = [NSString stringWithFormat:@"%@_%d",lastCollectionReference,i];
                    
                    if ([imgObj.imageType isEqualToString:kReceiptImageType] && ![NSString isEmpty:imgObj.imageID]) {
                        
                        NSMutableArray *InsertDocAdditionalInfoParameters = [[NSMutableArray alloc]initWithObjects: [NSString createGuid], lastCollectionReference, @"C", @"RECEIPT_IMAGE", collectionImageName, @"", nil];
                        insertDocInfoStatus= [database executeUpdate:InsertDocAdditionalInfoQuery withArgumentsInArray:InsertDocAdditionalInfoParameters];
                        i++;
                    }
                    else if ([imgObj.imageType isEqualToString:kChequeImageType] && ![NSString isEmpty:imgObj.imageID])
                    {
                        NSMutableArray *InsertDocAdditionalInfoParameters = [[NSMutableArray alloc]initWithObjects: [NSString createGuid], lastCollectionReference, @"C", @"COL_IMG", collectionImageName, @"", nil];
                        insertDocInfoStatus= [database executeUpdate:InsertDocAdditionalInfoQuery withArgumentsInArray:InsertDocAdditionalInfoParameters];
                        i++;
                    }
                }
            }
            
            [database commit];
            [database close];
            status=YES;
        }
    }
    
    return status;
}



#pragma mark Customer Details


+(NSMutableArray*)fetchAllCustomers
{
    NSString* customersQry=@"SELECT IFNULL(Customer_ID,'0') AS Customer_ID,IFNULL(Site_Use_ID,'0') AS Site_Use_ID, IFNULL(Customer_No,'0') AS Customer_No,IFNULL(Customer_Name,'0') AS Customer_Name,IFNULL(Address,'0') AS Address,IFNULL(City,'0') AS City,IFNULL(Cash_Cust,'N/A') AS Cash_Cust,IFNULL(Avail_Bal,'N/A') AS Avail_Bal,IFNULL(Cust_Status,'N/A') AS Cust_Status FROM tbl_customer";
    
    NSMutableArray * customersArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:customersQry];
    
    NSMutableArray * customerObjectsArray=[[NSMutableArray alloc]init];
    
    
    if (customersArray.count>0) {
        

        
        for (NSInteger i=0; i< customersArray.count; i++) {
            
            NSMutableDictionary * customerDict=[customersArray objectAtIndex:i];
            
            
            Customer *currentCustomer=[[Customer alloc]init];
            
            currentCustomer.Customer_ID=[SWDefaults getValidStringValue:[customerDict valueForKey:@"Customer_ID"]];
            currentCustomer.Site_Use_ID=[SWDefaults getValidStringValue:[customerDict valueForKey:@"Site_Use_ID"]];
            currentCustomer.Customer_No=[SWDefaults getValidStringValue:[customerDict valueForKey:@"Customer_No"]];
            currentCustomer.Customer_Name=[SWDefaults getValidStringValue:[customerDict valueForKey:@"Customer_Name"]];
            currentCustomer.Address=[SWDefaults getValidStringValue:[customerDict valueForKey:@"Address"]];
            currentCustomer.City=[SWDefaults getValidStringValue:[customerDict valueForKey:@"City"]];
            currentCustomer.Cash_Cust=[SWDefaults getValidStringValue:[customerDict valueForKey:@"Cash_Cust"]];
            currentCustomer.Avail_Bal=[SWDefaults getValidStringValue:[customerDict valueForKey:@"Avail_Bal"]];
            currentCustomer.Cust_Status=[SWDefaults getValidStringValue:[customerDict valueForKey:@"Cust_Status"]];
            [customerObjectsArray addObject:currentCustomer];
            
            
        }
        
        
        
    }
    
    if (customerObjectsArray.count>0) {
        
        return customerObjectsArray;
    }
    
    else
    {
        return nil;
        
    }
    
}


-(NSMutableArray*)fetchBrandsforCustomer:(Customer*)selectedCustomer

{
    NSString* brandsQry=[NSString stringWithFormat:@"select distinct IFNULL(Classification_1,'N/A') AS Classification_1 from  TBL_Sales_Target_Items where Classification_2 ='%@' ", selectedCustomer.Customer_No];
    
    NSLog(@"brands qry is %@", brandsQry);
    
    NSMutableArray * brandsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:brandsQry];
    
  //  NSLog(@"brands array is %@", brandsArray);
    
    if (brandsArray.count>0) {
        
        
//        for (NSInteger i=0; <#condition#>; <#increment#>) {
//            <#statements#>
//        }
        
        return brandsArray;
    }
    
    else
    {
        return nil;
    }
}
-(NSMutableArray*)fetchTargetInformationForCustomer:(Customer*)selectedCustomer
{
    
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    AppControl * appControl=[AppControl retrieveSingleton];

    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate: currentDate];
    NSString* targetQry;
    
    if([appControl.FSR_VALUE_TYPE isEqualToString:@"Q"])/*quantity*/{
        
        targetQry=[NSString stringWithFormat:@"SELECT IFNULL(SUM(Custom_Value_1),'0') As [Target_Value],IFNULL(SUM(Custom_Value_2),'0') As [Sales_Value],IFNULL(SUM(custom_Attribute_7),'0') AS Balance_To_Go, SUM(Custom_Attribute_6) As [Previous_Day_Sales] FROM TBL_Sales_Target_Items where Classification_2 ='%@' and Month='%ld' and  Classification_1 ='%@' ",selectedCustomer.Customer_No,(long)[components month],selectedCustomer.selectedBrand.brandCode];
        
        if([appControl.FSR_TARGET_TYPE isEqualToString:@"P"] && [appControl.FSR_TARGET_PRIMARY isEqualToString:@"Area"]){
            targetQry=[NSString stringWithFormat:@"SELECT IFNULL( SUM(Custom_Value_1),'0') As [Target_Value],IFNULL( SUM(Custom_Value_2),'0') As [Sales_Value],IFNULL(SUM(custom_Attribute_7),'0') AS Balance_To_Go, SUM(Custom_Attribute_6) As [Previous_Day_Sales] FROM TBL_Sales_Target_Items where Classification_2 ='%@' and Month='%ld' ",selectedCustomer.Area,(long)[components month]];/**City replace with Area in future*/
        }
    }else /*Value*/{
         targetQry=[NSString stringWithFormat:@"SELECT IFNULL(SUM(Target_Value),'0') As [Target_Value],IFNULL(SUM(Sales_Value),'0') As [Sales_Value],IFNULL(SUM(custom_Attribute_7),'0') AS Balance_To_Go, SUM(Custom_Attribute_6) As [Previous_Day_Sales] FROM TBL_Sales_Target_Items where Classification_2 ='%@' and Month='%ld' and  Classification_1 ='%@' ",selectedCustomer.Customer_No,(long)[components month],selectedCustomer.selectedBrand.brandCode];
        
        if([appControl.FSR_TARGET_TYPE isEqualToString:@"P"] && [appControl.FSR_TARGET_PRIMARY isEqualToString:@"Area"]){
            targetQry=[NSString stringWithFormat:@"SELECT IFNULL( SUM(Target_Value),'0') As [Target_Value],IFNULL( SUM(Sales_Value),'0') As [Sales_Value],IFNULL(SUM(custom_Attribute_7),'0') AS Balance_To_Go, SUM(Custom_Attribute_6) As [Previous_Day_Sales] FROM TBL_Sales_Target_Items where Classification_2 ='%@' and Month='%ld' ",selectedCustomer.Area,(long)[components month]];/**City replace with Area in future*/
            
        }
    }
    
    

    
    
    
    NSLog(@"query for customer statistics %@", targetQry);
    NSMutableArray * customerTargetArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:targetQry];
    
    if (customerTargetArray.count>0) {
        
        for (NSInteger i=0; i<customerTargetArray.count; i++) {
            
            NSMutableDictionary * currentDict=[customerTargetArray objectAtIndex:i];
            
            NSMutableDictionary *refinedDict=[NSMutableDictionary nullFreeDictionaryWithDictionary:currentDict];
            
            [customerTargetArray replaceObjectAtIndex:i withObject:refinedDict];
        }
        
        NSLog(@"customer target data is %@", customerTargetArray);
        return customerTargetArray;
    }
    
    else
    {
        return nil;
    }

    
}


-(NSMutableArray*)fetchTargetInformationForCurrentMonth
{
    AppControl * appControl=[AppControl retrieveSingleton];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSString* targetQry;
    
    if([appControl.FSR_VALUE_TYPE isEqualToString:@"Q"])/*quantity*/{
        
        targetQry=[NSString stringWithFormat:@"SELECT IFNULL(SUM(Custom_Value_1),'0') As [Target_Value], IFNULL(SUM(Custom_Value_2),'0') As [Sales_Value], IFNULL(SUM(custom_Attribute_7),'0') AS Balance_To_Go FROM TBL_Sales_Target_Items where Month='%ld'",(long)[components month]];
    }else {
        targetQry=[NSString stringWithFormat:@"SELECT IFNULL(SUM(Target_Value),'0') As [Target_Value], IFNULL(SUM(Sales_Value),'0') As [Sales_Value], IFNULL(SUM(custom_Attribute_7),'0') AS Balance_To_Go FROM TBL_Sales_Target_Items where Month='%ld'",(long)[components month]];
    }
    
    NSLog(@"query for customer statistics %@", targetQry);
    NSMutableArray * customerTargetArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:targetQry];
    
    if (customerTargetArray.count>0) {
        
        for (NSInteger i=0; i<customerTargetArray.count; i++) {
            
            NSMutableDictionary * currentDict=[customerTargetArray objectAtIndex:i];
            
            NSMutableDictionary *refinedDict=[NSMutableDictionary nullFreeDictionaryWithDictionary:currentDict];
            
            [customerTargetArray replaceObjectAtIndex:i withObject:refinedDict];
        }
        
        NSLog(@"customer target data is %@", customerTargetArray);
        return customerTargetArray;
    }
    else
    {
        return nil;
    }
}



-(NSMutableArray*)fetchTotalOutStandingforCustomer:(Customer*)selectedCustomer

{
    

    
    
    NSString * outStandingQry=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"Select IFNULL(Total_Due,'N/A')  AS Total, IFNULL(Over_Due,'N/A') AS Over_Due from TBL_Customer_Dues where Customer_ID = '%@'",[NSString stringWithFormat:@"%@",selectedCustomer.Customer_ID]]];
    
    NSLog(@"total outstanding qry %@", outStandingQry);
    
        NSMutableArray *temp=  [[self fetchDataForQuery:outStandingQry] mutableCopy];
    
    if (temp.count>0) {
        
        return temp;
    }
    
    
    else
    {
        return nil;
    }
    
    
}



////////////////Dashboard////////////
-(NSArray *)dbGetTotalPlannedVisits:(NSString *)date
{
    NSString* dummytimeStr=@" 00:00:00";
    NSString *dateString=[date stringByAppendingString:dummytimeStr];
    
    NSLog(@"check time stamp %@", dateString);
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*)AS Total FROM TBL_FSR_Planned_Visits where Visit_Date Like '%%%@%%'", date]];
    
    
    //    NSDateFormatter *formatter = [NSDateFormatter new];
    //    [formatter setDateFormat:@"yyyy-MM-dd"];
    //    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    //    [formatter setLocale:usLocale];
    //
    //
    //
    //    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    //
    //    [dateformate setDateFormat:@"YYYY-MM-dd"];
    //    NSString* dummytimeStr=@" 00:00:00";
    //
    //    NSString *dateString=[[dateformate stringFromDate:[NSDate date]] stringByAppendingString:dummytimeStr];
    //
    //
    //    NSLog(@"check time stamp %@", dateString);
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*)AS Total FROM TBL_FSR_Planned_Visits where Visit_Date Like '%%%@%%'", [formatter stringFromDate:[NSDate date]]]];
    //
    //    //    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*)AS Total FROM TBL_FSR_Planned_Visits where Visit_Date Like '%%%@%%'", dateString]];
    //    formatter=nil;
    //    usLocale=nil;
    
    return temp;
}







-(NSArray *)dbGetTotalCompletedVisits:(NSString *)date
{
    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*) AS Total FROM TBL_FSR_Planned_Visits where Visit_Date Like '%%%@%%' AND Visit_Status = 'Y' ", date]];
    
    
    //    NSDateFormatter *formatter = [NSDateFormatter new];
    //    [formatter setDateFormat:@"yyyy-MM-dd"];
    //    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    //    [formatter setLocale:usLocale];
    //
    //    // [query setTag:kQueryTypeCompletedVisit];
    //
    //
    //
    //    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*) AS Total FROM TBL_FSR_Planned_Visits where Visit_Date Like '%%%@%%' AND Visit_Status = 'Y'", [formatter stringFromDate:[NSDate date]]]];
    //    formatter=nil;
    //    usLocale=nil;
    
    return temp ;
}


-(NSArray *)dbGetCustomerOutOfRoute:(NSString *)date
{
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT Customer_ID , Site_Use_ID FROM TBL_FSR_Actual_Visits where Visit_Start_Date Like '%%%@%%'", date]];
    
    
    //    NSDateFormatter *formatter = [NSDateFormatter new];
    //    [formatter setDateFormat:@"yyyy-MM-dd"];
    //    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    //    [formatter setLocale:usLocale];
    //    //[query setQuery:];
    //    //[query setTag:kQueryTypeTotalOutOfRout];
    //
    //    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT Customer_ID , Site_Use_ID FROM TBL_FSR_Actual_Visits where Visit_Start_Date Like '%%%@%%'", [formatter stringFromDate:[NSDate date]]]];
    //    formatter=nil;
    //    usLocale=nil;
    return [self CalculateOutOfRouteVisits:temp];
}



-(NSArray *)dbGetCustomerOutOfRouteCount:(NSString *)date
{
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT Customer_ID , Site_Use_ID FROM TBL_FSR_Actual_Visits where Visit_Start_Date Like '%%%@%%' and FSR_Plan_Detail_ID='0' group by Customer_ID || '-' || Site_Use_ID", date]];
    
    
    //    NSDateFormatter *formatter = [NSDateFormatter new];
    //    [formatter setDateFormat:@"yyyy-MM-dd"];
    //    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    //    [formatter setLocale:usLocale];
    //    //[query setQuery:];
    //    //[query setTag:kQueryTypeTotalOutOfRout];
    //
    //    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT Customer_ID , Site_Use_ID FROM TBL_FSR_Actual_Visits where Visit_Start_Date Like '%%%@%%'", [formatter stringFromDate:[NSDate date]]]];
    //    formatter=nil;
    //    usLocale=nil;
    return temp;
}


-(NSString*)fetchCreationDateforInvoice:(NSString*)erpRefNo
{
    NSString* creationDateQry=[NSString stringWithFormat:@"select IFNULL(creation_date,'N/A') AS creation_date from tbl_sales_history where ERP_Ref_no ='%@'",erpRefNo ];
    
    NSArray* creationDateArray=[self fetchDataForQuery:creationDateQry];
    
    if (creationDateArray.count>0) {
        
        return [[creationDateArray objectAtIndex:0]valueForKey:@"creation_date"];
    }
    
    else
    {
        return @"N/A";
    }
    
}


#pragma mark Products


-(NSMutableArray*)fetchProductDetailsForInventoryItemId:(NSString*)inventory_Item_ID AndOrganization_Id:(NSString*)productOrganizationId
{
    NSString* productDetailsQry=[NSString stringWithFormat:@"Select IFNULL(D.Get_Qty,'0')AS Bonus,E.* from (SELECT A.Primary_UOM_Code As UOM,A.Inventory_Item_ID,IFNULL(A.Discount,'0') AS Discount,IFNULL(SUM(B.Lot_Qty),'0')AS Lot_Qty, A.Item_Code,A.Category,A.Agency,A.Brand_Code, A.Description,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y'END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'	ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID FROM TBL_Product AS A  left join TBL_Product_Stock AS B on A.Inventory_Item_ID=B.Item_ID LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID   GROUP by   A.Inventory_Item_ID order by A.Description) AS E left join (SELECT * FROM TBL_BNS_Promotion GROUP BY Item_Code) AS D on E.Item_Code=D.Item_Code  where E.Inventory_Item_ID='%@' AND orgID='%@'  group by E.Item_Code order by E.Description asc", inventory_Item_ID,productOrganizationId];
    NSMutableArray* selectedProductArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* tempProductArray=[self fetchDataForQuery:productDetailsQry];
    
    if (tempProductArray.count>0) {
        
        for (NSInteger i=0; i<tempProductArray.count;i++ ) {
            
            NSMutableDictionary * currentProductDict=[tempProductArray objectAtIndex:i];
            
            
            
            Products * currentProduct=[[Products alloc]init];
            currentProduct.Brand_Code=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Brand_Code"]];
            currentProduct.selectedUOM=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"UOM"]];
            currentProduct.Discount=[NSString stringWithFormat:@"%0.2f",[[currentProductDict valueForKey:@"Discount"] doubleValue]];
            currentProduct.Agency=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Agency"]];
            currentProduct.Category=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Category"]];
            currentProduct.Description=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Description"]];
            currentProduct.IsMSL=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"IsMSL"]];
            currentProduct.ItemID=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"ItemID"]];
            currentProduct.Item_Code=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Item_Code"]];
            currentProduct.OrgID=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"OrgID"]];
            currentProduct.Sts=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Sts"]];
            currentProduct.Inventory_Item_ID=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Inventory_Item_ID"]];
            
            currentProduct.bonus=[NSString stringWithFormat:@"%@", [currentProductDict valueForKey:@"Bonus"]];
            currentProduct.stock=[NSString stringWithFormat:@"%@", [currentProductDict valueForKey:@"Lot_Qty"]];
            
            [selectedProductArray addObject:currentProduct];
            
        }
        
    }
    return selectedProductArray;
    
}

-(NSMutableArray*)fetchProductsArray
{
    NSString* productsQry=@"SELECT A.Inventory_Item_ID, A.Item_Code,A.Category,A.Agency,A.Brand_Code, A.Description,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y'END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'	ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID,IFNULL(A.EANNO,'N/A') As ProductBarCode FROM TBL_Product AS A LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID  ORDER BY A.Description";
    
    //    NSString* productsQry=@"SELECT  A.Item_Code,A.Category,A.Agency,A.Brand_Code, A.Description,B.Unit_Selling_Price AS WholeSale_Price,B.Unit_List_Price AS Retail_Price,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y'END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'	ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID FROM TBL_Product AS A LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID  left outer join TBL_Price_List AS B on A.Inventory_Item_ID=B.Inventory_Item_ID ORDER BY A.Description ";
    
    
    /*
     The Promo_Item flag in TBL_Product will be used for identifying promotional
     items.
     
     And if the ENABLE_PROMO_ITEM_VALIDITY app-control flag is set as Y, then
     this item has to be cross checked with TBL_Product_Addl_Info table as well
     to make sure that this item is valid for displaying. In this table it would
     have an attribute named as PV, and validity for this item will be stored in
     Custom_Attribute_1 (i.e., Valid_From) and Custom_Attribute_2 (i.e.,
     Valid_To)
     
     i.e., if the ENABLE_PROMO_ITEM_VALIDITY flag is set as Y, but either you
     don't have the validity attribute in TBL_Product_Addl_Info for this item, or
     if it exists and the validity has expired then this item should not be shown
     during order-taking.
     */
    
    
    NSLog(@"fetching products with flag called");
    
    AppControl * appCtrl=[AppControl retrieveSingleton];
    
    NSString* isPromoFlagEnabled =appCtrl.ENABLE_PROMO_ITEM_VALIDITY;
    
    NSString* productQrywithStockandBonus=[[NSString alloc]init];
    
    if ([isPromoFlagEnabled isEqualToString:KAppControlsYESCode]) {
        
        productQrywithStockandBonus=@"Select IFNULL(D.Get_Qty,'0')AS Bonus,E.* from (SELECT CASE WHEN AI.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y'END    AS Valid_Promo , AI.Custom_Attribute_1 AS Valid_From, AI.Custom_Attribute_2 AS Valid_To,A.Promo_Item,A.Primary_UOM_Code As UOM,A.Inventory_Item_ID,IFNULL(A.Discount,'0') AS Discount,IFNULL(SUM(B.Lot_Qty),'0')AS Lot_Qty, A.Item_Code,A.Category,A.Agency,A.Brand_Code, A.Description,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y' END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'	ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID,IFNULL(A.EANNO,'N/A') As ProductBarCode FROM TBL_Product AS A  left join TBL_Product_Stock AS B on A.Inventory_Item_ID=B.Item_ID LEFT JOIN  TBL_Product_Addl_Info AS AI ON A.Inventory_Item_ID=AI.Inventory_Item_ID AND A.Organization_ID=AI.Organization_ID AND AI.Attrib_Name='PV' and date('now') between  Valid_From and Valid_To   LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID    GROUP by   A.Inventory_Item_ID order by A.Description) AS E left join (SELECT * FROM TBL_BNS_Promotion GROUP BY Item_Code) AS D on E.Item_Code=D.Item_Code  group by E.Item_Code order by E.Description asc";
    }
    else
    {
        productQrywithStockandBonus=@"Select IFNULL(D.Get_Qty,'0')AS Bonus,E.* from (SELECT A.Primary_UOM_Code As UOM,A.Promo_Item,A.Inventory_Item_ID,IFNULL(A.Discount,'0') AS Discount,IFNULL(SUM(B.Lot_Qty),'0')AS Lot_Qty, A.Item_Code,A.Category,A.Agency,A.Brand_Code, A.Description,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y'END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'	ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID,IFNULL(A.EANNO,'N/A') As ProductBarCode FROM TBL_Product AS A  left join TBL_Product_Stock AS B on A.Inventory_Item_ID=B.Item_ID LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID   GROUP by   A.Inventory_Item_ID order by A.Description) AS E left join (SELECT * FROM TBL_BNS_Promotion GROUP BY Item_Code) AS D on E.Item_Code=D.Item_Code  group by E.Item_Code order by E.Description asc";
        
    }
    
    NSMutableArray *tempProductsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productQrywithStockandBonus];
    
    NSLog(@"TEMP PROD ARRAY qry %@", productQrywithStockandBonus);
    
    NSMutableArray * productsArray=[[NSMutableArray alloc]init];
    
    //NSLog(@"temp prod array is %@",[tempProductsArray objectAtIndex:0]);
    AppControl *appControl = [AppControl retrieveSingleton];
    NSMutableArray *AssttBonusAvailableOfferProductCodes=[[NSMutableArray alloc]init];
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode])
    {
        AssttBonusAvailableOfferProductCodes=[[self fetchDataForQuery:@"select Item_Code from TBL_BNS_Assortment_Items where  Is_Get_Item='N' AND Assortment_Plan_ID IN (select Assortment_Plan_ID from TBL_BNS_Assortment_Slabs)"] mutableCopy];
        if(AssttBonusAvailableOfferProductCodes.count>0)
            AssttBonusAvailableOfferProductCodes=[AssttBonusAvailableOfferProductCodes valueForKey:@"Item_Code"];
    }

    
    if (tempProductsArray.count>0) {
        
        for (NSInteger i=0; i<tempProductsArray.count;i++ ) {
            
            NSMutableDictionary * currentProductDict=[tempProductsArray objectAtIndex:i];
            Products * currentProduct=[[Products alloc]init];
            currentProduct.Brand_Code=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Brand_Code"]];
            
            if([[currentProductDict valueForKey:@"Promo_Item"] isEqualToString:@"Y"]&&
               [[currentProductDict valueForKey:@"Valid_Promo"] isEqualToString:@"Y"]){
                currentProduct.Promo_Item=@"Y";
                NSLog(@"promo item is %@", currentProductDict);
            }
            else{
                currentProduct.Promo_Item=@"N";
            }
            
            currentProduct.selectedUOM=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"UOM"]];
            currentProduct.Discount=[NSString stringWithFormat:@"%0.2f",[[currentProductDict valueForKey:@"Discount"] doubleValue]];
            currentProduct.Agency=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Agency"]];
            currentProduct.Category=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Category"]];
            currentProduct.Description=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Description"]];
            currentProduct.IsMSL=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"IsMSL"]];
            currentProduct.ItemID=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"ItemID"]];
            currentProduct.Item_Code=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Item_Code"]];
            currentProduct.OrgID=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"OrgID"]];
            currentProduct.Sts=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Sts"]];
            currentProduct.Inventory_Item_ID=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Inventory_Item_ID"]];
            currentProduct.primaryUOM=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"UOM"]];

            currentProduct.bonus=[NSString stringWithFormat:@"%@", [currentProductDict valueForKey:@"Bonus"]];
            currentProduct.stock=[NSString stringWithFormat:@"%@", [currentProductDict valueForKey:@"Lot_Qty"]];
            
            currentProduct.ProductBarCode=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:[currentProductDict valueForKey:@"ProductBarCode"]];
            if(AssttBonusAvailableOfferProductCodes.count>0 && [AssttBonusAvailableOfferProductCodes containsObject:currentProduct.Item_Code])
                currentProduct.bonus=@"1"; /** Assigning dummy value for identifying the bonus availablity*/

            
            [productsArray addObject:currentProduct];
        }
    }
    
    if (productsArray.count>0) {
        return productsArray;
    }
    
    else
    {
        return nil;
    }
}

-(NSMutableArray*)fetchProductsDetails{
    
    NSString* productQrywithStockandBonus=[[NSString alloc]init];
    productQrywithStockandBonus=@"select A.Agency AS Agency,A.Inventory_Item_ID AS Inventory_Item_ID,A.Brand_Code AS Brand_Code,A.Category AS Category,A.Description AS Description,A.Inventory_Item_ID AS ItemID,A.Item_Code AS Item_Code,A.Organization_ID AS OrgID,IFNULL(A.Discount,'0') AS Discount,A.Primary_UOM_Code AS selectedUOM,A.Primary_UOM_Code AS primaryUOM,IFNULL(A.EANNO,'N/A') As ProductBarCode, CASE WHEN B.MSL_Inventory_Item_ID IS NULL THEN 'N' ELSE 'Y'END As IsMSL, CASE WHEN C.Inventory_Item_ID IS NOT NULL AND A.Promo_item='Y' AND Promo_Item_App_Control_Flag='Y' THEN 'Y' ELSE 'N'END As Promo_Item, IFNULL(D.lotsTotalQty,'0') AS stock, IFNULL(D.nearestExpiryDate,'')  AS nearestExpiryDate,IFNULL(D.maxExpiryDate,'') AS maxExpiryDate,CASE WHEN (E.Item_Code IS NOT NULL AND ENABLE_ASSORTMENT_BONUS_App_Control_Flag='Y') OR F.Item_Code IS NOT NULL THEN '1' ELSE '0'END As bonus, CASE WHEN (G.Inventory_Item_ID IS NOT NULL AND ALLOW_SPECIAL_DISCOUNT_App_Control_Flag='Y') THEN IFNULL(G.Attrib_Value,'0') ELSE '0'END As specialDicount, CASE WHEN (H.Inventory_Item_ID IS NOT NULL AND SHOW_ON_ORDER_QTY_App_Control_Flag='Y') THEN IFNULL(H.WareHousesOnOrderqty,'0') ELSE '0'END As OnOrderQty from tbl_product AS A LEFT JOIN (Select Inventory_Item_ID AS MSL_Inventory_Item_ID,Organization_ID AS MSL_Organization_ID from TBL_Product_MSL GROUP by Inventory_Item_ID) AS B ON A.Inventory_Item_ID=B.MSL_Inventory_Item_ID AND A.Organization_ID=B.MSL_Organization_ID LEFT JOIN TBL_Product_Addl_Info AS C ON A.Inventory_Item_ID=C.Inventory_Item_ID AND A.Organization_ID=C.Organization_ID AND C.Attrib_Name='PV' and date(C.Custom_Attribute_1)<=date('now') and date(C.Custom_Attribute_2)>=date('now') LEFT JOIN (select IFNULL(Sum(Lot_Qty),'0') AS lotsTotalQty,Item_ID AS Inventory_Item_ID,IFNULL(max(Expiry_Date),'') AS maxExpiryDate,IFNULL(min(Expiry_Date),'') AS nearestExpiryDate from TBL_Product_Stock GROUP by Item_ID) AS D ON A.Inventory_Item_ID=D.Inventory_Item_ID LEFT JOIN (select Item_Code from TBL_BNS_Assortment_Items where Is_Get_Item='N' GROUP BY Item_Code) AS E ON A.Item_Code=E.Item_Code LEFT JOIN (select Item_Code from TBL_BNS_Promotion GROUP BY Item_Code) AS F on A.Item_Code=F.Item_Code LEFT JOIN TBL_Product_Addl_Info AS G ON A.Inventory_Item_ID=G.Inventory_Item_ID AND A.Organization_ID=G.Organization_ID AND G.Attrib_Name='SPECIAL_DISCOUNT' LEFT JOIN (select Sum(Attrib_Value) AS WareHousesOnOrderqty,Inventory_Item_ID from TBL_Product_Addl_Info where Attrib_Name='ON_ORDER_QTY' group by Inventory_Item_ID) AS H ON A.Inventory_Item_ID=H.Inventory_Item_ID LEFT JOIN (Select IFNULL(Control_Value,'N') as Promo_Item_App_Control_Flag from TBL_App_Control where control_key='ENABLE_PROMO_ITEM_VALIDITY') LEFT JOIN (Select IFNULL(Control_Value,'N') as ENABLE_ASSORTMENT_BONUS_App_Control_Flag from TBL_App_Control where control_key='ENABLE_ASSORTMENT_BONUS') LEFT JOIN (Select IFNULL(Control_Value,'N') as ALLOW_SPECIAL_DISCOUNT_App_Control_Flag from TBL_App_Control where control_key='ALLOW_SPECIAL_DISCOUNT')LEFT JOIN (Select IFNULL(Control_Value,'N') as SHOW_ON_ORDER_QTY_App_Control_Flag from TBL_App_Control where control_key='SHOW_ON_ORDER_QTY')  order by A.Description";
    
    
   // productQrywithStockandBonus=[productQrywithStockandBonus stringByAppendingString:@" limit 40"];
    
    
    NSMutableArray *tempProductsArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productQrywithStockandBonus];
    
    
    return tempProductsArray;
    
}

-(NSMutableArray*)fetchProductTarget:(NSString*)agency
{
    //we dont have product target we have agency level target
    NSString* productTargetQry;
    AppControl *appControl = [AppControl retrieveSingleton];

    
    productTargetQry=@"";
    if([appControl.FSR_VALUE_TYPE isEqualToString:@"Q"])/*quantity*/{
        productTargetQry=@"SELECT IFNULL(SUM(Custom_Value_1),'0') As [Target_Value],IFNULL(SUM(Custom_Value_2),'0') As [Sales_Value],IFNULL(SUM(custom_Attribute_7),'0') AS Balance_To_Go,Count(Target_Value) AS NumberOFTargetValues FROM TBL_Sales_Target_Items where   Month='%d' and  Classification_1 ='%@'";

    }else /*Value*/{
        productTargetQry=@"SELECT IFNULL(SUM(Target_Value),'0') As [Target_Value],IFNULL(SUM(Sales_Value),'0') As [Sales_Value],IFNULL(SUM(custom_Attribute_7),'0') AS Balance_To_Go,Count(Target_Value) AS NumberOFTargetValues FROM TBL_Sales_Target_Items where   Month='%d' and  Classification_1 ='%@'";
    }
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate: currentDate];
    NSMutableArray *temp = [[self fetchDataForQuery:[NSString stringWithFormat:productTargetQry, [components month],agency]] mutableCopy];
    
    for (NSInteger i=0; i<temp.count; i++) {
        NSMutableDictionary*   currentDict=[[NSDictionary nullFreeDictionaryWithDictionary:[temp objectAtIndex:i]] mutableCopy];
        [temp replaceObjectAtIndex:i withObject:currentDict];
    }
    
    NSLog(@"product target is %@", temp);
    if (temp.count>0) {
        return [[temp objectAtIndex:0] mutableCopy];
    }
    else{
        return nil;
    }
    
}


-(NSMutableArray*)fetchProductGenericPriceList:(Products*)selectedProduct
{
    NSString* priceListQry=[NSString stringWithFormat:@"select IFNULL(Price_List_ID,'N/A') AS Price_List_ID, IFNULL(Inventory_Item_ID,'N/A') AS Inventory_Item_ID, IFNULL(Organization_ID,'N/A') AS Organization_ID, IFNULL(Item_UOM,'N/A') AS Item_UOM,IFNULL(Unit_Selling_Price,'0') AS Unit_Selling_Price, IFNULL(Unit_List_Price,'0') AS Unit_List_Price from TBL_Price_List where Inventory_Item_ID ='%@' and  Is_Generic='Y' ",selectedProduct.Inventory_Item_ID];
    // NSLog(@"price list qry is %@", priceListQry);
    
    NSMutableArray* tempPriceListArray=[self fetchDataForQuery:priceListQry];
    NSMutableArray * priceListArray=[[NSMutableArray alloc]init];
    
    
    if (tempPriceListArray.count>0) {
        
        NSLog(@"price list is %@", tempPriceListArray);
        
        for (NSMutableDictionary *priceListDict in tempPriceListArray ) {
            
            PriceList * priceList=[[PriceList alloc]init];
            priceList.Price_List_ID=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Price_List_ID"]];
            priceList.Inventory_Item_ID=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Inventory_Item_ID"]];
            priceList.Organization_ID=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Organization_ID"]];
            priceList.Item_UOM=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Item_UOM"]];
            priceList.Unit_Selling_Price=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Unit_Selling_Price"]];
            priceList.Unit_List_Price=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Unit_List_Price"]];
            
            [priceListArray addObject:priceList];
        }
        
        return priceListArray;
    }
    else
    {
        return nil;
    }

}
-(NSMutableArray*)fetchProductPriceList:(Products*)selectedProd withCustomer:(Customer*)customerObject
{
    NSString* priceListQry=[NSString stringWithFormat:@"select IFNULL(Is_Generic,'N') AS Is_Generic,IFNULL(Price_List_ID,'N/A') AS Price_List_ID, IFNULL(Inventory_Item_ID,'N/A') AS Inventory_Item_ID, IFNULL(Organization_ID,'N/A') AS Organization_ID, IFNULL(Item_UOM,'N/A') AS Item_UOM,IFNULL(Unit_Selling_Price,'0') AS Unit_Selling_Price, IFNULL(Unit_List_Price,'0') AS Unit_List_Price from TBL_Price_List where Inventory_Item_ID ='%@' and (Price_List_ID='%@' OR Is_Generic='Y') and Item_UOM='%@' and Unit_Selling_Price>0",selectedProd.Inventory_Item_ID,customerObject.Price_List_ID,selectedProd.selectedUOM];
    
    NSMutableArray* tempPriceListArray=[self fetchDataForQuery:priceListQry];
    NSMutableArray * priceListArray=[[NSMutableArray alloc]init];
    
    
    if (tempPriceListArray.count>0) {
        
        NSLog(@"price list count is %lu", (unsigned long)tempPriceListArray.count);
        
        PriceList *genreicPriceList;
        PriceList *customerPriceList;
        
        for (NSMutableDictionary *priceListDict in tempPriceListArray ) {
            
            PriceList * priceList=[[PriceList alloc]init];
            priceList.Price_List_ID=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Price_List_ID"]];
            priceList.Inventory_Item_ID=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Inventory_Item_ID"]];
            priceList.Is_Generic=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Is_Generic"]];
            
            priceList.Organization_ID=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Organization_ID"]];
            priceList.Item_UOM=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Item_UOM"]];
            priceList.Unit_Selling_Price=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Unit_Selling_Price"]];
            priceList.Unit_List_Price=[SWDefaults getValidStringValue:[priceListDict valueForKey:@"Unit_List_Price"]];
            if([[SWDefaults getValidStringValue:customerObject.Price_List_ID] isEqualToString:[SWDefaults getValidStringValue:priceList.Price_List_ID]])
            {
                customerPriceList=priceList;
            }
            else if([priceList.Is_Generic isEqualToString:@"Y"] )
            {
                genreicPriceList=priceList;
            }
            
        }
        if (customerPriceList!=nil) {
            
            [priceListArray addObject:customerPriceList];
        }
        else if (genreicPriceList!=nil) {
            
            [priceListArray addObject:genreicPriceList];
        }
        
        return priceListArray;
    }
    else
    {
        return nil;
    }
    
}



-(NSMutableArray*)fetchBonusforSelectedProduct:(Products*)selectedProd
{
    
    NSString* bonusQry=[[NSString alloc]initWithFormat:@"SELECT IFNULL(A.Prom_Qty_From,'0') AS Prom_Qty_From ,IFNULL(A.Prom_Qty_To,'0') AS Prom_Qty_To,IFNULL(A.Price_Break_Type_Code,'0') AS Price_Break_Type_Code,IFNULL(A.Get_Item,'0') AS Get_Item, IFNULL(A.Get_Qty ,'0') AS Get_Qty,IFNULL(A.Get_Add_Per,'0') AS Get_Add_Per ,IFNULL(B.Description,'N/A') AS Description  FROM TBL_BNS_Promotion AS A INNER JOIN TBL_Product AS B ON A.Get_Item = B.Item_Code   WHERE   A.Item_Code='%@' ORDER BY A.Prom_Qty_From ASC",selectedProd.Item_Code];
  //  NSLog(@"bonus qry is %@", bonusQry);
    NSMutableArray* tempBonusArray=[self fetchDataForQuery:bonusQry];
    NSMutableArray* bonusArray=[[NSMutableArray alloc]init];
    if (tempBonusArray.count>0) {
        //NSLog(@"bonus response is %@", tempBonusArray);
        for (NSMutableDictionary * bonusDict in tempBonusArray) {
            BonusItem * bonusItem=[[BonusItem alloc]init];
            bonusItem.Description=[NSString stringWithFormat:@"%@",[bonusDict valueForKey:@"Description"]];
            bonusItem.Get_Add_Per=[NSString stringWithFormat:@"%@",[bonusDict valueForKey:@"Get_Add_Per"]];
            bonusItem.Get_Item=[NSString stringWithFormat:@"%@",[bonusDict valueForKey:@"Get_Item"]];
            bonusItem.Get_Qty=[NSString stringWithFormat:@"%@",[bonusDict valueForKey:@"Get_Qty"]];
            bonusItem.Price_Break_Type_Code=[NSString stringWithFormat:@"%@",[bonusDict valueForKey:@"Price_Break_Type_Code"]];
            bonusItem.Prom_Qty_From=[NSString stringWithFormat:@"%@",[bonusDict valueForKey:@"Prom_Qty_From"]];
            bonusItem.Prom_Qty_To=[NSString stringWithFormat:@"%@",[bonusDict valueForKey:@"Prom_Qty_To"]];
            [bonusArray addObject:bonusItem];
        }
        return bonusArray;
    }
    else
    {
        return nil;
    }
    
    
}
-(NSMutableArray*)fetchUOMforProduct:(NSString*)itemCode
{
    NSString* UOMQuery=[NSString stringWithFormat:@"SELECT DISTINCT Item_UOM FROM TBL_Item_UOM where Item_Code ='%@'",itemCode];
    NSMutableArray* UOM=[self fetchDataForQuery:UOMQuery];
    if (UOM.count>0) {
        
        return [UOM valueForKey:@"Item_UOM"];
    }
    else
    {
        return nil;
    }
}

//-(NSMutableArray*)fetchStockforAllProducts
//{
//    NSString* stockQry=[NSString stringWithFormat:@"SELECT IFNULL(Org_ID,'N/A') AS Org_ID, IFNULL(Lot_Qty,'0') AS Lot_Qty, IFNULL(Lot_No,'N/A') AS Lot_No, IFNULL(Expiry_Date,'N/A') AS Expiry_Date, IFNULL(Stock_ID,'N/A') AS Stock_ID FROM TBL_Product_Stock  ORDER BY Org_ID ASC, Expiry_Date ASC"];
//    
//    NSMutableArray* stockArray=[self fetchDataForQuery:stockQry];
//    if (stockArray.count>0) {
//        
//    }
//}

-(NSMutableArray*)fetchProductImagesforSelectedProduct:(Products*)selectedProd
{
    NSString* productImagesQry=[NSString stringWithFormat:@"select IFNULL(M.Media_File_ID,'N/A') AS Media_File_ID, IFNULL(M.Entity_ID_1,'N/A') AS Entity_ID_1,IFNULL(M.Entity_ID_2,'N/A') AS Entity_ID_2,IFNULL(M.Entity_Type,'N/A')AS Entity_Type, IFNULL(M.Media_Type,'N/A')AS Media_Type,IFNULL(M.Filename,'N/A') AS Filename,IFNULL(M.Caption,'N/A') AS Caption, IFNULL(M.Thumbnail,'N/A') AS Thumbnail,IFNULL(M.Is_Deleted,'N/A') AS Is_Deleted, IFNULL(M.Download_Flag,'N/A') AS Download_Flag,IFNULL(M.Custom_Attribute_1,'N/A') AS Custom_Attribute_1,IFNULL(M.Custom_Attribute_2,'N/A') AS Custom_Attribute_2,IFNULL(M.Custom_Attribute_3,'N/A') AS Custom_Attribute_3,IFNULL(M.Custom_Attribute_4,'N/A') AS Custom_Attribute_4 from TBL_Media_Files AS M inner join TBL_Product AS P on M.Entity_ID_1 = P.Inventory_Item_ID where M.Media_Type ='Image' and M.Entity_ID_1='%@' and M.Entity_ID_2='%@' and M.IS_Deleted ='N' and M.Download_Flag ='N'", selectedProd.Inventory_Item_ID,selectedProd.OrgID];
    
   // NSLog(@"query for images %@", productImagesQry);
    
    NSMutableArray* tempProductImagesArray=[self fetchDataForQuery:productImagesQry];
    NSMutableArray* productImagesArray=[[NSMutableArray alloc]init];
    NSString* filePath=[SWDefaults applicationDocumentsDirectory];
    
    if (tempProductImagesArray.count>0) {
        
        for (NSMutableDictionary *currentMediaFileDict in tempProductImagesArray) {
            
            ProductMediaFile * mediaFile=[[ProductMediaFile alloc]init];
            
            mediaFile.Media_File_ID=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Media_File_ID"]];
            mediaFile.Entity_ID_1=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Entity_ID_1"]];
            mediaFile.Entity_ID_2=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Entity_ID_2"]];
            mediaFile.Entity_Type=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Entity_Type"]];
            mediaFile.Media_Type=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Media_Type"]];
            mediaFile.Filename=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Filename"]];
            mediaFile.Caption=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Caption"]];
            mediaFile.Thumbnail=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Thumbnail"]];
            mediaFile.Is_Deleted=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Is_Deleted"]];
            mediaFile.Download_Flag=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Download_Flag"]];
            mediaFile.Custom_Attribute_1=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Custom_Attribute_1"]];
            mediaFile.Custom_Attribute_2=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Custom_Attribute_2"]];
            mediaFile.Custom_Attribute_3=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Custom_Attribute_3"]];
            mediaFile.Custom_Attribute_4=[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Custom_Attribute_4"]];
            mediaFile.File_Path= [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[currentMediaFileDict valueForKey:@"Filename"]]];
            
            [productImagesArray addObject:mediaFile];
        }
        
    }
    if (productImagesArray.count>0) {
        
        return productImagesArray;
    }
    else
    {
        return nil;
    }
}


-(NSMutableArray*)fetchStockwithOnOrderQty:(Products*)selectedProduct WareHouseId:(NSString *)OrgId
{
   // NSString* stockQry=[NSString stringWithFormat:@"select IFNULL(A.Org_ID,'N/A') AS Org_ID, IFNULL(A.Lot_Qty,'0') AS Lot_Qty,IFNULL(Lot_No,'N/A') AS Lot_No, IFNULL(Expiry_Date,'N/A') AS Expiry_Date, IFNULL(Stock_ID,'N/A') AS Stock_ID, IFNULL(B.Attrib_Name,'N/A') AS Attrib_Name, IFNULL(B.Attrib_Value,'0') AS On_Order_Qty  from  TBL_Product_Stock AS A left join TBL_Product_Addl_Info AS B on A.Item_ID=B.Inventory_Item_ID and A.Org_ID =B.Organization_ID  where A.Item_ID='%@'", selectedProduct.ItemID];
    NSString* stockQry;
    if([OrgId isEqualToString:KNotApplicable])
    {
       stockQry =[NSString stringWithFormat:@"select IFNULL(A.Org_ID,'N/A') AS Org_ID, IFNULL(A.Lot_Qty,'0') AS Lot_Qty,IFNULL(Lot_No,'N/A') AS Lot_No,C.Description, IFNULL(Expiry_Date,'N/A') AS Expiry_Date, IFNULL(Stock_ID,'N/A') AS Stock_ID, IFNULL(B.Attrib_Name,'N/A') AS Attrib_Name, IFNULL(B.Attrib_Value,'0') AS On_Order_Qty  from  TBL_Product_Stock AS A left join TBL_Product_Addl_Info AS B on A.Item_ID=B.Inventory_Item_ID and A.Org_ID =B.Organization_ID INNER JOIN TBL_Org_Info As C ON A.Org_ID=C.Org_ID  where A.Item_ID='%@'", selectedProduct.ItemID];
    }
    else
    {
        stockQry =[NSString stringWithFormat:@"select IFNULL(A.Org_ID,'N/A') AS Org_ID, IFNULL(A.Lot_Qty,'0') AS Lot_Qty,IFNULL(Lot_No,'N/A') AS Lot_No,C.Description, IFNULL(Expiry_Date,'N/A') AS Expiry_Date, IFNULL(Stock_ID,'N/A') AS Stock_ID, IFNULL(B.Attrib_Name,'N/A') AS Attrib_Name, IFNULL(B.Attrib_Value,'0') AS On_Order_Qty  from  TBL_Product_Stock AS A left join TBL_Product_Addl_Info AS B on A.Item_ID=B.Inventory_Item_ID and A.Org_ID =B.Organization_ID INNER JOIN TBL_Org_Info As C ON A.Org_ID=C.Org_ID  where A.Item_ID='%@'  AND A.Org_Id='%@'", selectedProduct.ItemID,OrgId];/** OrgId represants warehouseid*/
    }
    
    
    NSMutableArray* stockArray=[self fetchDataForQuery:stockQry];
    if (stockArray.count>0) {
        
        //NSLog(@"stock array is %@", [stockArray description]);
        
        return stockArray;
    }
    
    else
    {
        return nil;
    }
    
}
/** WareHouse Stock Position****/
-(NSMutableArray *)fetchProductWareHouseStock:(Products*)selectedProduct{
    NSString* stockQry=[NSString stringWithFormat:@"Select A.Organization_ID AS OrganizationID, IFNULL(A.Outstanding_PO_Qty,'0') AS OSPoQty,IFNULL(A.Awaiting_Ship_Qty,'0') AS AWShipQty,IFNULL(A.OnHold_Qty,'0') AS OnHoldQty,A.WH_ID AS WareHouseID,IFNULL(B.ReserveQty,'0') AS ReserveQty from TBL_Product_WH_Stock AS A LEFT JOIN (select sum(Lot_Qty) AS ReserveQty,Item_ID,Org_ID from TBL_Product_Stock GROUP BY Org_ID ,Item_ID) AS B ON A.Item_ID=B.Item_ID AND A.WH_ID=B.Org_ID Where A.Item_ID='%@' AND Organization_ID IS NOT NULL AND WH_ID IS NOT NULL", selectedProduct.Inventory_Item_ID];
    
    NSMutableArray* stockArray=[NSMutableArray arrayWithArray:[self fetchDataForQuery:stockQry]];
    return stockArray;
}
/** Product PO Stock*/
-(NSMutableArray *)fetchProductPOStock:(Products*)selectedProduct{
    NSString* stockQry=[NSString stringWithFormat:@"select IFNULL(PO_No,'0') AS PoNumber,IFNULL(PO_Line_No,'0') AS PoLineNumber,IFNULL(PO_Date,'0') AS PODate,IFNULL(PO_Due_Date,'') AS PODueDate,IFNULL(Outstanding_PO_Qty,'0') AS OSPoQty from TBL_Product_PO_Details where Organization_ID IS NOT NULL AND Item_ID='%@'", selectedProduct.Inventory_Item_ID];
    
    NSMutableArray* POStockArray=[NSMutableArray arrayWithArray:[self fetchDataForQuery:stockQry]];
    return POStockArray;
}

-(NSString*)fetchConversionRateForSelectedUOM:(Products*)selectedProd
{
    NSString* uomQry=[NSString stringWithFormat:@"select IFNULL(Conversion,'0') AS Conversion from TBL_Item_UOM where Item_Code ='%@' and ITEM_UOM ='%@'", selectedProd.Item_Code,selectedProd.selectedUOM];
    NSMutableArray* ConversionArray=[self fetchDataForQuery:uomQry];
    if (ConversionArray.count>0) {
        return [[ConversionArray objectAtIndex:0] valueForKey:@"Conversion"];
    }
    else
    {
        return nil;
    }
    
}


-(NSMutableArray *)fetchDuesForCustomer:(Customer *)selectdCust
{
    
    NSMutableArray *temp =[[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT Customer_ID, IFNULL(M0_Due,'0') AS M0_Due ,IFNULL(M1_Due,'0') AS M1_Due ,IFNULL(M2_Due,'0') AS M2_Due,IFNULL(M3_Due,'0') AS M3_Due,IFNULL(M4_Due,'0') AS M4_Due,IFNULL(M5_Due,'0') AS M5_Due,IFNULL(Prior_Due,'0') AS Prior_Due ,IFNULL(Total_Due,'0') AS Total_Due ,IFNULL(PDC_Due,'0') AS PDC_Due FROM TBL_Customer_Dues WHERE Customer_ID = '%@'",selectdCust.Customer_ID]] mutableCopy];
    
    
    if (temp.count>0) {
        
        return temp;
    }
    else
    {
        return nil;
    }
}

-(SalesWorxVisit*)fetchRecentOrdersforCustomer:(SalesWorxVisit*)currentVisit
{
  NSMutableArray*  customerOrderHistoryArray=[[[SWDatabaseManager retrieveManager] dbGetRecentOrdersForCustomer:currentVisit.visitOptions.customer] mutableCopy];
    if (customerOrderHistoryArray.count>0) {
        
        currentVisit.visitOptions.customer.totalOrderAmount=0.0;

        currentVisit.visitOptions.customer.customerOrderHistoryArray=[[NSMutableArray alloc]init];
        
        for (NSMutableDictionary   * currentCustOrderHistoryDict in customerOrderHistoryArray) {
            CustomerOrderHistory * currentCustomerOrderHistory=[[CustomerOrderHistory alloc] init];
            currentCustomerOrderHistory.Creation_Date=[currentCustOrderHistoryDict valueForKey:@"Creation_Date"];
            currentCustomerOrderHistory.Customer_Name=[currentCustOrderHistoryDict valueForKey:@"Customer_Name"];
            currentCustomerOrderHistory.Shipping_Instructions=[currentCustOrderHistoryDict valueForKey:@"Shipping_Instructions"];
            currentCustomerOrderHistory.ERP_Status=[currentCustOrderHistoryDict valueForKey:@"ERP_Status"];
            currentCustomerOrderHistory.FSR=[currentCustOrderHistoryDict valueForKey:@"FSR"];
            currentCustomerOrderHistory.Orig_Sys_Document_Ref=[currentCustOrderHistoryDict valueForKey:@"Orig_Sys_Document_Ref"];
            currentCustomerOrderHistory.Transaction_Amt=[currentCustOrderHistoryDict valueForKey:@"Transaction_Amt"];
            currentCustomerOrderHistory.End_Time=[currentCustOrderHistoryDict valueForKey:@"End_Time"];
            currentCustomerOrderHistory.ERP_Ref_Number=[currentCustOrderHistoryDict valueForKey:@"ERP_Ref_No"];

            
            double currentOrderTotal=[[currentCustOrderHistoryDict valueForKey:@"Transaction_Amt"] doubleValue];
            // NSLog(@"current tot temp %0.2f",currentOrderTotal);
            
            
            currentVisit.visitOptions.customer.totalOrderAmount=currentVisit.visitOptions.customer.totalOrderAmount+currentOrderTotal;
            NSLog(@"check total %0.3f",currentVisit.visitOptions.customer.totalOrderAmount);
            
            
            [currentVisit.visitOptions.customer.customerOrderHistoryArray addObject:currentCustomerOrderHistory];
        }
    }
    return currentVisit;
    
}

#pragma mark Product Categories queries

-(NSMutableArray*)fetchProductCategoriesforCustomer:(SalesWorxVisit*)currentVisit
{
    NSString* categoriesQry=[NSString stringWithFormat:@"SELECT DISTINCT IFNULL(A.Category,'N/A') AS Category,IFNULL(C.Description,'N/A') As [Description], IFNULL(B.Org_ID,'N/A')AS Org_ID,IFNULL(A.Item_No,'N/A') AS Item_No FROM TBL_Product As A INNER JOIN TBL_Customer_Stock_Map As B ON A.Category=B.Product_Category AND B.Customer_ID='%@' AND B.Site_Use_ID='%@' INNER JOIN TBL_Org_Info As C ON B.Org_ID=C.Org_ID ORDER BY B.Org_ID, A.Item_No ASC",currentVisit.visitOptions.customer.Customer_ID,currentVisit.visitOptions.customer.Ship_Site_Use_ID];
    NSLog(@"qry for fetching categories %@",categoriesQry);
    
    NSMutableArray* categoriesArray=[self fetchDataForQuery:categoriesQry];
    NSMutableArray *tempCategoriesarray=[[NSMutableArray alloc]init];
    if (categoriesArray.count>0) {
        NSLog(@"categories array is %@",categoriesArray);
        currentVisit.visitOptions.salesOrder.productCategoriesArray=[[NSMutableArray alloc]init];

        for ( NSInteger i=0;i<categoriesArray.count;i++) {
            
            NSMutableDictionary* currentCategoryDictionary =[categoriesArray objectAtIndex:i];
            
            ProductCategories* currentCategory=[[ProductCategories alloc]init];
            
            currentCategory.Category=[NSString stringWithFormat:@"%@",[currentCategoryDictionary valueForKey:@"Category"]];
            currentCategory.Description=[NSString stringWithFormat:@"%@",[currentCategoryDictionary valueForKey:@"Description"]];
            currentCategory.Org_ID=[NSString stringWithFormat:@"%@",[currentCategoryDictionary valueForKey:@"Org_ID"]];
            currentCategory.Item_No=[NSString stringWithFormat:@"%@",[currentCategoryDictionary valueForKey:@"Item_No"]];
            currentCategory.isSelected=@"N";
            
            [tempCategoriesarray addObject:currentCategory];

            
        }
        return tempCategoriesarray;
  
    }
    else
    {
        return nil;
    }
    
}

#pragma mark Templete Methods

-(NSMutableArray*)fetchConventionalTemplate:(NSMutableArray*)templateObjectsArray
{
    
    NSMutableArray* conventionalTemplateArray=[[NSMutableArray alloc]init];
    for (NSInteger i=0; i<templateObjectsArray.count; i++) {
        NSMutableDictionary * currentDict=[[NSMutableDictionary alloc]init];
        SalesOrderTemplete * currentTemplate=[[SalesOrderTemplete alloc]init];
        currentTemplate=[templateObjectsArray objectAtIndex:i];
      
        
        [currentDict setValue:currentTemplate.Order_Template_ID forKey:@"Order_Template_ID"];
        [currentDict setValue:currentTemplate.Template_Name forKey:@"Template_Name"];
        [currentDict setValue:currentTemplate.SalesRep_ID forKey:@"SalesRep_ID"];
        [currentDict setValue:currentTemplate.Custom_Attribute_1 forKey:@"Custom_Attribute_1"];
        [currentDict setValue:currentTemplate.Custom_Attribute_2 forKey:@"Custom_Attribute_2"];
        [currentDict setValue:currentTemplate.Custom_Attribute_3 forKey:@"Custom_Attribute_3"];
        
        [conventionalTemplateArray addObject:currentDict];
    }
    NSLog(@"check conventional data %@",conventionalTemplateArray);
    return conventionalTemplateArray;
}

-(NSMutableArray*)fetchOrderTempletes
{

    NSString* templeteQry=[NSString stringWithFormat:@"select Order_Template_ID, IFNULL(Template_Name,'N/A')  AS Template_Name, IFNULL(SalesRep_ID,'N/A')AS SalesRep_ID,IFNULL(Custom_Attribute_1,'N/A')AS Custom_Attribute_1,IFNULL(Custom_Attribute_2,'N/A')AS Custom_Attribute_2,IFNULL(Custom_Attribute_3,'N/A')AS Custom_Attribute_3  from TBL_Order_Template"];
    NSMutableArray* templeteArray=[self fetchDataForQuery:templeteQry];
    if (templeteArray.count>0) {
        NSMutableArray * salesOrderTempleteArray=[[NSMutableArray alloc]init];
        
        for (NSInteger i=0; i<templeteArray.count; i++) {
            NSMutableDictionary * currentTempleteDict=[templeteArray objectAtIndex:i];
            
            
            SalesOrderTemplete * salesOrderTemplete=[[SalesOrderTemplete alloc]init];

            salesOrderTemplete.Order_Template_ID=[currentTempleteDict valueForKey:@"Order_Template_ID"];
            salesOrderTemplete.Template_Name=[currentTempleteDict valueForKey:@"Template_Name"];
            salesOrderTemplete.SalesRep_ID=[currentTempleteDict valueForKey:@"SalesRep_ID"];
            salesOrderTemplete.Custom_Attribute_1=[currentTempleteDict valueForKey:@"Custom_Attribute_1"];
            salesOrderTemplete.Custom_Attribute_2=[currentTempleteDict valueForKey:@"Custom_Attribute_2"];
            salesOrderTemplete.Custom_Attribute_3=[currentTempleteDict valueForKey:@"Custom_Attribute_3"];
            
            [salesOrderTempleteArray addObject:salesOrderTemplete];
        }
        

        return salesOrderTempleteArray;
    }
    else{
        return nil;
    }
}
-(NSMutableArray*)fetchOrderTempletesForCustomer:(Customer*)customer
{
    
    NSString* templeteQry=[NSString stringWithFormat:@"select Order_Template_ID, IFNULL(Template_Name,'N/A')  AS Template_Name, IFNULL(SalesRep_ID,'N/A')AS SalesRep_ID,IFNULL(Custom_Attribute_1,'N/A')AS Custom_Attribute_1,IFNULL(Custom_Attribute_2,'N/A')AS Custom_Attribute_2,IFNULL(Custom_Attribute_3,'N/A')AS Custom_Attribute_3  from TBL_Order_Template AS A INNER JOIN TBL_Customer_Stock_Map As B ON A.Custom_Attribute_1=B.Product_Category where B.Customer_ID='%@' and B.Site_Use_ID='%@' group by Order_Template_ID",customer.Customer_ID,customer.Ship_Site_Use_ID];
    NSMutableArray* templeteArray=[self fetchDataForQuery:templeteQry];
    if (templeteArray.count>0) {
        NSMutableArray * salesOrderTempleteArray=[[NSMutableArray alloc]init];
        
        for (NSInteger i=0; i<templeteArray.count; i++) {
            NSMutableDictionary * currentTempleteDict=[templeteArray objectAtIndex:i];
            
            
            SalesOrderTemplete * salesOrderTemplete=[[SalesOrderTemplete alloc]init];
            
            salesOrderTemplete.Order_Template_ID=[currentTempleteDict valueForKey:@"Order_Template_ID"];
            salesOrderTemplete.Template_Name=[currentTempleteDict valueForKey:@"Template_Name"];
            salesOrderTemplete.SalesRep_ID=[currentTempleteDict valueForKey:@"SalesRep_ID"];
            salesOrderTemplete.Custom_Attribute_1=[currentTempleteDict valueForKey:@"Custom_Attribute_1"];
            salesOrderTemplete.Custom_Attribute_2=[currentTempleteDict valueForKey:@"Custom_Attribute_2"];
            salesOrderTemplete.Custom_Attribute_3=[currentTempleteDict valueForKey:@"Custom_Attribute_3"];
            
            [salesOrderTempleteArray addObject:salesOrderTemplete];
        }
        
        
        return salesOrderTempleteArray;
    }
    else{
        return nil;
    }
}

-(NSMutableArray*)fetchPriceforTemplateLineItem:(SalesOrderTemplateLineItems*)selectedLineItem
{
    NSString* priceQry=[NSString stringWithFormat:@"select IFNULL(Unit_List_Price,'N/A') AS WholeSale_Price, IFNULL(Unit_Selling_Price,'N/A') AS Unit_Selling_Price from tbl_price_list where Inventory_Item_ID='%@' and Organization_ID='%@'",selectedLineItem.Inventory_Item_ID,[[SWDefaults userProfile] valueForKey:@"Organization_ID"]];
    NSMutableArray* priceArray=[self fetchDataForQuery:priceQry];
    if (priceArray.count>0) {
        
        
        return [priceArray objectAtIndex:0];
    }
    else{
        return nil;
    }
}

-(NSMutableArray*)fetchOrderTemplateLineItems:(SalesOrderTemplete*)selectedTemplate
{
    NSString* templateLineItemsQry=[NSString stringWithFormat:@"select Order_Template_ID,IFNULL(Inventory_Item_ID,'N/A') AS Inventory_Item_ID, IFNULL(Organization_ID,'N/A') AS Organization_ID,IFNULL(Quantity,'0') AS Quantity,IFNULL(Custom_Attribute_1,'') AS UOM from TBL_Order_Template_Items where Order_Template_ID='%@'",selectedTemplate.Order_Template_ID];
    NSLog(@"check iline item qty %@", templateLineItemsQry);
    NSMutableArray * salesOrderLineItemArray=[[NSMutableArray alloc]init];

    NSMutableArray* templateLineItemsArray=[self fetchDataForQuery:templateLineItemsQry];
    if (templateLineItemsArray.count>0) {
        NSMutableArray* lineItemsArray=[[NSMutableArray alloc]init];
        
        for (NSInteger i=0; i<templateLineItemsArray.count; i++)
        {
            
            //fetch product details
            
            NSDictionary *templateLineItem=[templateLineItemsArray objectAtIndex:i];
            
            NSMutableArray* productDetails=[self fetchProductDetailsForInventoryItemId:[templateLineItem valueForKey:@"Inventory_Item_ID"] AndOrganization_Id:[templateLineItem valueForKey:@"Organization_ID"]] ;
            if(productDetails.count>0)
            {
                
                NSString *selectedUOM=[templateLineItem valueForKey:@"UOM"];
                SalesOrderLineItem * salesOrderLineItem=[[SalesOrderLineItem alloc]init];
                salesOrderLineItem.OrderProduct=[productDetails objectAtIndex:0];
                
                /** if selectedUOM is nulll keeping primary uom as selected uom . if selected uom different from primary uom updating UOM*/
                if(selectedUOM.length>0 && ![salesOrderLineItem.OrderProduct.selectedUOM isEqualToString:selectedUOM])
                {
                    salesOrderLineItem.OrderProduct.selectedUOM=selectedUOM;
                }
                
                salesOrderLineItem.manualFocProduct=nil;
                salesOrderLineItem.manualFOCQty=0.0;
                salesOrderLineItem.bonusProduct=nil;
                salesOrderLineItem.defaultBonusQty=0.0;
                salesOrderLineItem.requestedBonusQty=0.0;
                salesOrderLineItem.productDiscount=0.0;
                salesOrderLineItem.productSelleingPrice=0.0;
                salesOrderLineItem.OrderProductQty=[NSDecimalNumber ValidDecimalNumberWithString:[SWDefaults getValidStringValue:[templateLineItem valueForKey:@"Quantity" ]]];
                salesOrderLineItem.OrderItemTotalamount=[NSDecimalNumber zero];
                salesOrderLineItem.OrderItemDiscountamount=[NSDecimalNumber zero];
                salesOrderLineItem.OrderItemNetamount=[NSDecimalNumber zero];
                salesOrderLineItem.appliedDiscount=0.0;
                salesOrderLineItem.bonusDetailsArray=[[NSMutableArray alloc]init];
                salesOrderLineItem.NotesStr=@"";
                [salesOrderLineItemArray addObject:salesOrderLineItem];
            }
        }
        

    return salesOrderLineItemArray;
    }
    else
    {
        return nil;
    }
}
//NSLog(@"qry for products %@",[NSString stringWithFormat:kSQLProductListFromCategoriesNA, category.Org_ID, category.Category]);

-(NSMutableArray*)fetchRecommendedOrderTemplateLineItemsForcustomer:(Customer *)customerDetails andCategory:(ProductCategories *)category AndCurrentVisitDcItems:(NSMutableArray *)dcItems
{
    NSString* ROLineItemsQry=[NSString stringWithFormat:@"Select IFNULL(RO.Custom_Attribute_1,'') AS UOM,RO.qty AS Qty,IFNULL(sum( Lot_Qty),0) AS Lot_Qty,P.* from TBL_Customer_RO as RO  inner join TBL_Product as P on RO.Inventory_Item_ID=P.Inventory_Item_ID left JOIN TBL_Product_Stock As B ON P.Inventory_Item_ID=B.Item_ID AND B.Org_ID='%@' and  RO.Organization_ID=P.Organization_ID where RO.Customer_Id='%@' and RO.Site_Use_ID='%@' and p.Category='%@'  GROUP BY P.Inventory_Item_ID",category.Org_ID,customerDetails.Ship_Customer_ID,customerDetails.Ship_Site_Use_ID,category.Category];
    NSLog(@"customer ro query is %@", ROLineItemsQry);
    
    
    NSMutableArray * salesOrderLineItemArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* ROLineItemsArray=[self fetchDataForQuery:ROLineItemsQry];
    if (ROLineItemsArray.count>0) {
        for (NSInteger i=0; i<ROLineItemsArray.count; i++)
        {
            //fetch product details
            NSDictionary * roLineItem=[ROLineItemsArray objectAtIndex:i];
            NSMutableArray* productDetails=[self fetchProductDetailsForInventoryItemId:[roLineItem valueForKey:@"Inventory_Item_ID"] AndOrganization_Id:[roLineItem valueForKey:@"Organization_ID"]] ;
            if(productDetails.count>0)
            {
                NSString *selectedUOM=[roLineItem valueForKey:@"UOM"];
                SalesOrderLineItem * salesOrderLineItem=[[SalesOrderLineItem alloc]init];
                salesOrderLineItem.OrderProduct=[productDetails objectAtIndex:0];
                /** if selectedUOM is nulll keeping primary uom as selected uom . if selected uom different from primary uom updating UOM*/
                if(selectedUOM.length>0 && ![salesOrderLineItem.OrderProduct.selectedUOM isEqualToString:selectedUOM])
                {
                    salesOrderLineItem.OrderProduct.selectedUOM=selectedUOM;
                }
                salesOrderLineItem.manualFocProduct=nil;
                salesOrderLineItem.manualFOCQty=0.0;
                salesOrderLineItem.bonusProduct=nil;
                salesOrderLineItem.defaultBonusQty=0.0;
                salesOrderLineItem.requestedBonusQty=0.0;
                salesOrderLineItem.productDiscount=0.0;
                salesOrderLineItem.productSelleingPrice=0.0;
                @try {
                    
                    salesOrderLineItem.OrderProductQty=[NSDecimalNumber decimalNumberWithString:[SWDefaults getValidStringValue:[roLineItem valueForKey:@"Qty"]]];
                }@catch (NSException *exception)
                {
                    NSLog(@"Invalid number");
                }
                salesOrderLineItem.OrderItemTotalamount=[NSDecimalNumber zero];
                salesOrderLineItem.OrderItemDiscountamount=[NSDecimalNumber zero];
                salesOrderLineItem.OrderItemNetamount=[NSDecimalNumber zero];
                salesOrderLineItem.appliedDiscount=0.00;
                salesOrderLineItem.bonusDetailsArray=[[NSMutableArray alloc]init];
                salesOrderLineItem.NotesStr=@"";
                /*AND self.ROOrgID==%@*/
                NSPredicate *DcProductQtyPredicate=[NSPredicate predicateWithFormat:@"self.DCItemID == %@ ",[roLineItem valueForKey:@"Inventory_Item_ID"]/*,[roLineItem valueForKey:@"Organization_ID"]*/];
                NSMutableArray * tempDcProductsArray=[[NSMutableArray alloc]initWithArray:[dcItems filteredArrayUsingPredicate:DcProductQtyPredicate]];
                
                if(tempDcProductsArray.count>0)/** if DC Qty specified in the current visit*/
                {
                    NSString *dcItemBalQty=[[tempDcProductsArray valueForKey:@"BalQty"] objectAtIndex:0];
                    if([dcItemBalQty doubleValue]>0)
                    {
                        @try {

                        salesOrderLineItem.OrderProductQty=[NSDecimalNumber decimalNumberWithString:dcItemBalQty];
                        [salesOrderLineItemArray addObject:salesOrderLineItem];
                        }@catch (NSException *exception)
                        {
                            NSLog(@"Invalid number");
                        }
                    }
                }
                else
                {
                    [salesOrderLineItemArray addObject:salesOrderLineItem];
                }
            }
        }
        return salesOrderLineItemArray;
    }
    else
    {
        return [[NSMutableArray alloc]init];
    }
}


-(NSMutableArray*)fetchDCReOrderItemsForCustomer:(Customer *)customerDetails andCategory:(ProductCategories *)category AndCurrentVisitDcItems:(NSMutableArray *)dcItems
{
    NSString *ROLineItemsQry = [NSString stringWithFormat:@"Select IFNULL(sum( Lot_Qty),0) AS Lot_Qty, P.* from TBL_Distribution_Check_Items as DCI inner join TBL_Distribution_Check as DC on DCI.DistributionCheck_ID = DC.DistributionCheck_ID inner join TBL_Product as P on DCI.Inventory_Item_ID = P.Inventory_Item_ID left JOIN TBL_Product_Stock As B ON P.Inventory_Item_ID = B.Item_ID AND B.Org_ID = '%@' where DC.Customer_Id = '%@' and DC.Site_Use_ID = '%@' and P.Category = '%@' GROUP BY P.Inventory_Item_ID",category.Org_ID, customerDetails.Ship_Customer_ID, customerDetails.Ship_Site_Use_ID, category.Category];
    
    NSLog(@"customer DC RO query is %@", ROLineItemsQry);
    
    NSMutableArray * salesOrderLineItemArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* ROLineItemsArray=[self fetchDataForQuery:ROLineItemsQry];
    if (ROLineItemsArray.count>0) {
        for (NSInteger i=0; i<ROLineItemsArray.count; i++)
        {
            //fetch product details
            NSDictionary * roLineItem=[ROLineItemsArray objectAtIndex:i];
            NSMutableArray* productDetails=[self fetchProductDetailsForInventoryItemId:[roLineItem valueForKey:@"Inventory_Item_ID"] AndOrganization_Id:[roLineItem valueForKey:@"Organization_ID"]] ;
            if(productDetails.count>0)
            {
                NSString *selectedUOM=[roLineItem valueForKey:@"UOM"];
                SalesOrderLineItem * salesOrderLineItem=[[SalesOrderLineItem alloc]init];
                salesOrderLineItem.OrderProduct=[productDetails objectAtIndex:0];
                /** if selectedUOM is nulll keeping primary uom as selected uom . if selected uom different from primary uom updating UOM*/
                if(selectedUOM.length>0 && ![salesOrderLineItem.OrderProduct.selectedUOM isEqualToString:selectedUOM])
                {
                    salesOrderLineItem.OrderProduct.selectedUOM=selectedUOM;
                }
                salesOrderLineItem.manualFocProduct=nil;
                salesOrderLineItem.manualFOCQty=0.0;
                salesOrderLineItem.bonusProduct=nil;
                salesOrderLineItem.defaultBonusQty=0.0;
                salesOrderLineItem.requestedBonusQty=0.0;
                salesOrderLineItem.productDiscount=0.0;
                salesOrderLineItem.productSelleingPrice=0.0;
                salesOrderLineItem.OrderProductQty=[NSDecimalNumber zero];
                salesOrderLineItem.OrderItemTotalamount=[NSDecimalNumber zero];
                salesOrderLineItem.OrderItemDiscountamount=[NSDecimalNumber zero];
                salesOrderLineItem.OrderItemNetamount=[NSDecimalNumber zero];
                salesOrderLineItem.appliedDiscount=0.00;
                salesOrderLineItem.bonusDetailsArray=[[NSMutableArray alloc]init];
                salesOrderLineItem.NotesStr=@"";
                
                NSPredicate *DcProductQtyPredicate=[NSPredicate predicateWithFormat:@"self.Inventory_Item_ID == %@ ",[roLineItem valueForKey:@"Inventory_Item_ID"]];
                NSMutableArray * tempDcProductsArray=[[NSMutableArray alloc]initWithArray:[dcItems filteredArrayUsingPredicate:DcProductQtyPredicate]];
                if(tempDcProductsArray.count>0)
                {
                    [salesOrderLineItemArray addObject:salesOrderLineItem];
                }
            }
        }
        return salesOrderLineItemArray;
    }
    else
    {
        return [[NSMutableArray alloc]init];
    }
}


#pragma mark Parent Orders

-(NSMutableArray*)fetchParentOrdersforVisitID:(SalesWorxVisit*)currentVisit
{
    NSString* parentOrdersQry=[NSString stringWithFormat:@"select Orig_Sys_Document_Ref,Order_Amt from TBL_Order where Visit_ID='%@' and Custom_Attribute_2 ='%@' and Shipping_Instructions='0' and Custom_Attribute_4 is  NULL",currentVisit.Visit_ID,currentVisit.visitOptions.salesOrder.selectedCategory.Category];
    NSLog(@"querry for fetching parent orders %@", parentOrdersQry);
    NSLog(@"query for parent orders %@", parentOrdersQry);
    NSMutableArray* parentOrdersArray=[self fetchDataForQuery:parentOrdersQry];
    NSLog(@"parent orders resp %@", parentOrdersArray);
    if (parentOrdersArray.count>0) {
        return parentOrdersArray;
    }
    else{
        return nil;
    }
}


#pragma mark Product Categories
#pragma mark Product Categories

- (NSArray *)fetchProductsforCategory:(ProductCategories *)category AndCustomer:(Customer *)customer andTransactionType:(NSString *)transactionTypeString{
    
    
    NSLog(@"product category received is %@", category);
    AppControl *appControl = [AppControl retrieveSingleton];
  //  NSLog(@"qry for products %@",[NSString stringWithFormat:kSQLProductListFromCategoriesNA, category.Org_ID, category.Category]);
    NSMutableArray *productsArray=[[NSMutableArray alloc]init];
    
    NSMutableArray *tempProductsArray=[[NSMutableArray alloc]init];
    
    if([appControl.SHOW_ORG_STOCK_ONLY isEqualToString:KAppControlsYESCode])
    {
        tempProductsArray=[self fetchDataForQuery:[NSString stringWithFormat:kSQLProductListFromCategoriesNA, category.Org_ID, category.Category]];
        
        NSLog(@"query for products of category in database manager %@", [NSString stringWithFormat:kSQLProductListFromCategoriesNA, category.Org_ID, category.Category]);

    }
    else
    {
       tempProductsArray= [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductListFromCategoriesAllWareHouses,category.Category]];
        
        NSLog(@"query for products of category in database manager %@", [NSString stringWithFormat:kSQLProductListFromCategoriesAllWareHouses,category.Category]);

    }
 
    
    
    NSMutableArray *AssttBonusAvailableOfferProductCodes=[[NSMutableArray alloc]init];
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode])
    {
        if([appControl.ENABLE_BONUS_GROUPS isEqualToString:KAppControlsYESCode])
        {
            AssttBonusAvailableOfferProductCodes=[[self fetchDataForQuery:[NSString stringWithFormat:@"Select Item_Code from TBL_BNS_Assortment_Items where  Is_Get_Item='N' AND Assortment_Plan_ID IN (select Assortment_Plan_ID from TBL_BNS_Assortment_Slabs) AND Assortment_Plan_ID IN(Select Bonus_Plan_ID from TBL_Customer_Bonus_Map where Customer_ID='%@' AND Site_Use_ID='%@' AND Plan_Type='Assortment')",customer.Customer_ID,customer.Site_Use_ID]] mutableCopy];
            if(AssttBonusAvailableOfferProductCodes.count>0)
            AssttBonusAvailableOfferProductCodes=[AssttBonusAvailableOfferProductCodes valueForKey:@"Item_Code"];
        }
        else{
            AssttBonusAvailableOfferProductCodes=[[self fetchDataForQuery:@"select Item_Code from TBL_BNS_Assortment_Items where  Is_Get_Item='N' AND Assortment_Plan_ID IN (select Assortment_Plan_ID from TBL_BNS_Assortment_Slabs)"] mutableCopy];
            if(AssttBonusAvailableOfferProductCodes.count>0)
            AssttBonusAvailableOfferProductCodes=[AssttBonusAvailableOfferProductCodes valueForKey:@"Item_Code"];

        }
    }

    
    
   // NSLog(@"products of category in database manager %@", [tempProductsArray description]);
    
    if (tempProductsArray.count>0) {
        
        for (NSInteger i=0; i<tempProductsArray.count;i++) {
            NSMutableDictionary * currentProductDict=[tempProductsArray objectAtIndex:i];
            
            Products * currentProduct=[[Products alloc]init];
            
            
            currentProduct.RESTRICTED_FOR_RETURN=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"RESTRICTED_FOR_RETURN"]];
            
           

            currentProduct.RESTRICTED=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"RESTRICTED"]];
            currentProduct.Brand_Code=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Brand_Code"]];
            currentProduct.selectedUOM=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Primary_UOM_Code"]];
            currentProduct.Discount=[NSString stringWithFormat:@"%0.2f",[[currentProductDict valueForKey:@"Discount"] doubleValue]];
            currentProduct.Agency=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Agency"]];
            currentProduct.Category=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Category"]];
            currentProduct.Description=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Description"]];
            currentProduct.IsMSL=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"IsMSL"]];
            currentProduct.ItemID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"ItemID"]];
            currentProduct.Item_Code=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Item_Code"]];
            currentProduct.OrgID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"OrgID"]];
            currentProduct.Sts=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Sts"]];
            currentProduct.Inventory_Item_ID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"ItemID"]];
            currentProduct.bonus=[SWDefaults getValidStringValue: [currentProductDict valueForKey:@"Bonus"]];
            currentProduct.stock=[SWDefaults getValidStringValue: [currentProductDict valueForKey:@"Lot_Qty"]];
            currentProduct.nearestExpiryDate=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Expiry_Date"]];
            currentProduct.isSellingPriceFieldEditable=[[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Control_1"]]isEqualToString:@"Y"]?KAppControlsYESCode:KAppControlsNOCode;
            currentProduct.primaryUOM=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Primary_UOM_Code"]];
            currentProduct.ProductBarCode=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:[currentProductDict valueForKey:@"ProductBarCode"]];

//            if ([currentProduct.RESTRICTED_FOR_RETURN isEqualToString:KAppControlsYESCode]) {
//
//                NSLog(@"This product is restricted for return %@",currentProduct.Description);
//            }
            currentProduct.REMOVE_IN_FOC_LIST=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"REMOVE_IN_FOC_LIST"]];

            if(AssttBonusAvailableOfferProductCodes.count>0 && [AssttBonusAvailableOfferProductCodes containsObject:currentProduct.Item_Code])
                currentProduct.bonus=@"1"; /** Assigning dummy value to know the bonus availablity*/
            
            if([[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Promo_Item"]]isEqualToString:KAppControlsYESCode])
            {
                NSArray *validPromoProductArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from  TBL_Product_Addl_Info Where Attrib_Name='PV' and date('now') between  Custom_Attribute_1 and Custom_Attribute_2  and Inventory_Item_ID='%@' and Organization_ID='%@'",currentProduct.Inventory_Item_ID,currentProduct.OrgID]];
                if(validPromoProductArray.count>0)
                    currentProduct.Promo_Item=KAppControlsYESCode;
                else
                    currentProduct.Promo_Item=KAppControlsNOCode;
                
            }
            else
            {
                currentProduct.Promo_Item=KAppControlsNOCode;
            }
        
            if(currentProduct.primaryUOM.length>0)
            {
                if ([transactionTypeString isEqualToString:kReturnTransaction] && [currentProduct.RESTRICTED_FOR_RETURN isEqualToString:KAppControlsYESCode]) {
                    
                }
                else
                {
                    [productsArray addObject:currentProduct];
                }
                
                
//                if (![[NSString getValidStringValue:[self isItemRestrictedForReturn:currentProduct.Inventory_Item_ID]] isEqualToString:KAppControlsYESCode]) {
//                [productsArray addObject:currentProduct];
//                }
            }
        }
    }
    
    return productsArray;
}
#pragma mark Product Categories

- (NSArray *)fetchProductsforCategoryWithRanking:(ProductCategories *)category AndCustomer:(Customer *)customer andTransactionType:(NSString*)transactionTypeString{

    NSDate* productsFetchingTime= [NSDate date];
    
    
    NSLog(@"product category received is %@", category);
    AppControl *appControl = [AppControl retrieveSingleton];
    NSLog(@"qry for products %@",[NSString stringWithFormat:kSQLProductListfromCategorywithRankingAndStockWithAllwareHouses,category.Category]);
    NSMutableArray *productsArray=[[NSMutableArray alloc]init];
    
    NSMutableArray *tempProductsArray=[[NSMutableArray alloc]init];
    
    if([appControl.SHOW_ORG_STOCK_ONLY isEqualToString:KAppControlsYESCode])
    {
        tempProductsArray=[self fetchDataForQuery:[NSString stringWithFormat:kSQLProductListfromCategorywithRanking, category.Org_ID, category.Category]];
    }
    else
    {
        tempProductsArray= [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductListfromCategorywithRankingAndStockWithAllwareHouses,category.Category]];
    }
    
  
    
   // NSLog(@"fetch products in SO qry %@",[NSString stringWithFormat:kSQLProductListfromCategorywithRankingAndStockWithAllwareHouses,category.Category]);
    
    NSMutableArray *AssttBonusAvailableOfferProductCodes=[[NSMutableArray alloc]init];
    if([appControl.ENABLE_ASSORTMENT_BONUS isEqualToString:KAppControlsYESCode])
    {
        if([appControl.ENABLE_BONUS_GROUPS isEqualToString:KAppControlsYESCode])
        {
            AssttBonusAvailableOfferProductCodes=[[self fetchDataForQuery:[NSString stringWithFormat:@"Select Item_Code from TBL_BNS_Assortment_Items where  Is_Get_Item='N' AND Assortment_Plan_ID IN (select Assortment_Plan_ID from TBL_BNS_Assortment_Slabs) AND Assortment_Plan_ID IN(Select Bonus_Plan_ID from TBL_Customer_Bonus_Map where Customer_ID='%@' AND Site_Use_ID='%@' AND Plan_Type='Assortment')",customer.Customer_ID,customer.Site_Use_ID]] mutableCopy];
            if(AssttBonusAvailableOfferProductCodes.count>0)
                AssttBonusAvailableOfferProductCodes=[AssttBonusAvailableOfferProductCodes valueForKey:@"Item_Code"];
        }
        else{
            AssttBonusAvailableOfferProductCodes=[[self fetchDataForQuery:@"select Item_Code from TBL_BNS_Assortment_Items where  Is_Get_Item='N' AND Assortment_Plan_ID IN (select Assortment_Plan_ID from TBL_BNS_Assortment_Slabs)"] mutableCopy];
            if(AssttBonusAvailableOfferProductCodes.count>0)
                AssttBonusAvailableOfferProductCodes=[AssttBonusAvailableOfferProductCodes valueForKey:@"Item_Code"];
        }
    }
  
    
    //NSLog(@"products of category is %@", [tempProductsArray description]);
    
    if (tempProductsArray.count>0) {
        
        for (NSInteger i=0; i<tempProductsArray.count;i++) {
            NSMutableDictionary * currentProductDict=[tempProductsArray objectAtIndex:i];
            
            Products * currentProduct=[[Products alloc]init];
            
            currentProduct.RESTRICTED=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"RESTRICTED"]];
            
            currentProduct.RESTRICTED_FOR_RETURN=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"RESTRICTED_FOR_RETURN"]];

            
            currentProduct.Brand_Code=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Brand_Code"]];
            currentProduct.selectedUOM=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Primary_UOM_Code"]];
            currentProduct.Discount=[NSString stringWithFormat:@"%0.2f",[[currentProductDict valueForKey:@"Discount"] doubleValue]];
            currentProduct.Agency=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Agency"]];
            currentProduct.Category=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Category"]];
            currentProduct.Description=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Description"]];
            currentProduct.IsMSL=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"IsMSL"]];
            currentProduct.ItemID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"ItemID"]];
            currentProduct.Item_Code=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Item_Code"]];
            currentProduct.OrgID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"OrgID"]];
            currentProduct.Sts=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Sts"]];
            currentProduct.Inventory_Item_ID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"ItemID"]];
            currentProduct.bonus=[SWDefaults getValidStringValue: [currentProductDict valueForKey:@"Bonus"]];
            currentProduct.stock=[SWDefaults getValidStringValue: [currentProductDict valueForKey:@"Lot_Qty"]];
            currentProduct.stock=[currentProduct.stock isEqualToString:@""]?@"0":currentProduct.stock;
            currentProduct.nearestExpiryDate=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Expiry_Date"]];
            currentProduct.isSellingPriceFieldEditable=[[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Control_1"]]isEqualToString:@"Y"]?KAppControlsYESCode:KAppControlsNOCode;
            currentProduct.primaryUOM=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Primary_UOM_Code"]];

            currentProduct.ProductBarCode=[SWDefaults getValidStringAndReplaceEmptyStringWithDefaultValue:[currentProductDict valueForKey:@"ProductBarCode"]];

            
            currentProduct.REMOVE_IN_FOC_LIST=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"REMOVE_IN_FOC_LIST"]];
            
            

            if(AssttBonusAvailableOfferProductCodes.count>0 && [AssttBonusAvailableOfferProductCodes containsObject:currentProduct.Item_Code])
                currentProduct.bonus=@"1"; /** Assigning dummy value to know the bonus availablity*/

            
            if([[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Promo_Item"]]isEqualToString:KAppControlsYESCode])
            {
               NSArray *validPromoProductArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from  TBL_Product_Addl_Info Where Attrib_Name='PV' and date('now') between  Custom_Attribute_1 and Custom_Attribute_2  and Inventory_Item_ID='%@' and Organization_ID='%@'",currentProduct.Inventory_Item_ID,currentProduct.OrgID]];
                if(validPromoProductArray.count>0)
                    currentProduct.Promo_Item=KAppControlsYESCode;
                else
                    currentProduct.Promo_Item=KAppControlsNOCode;
                    
            }
            else
            {
                currentProduct.Promo_Item=KAppControlsNOCode;
            }
            
            if(currentProduct.primaryUOM.length>0)
            {
                if ([transactionTypeString isEqualToString:kReturnTransaction] && [currentProduct.RESTRICTED_FOR_RETURN isEqualToString:KAppControlsYESCode]) {
                    
                }
                else
                {
                    [productsArray addObject:currentProduct];
                }
                
//                if (![[NSString getValidStringValue:[self isItemRestrictedForReturn:currentProduct.Inventory_Item_ID]] isEqualToString:KAppControlsYESCode]) {
//                    [productsArray addObject:currentProduct];
//                }
            }
        }
    }
    
    NSDate * methodFinish=[NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:productsFetchingTime];
    NSLog(@"executionTime for products after loop  = %f", executionTime);
    
    return productsArray;
}

-(NSArray *)dbGetAssignedLotsForOrderId:(NSString*)OrderId AndLineNumber:(NSString *)lineNumber
{
    
    NSArray *tempLotsArray=[self fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Order_Lots where Orig_Sys_Document_Ref='%@' and Line_Number='%@'",OrderId,lineNumber]];
    return tempLotsArray;

}

-(NSArray *)dbGetAssignedLotsForProformaOrderId:(NSString*)OrderId AndLineNumber:(NSString *)lineNumber
{
    
    NSArray *tempLotsArray=[self fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Proforma_Order_lots where Orig_Sys_Document_Ref='%@' and Line_Number='%@'",OrderId,lineNumber]];
    return tempLotsArray;
    
}

-(BOOL)DeleteProformaOrderWithId:(NSString *)orderId
{
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    NSString *DeleteProformaOrderStmt=@"DELETE FROM TBL_Proforma_Order WHERE Orig_Sys_Document_Ref = ?";
    NSArray *deleteProformaOrderParametersArray=[[NSArray alloc]initWithObjects:orderId, nil];
    BOOL isProformaOrderDeleted= [database executeUpdate:DeleteProformaOrderStmt withArgumentsInArray:deleteProformaOrderParametersArray];
    
    NSString *DeleteProformaOrderLineItemsStmt=@"Delete from TBL_Proforma_Order_Line_Items where Orig_Sys_Document_Ref= ?";
    NSArray *deleteProformaOrderLineItemsParametersArray=[[NSArray alloc]initWithObjects:orderId, nil];
    BOOL isProformaOrderLineItemsDeleted= [database executeUpdate:DeleteProformaOrderLineItemsStmt withArgumentsInArray:deleteProformaOrderLineItemsParametersArray];
    
    
    NSString *DeleteProformaOrderLotsStmt=@"Delete from TBL_Proforma_Order_lots where Orig_Sys_Document_Ref=?";
    NSArray *deleteProformaOrderLotsParametersArray=[[NSArray alloc]initWithObjects:orderId, nil];
    BOOL isProformaOrderLotsDeleted= [database executeUpdate:DeleteProformaOrderLotsStmt withArgumentsInArray:deleteProformaOrderLotsParametersArray];
    if(isProformaOrderDeleted && isProformaOrderLineItemsDeleted && isProformaOrderLotsDeleted)
    {
        [database commit];
        [database close];
        return YES;
    }
    [database rollback];
    [database close];

    return NO;

}

-(NSMutableArray *)DBGetRMALotTypes
{
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    NSString *RMALotTypeQry=@"select * from TBL_RMA_Lot_Types";
    FMResultSet *RMALotTypeResults = [database executeQuery:RMALotTypeQry];
    RMALotType *rmaLotType;
    NSMutableArray *tempArray=[[NSMutableArray alloc]init];
    while ([RMALotTypeResults next]) {
        rmaLotType=[[RMALotType alloc]init];
        rmaLotType.lotType=[SWDefaults getValidStringValue:[RMALotTypeResults stringForColumn:@"RMA_Lot_Type"]];
        rmaLotType.Description=[SWDefaults getValidStringValue:[RMALotTypeResults stringForColumn:@"Description"]];
        [tempArray addObject:rmaLotType];
    }

    [database close];
    return tempArray;

}
-(NSMutableArray *)DBGetTBLReasonCodesForPurpose:(NSString*)purpose
{
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    NSString *reasonsQry=@"select * from TBL_Reason_Codes Where Purpose=?";
    
    FMResultSet *reasonResults = [database executeQuery:reasonsQry withArgumentsInArray:[[NSArray alloc] initWithObjects:purpose, nil]];
    
    NSMutableArray *tempArray=[[NSMutableArray alloc]init];
    while ([reasonResults next]) {
        SalesWorxReasonCode *reasonCode=[[SalesWorxReasonCode alloc]init];
        reasonCode.Reasoncode=[SWDefaults getValidStringValue:[reasonResults stringForColumn:@"Reason_Code"]];
        reasonCode.Description=[SWDefaults getValidStringValue:[reasonResults stringForColumn:@"Description"]];
        reasonCode.Purpose=[SWDefaults getValidStringValue:[reasonResults stringForColumn:@"Purpose"]];
        [tempArray addObject:reasonCode];
    }
    
    [database close];
    return tempArray;
    
}
-(NSString *)getLongDescriptionForInventoryItemId:(NSString*)InventoryItemId
{
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    NSString *LongDescriptionQry=@"select * from TBL_Product_Addl_Info where Inventory_Item_ID=? and Attrib_Name='Long_Description'";
    
    FMResultSet *LongDescriptionQryResults = [database executeQuery:LongDescriptionQry withArgumentsInArray:[[NSArray alloc] initWithObjects:InventoryItemId, nil]];
    
    NSString *Description;
    while ([LongDescriptionQryResults next]) {
       Description=[SWDefaults getValidStringValue:[LongDescriptionQryResults stringForColumn:@"Attrib_Value"]];
    }
    
    [database close];
    return Description;
}


-(BOOL)closePlannedVisitwithVisitID:(NSString*)visitID
{
    NSString *temp = kSQLUpdatePlannedVisitStatus;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:@"Y"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:visitID];
    
    
    BOOL status=[self executeNonQueryOne:temp];
    
    return status;
    
}



#pragma  mark open  Visit method

#define kSQLOpenVisit @"SELECT A.Actual_Visit_ID,A.Customer_ID,A.Visit_Start_Date,A.Visit_End_Date FROM  TBL_FSR_Actual_Visits AS A  WHERE  A.Customer_ID = '%@' AND A.Site_Use_ID = '%@' AND A.Visit_End_Date =''"

-(NSMutableArray*)fetchOpenVisitsforOtherCustomers:(NSString*)currentCustomerID
{
    //consoder only current days open visits
    NSMutableArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Custom_Attribute_4 AS Visit_Type,A.Site_Use_ID,A.Actual_Visit_ID,A.FSR_Plan_Detail_ID,A.Customer_ID,B.Customer_Name,A.Visit_Start_Date,A.Visit_End_Date FROM  TBL_FSR_Actual_Visits AS A inner join TBL_Customer_Ship_Address AS B on A.Customer_ID=B.Customer_ID WHERE A.Visit_End_Date = '' and A.Visit_Start_Date like '%%%@%%'",[MedRepQueries fetchDatabaseDateFormat]]];
    
    NSLog(@"open visits qry %@",[NSString stringWithFormat:@"SELECT A.Custom_Attribute_4 AS Visit_Type,A.Site_Use_ID,A.Actual_Visit_ID,A.FSR_Plan_Detail_ID,A.Customer_ID,B.Customer_Name,A.Visit_Start_Date,A.Visit_End_Date FROM  TBL_FSR_Actual_Visits AS A inner join TBL_Customer_Ship_Address AS B on A.Customer_ID=B.Customer_ID WHERE A.Visit_End_Date = '' and A.Visit_Start_Date like '%%%@%%'",[MedRepQueries fetchDatabaseDateFormat]]);
    
    
    //as suggested by harpal on 2018-11-15, if a customer has started a visit today and did not close is for some reason, and starts another visits tomorrow, the previous day visits shall not be considered as open visits and open visit alert shall not be displayed, we should have an app control flag VISIT_CLOSURE_INTERVAL to be used to set the interval if the difference between visit start time and current time is greater then the interval set from the app control we shall not consider it as open visit.
    
    //close off the of open visits
    //this will give open visitds which are not of today
    NSString* openVisitQuery = [NSString stringWithFormat:@"SELECT Actual_Visit_ID,Visit_Start_Date FROM  TBL_FSR_Actual_Visits  WHERE Visit_End_Date = '' and Visit_Start_Date NOT LIKE  '%%%@%%' ",[MedRepQueries fetchDatabaseDateFormat]];
    NSArray* contentArray=[self fetchDataForQuery:openVisitQuery];
    if(contentArray.count>0){
        for(NSInteger i=0;i<contentArray.count;i++){
            NSString* openActualVIsitID = [SWDefaults getValidStringValue:[[contentArray objectAtIndex:i]valueForKey:@"Actual_Visit_ID"]];
            NSLog(@"closing visit with id %@ on %@",openActualVIsitID,[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]);
            [self updateEndTimeWithID:openActualVIsitID];
        }
    }
    if (temp.count>0) {
        //for today's open visits check if the time has crossed the app control limit;
        NSCalendar *sysCalendar = [NSCalendar currentCalendar];
        NSCalendarUnit unitFlags = NSCalendarUnitMinute;
        NSString* visitStartDate=[SWDefaults getValidStringValue:[[temp objectAtIndex:0]valueForKey:@"Visit_Start_Date"]];
        NSDateComponents *visitTime = [sysCalendar components:unitFlags fromDate:[MedRepQueries convertNSStringtoNSDate:visitStartDate]  toDate:[MedRepQueries convertNSStringtoNSDate:[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]]  options:0];
        if([visitTime minute]<[[[AppControl retrieveSingleton] VISIT_CLOSURE_INTERVAL] integerValue]){
            NSMutableDictionary* openVisitDict= [temp objectAtIndex:0];
            NSString* visitType = [SWDefaults getValidStringValue:[openVisitDict valueForKey:@"Visit_Type"]];
            if([visitType isEqualToString:kOnsiteVisitDatabaseTitle]){
                visitType=kOnsiteVisitTitle;
            }else if([visitType isEqualToString:kTelephonicVisitDatabaseTitle]){
                visitType=kTelephonicVisitTitle;
            }
            [openVisitDict setValue:visitType forKey:@"Visit_Type"];
            NSMutableArray * responseArray = [[NSMutableArray alloc]init];
            [responseArray addObject:openVisitDict];
            return [responseArray objectAtIndex:0];
        }else{
            //the time interval has passed, close the open visits for today
//            for(NSInteger i=0;i<temp.count;i++){
//                NSString* openActualVIsitID = [SWDefaults getValidStringValue:[[temp objectAtIndex:i]valueForKey:@"Actual_Visit_ID"]];
//                NSLog(@"closing visit with id for current date %@ on %@",openActualVIsitID,[MedRepQueries fetchCurrentDateTimeinDatabaseFormat]);
//                [self updateEndTimeWithID:openActualVIsitID];
//            }
            return [[NSMutableArray alloc]init];
        }

    }
    else
    {
        return nil;
    }
    
}


-(BOOL)updateVisitEndDatewithAlert:(NSString*)visitID
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    NSString *selectSQL =[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Visit_End_Date='%@' WHERE Actual_Visit_ID='%@'",dateString,visitID];
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:selectSQL];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    
    formatter=nil;
    usLocale=nil;
    
    return success;
    
}

-(NSDictionary*)FetchTodayIncompletedPlannedVisitDetailsForCustomer:(Customer*)customer
{
   

    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    NSString *plannedVisitQuery=@"select * from TBL_FSR_Planned_Visits where Customer_ID=? and Site_Use_ID=?  and Visit_Date like ?";
    
    FMResultSet *plannedVisitQueryResults = [database executeQuery:plannedVisitQuery withArgumentsInArray:[[NSArray alloc] initWithObjects:customer.Customer_ID,customer.Ship_Site_Use_ID,[[MedRepQueries fetchDatabaseDateFormat] stringByAppendingString:@"%"], nil]];
    
    NSString *FSRDetailId;
    NSString *PlannedVisitId;
    NSMutableDictionary *visitDetails=[[NSMutableDictionary alloc]init];
    while ([plannedVisitQueryResults next]) {
        FSRDetailId=[SWDefaults getValidStringValue:[plannedVisitQueryResults stringForColumn:@"FSR_Plan_Detail_ID"]];
        PlannedVisitId=[SWDefaults getValidStringValue:[plannedVisitQueryResults stringForColumn:@"Planned_Visit_ID"]];
        [visitDetails setValue:FSRDetailId forKey:@"FSR_Plan_Detail_ID"];
        [visitDetails setValue:PlannedVisitId forKey:@"Planned_Visit_ID"];

    }
    
    [database close];
    
    return visitDetails;
    
}
#pragma markGetOrdersCountForVisit
-(NSInteger)dbGetOrderCountForVisit:(NSString *)visitId{
    NSString* totalOrderQry=[NSString stringWithFormat:@"select *  from TBL_Order_History where Visit_ID='%@' and Doc_Type='I'", visitId];
    NSArray *temp= [self fetchDataForQuery:totalOrderQry];
    return temp.count;
}
-(NSString*)dbGetOrdersValueForVisit:(NSString *)visitId{
    NSString* totalOrderQry=[NSString stringWithFormat:@"select IFNULL(sum( Transaction_Amt),0) AS OrderValue  from TBL_Order_History where Visit_ID='%@' and Doc_Type='I'", visitId];
    NSArray *temp= [self fetchDataForQuery:totalOrderQry];
    if(temp.count==0)
        return @"0";
    return [SWDefaults getValidStringValue:[[temp valueForKey:@"OrderValue"] objectAtIndex:0]];
}

#pragma mark TO DO

+(BOOL)InsertToDoList:(ToDo*)currentToDo
{
    BOOL status =NO;
    
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        documentDir = [filePath path] ;
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    [database open];
    [database beginTransaction];
    
    
        @try {
            
            status =  [database executeUpdate:@"INSERT OR Replace INTO TBL_Todo (Visit_ID,Row_ID,Title, Description,Todo_Date,Inv_Customer_ID,Inv_Site_Use_ID, Created_At,Created_By,Last_Updated_At, Last_Updated_By,Status,Ship_Customer_ID,Ship_Site_Use_ID,Remind_At)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", currentToDo.Visit_ID, currentToDo.Row_ID,currentToDo.Title,currentToDo.Description,currentToDo.Todo_Date,currentToDo.Inv_Customer_ID,currentToDo.Inv_Site_Use_ID,currentToDo.Created_At,currentToDo.Created_By,currentToDo.Last_Updated_At,currentToDo.Last_Updated_By,currentToDo.Status,currentToDo.Ship_Customer_ID,currentToDo.Ship_Site_Use_ID,currentToDo.remindAt,nil];
        }@catch (NSException *exception)
        {
            NSLog(@"Exception while inserting to do: %@", exception.reason);
            status=NO;
        }
        @finally
        {
            //[database close];
        }
    
    
    if (status==YES) {
        
        [database commit];
        [database close];
    }
    else
    {
        [database rollback];
        [database close];
    }

    
    return status;
}



-(NSMutableArray*)fetchTodoforCustomer:(Customer*)customer
{
    
    NSString * toDoQry=[NSString stringWithFormat:@"select IFNULL(Row_ID,'0') AS Row_ID,IFNULL(Title,'0') AS Title,IFNULL(Description,'0') AS Description,IFNULL(Todo_Date,'0') AS Todo_Date,IFNULL(Inv_Customer_ID,'0') AS Inv_Customer_ID,IFNULL(Inv_Site_Use_ID,'0') AS Inv_Site_Use_ID,IFNULL(Created_At,'0') AS Created_At,IFNULL(Created_By,'0') AS Created_By,IFNULL(Last_Updated_At,'0') AS Last_Updated_At,IFNULL(Last_Updated_By,'0') AS Last_Updated_By,IFNULL(Status,'0') AS Status, IFNULL(Ship_Customer_ID,'0') AS Ship_Customer_ID,IFNULL(Ship_Site_Use_ID,'0') AS Ship_Site_Use_ID,IFNULL(Visit_ID,'0') AS Visit_ID, IFNULL(Remind_At,'0') AS Remind_At from TBL_Todo where Inv_Customer_ID ='%@'",customer.Customer_ID];
    
    NSMutableArray *temp= [[self fetchDataForQuery:toDoQry] mutableCopy];
    
    NSMutableArray* todoArray=[[NSMutableArray alloc]init];
    
    if (temp.count>0) {
        
        for (NSInteger i=0; i<temp.count; i++) {
            NSMutableDictionary * currentDict=[temp objectAtIndex:i];
            ToDo * currentToDo=[[ToDo alloc]init];
            currentToDo.Row_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Row_ID"]];
            currentToDo.Title=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Title"]];
            currentToDo.Description=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Description"]];
            currentToDo.Todo_Date=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Todo_Date"]];
            currentToDo.Inv_Customer_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Inv_Customer_ID"]];
            currentToDo.Inv_Site_Use_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Inv_Site_Use_ID"]];
            currentToDo.Created_At=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Created_At"]];
            currentToDo.Created_By=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Created_By"]];
            currentToDo.Last_Updated_At=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Last_Updated_At"]];
            currentToDo.Last_Updated_By=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Last_Updated_By"]];
            currentToDo.Status=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Status"]];
            currentToDo.Ship_Customer_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Ship_Customer_ID"]];
            currentToDo.Ship_Site_Use_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Ship_Site_Use_ID"]];
            currentToDo.Visit_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Visit_ID"]];
            currentToDo.remindAt=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Remind_At"]];

            [todoArray addObject:currentToDo];
        }
        
        
    }
    if (todoArray.count>0) {
        return todoArray;
    }
    else
    {
        return nil;
    }
}

-(NSMutableArray*)fetchTodoList
{
    
    NSString * toDoQry=[NSString stringWithFormat:@"select IFNULL(C.Customer_Name,'0') AS Customer_Name, IFNULL(Row_ID,'0') AS Row_ID,IFNULL(Title,'0') AS Title,IFNULL(Description,'0') AS Description,IFNULL(Todo_Date,'0') AS Todo_Date,IFNULL(Inv_Customer_ID,'0') AS Inv_Customer_ID,IFNULL(Inv_Site_Use_ID,'0') AS Inv_Site_Use_ID,IFNULL(Created_At,'0') AS Created_At,IFNULL(Created_By,'0') AS Created_By,IFNULL(Last_Updated_At,'0') AS Last_Updated_At,IFNULL(Last_Updated_By,'0') AS Last_Updated_By,IFNULL(Status,'0') AS Status, IFNULL(Ship_Customer_ID,'0') AS Ship_Customer_ID,IFNULL(Ship_Site_Use_ID,'0') AS Ship_Site_Use_ID,IFNULL(Visit_ID,'0') AS Visit_ID from TBL_Todo AS A Left join TBL_Customer AS C on C.Customer_ID=A.Inv_Customer_ID AND C.Site_Use_ID=A.Inv_Site_Use_ID "];
    
    NSMutableArray *temp= [[self fetchDataForQuery:toDoQry] mutableCopy];
    
    NSMutableArray* todoArray=[[NSMutableArray alloc]init];
    
    if (temp.count>0) {
        
        for (NSInteger i=0; i<temp.count; i++) {
            NSMutableDictionary * currentDict=[temp objectAtIndex:i];
            ToDo * currentToDo=[[ToDo alloc]init];
            currentToDo.Row_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Row_ID"]];
            currentToDo.Title=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Title"]];
            currentToDo.Description=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Description"]];
            currentToDo.Todo_Date=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Todo_Date"]];
            currentToDo.Inv_Customer_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Inv_Customer_ID"]];
            currentToDo.Inv_Site_Use_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Inv_Site_Use_ID"]];
            currentToDo.Created_At=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Created_At"]];
            currentToDo.Created_By=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Created_By"]];
            currentToDo.Last_Updated_At=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Last_Updated_At"]];
            currentToDo.Last_Updated_By=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Last_Updated_By"]];
            currentToDo.Status=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Status"]];
            currentToDo.Ship_Customer_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Ship_Customer_ID"]];
            currentToDo.Ship_Site_Use_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Ship_Site_Use_ID"]];
            currentToDo.Visit_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Visit_ID"]];
            currentToDo.customerName=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Customer_Name"]];
            [todoArray addObject:currentToDo];
        }
        
        
    }
    if (todoArray.count>0) {
        return todoArray;
    }
    else
    {
        return nil;
    }
}
#pragma mark sync_data_delete methods
-(void)deleteOrdersAndReturnsAfterBackGroundSync:(NSString *)syncStr AndLastSyncTime:(NSString *)lastSync
{
    FMDatabase *db = [self getDatabase];
    [db open];
    [db beginTransaction];
    BOOL success = NO;
    NSMutableArray * bsDeleteQueriesarray=[[NSMutableArray alloc]initWithObjects:
                                           [NSString stringWithFormat:K_BS_SQLdbGetDeleteOrderLots,lastSync,syncStr],
                                           [NSString stringWithFormat:K_BS_SQLdbGetDeleteOrderLineItems,lastSync,syncStr],
                                           [NSString stringWithFormat:K_BS_SQLdbDeleteOrders,lastSync,syncStr],
                                           [NSString stringWithFormat:K_BS_SQLdbDeleteReturnLots,lastSync,syncStr],
                                           [NSString stringWithFormat:K_BS_SQLdbDeleteReturnLiteItems,lastSync,syncStr],
                                           [NSString stringWithFormat:K_BS_SQLdbDeleteReturns,lastSync,syncStr],
                                           nil];
    
    for (NSInteger i=0; i<bsDeleteQueriesarray.count; i++) {
        @try
        {
            success =  [db executeUpdate:[bsDeleteQueriesarray objectAtIndex:i]];
            if(success)
                NSLog(@"Order line items deleted");
        }
        @catch (NSException *exception)
        {
            break;
            NSLog(@"Exception while deleting events from database: %@", exception.reason);
        }
        @finally {
           // [db close];
        }
    }
    
    [db commit];
    [db close];


}



-(BOOL)updateOnsiteExceptionReason:(SalesWorxVisit*)currentVisit
{
    NSString *selectSQL =[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Custom_Attribute_5='%@' WHERE Actual_Visit_ID='%@'",currentVisit.onSiteExceptionReason,[SWDefaults currentVisitID]];
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:selectSQL];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    
    
    return success;
    
}

-(NSString*)fetchCustomerLocationRange:(Customer*)selectedCustomer
{
    
    
    NSString* rangeQry=[NSString stringWithFormat:@"select Attrib_Value AS Range from TBL_Customer_Addl_Info where Attrib_Name ='CUST_LOC_RNG' and  Customer_ID ='%@' and site_use_ID ='%@' and Is_Ship_To='Y' ",selectedCustomer.Ship_Customer_ID,selectedCustomer.Ship_Site_Use_ID];
    NSLog(@"query for fetching range %@", rangeQry);
    
    NSMutableArray * rangeArray=[self fetchDataForQuery:rangeQry];
    
    if (rangeArray.count>0) {
        
        NSString* range=[[rangeArray objectAtIndex:0] valueForKey:@"Range"];
        
        if ([NSString isEmpty:range]==NO) {
            return range;
        }
        else
        {
            NSString* defaultRange = [NSString getValidStringValue:[[AppControl retrieveSingleton]DEFAULT_GEO_LOCATION_RANGE]];
            return defaultRange;
        }
        
    }
    else
    {
        //if there is no data in TBL_Customer_Addl_Info return the default data from app control
        NSString* defaultRange = [NSString getValidStringValue:[[AppControl retrieveSingleton]DEFAULT_GEO_LOCATION_RANGE]];
        
        return defaultRange;
    }
    
}

-(BOOL)isDistributionCheckAvailable
{
    NSString* distributionCheckQry=@"SELECT A.Description, A.Inventory_Item_ID, A.Primary_UOM_Code ,Item_Code,A.Organization_ID  FROM TBL_Product_MSL AS M  INNER JOIN TBL_Product A ON M.Inventory_Item_ID=A.Inventory_Item_ID AND M.Organization_ID=A.Organization_ID ORDER BY Item_Code ASC";
    NSMutableArray *distributionCheckContentArray=[self fetchDataForQuery:distributionCheckQry];
    if (distributionCheckContentArray.count>0) {
        
        return YES;
    }
    else
    {
        return NO;
    }
}
-(void)InsertFSRNewMessages:(NSArray *)newMessages
{

    
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    
    NSString *InsertFSRMessageQuery=@"insert into TBL_FSR_Messages (Message_ID,SalesRep_ID,Message_Title,Message_Content,Message_Date,Message_Expiry_Date,Message_Read)values(?,?,?,?,?,?,?)";

    
    for (NSInteger i=0; i<newMessages.count; i++) {
        
            NSDictionary *message=[newMessages objectAtIndex:i];
        [message setValue:[self getMessagesDatesInDatabseDateTimeFormat:[message valueForKey:@"Message_Date"]] forKey:@"Message_Date"];
        
        [message setValue:[self getMessagesDatesInDatabseDateTimeFormat:[message valueForKey:@"Message_Expiry_Date"]] forKey:@"Message_Expiry_Date"];
        
            NSMutableArray *InsertFSRMessageQueryParameters=[[NSMutableArray alloc]initWithObjects:
                                                           [message valueForKey:@"Message_ID"],
                                                           [message valueForKey:@"SalesRep_ID"],
                                                           [message valueForKey:@"Message_Title"],
                                                           [message valueForKey:@"Message_Content"],
                                                           [message valueForKey:@"Message_Date"],
                                                           [message valueForKey:@"Message_Expiry_Date"],
                                                           [message valueForKey:@"Message_Read"],
                                                           nil];
            [database executeUpdate:InsertFSRMessageQuery withArgumentsInArray:InsertFSRMessageQueryParameters];

    }
    [database commit];
    [database close];
    
    

}
#pragma mark CollectionBankNames
-(NSMutableArray*)fetchListValueOfBankNames
{
    NSString* specilizationQry=[NSString stringWithFormat:@"select IFNULL(code_Value,'N/A') AS BANKNAMES from TBL_App_Codes  where code_type ='BANK'"];
    NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:specilizationQry];
    if (contentArray.count>0)
    {
        NSLog(@"bank Locations  are %@", [contentArray description]);
        NSMutableArray* tempArry=[[NSMutableArray alloc]init];
        [tempArry addObject:@"N/A"];
        for (NSInteger i=0; i<contentArray.count; i++)
        {
            [tempArry addObject:[[contentArray valueForKey:@"BANKNAMES"] objectAtIndex:i]];
        }
        return tempArry;
    }
    {
        return 0;
    }
}

#pragma mark Dashboard Data

-(NSMutableArray*)fetchCustomerdataforPlannedVisits
{
//    NSMutableArray *customerListArray=[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT B.Customer_Name ,A.Customer_ID , A.Site_Use_ID,A.Start_Time AS Visit_Start_Time  FROM  TBL_FSR_Planned_Visits AS A INNER JOIN TBL_Customer_Ship_Address  AS B ON A.Customer_ID = B.Customer_ID AND A.Site_Use_ID = B.Site_Use_ID   WHERE A.Visit_Date LIKE  '%%%@%%' ORDER BY A.Start_Time ASC , B.Customer_Name ASC", [MedRepQueries fetchDatabaseDateFormat]]];
//    
//    NSLog(@"customers qry %@",[NSString stringWithFormat:@"SELECT B.Customer_Name ,A.Customer_ID , A.Site_Use_ID,A.Start_Time AS Visit_Start_Time  FROM  TBL_FSR_Planned_Visits AS A INNER JOIN TBL_Customer_Ship_Address  AS B ON A.Customer_ID = B.Customer_ID AND A.Site_Use_ID = B.Site_Use_ID   WHERE A.Visit_Date LIKE  '%%%@%%' ORDER BY A.Start_Time ASC , B.Customer_Name ASC", [MedRepQueries fetchDatabaseDateFormat]]);
//    
    
    NSString * customersQry=[NSString stringWithFormat:@"SELECT A.Sales_District_ID,A.Postal_Code ,A.Location,A.Dept,A.Customer_Segment_ID,B.Customer_NO,B.Customer_barCode, A.Cust_lat,A.Cust_long, A.City,A.Address,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, IFNULL(B.Phone,'N/A') AS Phone,IFNULL(B.Credit_Limit,'0')AS Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, IFNULL(B.Avail_Bal,'0') AS Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID inner join  TBL_FSR_Planned_Visits AS C on      A.Customer_ID = C.Customer_ID AND  A.Site_Use_ID = C.Site_Use_ID  where  C.Visit_Date LIKE  '%%%@%%' AND C.Visit_Status != 'Y' ORDER BY C.Start_Time ASC , A.Customer_Name ASC",[MedRepQueries fetchDatabaseDateFormat]];
    
    
    NSMutableArray *customerListArray=[self fetchDataForQuery:customersQry];
    
                                       
   NSMutableArray* customersArray=[[NSMutableArray alloc]init];
    if (customerListArray.count>0) {
        //doing fast enumeration and modifying the dict so added __strong
        for (NSMutableDictionary   * currentDict in customerListArray) {
            
            NSMutableDictionary   * currentCustDict =[NSDictionary nullFreeDictionaryWithDictionary:currentDict];
            Customer * currentCustomer=[[Customer alloc] init];
            currentCustomer.Address=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Address"]];
            currentCustomer.Allow_FOC=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Allow_FOC"]];
            currentCustomer.Avail_Bal= [SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Avail_Bal"]]];
            currentCustomer.Customer_Name=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Customer_Name"]];
            currentCustomer.Bill_Credit_Period=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Bill_Credit_Period"]];
            currentCustomer.Cash_Cust=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Cash_Cust"]];
            currentCustomer.Chain_Customer_Code=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Chain_Customer_Code"]];
            currentCustomer.City=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"City"]];
            currentCustomer.Contact=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Contact"]];
            currentCustomer.Creation_Date=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Creation_Date"]];
            currentCustomer.Credit_Hold=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Credit_Hold"]];
            currentCustomer.Credit_Limit= [SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Credit_Limit"]]];
            currentCustomer.Cust_Lat=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Cust_Lat"]];
            currentCustomer.Cust_Long=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Cust_Long"]];
            currentCustomer.Cust_Status=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Cust_Status"]];
            currentCustomer.Customer_Barcode=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Customer_Barcode"]];
            currentCustomer.Customer_Class=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Customer_Class"]];
            currentCustomer.Customer_ID=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_ID"]]];
            currentCustomer.Customer_No=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Customer_No"]]];
            currentCustomer.Customer_OD_Status=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Customer_OD_Status"]];
            currentCustomer.Customer_Segment_ID=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Customer_Segment_ID"]];
            currentCustomer.Customer_Type=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Customer_Type"]];
            currentCustomer.Dept=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Dept"]];
            currentCustomer.Location=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Location"]];
            currentCustomer.Phone=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Phone"]];
            currentCustomer.Postal_Code=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Postal_Code"]];
            currentCustomer.Price_List_ID=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Price_List_ID"]];
            currentCustomer.SalesRep_ID=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"SalesRep_ID"]];
            currentCustomer.Sales_District_ID=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Sales_District_ID"]];
            currentCustomer.Ship_Customer_ID=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Customer_ID"]]];
            currentCustomer.Ship_Site_Use_ID=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Ship_Site_Use_ID"]]];
            currentCustomer.Site_Use_ID=[SWDefaults getValidStringValue:[NSString stringWithFormat:@"%@",[currentCustDict valueForKey:@"Site_Use_ID"]]];
            currentCustomer.Trade_Classification=[SWDefaults getValidStringValue:[currentCustDict valueForKey:@"Trade_Classification"]];
            currentCustomer.CUST_LOC_RNG=[SWDefaults getValidStringValue:[SWDefaults getValidStringValue:@"CUST_LOC_RNG"]];
            
            [customersArray addObject:currentCustomer];
            
        }
        
    }
    
    if (customersArray.count>0) {
        return customersArray;
    }
    else{
        return nil;
    }
}

-(NSMutableArray*)fetchSalesTrendInformation
{
    NSDateComponents* components = [[NSCalendar autoupdatingCurrentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];

    NSMutableArray* targetArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items where Month='%ld'",(long)[components month]]];
    NSLog(@"%lu",(unsigned long)[targetArray count]);
    
    if (targetArray.count>0) {
        return targetArray;
    }
    else{
        return nil;
    }
}



-(NSArray *)dbGetTotalOrdersForCurrentMonth{
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString=[formatter stringFromDate:[NSDate date] ];
    NSLog(@"check time stamp %@", dateString);
    
    NSString* totalOrderQry=[NSString stringWithFormat:@"SELECT  COUNT(*) AS Total from TBL_Order_History Where  Creation_Date like  '%%%@%%'  and Doc_Type='I'", dateString];

    NSArray *temp= [self fetchDataForQuery:totalOrderQry];
    
    formatter=nil;
    usLocale=nil;
    
    return temp;
    
}
-(NSArray *)dbGetTotalReturnsForCurrentMonth{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];


    NSString *dateString=[formatter stringFromDate:[NSDate date]];
    
    NSArray *temp= [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT  COUNT(*) AS Total from TBL_Order_History Where Creation_Date like  '%%%@%%' and Doc_Type='R'", dateString]];
    formatter=nil;
    usLocale=nil;
    return temp;
}
//-(NSInteger)fetchTotalTransactionValue:(NSString*)documentType frequency:(BOOL)isMonthly
//{
//    NSDateFormatter *formatter = [NSDateFormatter new];
//    if (isMonthly==YES) {
//        [formatter setDateFormat:@"MM"];
//    }
//    else{
//        [formatter setDateFormat:@"yyyy-MM-dd"];
//    }
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    NSString *dateString=[formatter stringFromDate:[NSDate date] ];
//    NSLog(@"check time stamp %@", dateString);
//    NSString* totalOrderQry=[[NSString alloc]init];
//    if ([documentType isEqualToString:kCollectionDocType]) {
//        totalOrderQry=[NSString stringWithFormat:@"select IFNULL(SUM(Amount),'0') AS Total from TBL_Collection where Collected_On like  '%%%@%%' ",dateString];
//    }
//    else{
//        totalOrderQry=[NSString stringWithFormat:@"SELECT  SUM(Transaction_Amt) AS Total from TBL_Order_History Where  Creation_Date like  '%%%@%%'  and Doc_Type='%@'", dateString,documentType];
//    }
//    NSLog(@"order trnsaction qry %@", totalOrderQry);
//    
//    NSArray *temp= [self fetchDataForQuery:totalOrderQry];
//    
//    if (temp.count>0) {
//        NSInteger count=[[SWDefaults getValidStringValue:[[temp objectAtIndex:0] valueForKey:@"Total"]] integerValue];
//        return count;
//    }
//    
//    formatter=nil;
//    usLocale=nil;
//    
//    return 0;
//}

-(double)fetchTotalTransactionValue:(NSString*)documentType frequency:(BOOL)isMonthly
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    if (isMonthly==YES) {
        [formatter setDateFormat:@"yyyy-MM"];
    }
    else{
        [formatter setDateFormat:@"yyyy-MM-dd"];
    }
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    //[formatter setLocale:usLocale];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];

    NSString *dateString=[formatter stringFromDate:[NSDate date] ];
    NSLog(@"check time stamp %@", dateString);
    NSString* totalOrderQry=[[NSString alloc]init];
    
    if ([documentType isEqualToString:kCollectionDocType]) {
        totalOrderQry=[NSString stringWithFormat:@"select IFNULL(SUM(Amount),'0') AS Total from TBL_Collection  WHERE Collected_On like  '%%%@%%' ",dateString];

    }else if ([documentType isEqualToString:kCollectionDocType] && isMonthly==YES){
        totalOrderQry=[NSString stringWithFormat:@"select IFNULL(SUM(Amount),'0') AS Total from TBL_Collection  WHERE Collected_On like  '%%%@%%' ",dateString];

    }
    else{
        totalOrderQry=[NSString stringWithFormat:@"SELECT  SUM(Transaction_Amt) AS Total from TBL_Order_History WHERE Creation_Date like  '%%%@%%'  and Doc_Type='%@'", dateString,documentType];
  
    }

    
//    
//    if ([documentType isEqualToString:kCollectionDocType]) {
//        
//        if (isMonthly==YES) {
//            totalOrderQry=[NSString stringWithFormat:@"select IFNULL(SUM(Amount),'0') AS Total from TBL_Collection  WHERE Collected_On like  '%%%@%%' ",dateString];
//            
//        }
//        else{
//            totalOrderQry=[NSString stringWithFormat:@"select IFNULL(SUM(Amount),'0') AS Total from TBL_Collection where Collected_On like  '%%%@%%' ",dateString];
//            
//            
//        }
//        
//    }
//    else{
//        
//        if (isMonthly==YES) {
//            totalOrderQry=[NSString stringWithFormat:@"SELECT  SUM(Transaction_Amt) AS Total from TBL_Order_History WHERE Creation_Date like  '%%%@%%'  and Doc_Type='%@'", dateString,documentType];
//        }
//        else{
//            totalOrderQry=[NSString stringWithFormat:@"SELECT  SUM(Transaction_Amt) AS Total from TBL_Order_History Where  Creation_Date like  '%%%@%%'  and Doc_Type='%@'", dateString,documentType];
//            NSLog(@"order qury %@", totalOrderQry);
//            
//        }
//        
//        
//    }
    NSLog(@"order trnsaction qry %@", totalOrderQry);
    
    NSArray *temp= [self fetchDataForQuery:totalOrderQry];
    
    if (temp.count>0) {
        double count=[[SWDefaults getValidStringValue:[[temp objectAtIndex:0] valueForKey:@"Total"]] doubleValue];
        return count;
    }
    
    formatter=nil;
    usLocale=nil;
    
    return 0;
}
#pragma mark Messages 2.0
-(BOOL)InsertV2MessagesIntoDb:(NSDictionary *)messageDetails
{
    FMDatabase *database = [[SWDatabaseManager retrieveManager] getDatabase];
    [database open];
    [database beginTransaction];
    NSString *newMessageInsertionQuery = @"Insert into tbl_msg (Msg_ID,Msg_Title,Msg_Body,Sender_ID,Sender_Name,Sent_At,Expires_At,Status,Logged_At) values (?,?,?,?,?,?,?,?,?)";
    NSString *newSentMessageInsertionQuery = @"Insert into TBL_Msg_Recipients (Msg_Rcpt_ID,Msg_ID,Recipient_ID,Recipient_Name) values (?,?,?,?)";
    
    NSString *messageId=[NSString createGuid];
    NSLog(@"message Details %@",messageDetails);
    NSMutableArray *messageItemParameters=[[NSMutableArray alloc]initWithObjects:
                                           messageId,
                                           /*_txtTitle.text*/[messageDetails valueForKey:@"Msg_Title"],
                                           /*_txtMessage.text*/[messageDetails valueForKey:@"Msg_Body"],
                                           [NSNumber numberWithInteger:[[[SWDefaults userProfile] stringForKey:@"User_ID"] integerValue]] ,
                                           [[SWDefaults userProfile] stringForKey:@"Username"],
                                           [SWDefaults convertDateToDeteWithZeroTimeFormat:[NSDate date]],
                                           [SWDefaults convertDateToDeteWithZeroTimeFormat:[SWDefaults getCommunicationV2DefaultExpiryDate]],
                                           @"Y" ,
                                           [MedRepQueries fetchCurrentDateTimeinDatabaseFormat] ,
                                           nil];
    
    NSLog(@"insert order line items %@", messageItemParameters);
    
    
    BOOL newMessageSaved= [database executeUpdate:newMessageInsertionQuery withArgumentsInArray:messageItemParameters];
    
    BOOL newSentMessageItemSaved;
    
    
    NSArray *recipientDetailsArray=[messageDetails valueForKey:@"recipientDetailsArray"];
    
    for (NSInteger i=0; i<recipientDetailsArray.count; i++) {
        NSDictionary *reciepient=[recipientDetailsArray objectAtIndex:i];
        NSMutableArray *sentMessageItemParameters=[[NSMutableArray alloc]initWithObjects:
                                                   [NSString createGuid],
                                                   messageId,
                                                   [NSNumber numberWithInteger:[[reciepient valueForKey:@"RecipientID"] integerValue]] ,
                                                   [reciepient valueForKey:@"RecipientName"],
                                                   
                                                   nil];
        
        NSLog(@"insert order line items %@", sentMessageItemParameters);
        
        
        newSentMessageItemSaved= [database executeUpdate:newSentMessageInsertionQuery withArgumentsInArray:sentMessageItemParameters];
        
        
        if(!newSentMessageItemSaved)
            break;
    }
    
    if(newMessageSaved && newSentMessageItemSaved)
    {
        [database commit];
        [database close];
        return YES;
    }
    else
    {
        [database rollback];
        [database close];
        return NO;
    }
}

- (NSArray*)dbGetV2FSRSentMessages{
    
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"select Msg_Title,Msg_Body,Sent_At,Expires_At,Msg_ID,Logged_At from  tbl_msg where Sender_ID='%@' AND Parent_Msg_ID is  Null AND Expires_At>date('now')",[NSNumber numberWithInteger:[[[SWDefaults userProfile] stringForKey:@"User_ID"] integerValue]]];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];

        while ([results next])
        {
            
            NSString *Message_Title = [results stringForColumnIndex:0];
            NSString *Message_Content =[results stringForColumnIndex:1];
            NSString *Message_date =[results stringForColumnIndex:5];
            NSString *User_Name = @"";
            
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            
            [tempDict setValue:Message_Title forKey:@"Message_Title"];
            [tempDict setValue:Message_Content forKey:@"Message_Content"];
            [tempDict setValue:Message_date forKey:@"Message_date"];
            [tempDict setValue:User_Name forKey:@"User_Name"];
            [tempDict setValue:[results stringForColumnIndex:5] forKey:@"Logged_At"];

            NSMutableArray *reciepientsNamesArray = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select Recipient_Name from TBL_Msg_Recipients where Msg_ID='%@'",[results stringForColumnIndex:4]]];
            
            reciepientsNamesArray=[reciepientsNamesArray valueForKey:@"Recipient_Name"];
            NSString *joinedReciepientsString = [reciepientsNamesArray componentsJoinedByString:@", "];

            
            [tempDict setValue:joinedReciepientsString forKey:@"SentUserNames"];

            [retVal addObject:tempDict];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
//Communication Module
- (NSArray*)dbGetV2FSRMessages{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"Select C.*,D.Sent_At As Reply_At,D.Msg_Body AS replyMessage from (select A.Sender_Name,A.Msg_ID,A.Sender_ID,A.Msg_Title,A.Msg_Body,A.Logged_At AS Sent_at,A.Expires_At,B.Read_At, B.Recipient_ID,B.Reply_Msg_ID,A.Parent_Msg_ID from tbl_msg AS A INNER join TBL_Msg_Recipients AS B ON A.msg_id=B.msg_id where A.Sender_ID!='%@' AND  A.Expires_At>date('now') order by A.Logged_At desc) As C  Left join tbl_msg AS D on C.Reply_Msg_ID=D.msg_id",[NSNumber numberWithInteger:[[[SWDefaults userProfile] stringForKey:@"User_ID"] integerValue]]];
    // NSString *sql = [NSString stringWithFormat:@"SELECT * FROM TBL_FSR_Messages"];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        int messageDummyNumber=1;
        while ([results next])
        {
            
            NSString* Message_ID=[results stringForColumnIndex:1] ;
            
            NSInteger SalesRep_ID=[[results stringForColumnIndex:2] intValue];
            
            NSString *Message_Title = [results stringForColumnIndex:3];
            NSString *Message_Content = [results stringForColumnIndex:4];
            NSString *Message_date = [SWDefaults getValidStringValue:[results stringForColumnIndex:5]];
            NSString *Message_Expiry_date = [SWDefaults getValidStringValue:[results stringForColumnIndex:6]];
            
            NSString *Message_Read;
            if([SWDefaults getValidStringValue:[results stringForColumnIndex:7]]==nil || [[SWDefaults getValidStringValue:[results stringForColumnIndex:7]] isEqualToString:@""])
            {
                Message_Read = @"N";
            }
            else
            {
                Message_Read = @"Y";
                
            }
            
            NSString *Emp_Code = [SWDefaults getValidStringValue:[results stringForColumnIndex:9]];
            
            
            NSString *parent_Message = [SWDefaults getValidStringValue:[results stringForColumnIndex:10]];
            if(parent_Message.length==0)
            {
                parent_Message=@"N";
            }
            else
            {
                parent_Message=@"Y";

            }
            
            NSString *Reply_Date = [SWDefaults getValidStringValue:[results stringForColumnIndex:11]];
            NSString *Message_Reply =[SWDefaults getValidStringValue: [results stringForColumnIndex:12]];
            
            
            
            FSRMessagedetails *info=[[FSRMessagedetails alloc] initWithMessage_Id:messageDummyNumber SalesRep_ID:SalesRep_ID Message_Title:Message_Title Message_Content:Message_Content Message_date:Message_date Message_Expiry_date:Message_Expiry_date Message_Read:Message_Read Message_Reply:Message_Reply Emp_Code:Emp_Code Reply_Date:Reply_Date msg_Id:Message_ID SalesRep_Name:[results stringForColumnIndex:0] isRepliedMessage:parent_Message];
            messageDummyNumber++;
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}

-(BOOL)InsertV2ReplyMessagesIntoDb:(NSDictionary *)messageDetails
{
    FMDatabase *database = [[SWDatabaseManager retrieveManager] getDatabase];
    [database open];
    [database beginTransaction];
    NSString *newMessageInsertionQuery = @"Insert into tbl_msg (Msg_ID,Msg_Title,Msg_Body,Sender_ID,Sender_Name,Sent_At,Expires_At,Status,Parent_Msg_ID,Logged_At) values (?,?,?,?,?,?,?,?,?,?)";
    
    
    NSString *newRepliedMessageUpdationQuery = @"Update TBL_Msg_Recipients SET Reply_Msg_ID=? Where msg_id=?";
    
    NSString *messageId=[NSString createGuid];
    NSLog(@"message Details %@",messageDetails);
    NSMutableArray *messageItemParameters=[[NSMutableArray alloc]initWithObjects:
                                           messageId,
                                           /*_txtTitle.text*/[messageDetails valueForKey:@"Msg_Title"],
                                           /*_txtMessage.text*/[messageDetails valueForKey:@"Msg_Body"],
                                           [NSNumber numberWithInteger:[[[SWDefaults userProfile] stringForKey:@"User_ID"] integerValue]] ,
                                           [[SWDefaults userProfile] stringForKey:@"Username"],
                                           [MedRepQueries fetchCurrentDateTimeinDatabaseFormat],
                                           [MedRepQueries convertNSDatetoDataBaseDateTimeFormat:[SWDefaults getCommunicationV2DefaultExpiryDate]],
                                           /* [MedRepQueries fetchCurrentDateTimeinDatabaseFormat] ,*/
                                           @"Y" ,
                                           [messageDetails valueForKey:@"Parent_Msg_ID"],
                                           [MedRepQueries fetchCurrentDateTimeinDatabaseFormat] ,
                                           nil];
    
    NSLog(@"insert order line items %@", messageItemParameters);
    
    
    BOOL newMessageSaved= [database executeUpdate:newMessageInsertionQuery withArgumentsInArray:messageItemParameters];
    
    
    
    NSMutableArray *sentMessageItemParameters=[[NSMutableArray alloc]initWithObjects:
                                               messageId,
                                               [messageDetails valueForKey:@"Parent_Msg_ID"],
                                               nil];
    
    NSLog(@"insert order line items %@", sentMessageItemParameters);
    
    
    BOOL newSentMessageItemSaved= [database executeUpdate:newRepliedMessageUpdationQuery withArgumentsInArray:sentMessageItemParameters];
    
    
        NSString *newSentMessageInsertionQuery = @"Insert into TBL_Msg_Recipients (Msg_Rcpt_ID,Msg_ID,Recipient_ID,Recipient_Name) values (?,?,?,?)";
    
        NSMutableArray *sentMessageItemParameters1=[[NSMutableArray alloc]initWithObjects:
                                                   [NSString createGuid],
                                                   messageId,
                                                   [messageDetails valueForKey:@"Reciepient_Id"] ,
                                                   [messageDetails valueForKey:@"Reciepient_Name"],
                                                   
                                                   nil];
        
        NSLog(@"insert order line items %@", sentMessageItemParameters1);
        
        
        newSentMessageItemSaved= [database executeUpdate:newSentMessageInsertionQuery withArgumentsInArray:sentMessageItemParameters1];
    
    if(newMessageSaved && newSentMessageItemSaved && newSentMessageItemSaved)
    {
        [database commit];
        [database close];
        return YES;
    }
    else
    {
        [database rollback];
        [database close];
        return NO;
    }
}
- (BOOL)updateV2MessageReadStatus:(NSString*)messageId{
    
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    NSString *messageReadStausUpdateQuery = @"Update TBL_Msg_Recipients SET Read_At=? Where msg_id=?";

    NSMutableArray *messageReadStausParameters=[[NSMutableArray alloc]initWithObjects:
                                               [MedRepQueries fetchCurrentDateTimeinDatabaseFormat],
                                               messageId,
                                               nil];
    
    NSLog(@"insert order line items %@", messageReadStausParameters);
    
    
    BOOL messageUpdated= [database executeUpdate:messageReadStausUpdateQuery withArgumentsInArray:messageReadStausParameters];
    
    if(messageUpdated )
    {
        [database commit];
        [database close];
        return YES;
    }
    else
    {
        [database rollback];
        [database close];
        return NO;
    }
}

-(void)InsertV2FSRTBLMSGs:(NSArray *)newMessages
{
    
    
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    
    NSString *InsertFSRMessageQuery=@"INSERT INTO TBL_Msg(Msg_ID, Msg_Title, Msg_Body, Sender_ID, Sender_Name,Sent_At, Expires_At, Parent_Msg_ID, Custom_Attribute_1, Custom_Attribute_2,Custom_Attribute_3, Status, Logged_At) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    

    
    
    for (NSInteger i=0; i<newMessages.count; i++) {
        NSDictionary *message=[newMessages objectAtIndex:i];

        
        [message setValue:[self getMessagesDatesInDatabseDateTimeFormat:[message valueForKey:@"Logged_At"]] forKey:@"Logged_At"];
    
        [message setValue:[self getMessagesDatesInDatabseDateTimeFormat:[message valueForKey:@"Sent_At"]] forKey:@"Sent_At"];
        
        if([[SWDefaults getValidStringValue:[message valueForKey:@"Expires_At"]] length]!=0)
        {
            [message setValue:[self getMessagesDatesInDatabseDateTimeFormat:[message valueForKey:@"Expires_At"]] forKey:@"Expires_At"];
        }
        
        
        
        NSMutableArray *InsertFSRMessageQueryParameters=[[NSMutableArray alloc]initWithObjects:
                                                         [[message valueForKey:@"Msg_ID"] uppercaseString],
                                                         [message valueForKey:@"Msg_Title"],
                                                         [message valueForKey:@"Msg_Body"],
                                                         [NSNumber numberWithInteger:[[message valueForKey:@"Sender_ID"] integerValue]],
                                                         [message valueForKey:@"Sender_Name"],
                                                         [message valueForKey:@"Sent_At"],
                                                         [message valueForKey:@"Expires_At"],
                                                         [[SWDefaults getValidStringValue:[message valueForKey:@"Parent_Msg_ID"]] length]==0?[NSNull null]:[[message valueForKey:@"Parent_Msg_ID"] uppercaseString],
                                                         [[SWDefaults getValidStringValue:[message valueForKey:@"Custom_Attribute_1"]] length]==0?[NSNull null]:[message valueForKey:@"Custom_Attribute_1"],
                                                         [[SWDefaults getValidStringValue:[message valueForKey:@"Custom_Attribute_2"]] length]==0?[NSNull null]:[message valueForKey:@"Custom_Attribute_2"],
                                                         [[SWDefaults getValidStringValue:[message valueForKey:@"Custom_Attribute_3"]] length]==0?[NSNull null]:[message valueForKey:@"Custom_Attribute_3"],
                                                         [message valueForKey:@"Status"],
                                                         [message valueForKey:@"Logged_At"],
                                                         nil];
        [database executeUpdate:InsertFSRMessageQuery withArgumentsInArray:InsertFSRMessageQueryParameters];
        
    }
    [database commit];
    [database close];
    
    
    
}
-(void)InsertV2FSRTBLMSGReciepients:(NSArray *)newMessages
{
    
    
    FMDatabase *database = [self getDatabase];
    [database open];
    [database beginTransaction];
    
    
    NSString *InsertFSRMessageQuery=@"INSERT INTO TBL_Msg_Recipients(Msg_Rcpt_ID,Msg_ID, Recipient_ID, Recipient_Name, Read_At, Reply_Msg_ID,Custom_Attribute_1, Custom_Attribute_2, Custom_Attribute_3) values(?,?,?,?,?,?,?,?,?)";
    
    for (NSInteger i=0; i<newMessages.count; i++) {
        NSDictionary *message=[newMessages objectAtIndex:i];
        
        if([[SWDefaults getValidStringValue:[message valueForKey:@"Read_At"]] length]!=0)
        {
            [message setValue:[self getMessagesDatesInDatabseDateTimeFormat:[message valueForKey:@"Read_At"]] forKey:@"Read_At"];
        }

        
        // NSLog(@"refined expiry date is %@", refinedExpiryDate);
        
        
     //   NSString * refinedLastUpdatedAt=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Last_Updated_At"]]];
        
        
        
        NSMutableArray *InsertFSRMessageQueryParameters=[[NSMutableArray alloc]initWithObjects:
                                                         [[message valueForKey:@"Msg_Rcpt_ID"] uppercaseString],
                                                         [[message valueForKey:@"Msg_ID"] uppercaseString],
                                                         [NSNumber numberWithInteger:[[message valueForKey:@"Recipient_ID"] integerValue]],
                                                         [message valueForKey:@"Recipient_Name"],
                                                         [[SWDefaults getValidStringValue:[message valueForKey:@"Read_At"]] length]==0?[NSNull null]:[message valueForKey:@"Read_At"],
                                                         [[SWDefaults getValidStringValue:[message valueForKey:@"Reply_Msg_ID"]] length]==0?[NSNull null]:[[message valueForKey:@"Reply_Msg_ID"] uppercaseString],
                                                         [[SWDefaults getValidStringValue:[message valueForKey:@"Custom_Attribute_1"]] length]==0?[NSNull null]:[message valueForKey:@"Custom_Attribute_1"],
                                                         [[SWDefaults getValidStringValue:[message valueForKey:@"Custom_Attribute_2"]] length]==0?[NSNull null]:[message valueForKey:@"Custom_Attribute_2"],
                                                         [[SWDefaults getValidStringValue:[message valueForKey:@"Custom_Attribute_3"]] length]==0?[NSNull null]:[message valueForKey:@"Custom_Attribute_3"],
                                                         
                                                         nil];
        [database executeUpdate:InsertFSRMessageQuery withArgumentsInArray:InsertFSRMessageQueryParameters];
        
    }
    [database commit];
    [database close];
    
    
    
}

-(NSString*)fetchLastStockSyncDate
{
    NSString* stockSyncQry=@"select max(Last_Updated_At) AS Last_Stock_Sync_Time from TBL_Product_Stock";
    NSMutableArray * stockSyncArray=[self fetchDataForQuery:stockSyncQry];
    if (stockSyncArray.count) {
        NSString* lastSyncTimeStamp=[[stockSyncArray objectAtIndex:0]valueForKey:@"Last_Stock_Sync_Time"];
        if ([lastSyncTimeStamp isKindOfClass:[NSNull class]]){
            return [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
        }else{
            return lastSyncTimeStamp;
        }
        return lastSyncTimeStamp;
    }
    else{
        return [MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    }
}
-(BOOL)updateStockfromStockSync:(NSMutableArray*)productStockArray
{
    
    
    BOOL stockSyncStatus=NO;
    
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        
        documentDir = [filePath path] ;
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    
    
    @try {
        
        for (NSInteger i=0; i<productStockArray.count; i++) {
            
            NSString* stockSyncQry=[[NSString alloc]init];
            
            SalesWorxProductStockSync * currentProductStock=[productStockArray objectAtIndex:i];
            stockSyncQry = @"insert or replace into  TBL_Product_Stock (Custom_Attribute_1, Custom_Attribute_2, Custom_Attribute_3, Custom_Attribute_4, Custom_Attribute_5,Custom_Attribute_6,Expiry_Date,Item_ID,Last_Updated_At,Lot_No,Lot_Qty,Org_ID,Stock_ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            NSMutableArray *stockSyncParametersArray=[[NSMutableArray alloc]initWithObjects: currentProductStock.Custom_Attribute_1,currentProductStock.Custom_Attribute_2,currentProductStock.Custom_Attribute_3,currentProductStock.Custom_Attribute_4,currentProductStock.Custom_Attribute_5,currentProductStock.Custom_Attribute_6,currentProductStock.Expiry_Date,currentProductStock.Item_ID,currentProductStock.Last_Updated_At,currentProductStock.Lot_No,currentProductStock.Lot_Qty,currentProductStock.Org_ID,currentProductStock.Stock_ID, nil];
            
            stockSyncStatus= [database executeUpdate:stockSyncQry withArgumentsInArray:stockSyncParametersArray];
            
        }
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        //[database close];
    }
    
    
    if (stockSyncStatus==YES) {
        [database commit];
    }
    else{
        [database rollback];
    }
    [database close];
    
    return stockSyncStatus;
}

-(NSString *)getMessagesDatesInDatabseDateTimeFormat:(NSString *)inputDate
{
    NSString* refinedDate;
   /* if(inputDate.length==23)
    {
        refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:[SWDefaults getValidStringValue:inputDate]];

    }
    else
    {
        refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss.SS" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:[SWDefaults getValidStringValue:inputDate]];
    }*/
    
    refinedDate=[MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS" destFormat:@"yyyy-MM-dd HH:mm:ss" scrString:[SWDefaults getValidStringValue:inputDate]];

    
    if(refinedDate.length==0)
    {
        return inputDate;
    }
    return refinedDate;
}
#pragma mark Reports
-(NSArray*)fetchSalesSummaryData
{
    NSString *query = @"SELECT A.Orig_Sys_Document_Ref, A.Creation_Date, A.Doc_Type, A.Transaction_Amt, A.Visit_ID, A.End_Time, A.ERP_Ref_No, A.Ship_To_Customer_Id, A.Ship_To_Site_Id, B.Customer_No, B.Customer_Name, C.Cash_Cust, C.Phone FROM TBL_Sales_History as A inner join TBL_Customer_Ship_Address as B on A.Ship_To_Customer_ID = B.Customer_ID and A.Ship_To_Site_ID = B.Site_Use_ID inner join TBL_Customer as C on C.Customer_ID = A.Inv_To_Customer_ID ORDER BY C.Cash_Cust Desc, A.End_Time ASC, A.ERP_Ref_No ASC";
    
    NSArray *temp = [self fetchDataForQuery:query];
    
    return temp;
}

-(NSArray*)fetchCustomerStatementData
{
    /*
    NSString *query = @"select Invoice_Ref_No,max(NetAmount)as NetAmount,sum(PaidAmt) as PaidAmt,max(InvDate) as InvDate, Customer_ID from (SELECT C.Invoice_Ref_No, C.Pending_Amount AS NetAmount, C.Customer_ID AS Customer_ID,  CASE WHEN A.ERP_Status='C' THEN 0 ELSE A.Amount END AS PaidAmt, '0' AS Amount, C.Invoice_Date AS InvDate  FROM TBL_Open_Invoices AS C LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID LEFT JOIN TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No UNION ALL  SELECT B.Orig_Sys_Document_Ref AS Invoice_Ref_No, B.Transaction_Amt As NetAmount, B.Inv_To_Customer_ID As Customer_ID,  CASE WHEN D.ERP_Status='C' THEN 0 ELSE D.Amount END AS PaidAmt, '0' AS Amount, B.Creation_Date AS InvDate FROM TBL_Sales_History AS B LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = B.Inv_To_Customer_ID AND  B.Inv_To_Site_ID = N.Site_Use_ID LEFT JOIN TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID AND B.Orig_Sys_Document_Ref = D.Invoice_No WHERE (B.ERP_Status = 'X') AND B.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices)) as X group by Invoice_Ref_No";
     */
    
    // added new coloum name Due_Date of table name : TBL_Open_Invoices
    NSString *query = @"select Due_Date,Invoice_Ref_No,max(NetAmount)as NetAmount,sum(PaidAmt) as PaidAmt,max(InvDate) as InvDate, Customer_ID from (SELECT C.Due_Date,C.Invoice_Ref_No, C.Pending_Amount AS NetAmount, C.Customer_ID AS Customer_ID, CASE WHEN A.ERP_Status='C' THEN 0 ELSE A.Amount END AS PaidAmt, '0' AS Amount, C.Invoice_Date AS InvDate FROM TBL_Open_Invoices AS C LEFT JOIN TBL_Collection AS N ON N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID LEFT JOIN TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No UNION ALL SELECT C.Due_Date,B.Orig_Sys_Document_Ref AS Invoice_Ref_No, B.Transaction_Amt As NetAmount, B.Inv_To_Customer_ID As Customer_ID, CASE WHEN D.ERP_Status='C' THEN 0 ELSE D.Amount END AS PaidAmt, '0' AS Amount, B.Creation_Date AS InvDate FROM TBL_Sales_History AS B LEFT JOIN TBL_Collection AS N ON N.Customer_ID = B.Inv_To_Customer_ID AND B.Inv_To_Site_ID = N.Site_Use_ID LEFT JOIN TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID LEFT join TBL_Open_Invoices C on C.Invoice_Ref_No= D.Invoice_No AND B.Orig_Sys_Document_Ref = D.Invoice_No WHERE (B.ERP_Status = 'X') AND B.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices as C)) as X group by Invoice_Ref_No,Due_Date,Customer_ID";
    
       NSArray *temp = [self fetchDataForQuery:query];
    
       return temp;
}

-(NSArray*)fetchBlockedCustomerData
{
    NSString *query = @"Select Customer_ID , Customer_Name , Contact , Phone from TBL_Customer where Cust_Status='N' OR Credit_Hold = 'Y'";
    
    NSArray *temp = [self fetchDataForQuery:query];
    
    return temp;
}

-(NSMutableArray*)fetchReviewDocumentsData
{
    NSMutableArray *arrReviewDocuments = [[NSMutableArray alloc]init];
    
    NSString *query = @"SELECT A.Collected_On AS Start_Time, A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(IFNULL(TBL_Doc_Addl_Info.Custom_Attribute_5,'1') *A.Amount) AS Amount, C.Customer_Name AS Customer_Name, C.Customer_No AS Customer_No ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status], C.Customer_ID FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer_Ship_Address AS C ON A.Customer_ID = C.Customer_ID LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO ORDER BY A.Collection_Ref_No ASC";
    [arrReviewDocuments setArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:query]];
    
    NSString *query2 = @"SELECT A.Start_Time, A.Orig_Sys_Document_Ref AS DocNo, A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Return' AS Doctype ,A.Shipping_Instructions, CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status], C.Customer_ID, C.Site_Use_ID FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID = C.Site_Use_ID  WHERE A.Doc_Type = 'R' ORDER BY A.Orig_Sys_Document_Ref ASC";
    [arrReviewDocuments addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:query2]];
    
    NSString *query3 = @"SELECT A.Start_Time, A.Orig_Sys_Document_Ref AS DocNo, A.Creation_Date As DocDate, A.Transaction_Amt AS Amount, C.Customer_Name,'Order' AS Doctype ,A.Shipping_Instructions, CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status], C.Customer_ID, C.Site_Use_ID FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID = C.Site_Use_ID  WHERE A.Doc_Type = 'I' ORDER BY A.Orig_Sys_Document_Ref ASC";
    [arrReviewDocuments addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:query3]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:FALSE];
    [arrReviewDocuments sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    return arrReviewDocuments;
}

-(NSMutableArray*)fetchPaymentSummaryData
{
    NSString *query =@"SELECT IFNULL(I.Amount,'0') AS Collected_Amount, A.Collected_On AS Start_Time, A.Collection_Ref_No AS Collection_Ref_No, A.Collected_On as DocDate, A.Collection_Type, A.Collected_On, A.Customer_ID,(IFNULL(TBL_Doc_Addl_Info.Custom_Attribute_5,'1') *A.Amount) AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Collection_Invoices AS I on A.Collection_ID=I.Collection_ID  LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO ORDER BY  Start_Time DESC";
    
    
    NSMutableArray *arrPaymentSummary = [[SWDatabaseManager retrieveManager]fetchDataForQuery:query];
    
    return arrPaymentSummary;
}
-(NSMutableArray*)fetchTargetVSAchievementReport:(NSString*)incentiveType year:(NSString *)year period:(int)period
{
    NSString *query;
    NSMutableArray *arrTargetVsAchiement;
    if(period == nil && incentiveType!=nil && year!=nil){
        
        query =[NSString stringWithFormat:@"Select Product_Family,IFNULL(Target,0) Target,IFNULL(Sales_Total,0) Sales_Total ,case when IFNULL(Target,0)=0 then 0 else IFNULL(Sales_Total,0)*100.00/Target end as Ach,case when Incentive_Type='M' then IFNULL(Monthly_Rate,0) when Incentive_Type='A' then IFNULL(Annual_Rate,0)  when Incentive_Type='Q' then IFNULL(Quarterly_Rate,0) end as Rate,case when Incentive_Type='M' then IFNULL(Monthly_Rate,0) when Incentive_Type='A' then IFNULL(Annual_Rate,0)  when Incentive_Type='Q' then IFNULL(Quarterly_Rate,0) end * IFNULL(Target,0) as Ideal,IFNULL(B.Incentive_Amt,0) Actual from TBL_Incentives A inner join TBL_Incentive_Details B on A.Incentive_ID=B.Incentive_ID Where Incentive_Type='%@' and Incentive_Year='%@'",incentiveType,year];
    }
    else if(period!=nil)
    {
        query =[NSString stringWithFormat:@"Select Product_Family,IFNULL(Target,0) Target,IFNULL(Sales_Total,0) Sales_Total ,case when IFNULL(Target,0)=0 then 0 else IFNULL(Sales_Total,0)*100.00/Target end as Ach,case when Incentive_Type='M' then IFNULL(Monthly_Rate,0) when Incentive_Type='A' then IFNULL(Annual_Rate,0)  when Incentive_Type='Q' then IFNULL(Quarterly_Rate,0) end as Rate,case when Incentive_Type='M' then IFNULL(Monthly_Rate,0) when Incentive_Type='A' then IFNULL(Annual_Rate,0)  when Incentive_Type='Q' then IFNULL(Quarterly_Rate,0) end * IFNULL(Target,0) as Ideal,IFNULL(B.Incentive_Amt,0) Actual from TBL_Incentives A inner join TBL_Incentive_Details B on A.Incentive_ID=B.Incentive_ID Where Incentive_Type='%@' and Incentive_Year='%@' AND period=%d",incentiveType,year,period];
    }
    if (query!=nil){
        arrTargetVsAchiement = [[SWDatabaseManager retrieveManager]fetchDataForQuery:query];
    }
    if (arrTargetVsAchiement.count > 0)
    {
        return arrTargetVsAchiement;
    }
   
    return nil;
    
}
-(NSMutableArray*)fetchTotalReport:(NSString*)incentiveType year:(NSString *)year period:(int)period
{
    NSString *query;
    
    if(period == nil){
        
        query =[NSString stringWithFormat:@"Select SUM(IFNULL(Target,0)) TotalTarget,SUM(IFNULL(Sales_Total,0)) SalesValue,SUM(case when Incentive_Type='M' then IFNULL(Monthly_Rate,0) when Incentive_Type='A' then IFNULL(Annual_Rate,0)  when Incentive_Type='Q' then IFNULL(Quarterly_Rate,0) end * IFNULL(Target,0)) as Ideal,SUM(IFNULL(B.Incentive_Amt,0)) Actual from TBL_Incentives A inner join TBL_Incentive_Details B on A.Incentive_ID=B.Incentive_ID Where Incentive_Type='%@' and Incentive_Year=%@",incentiveType,year];
    }else{
        query = [NSString stringWithFormat:@"Select SUM(IFNULL(Target,0)) TotalTarget,SUM(IFNULL(Sales_Total,0)) SalesValue,SUM(case when Incentive_Type='M' then IFNULL(Monthly_Rate,0) when Incentive_Type='A' then IFNULL(Annual_Rate,0)  when Incentive_Type='Q' then IFNULL(Quarterly_Rate,0) end * IFNULL(Target,0)) as Ideal,SUM(IFNULL(B.Incentive_Amt,0)) Actual from TBL_Incentives A inner join TBL_Incentive_Details B on A.Incentive_ID=B.Incentive_ID Where Incentive_Type='%@' and Incentive_Year=%@ AND period=%d",incentiveType,year,period];
    }
  NSMutableArray  *arrTargetVsAchiement = [[SWDatabaseManager retrieveManager]fetchDataForQuery:query];
    if (arrTargetVsAchiement.count > 0)
    {
        return arrTargetVsAchiement;
    }
    
    return nil;
    
    
}

-(NSMutableArray*)fetchVisitOptionsFlagsforCustomer:(NSString*)customerID
{
    NSMutableArray * visitoptionsArray=[[NSMutableArray alloc]init];
    NSString * visitOptionsQry=[NSString stringWithFormat:@"select IFNULL(Custom_Attribute_2,'N/A') AS Visit_Options from TBL_Customer_Addl_Info where Attrib_Name ='EXT' and Customer_ID='%@'",customerID];
    visitoptionsArray=[self fetchDataForQuery:visitOptionsQry];
    if (visitoptionsArray.count>0) {
        
        NSString* flagsString=[SWDefaults getValidStringValue:[[visitoptionsArray objectAtIndex:0] valueForKey:@"Visit_Options"]];
        
        NSMutableArray* flagsArray=[[flagsString componentsSeparatedByString:@","] mutableCopy];
        if (flagsArray.count>0) {
            return flagsArray;
        }
    }
    
    return nil;
    
}

-(NSMutableArray*)fetchAccompaniedByForFieldSalesVisit
{
    NSMutableArray * accompaniedByArray=[[NSMutableArray alloc]init];
    NSMutableArray * contentArray=[[NSMutableArray alloc]init];

    NSString * accompaniedByQry=@"select IFNULL(User_ID,'N/A') AS User_ID, IFNULL(Username, 'N/A') AS Username, IFNULL(Password,'N/A') AS Password, IFNULL(PDA_Rights,'N/A') AS PDA_Rights, IFNULL(is_SS,'N/A') As is_SS from Tbl_Users_All where Is_SS NOT IN ('A','N')";
    if ([AppControl.retrieveSingleton.USE_EMPNAME_IN_USERLIST isEqualToString:KAppControlsYESCode]) {
        accompaniedByQry = @"select IFNULL(User_ID,'N/A') AS User_ID, IFNULL(Emp_Name, 'N/A') AS Username, IFNULL(Password,'N/A') AS Password, IFNULL(PDA_Rights,'N/A') AS PDA_Rights, IFNULL(is_SS,'N/A') As is_SS from Tbl_Users_All where Is_SS NOT IN ('A','N') order by Username COLLATE nocase ASC";
    }
    
    accompaniedByArray=[self fetchDataForQuery:accompaniedByQry];
    if (accompaniedByArray.count>0) {
    for (NSMutableDictionary * currentDict in accompaniedByArray) {
        SalesWorxFieldSalesAccompaniedBy * accompaniedBy=[[SalesWorxFieldSalesAccompaniedBy alloc]init];
        accompaniedBy.User_ID=[SWDefaults getValidStringValue:[currentDict valueForKey:@"User_ID"]];
        accompaniedBy.Username=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Username"]];
        accompaniedBy.Password=[SWDefaults getValidStringValue:[currentDict valueForKey:@"Password"]];
        accompaniedBy.PDA_Rights=[SWDefaults getValidStringValue:[currentDict valueForKey:@"PDA_Rights"]];
        accompaniedBy.is_SS=[SWDefaults getValidStringValue:[currentDict valueForKey:@"is_SS"]];
        [contentArray addObject:accompaniedBy];
    }
    }
    
    return contentArray;
}


-(NSMutableArray*)fetchDistributionCheckItemsForcustomer
{
    NSMutableArray * DCLineItemArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* DCLineItemsArray=[self fetchDataForQuery:@"select * from TBL_Product_MSL"];
    if (DCLineItemsArray.count>0) {
        for (NSInteger i=0; i<DCLineItemsArray.count; i++)
        {
            //fetch product details
            NSDictionary * DCLineItem=[DCLineItemsArray objectAtIndex:i];
            NSMutableArray* productDetails=[self fetchProductDetailsForInventoryItemId:[DCLineItem valueForKey:@"Inventory_Item_ID"] AndOrganization_Id:[DCLineItem valueForKey:@"Organization_ID"]] ;
            if(productDetails.count>0)
            {
                DistriButionCheckItem * DCItem=[[DistriButionCheckItem alloc]init];
                DCItem.DcProduct=[productDetails objectAtIndex:0];
                [DCLineItemArray addObject:DCItem];

            }
        }
        return DCLineItemArray;
    }
    else
    {
        return [[NSMutableArray alloc]init];
    }
}

-(NSMutableArray*)fetchDCMinStockOfMSLItems
{
    NSMutableArray *DCLineItemArray = [[NSMutableArray alloc]init];
    
    NSMutableArray *DCMinStockArray = [self fetchDataForQuery:@"select IFNULL(Inventory_Item_ID,'N/A') AS Inventory_Item_ID, IFNULL(Organization_ID, 'N/A') AS Organization_ID, IFNULL(Attrib_Value,'N/A') AS Attrib_Value from TBL_Product_Addl_Info where Attrib_Name = 'DC_MIN_STOCK'"];
    if (DCMinStockArray.count>0) {
        for (NSInteger i=0; i<DCMinStockArray.count; i++)
        {
            //fetch product details
            NSDictionary *DCLineItem = [DCMinStockArray objectAtIndex:i];
            
            DistriButionCheckMinStock *DCItem = [[DistriButionCheckMinStock alloc]init];
            DCItem.InventoryItemID = [SWDefaults getValidStringValue:[DCLineItem valueForKey:@"Inventory_Item_ID"]];
            DCItem.organizationID = [SWDefaults getValidStringValue:[DCLineItem valueForKey:@"Organization_ID"]];
            DCItem.attributeValue = [SWDefaults getValidStringValue:[DCLineItem valueForKey:@"Attrib_Value"]];
            
            [DCLineItemArray addObject:DCItem];
        }
        return DCLineItemArray;
    }
    else
    {
        return [[NSMutableArray alloc]init];
    }
}

-(NSMutableArray *)fetchDistriButionCheckLocations
{
    
    NSMutableArray * DCLocationsArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* DClocArray=[self fetchDataForQuery:@"Select * from TBL_App_Codes  where code_type ='DIST_CHECK_LOCATIONS'"];
    if (DClocArray.count>0) {
        for (NSInteger i=0; i<DClocArray.count; i++)
        {
            //fetch product details
            NSDictionary * DCloc=[DClocArray objectAtIndex:i];
            DistriButionCheckLocation * DCLocation=[[DistriButionCheckLocation alloc]init];
            DCLocation.LocationID=[DCloc valueForKey:@"Code_Value"];
            DCLocation.LocationName=[DCloc valueForKey:@"Code_Description"];
            [DCLocationsArray addObject:DCLocation];
        }
        return DCLocationsArray;
    }
    else
    {
        
        DistriButionCheckLocation * DCLocation=[[DistriButionCheckLocation alloc]init];
        DCLocation.LocationID=@"S";
        DCLocation.LocationName=@"Shelf";
        [DCLocationsArray addObject:DCLocation];
        
        return DCLocationsArray;
    }
    
}

-(NSMutableArray *)getPriceListAvailableUOMSForSalesOrderProduct:(Products *)product AndCustomer:(Customer *)customer
{
    NSMutableArray * productUOMArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* UomArray=[self fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Item_UOM where item_code='%@' and Organization_ID='%@' and  item_uom in (select item_uom from TBL_Price_List where Inventory_Item_ID ='%@' and (Price_List_ID='%@' OR Is_Generic='Y') and Unit_Selling_Price>0 GROUP by Item_UOM)",product.Item_Code,product.OrgID,product.Inventory_Item_ID,customer.Price_List_ID]];
    
    if(UomArray.count>0)
    {
        for (NSInteger i=0; i<UomArray.count; i++) {
            NSDictionary *tempUOM=[UomArray objectAtIndex:i];
            ProductUOM *UOM=[[ProductUOM alloc]init];
            UOM.Item_UOM=[tempUOM valueForKey:@"Item_UOM"];
            UOM.Item_UOM_ID=[tempUOM valueForKey:@"Item_UOM_ID"];
            UOM.Item_Code=[tempUOM valueForKey:@"Item_Code"];
            UOM.Organization_ID=[tempUOM valueForKey:@"Organization_ID"];
            UOM.Conversion=[tempUOM valueForKey:@"Conversion"];
            
            if([UOM.Item_UOM isEqualToString:product.primaryUOM])
            {
                UOM.isPrimaryUOM=@"Y";
                [productUOMArray addObject:UOM];

            }
            else{
                UOM.isPrimaryUOM=@"N";
                [productUOMArray addObject:UOM];
            }
            
        }
    }
    
    return productUOMArray;
}
-(NSMutableArray *)getUOMsForProduct:(Products *)product;
{
    NSMutableArray * productUOMArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* UomArray=[self fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Item_UOM where item_code='%@' and Organization_ID='%@'",product.Item_Code,product.OrgID]];
    
    if(UomArray.count>0)
    {
        for (NSInteger i=0; i<UomArray.count; i++) {
            NSDictionary *tempUOM=[UomArray objectAtIndex:i];
            ProductUOM *UOM=[[ProductUOM alloc]init];
            UOM.Item_UOM=[tempUOM valueForKey:@"Item_UOM"];
            UOM.Item_UOM_ID=[tempUOM valueForKey:@"Item_UOM_ID"];
            UOM.Item_Code=[tempUOM valueForKey:@"Item_Code"];
            UOM.Organization_ID=[tempUOM valueForKey:@"Organization_ID"];
            UOM.Conversion=[SWDefaults getValidStringValue:[tempUOM valueForKey:@"Conversion"]];
            
            if([UOM.Item_UOM isEqualToString:product.primaryUOM])
            {
                UOM.isPrimaryUOM=@"Y";
                
            }
            else{
                UOM.isPrimaryUOM=@"N";
            }
            [productUOMArray addObject:UOM];

        }
    }
    
    if(UomArray.count==0)
    {
        ProductUOM *UOM=[[ProductUOM alloc]init];
        UOM.Item_UOM=product.primaryUOM;
        UOM.Item_UOM_ID=@"1000";
        UOM.Item_Code=product.Item_Code;
        UOM.Organization_ID=product.OrgID;
        UOM.Conversion=@"1";
        UOM.isPrimaryUOM=@"Y";
        
        [productUOMArray addObject:UOM];
    }
    
    return productUOMArray;
}

#pragma mark Beacon Methods

-(NSMutableArray*)fetchCustomerIDwithBeacon:(SalesWorxBeacon*)matchedBeacon
{
    NSString * customerIdQry=[NSString stringWithFormat:@"select IFNULL(Customer_ID,'N/A') AS Customer_ID,IFNULL(Customer_Name,'N/A') AS Customer_Name from TBL_Customer_Ship_Address where Beacon_UUID ='%@' and Beacon_Major ='%@' and Beacon_Minor ='%@'", [SWDefaults getValidStringValue:matchedBeacon.Beacon_UUID],[SWDefaults getValidStringValue:matchedBeacon.Beacon_Major],[SWDefaults getValidStringValue:matchedBeacon.Beacon_Minor]];
    
    NSMutableArray * beaconArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:customerIdQry];
    
    if (beaconArray.count>0) {
        
        return [beaconArray objectAtIndex:0] ;
    }
    else{
        return nil;
    }
    
}

-(NSMutableArray*)fetchBeaconDetailsforCustomers{
    NSString * beaconDetailsQry=[NSString stringWithFormat:@"SELECT Customer_ID,Site_Use_ID, IFNULL(Beacon_UUID,'N/A') AS Beacon_UUID, IFNULL(Beacon_Major,'N/A') AS Beacon_Major,IFNULL(Beacon_Minor,'N/A') AS Beacon_Minor  from TBL_Customer_Ship_Address where Beacon_UUID not null and Beacon_Major not null and beacon_minor not null"];
    
    NSMutableArray * beaconDataArray=[self fetchDataForQuery:beaconDetailsQry];
    if (beaconDataArray.count>0) {
        return beaconDataArray;
    }
    else{
        return nil;
    }
}
#pragma mark PriceLists
-(NSMutableArray*)fetchavAvailableProductPriceLists:(Products*)selectedProd
{
    NSString* priceListQry=[NSString stringWithFormat:@"Select IFNULL(Is_Generic,'N') AS Is_Generic,IFNULL(Price_List_ID,'N/A') AS Price_List_ID,Inventory_Item_ID, IFNULL(Organization_ID,'N/A') AS Organization_ID, Item_UOM,IFNULL(Unit_Selling_Price,'0') AS Unit_Selling_Price, IFNULL(Unit_List_Price,'0') AS Unit_List_Price from TBL_Price_List where Inventory_Item_ID ='%@' and Item_UOM='%@' and Unit_Selling_Price>0",selectedProd.Inventory_Item_ID,selectedProd.selectedUOM];
    
    NSMutableArray* tempPriceListArray=[self fetchDataForQuery:priceListQry];
    NSLog(@"price list array count is %lu", (unsigned long)tempPriceListArray.count);

    return tempPriceListArray;
}
#pragma mark PriceListDescription
-(NSMutableArray*)fetchavAvailableProductPriceListsDescription:(Products*)selectedProd
{
    NSString* priceListQry=[NSString stringWithFormat:@"SELECT B.Description,A.Price_List_ID FROM TBL_Price_List A inner join TBL_Price_List_H B on A.Price_List_Id=B.Price_List_ID where Inventory_Item_ID in(Select Inventory_Item_Id from TBL_Product where Item_Code='%@')",selectedProd.Item_Code];
    
    NSMutableArray* tempPriceListDescriptionArray=[self fetchDataForQuery:priceListQry];
    NSLog(@"price list array count is %lu", (unsigned long)tempPriceListDescriptionArray.count);
    
    return tempPriceListDescriptionArray;
}

#pragma mark Collection Target
-(NSMutableArray*)fetchCollectionTarget:(NSString *)Customer_No{
    
    NSMutableArray *collectionTargetArray = [[NSMutableArray alloc]init];
    NSMutableArray *tempCollectionTargetArray = [[NSMutableArray alloc]init];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]]; // Get necessary date components
    NSString *currentMonth = [NSString stringWithFormat:@"%ld",(long)[dateComponents month]];
    NSString *currentYear = [NSString stringWithFormat:@"%ld",(long)[dateComponents year]];
    if (Customer_No.length == 0) {

        tempCollectionTargetArray = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT IFNULL(SUM(Target_Value),'0') as Target_Value, IFNULL(SUM(Achieved_Value),'0')  AS Achieved_Value FROM TBL_Collection_Target where Target_Month = '%@' and Target_Year = '%@'",currentMonth, currentYear]];
        
        NSLog(@"Colelction Target Query -- %@",[NSString stringWithFormat:@"SELECT IFNULL(SUM(Target_Value),'0') as Target_Value, IFNULL(SUM(Achieved_Value),'0')  AS Achieved_Value FROM TBL_Collection_Target where Target_Month = '%@' and Target_Year = '%@'",currentMonth, currentYear]);
        
        if (tempCollectionTargetArray.count>0) {
            //doing fast enumeration and modifying the dict so added __strong
            for (NSMutableDictionary *currentDict in tempCollectionTargetArray) {
                
                NSMutableDictionary *currentCollectionTargetDict = [NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                CustomerCollectionTarget *currentCollectionTarget = [[CustomerCollectionTarget alloc] init];
     
                currentCollectionTarget.Target_Value = [NSString stringWithFormat:@"%f",[[currentCollectionTargetDict valueForKey:@"Target_Value"]doubleValue]];
                currentCollectionTarget.Achieved_Value = [NSString stringWithFormat:@"%f",[[currentCollectionTargetDict valueForKey:@"Achieved_Value"]doubleValue]];

                [collectionTargetArray addObject:currentCollectionTarget];
            }
        }
        
    } else {
        tempCollectionTargetArray = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT IFNULL(SUM(Target_Value),'0') as Target_Value, IFNULL(SUM(Achieved_Value),'0')  AS Achieved_Value,IFNULL((SUM(Target_Value)-SUM(Achieved_Value)),'0') AS Balance_to_Go,Target_Month,Target_Year  FROM TBL_Collection_Target where Customer_No = '%@' GROUP BY Target_Month,Target_Year Order By Target_Month DESC",Customer_No]];
        
        if (tempCollectionTargetArray.count>0) {
            //doing fast enumeration and modifying the dict so added __strong
            for (NSMutableDictionary *currentDict in tempCollectionTargetArray) {
                
                NSMutableDictionary *currentCollectionTargetDict = [NSDictionary nullFreeDictionaryWithDictionary:currentDict];
                CustomerCollectionTarget *currentCollectionTarget = [[CustomerCollectionTarget alloc] init];
                
                currentCollectionTarget.Customer_No = Customer_No;
                currentCollectionTarget.Target_Year = [NSString stringWithFormat:@"%@",[currentCollectionTargetDict valueForKey:@"Target_Year"]];
                currentCollectionTarget.Target_Value = [NSString stringWithFormat:@"%f",[[currentCollectionTargetDict valueForKey:@"Target_Value"]doubleValue]];
                currentCollectionTarget.Achieved_Value = [NSString stringWithFormat:@"%f",[[currentCollectionTargetDict valueForKey:@"Achieved_Value"]doubleValue]];
                currentCollectionTarget.Balance_ToGo = [NSString stringWithFormat:@"%f",[[currentCollectionTargetDict valueForKey:@"Balance_to_Go"]doubleValue]];
                
                if(currentCollectionTarget.Balance_ToGo<0){
                    currentCollectionTarget.Balance_ToGo=@"0";
                }
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"MM"];
                NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [dateFormatter setLocale:usLocale];
                NSDate * myDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[currentCollectionTargetDict valueForKey:@"Target_Month"]]];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"MMMM"];
                [formatter setLocale:usLocale];
                currentCollectionTarget.Target_Month = [formatter stringFromDate:myDate];
                
                [collectionTargetArray addObject:currentCollectionTarget];
            }
        }
    }

    return  collectionTargetArray;
}


#pragma mark Merchandising Module Queries

-(NSMutableArray*)fetchPlanogramData:(NSString*)brandCode
{
    NSMutableArray* planogramDataArray=[[NSMutableArray alloc]init];
    NSString* planogramQuery = [NSString stringWithFormat:@"Select * from TBL_Media_Files WHERE Entity_ID_1= '%@' AND Download_Flag='N' AND   Is_Deleted='N' AND Custom_Attribute_3 ='2'",brandCode];
    NSLog(@"planogram query is %@", planogramQuery);
    NSMutableArray * contentArray = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:planogramQuery]];
    for (NSMutableDictionary *currentMediaFile in contentArray) {
        
        MerchandisingPlanogram * currentPlanogram=[[MerchandisingPlanogram alloc]init];
        currentPlanogram.isUpdated=KAppControlsNOCode;
        currentPlanogram.imageMatched=KAppControlsNOCode;
        
        MediaFile *file = [[MediaFile alloc]init];
        file.Media_File_ID=[NSString getValidStringValue:[currentMediaFile valueForKey:@"Media_File_ID"]];
        file.Entity_ID_1=[NSString getValidStringValue:[currentMediaFile valueForKey:@"Entity_ID_1"]];
        file.Entity_ID_2=[NSString getValidStringValue:[currentMediaFile valueForKey:@"Entity_ID_2"]];
        file.Media_Type=[NSString getValidStringValue:[currentMediaFile valueForKey:@"Media_Type"]];
        file.Filename=[NSString getValidStringValue:[currentMediaFile valueForKey:@"Filename"]];
        file.Caption=[NSString getValidStringValue:[currentMediaFile valueForKey:@"Caption"]];
        file.Thumbnail=[NSString getValidStringValue:[currentMediaFile valueForKey:@"Thumbnail"]];
        file.Is_Deleted=[NSString getValidStringValue:[currentMediaFile valueForKey:@"Is_Deleted"]];
        file.Download_Flag=[NSString getValidStringValue:[currentMediaFile valueForKey:@"Download_Flag"]];
        currentPlanogram.sourceImage = file;
        
        //in case if user is hoping around withour saving data just check if image is in temp
       // NSString* tempFilePath=
        //[NSFileManager defaultManager]fileExistsAtPath:<#(nonnull NSString *)#>
        
        //currentPlanogram.capturedImageName
        
        
      //  imgView.image=[UIImage imageWithContentsOfFile:[[[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kMerchandisingPlanogramImages]stringByAppendingPathComponent:@"temp"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",planoGram.capturedImageName]]];

        
        
        [planogramDataArray addObject:currentPlanogram];
    }
    return planogramDataArray;
}

-(NSMutableArray*)fetchAppCodeValueforPOS:(NSString*)codeType
{
    NSMutableArray* contentArray=[[NSMutableArray alloc]init];
    NSString* appCodeQry=[NSString stringWithFormat:@"select  Code_Description from TBL_App_Codes  where Code_Type='%@'",codeType];
    NSMutableArray* appCodeArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:appCodeQry];
    if (appCodeArray.count>0) {
        [contentArray addObjectsFromArray:[appCodeArray valueForKey:@"Code_Description"]];
        return contentArray;
    }
    else
    {
        return nil;
    }
}

-(NSMutableArray*)fetchMerchandisingMasterData
{
    NSMutableArray* masterDataArray=[[NSMutableArray alloc]init];
    
    NSString* masterDataQry=[NSString stringWithFormat:@"select M.Inventory_Item_ID,M.Organization_ID,M.Classification,IFNULL(P.Description,'N/A')AS Description,IFNULL(P.Brand_Code,'N/A') AS Brand_Code from TBL_Merch_MSL AS M left join TBL_Product AS P on M.Inventory_Item_ID = P.Inventory_Item_ID where M.Organization_ID ='%@'",[NSString getValidStringValue:[[SWDefaults userProfile] valueForKey:@"Organization_ID"]]];
    NSLog(@"master qry is %@", masterDataQry);
    
    masterDataArray = [self fetchDataForQuery:masterDataQry];
    if (masterDataArray.count>0) {
        return masterDataArray;
    }
    else
    {
        return nil;
    }
}
-(NSMutableArray*)fetchMerchandisingProductsForSelectedBrand:(NSString*)selectedBrandCode withClassification:(SalesWorxVisit*)currentVisit
{
    
    NSString* mslClassification=[[AppControl retrieveSingleton]MERCH_MSL_CLASSIFICATION];
    
    
    NSString * customerMSL=[NSString getValidStringValue:[currentVisit.visitOptions.customer valueForKey:mslClassification]];
    
    NSLog(@"customer MSL while fetching products is %@", customerMSL);
    
    if ([NSString isEmpty:customerMSL] || [customerMSL isEqualToString:KNotApplicable]) {
        
        return nil;
    }
    else{
    //NSString *productsQry = [NSString stringWithFormat:@"select IFNULL(P.Description,'N/A') AS Product_Name,P.Inventory_Item_ID,IFNULL(P.Brand_Code,'N/A') AS Brand_Code from TBL_Merch_MSL AS M left join TBL_Product AS P on  P.Inventory_Item_ID = M.Inventory_Item_ID where M.Organization_ID ='%@' and Brand_Code ='%@'  and M.Classification = '%@'",[[SWDefaults userProfile] valueForKey:@"Organization_ID"],selectedBrandCode,customerMSL];
    
      //orgid is being hard coded to 0 when the backend team is done with orgwise classification of products then it needs to be updated.
        NSString *productsQry = [NSString stringWithFormat:@"select IFNULL(P.Product_Name,'N/A') AS Product_Name,P.Product_ID,IFNULL(P.Brand_Code,'N/A') AS Brand_Code from TBL_Merch_MSL AS M left join PHX_TBL_Products AS P on  P.Product_ID = M.Inventory_Item_ID where M.Organization_ID ='%@' and Brand_Code ='%@'  and M.Classification = '%@'",@"0",selectedBrandCode,customerMSL];
    NSLog(@"products query  in merch is %@",productsQry);
    
    NSMutableArray *productsArray = [self fetchDataForQuery:productsQry];
    
    if (productsArray.count>0) {
        return productsArray;;
    }
    else
    {
        return  nil;
    }
    }
}

-(NSMutableArray*)fetchBrandCodes
{
    NSMutableArray *refinedBrandCodesArray = [[NSMutableArray alloc]init];
   // NSString *brandCodesQry = [NSString stringWithFormat:@"select IFNULL(Brand_Code,'N/A') AS Brand_Code from TBL_Product AS P left join TBL_Merch_MSL AS M ON  P.Inventory_Item_ID = M.Inventory_Item_ID where M.Organization_ID ='%@' group by Brand_Code order by Brand_Code",[[SWDefaults userProfile] valueForKey:@"Organization_ID"]];
    
    //org id is hardcoded to 0 it needs to be changed once the backedn team does org wise classification of field marketing products.
    NSString* brandCodesQry =[NSString stringWithFormat:@"select IFNULL(Brand_Code,'N/A') AS Brand_Code from PHX_TBL_Products AS P left join TBL_Merch_MSL AS M ON  P.Product_ID = M.Inventory_Item_ID where M.Organization_ID ='0' group by Brand_Code order by Brand_Code"];
    
    
    NSLog(@"brand code query is %@", brandCodesQry);
    NSMutableArray *brandCodesArray = [self fetchDataForQuery:brandCodesQry];
    
    if (brandCodesArray.count>0) {
        return [brandCodesArray valueForKey:@"Brand_Code"];;
    }
    else
    {
        return  refinedBrandCodesArray;
    }
}
-(NSMutableArray*)fetchAvailabilityDatawithBrandCode:(NSString*)brandCode andVisit:(SalesWorxVisit*)currentVisit
{
    
    
    NSString* mslClassification=[[AppControl retrieveSingleton]MERCH_MSL_CLASSIFICATION];
    
    
    NSString * customerMSL=[NSString getValidStringValue:[currentVisit.visitOptions.customer valueForKey:mslClassification]];
    
    NSLog(@"customer MSL is %@", customerMSL);
    
    if ([NSString isEmpty:customerMSL] || [customerMSL isEqualToString:KNotApplicable]) {
       
        return nil;
    }
    else{
        
    NSMutableArray *availabilityArray = [[NSMutableArray alloc]init];
       // NSString *avblQry = [NSString stringWithFormat:@"select M.Inventory_Item_ID,M.Organization_ID,M.Classification,IFNULL(P.Description,'N/A')AS Description, IFNULL(P.Brand_Code,'N/A') AS Brand_Code, IFNULL(Previous_Details.Previous_Price,'')AS Previous_Price,IFNULL(Previous_Details.Previous_Stock,'')AS Previous_Stock from TBL_Merch_MSL AS M left join TBL_Product AS P on M.Inventory_Item_ID = P.Inventory_Item_ID left join (Select X.Stock as Previous_Stock,X.Price  as Previous_Price, x.Inventory_Item_ID from TBL_Merch_Product_Availability As X inner join  TBL_Merch_Session s  on X.Session_ID = S.Session_ID where s.Start_Date = (select max(Start_Date) from TBL_Merch_Session)) AS Previous_Details on Previous_Details.Inventory_Item_ID=M.Inventory_Item_ID where M.Organization_ID ='%@' AND P.Brand_Code = '%@' AND M.Classification='%@' order by Description ASC",[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"Organization_ID"]],[NSString getValidStringValue:brandCode],customerMSL];
        
        //org id is being hard coded to 0 and it needs to be updated once the backend team is done with the org wise classification of products.
        NSString *avblQry = [NSString stringWithFormat:@"select M.Inventory_Item_ID,M.Organization_ID,M.Classification,IFNULL(P.Product_Name,'N/A')AS Description, IFNULL(P.Brand_Code,'N/A') AS Brand_Code, IFNULL(Previous_Details.Previous_Price,'')AS Previous_Price,IFNULL(Previous_Details.Previous_Stock,'')AS Previous_Stock from TBL_Merch_MSL AS M left join PHX_TBL_Products AS P on M.Inventory_Item_ID = P.Product_ID left join (Select X.Stock as Previous_Stock,X.Price  as Previous_Price, x.Inventory_Item_ID from TBL_Merch_Product_Availability As X inner join  TBL_Merch_Session s  on X.Session_ID = S.Session_ID where s.Start_Date = (select max(Start_Date) from TBL_Merch_Session)) AS Previous_Details on Previous_Details.Inventory_Item_ID=M.Inventory_Item_ID where M.Organization_ID ='%@' AND P.Brand_Code = '%@' AND M.Classification='%@' order by Description ASC",@"0",[NSString getValidStringValue:brandCode],customerMSL];
        
        NSLog(@"availability qry is %@",avblQry);
        
    
    availabilityArray = [self fetchDataForQuery:avblQry];
    if (availabilityArray.count>0) {
        return availabilityArray;
    }
    else
    {
        return  nil;
    }
    }
}
-(BOOL)insertMerchandisingSessionData:(SalesWorxVisit*)currentVisit
{
    BOOL insertSession = NO;
    FMDatabase *fmdatabase = [FMDatabase databaseWithPath:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    [fmdatabase open];
    [fmdatabase beginTransaction];

    insertSession =  [fmdatabase executeUpdate:@"INSERT INTO TBL_Merch_Session (Session_ID, Actual_Visit_ID, Start_Date, SalesRep_ID,Customer_ID ,Site_Use_ID ) VALUES (?,?,?,?,?,?)",currentVisit.visitOptions.salesWorxMerchandising.Session_ID,currentVisit.Visit_ID,[MedRepQueries fetchCurrentDateTimeinDatabaseFormat],[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"],currentVisit.visitOptions.customer.Customer_ID,currentVisit.visitOptions.customer.Site_Use_ID, nil];
    
    NSLog(@"insert session ? %hhd", insertSession);
    [fmdatabase commit];
    [fmdatabase close];
    return insertSession;
    
}


-(NSMutableDictionary*)fetchFieldSalesVisitContactDetailsForCustomerinCurrentVisit:(SalesWorxVisit*)selectedVisit
{

    
    NSMutableDictionary * detailsDict=[[NSMutableDictionary alloc]init];
    
    NSString* qryStr=[NSString stringWithFormat:@"select Attrib_Value from  TBL_Visit_Addl_Info  where Attrib_Name ='CUST_CONTACT' and Custom_Attribute_1 ='%@' and Custom_Attribute_2='%@' and Visit_ID = '%@' order by Created_At  desc",selectedVisit.visitOptions.customer.Customer_ID,selectedVisit.visitOptions.customer.Ship_Site_Use_ID,selectedVisit.Visit_ID];
    NSLog(@"cust contact query %@", qryStr);

    NSArray* detailsArray=[self fetchDataForQuery:qryStr];
    NSLog(@"contact details is %@",detailsArray );
    if (detailsArray.count>0) {
        
        NSArray* tempArray=[[NSString getValidStringValue:[[detailsArray objectAtIndex:0] valueForKey:@"Attrib_Value"]] componentsSeparatedByString:@","];
        NSString* contactName=[NSString getValidStringValue:tempArray.count>0?[tempArray objectAtIndex:0]:@"N/A"];
        NSString* contactNumber=[[NSString alloc]init];
        if (tempArray.count>1) {
            contactNumber=[NSString getValidStringValue:[tempArray objectAtIndex:1]];
        }
        [detailsDict setObject:contactName forKey:@"Contact_Name"];
        [detailsDict setObject:contactNumber forKey:@"Contact_Number"];
        NSLog(@"temp details in visit addl info %@", detailsDict);
    }
    return detailsDict;
}

-(NSMutableDictionary*)fetchFieldSalesVisitContactDetailsForCustomer:(Customer*)selectedCustomer
{
    NSString* lastVisitID=[[NSString alloc]init];
    NSString* lastVisitIDQry=[NSString stringWithFormat:@"select Actual_Visit_ID from TBL_FSR_Actual_Visits where Customer_ID ='%@' and Site_Use_ID = '%@' order by visit_end_date DESC",selectedCustomer.Customer_ID,selectedCustomer.Ship_Site_Use_ID];
    NSArray* lastVisitArray=[self fetchDataForQuery:lastVisitIDQry];
    if (lastVisitArray.count>0) {
        NSLog(@"last visit details is %@", [[lastVisitArray objectAtIndex:0]valueForKey:@"Actual_Visit_ID"]);
        lastVisitID=[NSString getValidStringValue:[[lastVisitArray objectAtIndex:0]valueForKey:@"Actual_Visit_ID"]];
    }
    
    NSMutableDictionary * detailsDict=[[NSMutableDictionary alloc]init];
    
    NSString* qryStr=[NSString stringWithFormat:@"select Attrib_Value from  TBL_Visit_Addl_Info  where Attrib_Name ='CUST_CONTACT' and Custom_Attribute_1 ='%@' and Custom_Attribute_2='%@' and Visit_ID = '%@' order by Created_At  desc",selectedCustomer.Customer_ID,selectedCustomer.Ship_Site_Use_ID,lastVisitID];
    NSArray* detailsArray=[self fetchDataForQuery:qryStr];
    NSLog(@"contact details is %@",detailsArray );
    if (detailsArray.count>0) {
        
        NSArray* tempArray=[[NSString getValidStringValue:[[detailsArray objectAtIndex:0] valueForKey:@"Attrib_Value"]] componentsSeparatedByString:@","];
        NSString* contactName=[NSString getValidStringValue:tempArray.count>0?[tempArray objectAtIndex:0]:@"N/A"];
        NSString* contactNumber=[[NSString alloc]init];
        if (tempArray.count>1) {
            contactNumber=[NSString getValidStringValue:[tempArray objectAtIndex:1]];
        }
        [detailsDict setObject:contactName forKey:@"Contact_Name"];
        [detailsDict setObject:contactNumber forKey:@"Contact_Number"];
        NSLog(@"temp details in visit addl info %@", detailsDict);
    }
    else{
        NSString* addlInfoQry=[NSString stringWithFormat:@"select Custom_Attribute_3 from tbl_customer_addl_info where Attrib_Name = 'EXT' AND Customer_ID ='%@' and Site_Use_ID ='%@'",selectedCustomer.Customer_ID,selectedCustomer.Ship_Site_Use_ID];
        NSArray* addlInfoArray=[self fetchDataForQuery:addlInfoQry];
        NSLog(@"addl info array is %@",addlInfoArray);
        if (addlInfoArray.count>0) {
            NSArray* tempArray=[[NSString getValidStringValue:[[addlInfoArray objectAtIndex:0] valueForKey:@"Custom_Attribute_3"]] componentsSeparatedByString:@","];
            NSString* contactName=[NSString getValidStringValue:tempArray.count>0?[tempArray objectAtIndex:0]:@"N/A"];
            NSString* contactNumber=[[NSString alloc]init];
            if (tempArray.count>1) {
                 contactNumber=[NSString getValidStringValue:[tempArray objectAtIndex:1]];
            }
            [detailsDict setObject:contactName forKey:@"Contact_Name"];
            [detailsDict setObject:contactNumber forKey:@"Contact_Number"];
            NSLog(@"temp details in customer addl info %@", detailsDict);

        }

    }

    return detailsDict;
}

-(NSString*)fetchFieldSalesVisitNotesForCustomer:(Customer*)selectedCustomer
{
    NSString* lastVisitID=[[NSString alloc]init];
    NSString* lastVisitIDQry=[NSString stringWithFormat:@"select Actual_Visit_ID from TBL_FSR_Actual_Visits where Customer_ID ='%@' and Site_Use_ID = '%@' order by visit_end_date DESC",selectedCustomer.Customer_ID,selectedCustomer.Ship_Site_Use_ID];
    NSArray* lastVisitArray=[self fetchDataForQuery:lastVisitIDQry];
    if (lastVisitArray.count>0) {
        NSLog(@"last visit details is %@", [[lastVisitArray objectAtIndex:0]valueForKey:@"Actual_Visit_ID"]);
        lastVisitID=[NSString getValidStringValue:[[lastVisitArray objectAtIndex:0]valueForKey:@"Actual_Visit_ID"]];
    }
    
    NSString* notesString=[[NSString alloc]init];
    NSString* qryStr=[NSString stringWithFormat:@"select Text_Notes from TBL_FSR_Actual_Visits where Customer_ID ='%@' and Site_Use_ID = '%@' and Actual_Visit_ID = '%@' order by visit_end_date DESC",[NSString getValidStringValue:selectedCustomer.Customer_ID],[NSString getValidStringValue:selectedCustomer.Ship_Site_Use_ID],lastVisitID];
    NSLog(@"notes query is %@",qryStr);
    NSArray* notesArray=[self fetchDataForQuery:qryStr];
    NSLog(@"Notes is %@",notesArray );
    if (notesArray.count>0) {
        notesString=[NSString getValidStringValue:[[notesArray objectAtIndex:0] valueForKey:@"Text_Notes"]];
    }
    else{
        NSString* qryStr=[NSString stringWithFormat:@"select Custom_Attribute_10 from TBl_Customer_Addl_Info where Customer_ID ='%@' and Site_Use_ID = '%@'",[NSString getValidStringValue:selectedCustomer.Customer_ID],[NSString getValidStringValue:selectedCustomer.Ship_Site_Use_ID]];
        NSArray* customerNotes=[self fetchDataForQuery:qryStr];
        if (customerNotes.count>0) {
            notesString=[NSString getValidStringValue:[[customerNotes objectAtIndex:0] valueForKey:@"Custom_Attribute_10"]];

        }
    }
    
    
    
    
    return notesString;
}

#pragma mark Coach Screen Methods
-(NSMutableArray*)fetchSurveys
{
    NSMutableArray* surveysArray=[[NSMutableArray alloc]init];
    
    NSString* surveyQry=[NSString stringWithFormat:@"select IFNULL(Survey_ID,'0')AS Survey_ID ,IFNULL(Survey_Title,'N/A') AS Survey_Title,Survey_Type_Code  from tbl_survey where  SalesRep_ID= '%@' and ( date('now') BETWEEN date(Start_Time) and date(End_Time)) and Survey_Type_Code IN ('C','M')",[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"]];
    NSMutableArray* contentArray=[self fetchDataForQuery:surveyQry];
    if (contentArray.count>0) {
        for (NSInteger i=0; i<contentArray.count; i++) {
            NSMutableDictionary* currentDict=[contentArray objectAtIndex:i];
            CoachSurveyType * currentType=[[CoachSurveyType alloc]init];
            currentType.Survey_Type_Code=[NSString getValidStringValue:[currentDict valueForKey:@"Survey_Type_Code"]];
            currentType.Survey_ID=[NSString getValidStringValue:[currentDict valueForKey:@"Survey_ID"]];
            currentType.Survey_Title=[NSString getValidStringValue:[currentDict valueForKey:@"Survey_Title"]];
            [surveysArray addObject:currentType];
        }
    }
    
    
    return surveysArray;
}

/*select * from tbl_survey where Survey_ID IN (select B.Attrib_Value AS Survey_ID from PHX_TBL_Actual_Visits AS A inner join  TBL_Visit_Addl_Info AS B  on A.Actual_Visit_ID  = B.Visit_ID where Location_ID = '97408109-18FC-496C-BE8C-DAFEE5194943' and B.Attrib_Name = 'SURVEY_ID' and B.Created_At like '%%2018-07-23%%')*/
-(NSMutableArray*)fetchMedicinaSurveys:(NSString*)locationID
{
    NSMutableArray* surveysArray=[[NSMutableArray alloc]init];
    
   // NSString* surveyQry=[NSString stringWithFormat:@"select IFNULL(Survey_ID,'0')AS Survey_ID ,IFNULL(Survey_Title,'N/A') AS Survey_Title,Survey_Type_Code  from tbl_survey where  SalesRep_ID= '%@' and ( date('now') BETWEEN date(Start_Time) and date(End_Time)) and Survey_Type_Code IN ('Q','D') and Survey_ID NOT IN (select B.Attrib_Value AS Survey_ID from PHX_TBL_Actual_Visits AS A inner join  TBL_Visit_Addl_Info AS B  on A.Actual_Visit_ID  = B.Visit_ID where Location_ID = '%@' and B.Attrib_Name = 'SURVEY_ID' and B.Created_At like '%%%@%%') ",[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"],locationID,[MedRepQueries fetchDatabaseDateFormat]];
    NSString* surveyQry = @"";
    surveyQry=[NSString stringWithFormat:@"select IFNULL(Survey_ID,'0')AS Survey_ID ,IFNULL(Survey_Title,'N/A') AS Survey_Title,Survey_Type_Code  from tbl_survey where  SalesRep_ID= '%@' and ( date('now') BETWEEN date(Start_Time) and date(End_Time)) and Survey_Type_Code IN ('Q','D')",[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"]];

    NSMutableArray* contentArray=[self fetchDataForQuery:surveyQry];
    if (contentArray.count>0) {
        for (NSInteger i=0; i<contentArray.count; i++) {
            NSMutableDictionary* currentDict=[contentArray objectAtIndex:i];
            CoachSurveyType * currentType=[[CoachSurveyType alloc]init];
            currentType.Survey_Type_Code=[NSString getValidStringValue:[currentDict valueForKey:@"Survey_Type_Code"]];
            currentType.Survey_ID=[NSString getValidStringValue:[currentDict valueForKey:@"Survey_ID"]];
            currentType.Survey_Title=[NSString getValidStringValue:[currentDict valueForKey:@"Survey_Title"]];
            [surveysArray addObject:currentType];
        }
    }
    
    
    return surveysArray;
}

-(NSInteger)fetchLimitofField:(NSString*)field in:(NSString*)tblName
{
    NSInteger limit = 100;
    NSString* pragmaQry = [NSString stringWithFormat:@"PRAGMA table_info(%@)",tblName];
    NSMutableArray* contentArray=[self fetchDataForQuery:pragmaQry];
    if (contentArray.count>0) {
        for (NSMutableDictionary* currentDict in contentArray)
        {
            NSString* currentField = [currentDict valueForKey:field];
            limit = currentField.integerValue;
        }
    }
    return limit;
}
-(NSMutableDictionary*)fetchCoachSurveyStructure:(CoachSurveyType*)selectedSurvey
{
    
    
    
    NSMutableArray* structureArray=[[NSMutableArray alloc]init];
    NSMutableDictionary* structureDict=[[NSMutableDictionary alloc]init];
    /*
     1.Those with parent group id 0 are treated as parents,fetch parents first,
     now fetch sub sections
     
     */

        NSString* parentsQry=[NSString stringWithFormat:@"select * from TBL_Survey_Question_Groups where survey_id = '%@' and parent_group_id = '0' order by Display_Sequence ",selectedSurvey.Survey_ID];
    NSLog(@"parents qry %@",parentsQry);
        NSMutableArray* parentsArray=[self fetchDataForQuery:parentsQry];
        if (parentsArray.count>0) {
            
            for (NSInteger i=0; i<parentsArray.count; i++) {
                NSMutableDictionary* currentParentDictionary = [parentsArray objectAtIndex:i];
                NSMutableDictionary* currentDictionary=[[NSMutableDictionary alloc]init];
                
                NSInteger level=0;
                //fetching sections
                NSString* sectionQry=[NSString stringWithFormat:@"select * from TBL_Survey_Question_Groups where parent_group_id = '%@' order by Display_Sequence",[currentParentDictionary valueForKey:@"Question_Group_ID"]];
                NSMutableArray* sectionArray=[self fetchDataForQuery:sectionQry];
                if (sectionArray.count>0) {
                    [currentDictionary setValue:[NSString getValidStringValue:[currentParentDictionary valueForKey:@"Description"]] forKey:@"name"];
                    [currentDictionary setValue:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)level]] forKey:@"level"];

                    NSMutableArray* objectsArray=[[NSMutableArray alloc]init];

                    //there are sub sections for this parent group
                    for (NSInteger j=0;j<sectionArray.count;j++) {

                        NSInteger sectionLevel=1;
                        
                        NSMutableDictionary* dataDict=[sectionArray objectAtIndex:j];
                      NSMutableDictionary*  currentSubSectionDictionary=[[NSMutableDictionary alloc]init];
                        [currentSubSectionDictionary setValue:[NSString getValidStringValue:[dataDict valueForKey:@"Description"]] forKey:@"name"];
                        [currentSubSectionDictionary setValue:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)sectionLevel]] forKey:@"level"];
                        
                        NSMutableArray* subsectionObjectsArray=[[NSMutableArray alloc]init];
                        
                        //fetch questions
                        NSString* questionsString=[NSString stringWithFormat:@"select * from TBL_Survey_Questions where Question_Group_ID = '%@' order by Display_Sequence  ",[dataDict valueForKey:@"Question_Group_ID"]];
                        NSMutableArray* questionsArray=[self fetchDataForQuery:questionsString];
                        if (questionsArray.count>0) {
                            NSInteger questionLevel=1;

                            NSLog(@"questions text is %@",[questionsArray valueForKey:@"Question_Text"]);
                            
                            for (NSInteger k=0; k<questionsArray.count; k++) {
                                NSMutableDictionary* currentQuestionDictionary=[[NSMutableDictionary alloc]init];
                                [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:k] valueForKey:@"Question_Text"]] forKey:@"name"];
                                [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:k] valueForKey:@"Response_Display_Type"]] forKey:@"Response_Display_Type"];

                                [currentQuestionDictionary setValue:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)questionLevel]] forKey:@"level"];
                                [currentQuestionDictionary setValue:[NSString getValidStringValue:KAppControlsYESCode] forKey:@"isQuestion"];
                                
                                [currentQuestionDictionary setValue:[NSString getValidStringValue:@"0"] forKey:@"sliderValue"];
                                [currentQuestionDictionary setValue:[NSString getValidStringValue:selectedSurvey.Survey_Type_Code] forKey:@"Survey_Type_Code"];

                                [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:k] valueForKey:@"Question_ID"] ] forKey:@"Question_ID"];

                                [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:k] valueForKey:@"Survey_ID"] ] forKey:@"Survey_ID"];

                                [subsectionObjectsArray addObject:currentQuestionDictionary];
                            }
                            
                            [currentSubSectionDictionary setValue:subsectionObjectsArray forKey:@"Objects"];
                        }
                        [objectsArray addObject:currentSubSectionDictionary];
                        
                    }
                    [currentDictionary setValue:objectsArray forKey:@"Objects"];
                    
                    
                

                }
                else{
                    //there are no sub sections for this parent group
                    
                    [currentDictionary setValue:[NSString getValidStringValue:[currentParentDictionary valueForKey:@"Description"]] forKey:@"name"];
                    [currentDictionary setValue:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)level]] forKey:@"level"];
                    NSMutableArray* objectsArray=[[NSMutableArray alloc]init];
                    
                    
                    NSString* questionsString=[NSString stringWithFormat:@"select * from TBL_Survey_Questions where Question_Group_ID = '%@' order by Display_Sequence ",[currentParentDictionary valueForKey:@"Question_Group_ID"]];
                    NSMutableArray* questionsArray=[self fetchDataForQuery:questionsString];

                    if (questionsArray.count>0) {

                        for (NSInteger i=0; i<questionsArray.count; i++) {
                            NSMutableDictionary* currentQuestionDictionary=[[NSMutableDictionary alloc]init];
                            [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:i] valueForKey:@"Question_Text"]] forKey:@"name"];
                            [currentQuestionDictionary setValue:[NSString getValidStringValue:[NSString stringWithFormat:@"%ld",(long)level]] forKey:@"level"];
                            [currentQuestionDictionary setValue:[NSString getValidStringValue:KAppControlsYESCode] forKey:@"isQuestion"];
                            [currentQuestionDictionary setValue:[NSString getValidStringValue:@"0"] forKey:@"sliderValue"];

                            [currentQuestionDictionary setValue:[NSString getValidStringValue:selectedSurvey.Survey_Type_Code] forKey:@"Survey_Type_Code"];

                            
                            [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:i] valueForKey:@"Response_Display_Type"]] forKey:@"Response_Display_Type"];

                            [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:i] valueForKey:@"Survey_ID"] ] forKey:@"Survey_ID"];

                            
                            [currentQuestionDictionary setValue:[NSString getValidStringValue:[[questionsArray objectAtIndex:i] valueForKey:@"Question_ID"] ] forKey:@"Question_ID"];

                            //add responses
//                            NSMutableArray* responsesArray=[[NSMutableArray alloc]init];
//                            NSString* questionID=[NSString getValidStringValue:[[questionsArray objectAtIndex:i] valueForKey:@"Question_ID"] ];
//                            NSString* questionString=[NSString stringWithFormat:@"select IFNULL(Response_ID,'N/A') AS Response_ID, IFNULL(Response_Text,'N/A') AS Response_Text from TBL_Survey_Responses where question_id = '%@' ",questionID];
//                            NSMutableArray* questionResponsesArray=[self fetchDataForQuery:questionString];
//                            if (questionResponsesArray.count>0) {
//
//                                NSLog(@"question response array is %@", questionResponsesArray);
//                            }
                            
                            

                            [objectsArray addObject:currentQuestionDictionary];
                        }
                        
                    }
                    else{
                        
                    }
                    [currentDictionary setValue:objectsArray forKey:@"Objects"];
                    
                }
                
                [structureArray addObject:currentDictionary];
                
            }
            
        }
        
        
   
    
    [structureDict setValue:structureArray forKey:@"Objects"];
    
    return structureDict;
}

-(NSMutableArray*)fetchCoachSurveyConfirmationData:(CoachSurveyType*)selectedSurvey
{
    NSMutableArray* contentArray=[[NSMutableArray alloc]init];

        
        NSString* surveyQuestionQry=[NSString stringWithFormat:@"SELECT A.Question_ID, IFNULL(A.Question_Text,'N/A')AS Question_Text, B.Response_Type_ID, B.Response_ID, IFNULL(A.Response_Display_Type, (CASE B.Response_Type_ID WHEN 1 THEN 'TEXT' WHEN 2 THEN 'RADIOBUTTON' WHEN 3 THEN 'CHECKBOX' END)) Response_Display_Type FROM TBL_Survey_Questions As A INNER JOIN TBL_Survey_Responses As B ON A.Question_ID=B.Question_ID WHERE A.Survey_ID='%@' and A.Question_Group_ID IS NULL order by Display_Sequence",selectedSurvey.Survey_ID];

    NSLog(@"confirmation data query %@",surveyQuestionQry);
    
    
        NSMutableArray* questionsArray=[self fetchDataForQuery:surveyQuestionQry];
        for (NSInteger i=0; i<questionsArray.count; i++) {
            NSMutableDictionary* currentDict=[questionsArray objectAtIndex:i];
            CoachSurveyConfirmationQuestion * currentQuestion=[[CoachSurveyConfirmationQuestion alloc]init];
            currentQuestion.Question_ID=[NSString getValidStringValue:[currentDict valueForKey:@"Question_ID"]];
            currentQuestion.Question_Text=[NSString getValidStringValue:[currentDict valueForKey:@"Question_Text"]];
            currentQuestion.Response_Type_ID=[NSString getValidStringValue:[currentDict valueForKey:@"Response_Type_ID"]];
            currentQuestion.Response_ID=[NSString getValidStringValue:[currentDict valueForKey:@"Response_ID"]];
            currentQuestion.Response_Display_Type=[NSString getValidStringValue:[currentDict valueForKey:@"Response_Display_Type"]];
            currentQuestion.Survey_ID=selectedSurvey.Survey_ID;

            [contentArray addObject:currentQuestion];
            
        }
        

    
    
    
    return contentArray;
}

-(BOOL)isCoachSurveyCompletedForToday:(NSString*)surveyID
{
    BOOL status=NO;
    NSString* qry = [NSString stringWithFormat:@"select * from TBL_Survey_Audit_Responses where Survey_Timestamp like '%%%@%%' and Survey_ID = '%@'",[MedRepQueries fetchDatabaseDateFormat],[SWDefaults getValidStringValue:surveyID]];
    NSLog(@"coach survey qry %@",qry);
    NSMutableArray* responseArray=[self fetchDataForQuery:qry];
    if (responseArray.count>0) {
        status=YES;
    }
    return status;
}

-(BOOL)insertCoachDataSurvey:(NSMutableArray*)surveyArray withConfirmation:(NSMutableArray*)confirmationArray andSurveyType:(CoachSurveyType*)selectedSurveyType
{
    
    BOOL status=NO;
    FMDatabase *database;
    NSString *documentDir;
    if ([[ [UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else
    {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        documentDir = [filePath path] ;
    }
    
    database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];

    [database open];
    [database beginTransaction];

    BOOL insertSurveySuccessfull = NO;
    BOOL insertConfirmationSuccessfull = NO;
    
    NSString* surveyTimeStamp=[MedRepQueries fetchCurrentDateTimeinDatabaseFormat];
    NSString* salesRepID=[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"SalesRep_ID"]];
    NSString* empCode=[NSString getValidStringValue:[[SWDefaults userProfile]valueForKey:@"Emp_Code"]];

    NSString* surveyedBy=[NSString getValidStringValue:[SWDefaults coachProfile] ];

    for (NSInteger i=0; i<surveyArray.count; i++) {
        
        NSString* remarks=[[NSString alloc]init];
        
        NSMutableDictionary* currentDictionary=[surveyArray objectAtIndex:i];
        NSString* response=[[NSString alloc]init];
        NSString *queryString=[[NSString alloc]init];
        NSMutableArray* argumentsArray=[[NSMutableArray alloc]init];
        
        NSString * auditSurveyID =[NSString createGuid];
        NSString* surveyID=[NSString getValidStringValue:[currentDictionary valueForKey:@"Survey_ID"]];
        NSString* questionID=[NSString getValidStringValue:[currentDictionary valueForKey:@"Question_ID"]];

        if([selectedSurveyType.Survey_Type_Code isEqualToString:@"D"])
        {
        //D has slider
            response=[NSString getValidStringValue:[currentDictionary valueForKey:@"sliderValue"]];
            remarks=[NSString getValidStringValue:[currentDictionary valueForKey:@"Remarks"]];
            queryString = @"Insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By,Custom_Attribute_1) Values(?,?,?,?,?,?,?,?,?)";
            argumentsArray= [[NSMutableArray alloc]initWithObjects:auditSurveyID,surveyID,questionID,response,salesRepID,empCode,surveyTimeStamp,surveyedBy,remarks,nil];
        }
        else if ([selectedSurveyType.Survey_Type_Code isEqualToString:@"Q"])
        {
            //Q has segment
            response=[NSString getValidStringValue:[currentDictionary valueForKey:@"segmentValue"]];
            remarks=[NSString getValidStringValue:[currentDictionary valueForKey:@"Remarks"]];
            queryString = @"Insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By,Custom_Attribute_1) Values(?,?,?,?,?,?,?,?,?)";
            argumentsArray=[[NSMutableArray alloc]initWithObjects:auditSurveyID,surveyID,questionID,response,salesRepID,empCode,surveyTimeStamp,surveyedBy,remarks,nil];
        }
        
       else if ([[currentDictionary valueForKey:@"Response_Display_Type"] isEqualToString:@"SEGMENT"]) {
            response=[NSString getValidStringValue:[currentDictionary valueForKey:@"Rating"]];
            remarks=[NSString getValidStringValue:[currentDictionary valueForKey:@"Remarks"]];
            queryString = @"Insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By,Custom_Attribute_1) Values(?,?,?,?,?,?,?,?,?)";
            argumentsArray= [[NSMutableArray alloc]initWithObjects:auditSurveyID,surveyID,questionID,response,salesRepID,empCode,surveyTimeStamp,surveyedBy,remarks,nil];
        }
        else{
            response=[NSString getValidStringValue:[currentDictionary valueForKey:@"sliderValue"]];
            NSInteger sliderValue = [response integerValue]+1;
            response=[NSString stringWithFormat:@"%ld",sliderValue];
            queryString = @"Insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By) Values(?,?,?,?,?,?,?,?)";
            argumentsArray=[[NSMutableArray alloc]initWithObjects:auditSurveyID,surveyID,questionID,response,salesRepID,empCode,surveyTimeStamp,surveyedBy,nil];
        }
        
        insertSurveySuccessfull = [database executeUpdate:queryString withArgumentsInArray:argumentsArray];
    }
    
    if (confirmationArray.count>0) {
        
    for (NSInteger j=0; j<confirmationArray.count; j++) {
        
        CoachSurveyConfirmationQuestion * currentQuestion=[confirmationArray objectAtIndex:j];
        NSString * auditSurveyID =[NSString createGuid];
        NSString* surveyID=[NSString getValidStringValue:currentQuestion.Survey_ID];
        NSString* questionID=[NSString getValidStringValue:currentQuestion.Question_ID];
        NSString* response=[[NSString alloc]init];
        
        if ([currentQuestion.Response_Display_Type isEqualToString:kSignatureResponseDisplayType]) {
            response=currentQuestion.image_Name;
        }
        else
        {
            response = [NSString getValidStringValue:currentQuestion.Response_Text];
        }
        

        NSString *queryString = @"Insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By) Values(?,?,?,?,?,?,?,?)";

        NSMutableArray* argumentsArray=[[NSMutableArray alloc]initWithObjects:auditSurveyID,surveyID,questionID,response,salesRepID,empCode,surveyTimeStamp,surveyedBy,nil];
        
        insertConfirmationSuccessfull = [database executeUpdate:queryString withArgumentsInArray:argumentsArray];

    }
    }
    else
    {
        //some surveys like store audit done have confirmation data(signature etc)
        insertConfirmationSuccessfull=YES;
    }
    
   
    
    
    if (insertSurveySuccessfull && insertConfirmationSuccessfull) {
        
        status=YES;
        [database commit];
        
        if (confirmationArray.count>0) {
            
        for (NSInteger k=0; k<confirmationArray.count; k++) {
            
            CoachSurveyConfirmationQuestion * currentQuestion=[confirmationArray objectAtIndex:k];
            if ([currentQuestion.Response_Display_Type isEqualToString:kSignatureResponseDisplayType]) {

            //save image to documents directory
            NSString* tempPath=[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:KCoachSurveySignatureFolderName] stringByAppendingPathComponent:@"temp"]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",currentQuestion.image_Name]];
            
            if ([[NSFileManager defaultManager]fileExistsAtPath:tempPath]) {
                NSError * error;
                NSString* actualPath = [[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:KCoachSurveySignatureFolderName]stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",currentQuestion.image_Name]];
                
                BOOL moveSuccessfull =[[NSFileManager defaultManager] moveItemAtPath:tempPath toPath:actualPath error:&error];
                
                if (moveSuccessfull) {
                    //now delete from temp
                    NSError * deleteError;
                    BOOL deleteStatus = NO;
                    deleteStatus=[[NSFileManager defaultManager]removeItemAtPath:tempPath error:&deleteError];
                }
                
            }
            }
        }
        }
    }
    else{
        [database rollback];
    }
    [database close];
    return status;
}

-(BOOL)surveyCompletedForToday
{
    BOOL status=NO;
    NSString* surveyQry = [NSString stringWithFormat:@"select * from TBL_Survey_Audit_Responses where survey_timestamp like '%%%@%%'",[MedRepQueries fetchCurrentDateInDatabaseFormat]];
    NSLog(@"Todays survey qry %@",surveyQry);
    NSMutableArray * contentArray=[self fetchDataForQuery:surveyQry];
    NSLog(@"Todays survey response %@",contentArray);

    if (contentArray.count>0) {
     status = YES;
    }
    return status;
}

-(NSMutableArray*)fetchStockCheckItemsForCustomer {
    
    NSMutableArray *DCLineItemArray = [[NSMutableArray alloc]init];
    NSMutableArray *productDetails = [self fetchDataForQuery:@"select * from PHX_TBL_Products order by Product_Name"];
    
    if (productDetails.count > 0) {
        for (NSInteger i=0; i<productDetails.count; i++)
        {
            DistriButionCheckItem *DCItem = [[DistriButionCheckItem alloc]init];
            DCItem.DcProduct = [productDetails objectAtIndex:i];
            [DCLineItemArray addObject:DCItem];
        }
    }
    return DCLineItemArray;
}

-(NSMutableArray *)fetchStockCheckLocations
{
    NSMutableArray *DCLocationsArray = [[NSMutableArray alloc]init];
    
    DistriButionCheckLocation *DCLocation = [[DistriButionCheckLocation alloc]init];
    DCLocation.LocationID=@"S";
    DCLocation.LocationName=@"Shelf";
    [DCLocationsArray addObject:DCLocation];
    
    return DCLocationsArray;
}

- (BOOL)saveStockCheckWithVisitDetails:(NSMutableDictionary*)visitDetailsDict andDistChectItemInfo:(NSMutableArray*)fetchDistriButionCheckLocations andVisitDetails:(VisitDetails*)currentVisitDetails
{
    BOOL insertDistributionCheckSuccessful = NO;
    BOOL insertDistributionCheckItemSuccessful = NO;
    
    
    NSString *documentDir;
    if ([[[UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        documentDir = [filePath path] ;
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];

    NSString *DistributionCheckID = [NSString createGuid];
    NSDictionary *FSR = [SWDefaults userProfile];
    

    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *LastUpdatedAt = [formatterTime stringFromDate:[NSDate date]];
    

    insertDistributionCheckSuccessful = [database executeUpdate:@"INSERT INTO PHX_TBL_DC (DC_ID, Emp_ID, Location_ID, Contact_ID, Visit_ID, Notes, Signee_Name, Start_Time, End_Time, Status, Created_At, Created_By, Last_Updated_At, Last_Updated_By) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",DistributionCheckID, [FSR stringForKey:@"Emp_Code"], [NSString getValidStringValue:[visitDetailsDict valueForKey:@"Location_ID"]], [NSString getValidStringValue:[visitDetailsDict valueForKey:@"Contact_ID"]], [NSString getValidStringValue:[visitDetailsDict valueForKey:@"Planned_Visit_ID"]], @"", @"", [NSString getValidStringValue:[visitDetailsDict valueForKey:@"StockCheck_StartTime"]], LastUpdatedAt, @"N", [NSString getValidStringValue:[visitDetailsDict valueForKey:@"Created_At"]], [NSString getValidStringValue:[visitDetailsDict valueForKey:@"Created_By"]], LastUpdatedAt, [FSR stringForKey:@"User_ID"], nil];
    

    if (insertDistributionCheckSuccessful == YES) {
        
        for (DistriButionCheckLocation *DCLocation in fetchDistriButionCheckLocations) {
            for (DistriButionCheckItem *DCItem in DCLocation.dcItemsArray)
            {
                for (int i =0; i<[DCItem.dcItemLots count]; i++) {
                    DistriButionCheckItemLot *DCItemLot = [DCItem.dcItemLots objectAtIndex:i];
                    
                    if (DCItem.imageName.length >0 && i == 0) {
                        DCItemLot.DistriButionCheckItemLotId = DCItem.imageName;
                    }
                    else{
                        DCItemLot.DistriButionCheckItemLotId = [NSString createGuid];
                    }
                    
                    NSString *Is_Available, *refinedExpiryDate;
                    if ([[AppControl retrieveSingleton].FM_ENABLE_STOCK_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
                    {
                        if (DCItemLot.Quntity.length > 0) {
                            Is_Available = @"Y";
                        }
                        else{
                            Is_Available = @"N";
                        }
                        
                        refinedExpiryDate = [MedRepDefaults refineDateFormat:kDateFormatWithoutTime destFormat:kDatabseDefaultDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:DCItemLot.expiryDate]];
                    }
                    else{
                        Is_Available = [SWDefaults getValidStringValue:DCItem.itemAvailability];
                        
                        refinedExpiryDate = DCItemLot.expiryDate;
                    }
                    
                    insertDistributionCheckItemSuccessful = [database executeUpdate:@"INSERT INTO PHX_TBL_DC_Items (Row_ID, DC_ID, Product_ID, Is_Available, Available_Qty, Product_UOM, Min_Expiry_Date) VALUES (?,?,?,?,?,?,?)", DCItemLot.DistriButionCheckItemLotId, DistributionCheckID, [SWDefaults getValidStringValue:[DCItem.DcProduct valueForKey:@"Product_ID"]], Is_Available, [SWDefaults getValidStringValue:DCItemLot.Quntity], [SWDefaults getValidStringValue:[DCItem.DcProduct valueForKey:@"Primary_UOM"]], [SWDefaults getValidStringValue:refinedExpiryDate], nil];
                    
                    NSLog(@"%d",insertDistributionCheckItemSuccessful);
                }
            }
        }
    }

    
    if (!insertDistributionCheckSuccessful  || !insertDistributionCheckItemSuccessful) {
        
        //something went wrong rollback

        [database rollback];
        [database close];
        return NO;
    }
    
    else
    {
        //everything alright commit
        [database commit];
        [database close];
        return YES;
    }
}

-(NSMutableArray*)fetchInvoicesforReportsPaymentReceipt:(NSString*)collection_ID{
    NSMutableArray* collectionArray = [[NSMutableArray alloc]init];
    NSString* collectionQry = [NSString stringWithFormat:@"select I.Invoice_No,I.Invoice_Date,I.Amount AS Invoice_Amount,A.Amount as Total_Amount,A.Collection_Ref_No AS Receipt_No,A.Collection_Type As Payment_Mode,IFNULL(A.Cheque_NO,'N/A') AS Cheque_No,IFNULL(A.Cheque_Date,'N/A') AS Cheque_Date,IFNULL(A.Bank_Name,'N/A') AS Bank_Name,  IFNULL(A.Bank_Branch,'N/A') AS Bank_Branch from TBL_Collection AS A left join TBL_Collection_Invoices AS I on A.Collection_ID = I.Collection_ID where A.collection_ID = '%@'", collection_ID];
    NSMutableArray* contentArray = [self fetchDataForQuery:collectionQry];
    if(contentArray.count>0){
        for (NSInteger i = 0;i<contentArray.count;i++){
            NSMutableDictionary* currentDict = contentArray[i];
            PaymentSummary* currentSummary = [[PaymentSummary alloc]init];
            currentSummary.Invoice_No = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Invoice_No"]];
            currentSummary.Invoice_Date = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Invoice_Date"]];
            currentSummary.Invoice_Amount = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Invoice_Amount"]];
            currentSummary.Total_Amount = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Total_Amount"]];
            currentSummary.Receipt_Number = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Receipt_No"]];
            currentSummary.Payment_Mode = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Payment_Mode"]];
            currentSummary.Cheque_No = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Cheque_No"]];
            currentSummary.Cheque_Date = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Cheque_Date"]];
            currentSummary.Bank_Name = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Bank_Name"]];
            currentSummary.Bank_Branch = [SWDefaults getValidStringValue:[currentDict valueForKey:@"Bank_Branch"]];
            

            [collectionArray addObject:currentSummary];
        }
    }
    return collectionArray;
}


-(NSString*)fetchBackEndURL{
    NSString* urlString = [[NSString alloc]init];
    
    NSString* appCodeQry=[NSString stringWithFormat:@"select Code_Value from TBL_App_Codes  where Code_Type='BO_URL'"];
    NSMutableArray* appCodeArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:appCodeQry];
    if (appCodeArray.count>0) {
        urlString = [[appCodeArray objectAtIndex:0] valueForKey:@"Code_Value"] ;
    }
    else
    {
    }
    
    return urlString;

}

-(NSString*)fetchEmailAddressViaCustomerID:(NSString*)customerID{
    
    
    if (customerID == nil || [[NSString stringWithFormat:@"%@",customerID ]isEqualToString:@"0"]) {
        return @"";
    }
    
    
    NSString* Qry=[NSString stringWithFormat:@"select IFNULL(Email,'') AS Email from TBL_Customer where Customer_ID ='%@'",customerID ];
    
    NSArray* arrayEmail=[self fetchDataForQuery:Qry];
    
    if (arrayEmail.count>0) {
         return [SWDefaults getValidStringValue:[[arrayEmail objectAtIndex:0]valueForKey:@"Email"]];
    }
    
    else
    {
        return @"";
    }
    
}


-(NSString*)fetchEmailAddressViaDoctorID:(NSString*)doctorID{
    
    
    if (doctorID == nil ||doctorID.length == 0) {
        return @"";
    }
    
    
    NSString* Qry=[NSString stringWithFormat:@"select IFNULL(Email,'') AS Email from PHX_TBL_Doctors where Doctor_ID ='%@'",doctorID ];
    
    NSArray* arrayEmail=[self fetchDataForQuery:Qry];
    
    if (arrayEmail.count>0) {
        return [SWDefaults getValidStringValue:[[arrayEmail objectAtIndex:0]valueForKey:@"Email"]];
    }
    
    else
    {
        return @"";
    }
    
}

-(NSString*)fetchEmailAddressViaContactID:(NSString*)contactID{
    
    
    if (contactID == nil ||contactID.length == 0) {
        return @"";
    }
    
    
    NSString* Qry=[NSString stringWithFormat:@"select  Email from PHX_TBL_Contacts  WHERE Contact_ID ='%@'",contactID];
    
    NSArray* arrayEmail=[self fetchDataForQuery:Qry];
    
    if (arrayEmail.count>0) {
        return [SWDefaults getValidStringValue:[[arrayEmail objectAtIndex:0]valueForKey:@"Email"]];
    }
    
    else
    {
        return @"";
    }
    
}



//
//
//    NSMutableArray* currentArray = [[NSMutableArray alloc]init];
//    /*
//    NSString* invQry = [NSString stringWithFormat:@"select * from TBL_Collection AS A inner join TBL_Collection_Invoices AS I on A.Collection_ID = I.Collection_ID and A.Collection_Ref_No = '%@'",collectionRefNO];
//    NSMutableArray* contentArray = [self fetchDataForQuery:invQry];
//    if(contentArray.count>0){
//        for (NSInteger i=0;i<contentArray.count;i++) {
//            NSMutableDictionary* currentDict = contentArray[i];
//
//            CustomerPaymentInvoiceClass *Invoice=[[CustomerPaymentInvoiceClass alloc]init];
//            /*@"select (IFNULL(A.Pending_Amount,'0')-IFNULL(C.Amount,'0')) AS DueAmount,A.Invoice_Ref_No AS Invoice_Ref_No,A.Pending_Amount AS NetAmount,IFNULL(C.Amount,'0') AS PaidAmt,'0' AS Amount,A.Invoice_Date AS InvDate, A.Due_Date as DueDate from tbl_open_Invoices AS A LEFT JOIN (select * from TBL_Collection group by Customer_ID,Site_Use_ID) AS B  ON A.Customer_ID = B.Customer_ID AND A.Site_Use_ID = B.Site_Use_ID  LEFT JOIN (select SUM(Amount)AS Amount,Invoice_No  from TBL_Collection_Invoices Where ERP_Status!='C' group by Invoice_No) AS C ON A.Invoice_Ref_No = C.Invoice_No WHERE A.Customer_ID='%@' AND A.Site_Use_ID='%@' UNION ALL select (IFNULL(A.Transaction_Amt,'0')-IFNULL(C.Amount,'0')) AS DueAmount,A.Orig_Sys_Document_Ref AS Invoice_Ref_No,IFNULL(A.Transaction_Amt,'0') As NetAmount,IFNULL(C.Amount,'0') AS PaidAmt,'0' AS Amount,A.Creation_Date AS InvDate,A.Due_Date as DueDate from tbl_Sales_History AS A LEFT JOIN (select * from TBL_Collection group by Customer_ID,Site_Use_ID) AS B  ON A.Ship_To_Customer_ID = B.Customer_ID AND A.Ship_To_Site_ID = B.Site_Use_ID   LEFT JOIN (select SUM(Amount)AS Amount,Invoice_No  from TBL_Collection_Invoices Where ERP_Status!='C' group by Invoice_No) AS C ON A.Orig_Sys_Document_Ref = C.Invoice_No  WHERE (A.ERP_Status = 'X') AND (A.Inv_To_Customer_ID='%@') AND (A.Inv_To_Site_ID='%@') AND A.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices WHERE Customer_ID='%@' AND Site_Use_ID='%@') ORDER BY InvDate ASC"*/
//
//            Invoice.InvoiceDate=[NSString stringWithFormat:@"%@",[tempInvoiceDic valueForKey:@"InvDate"]];
//            Invoice.dueNetAmount=[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],[[tempInvoiceDic valueForKey:@"NetAmount"] doubleValue]]];
//            Invoice.dueDate=[NSString stringWithFormat:@"%@",[tempInvoiceDic valueForKey:@"DueDate"]];
//            Invoice.invoiceNumber=[NSString stringWithFormat:@"%@",[tempInvoiceDic valueForKey:@"Invoice_Ref_No"]];
//            Invoice.previousSettledAmount=[NSDecimalNumber ValidDecimalNumberWithString:[NSString stringWithFormat:@"%.*f",[self getUserProfileNumberOfDecimalsStr],[[tempInvoiceDic valueForKey:@"PaidAmt"] doubleValue]]];
//            Invoice.DueAmount=[Invoice.dueNetAmount decimalNumberBySubtracting:Invoice.previousSettledAmount];
//            Invoice.settledAmountForInvoice=[NSDecimalNumber zero];
//            TempInvoiceAmount=[TempInvoiceAmount decimalNumberByAdding:Invoice.dueNetAmount];
//
//            if([Invoice.DueAmount isGraterThanDecimal:[NSDecimalNumber zero]]){
//                [invoicesObjectsArray addObject:Invoice];
//            }        }
//    }
//    //CustomerPaymentInvoiceClass
//    */
//    return currentArray;
//}


- (BOOL)saveRouteRescheduleDetails:(RouteRescheduleDetails*)routeDetail
{
    BOOL insertRouteRescheduleDetailsSuccessful = NO;
    
    NSString *documentDir;
    if ([[[UIDevice currentDevice] systemVersion] floatValue]<8.0) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    else {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        documentDir = [filePath path] ;
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    
    insertRouteRescheduleDetailsSuccessful = [database executeUpdate:@"INSERT INTO TBL_Visit_Change_Log (Visit_Change_ID, FSR_Plan_Detail_ID, Visit_Date, Customer_ID, Site_Use_ID, SalesRep_ID, Change_Type, New_Visit_Date, Proc_Status, Approval_Status, Requested_At, Requested_By, Last_Updated_At, Last_Updated_By, Approval_Action_At, Approval_Action_By, Cancelled_At, Cancelled_By, Reason, Comments, Remarks) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",routeDetail.Visit_Change_ID, routeDetail.FSR_Plan_Detail_ID, routeDetail.Visit_Date, routeDetail.Customer_ID, routeDetail.Site_Use_ID, routeDetail.SalesRep_ID, routeDetail.Change_Type, routeDetail.New_Visit_Date, routeDetail.Proc_Status, routeDetail.Approval_Status, routeDetail.Requested_At, routeDetail.Requested_By, routeDetail.Last_Updated_At, routeDetail.Last_Updated_By, routeDetail.Approval_Action_At, routeDetail.Approval_Action_By, routeDetail.Cancelled_At, routeDetail.Cancelled_By, routeDetail.Reason, routeDetail.Comments, routeDetail.Remarks];
    
    if (!insertRouteRescheduleDetailsSuccessful) {
        //something went wrong rollback
        
        [database rollback];
        [database close];
        return NO;
    }
    else {
        //everything alright commit
        [database commit];
        [database close];
        return YES;
    }
}


- (NSMutableArray *)fetchProductsWithMedia:(NSString *)productID
{
    NSString *query = [NSString stringWithFormat:@"select IFNULL(Qty,'0') AS QTY, IFNULL(Media_File_ID,'N/A') AS Product_ID, IFNULL(Inventory_Item_ID,'N/A') AS Inventory_Item_ID, IFNULL(Description,'N/A') AS Product_Name,IFNULL(Item_Code,'N/A') AS Product_Code, IFNULL(Primary_UOM_Code,'N/A') AS Primary_UOM,IFNULL(Brand_Code,'N/A') AS Brand_Code, IFNULL(Agency,'N/A') AS Agency, IFNULL(List_Price,'0') AS List_Price, IFNULL(Net_Price,'0') AS Net_Price from (SELECT B.Qty, M.Media_File_ID, TBL_Product.* FROM TBL_Product LEFT JOIN ((select Lot_Qty AS Qty, Item_ID FROM TBL_Product_Stock where Expiry_Date>datetime('now') group by Item_ID)) AS B ON TBL_Product.Inventory_Item_ID=B.Item_ID inner join TBL_Media_File_Mapping as M ON TBL_Product.Inventory_Item_ID = M.Inventory_Item_ID) group by Inventory_Item_ID"];
    
    if (productID.length > 0) {
        query = [NSString stringWithFormat:@"select IFNULL(Qty,'0') AS QTY, IFNULL(Media_File_ID,'N/A') AS Product_ID, IFNULL(Inventory_Item_ID,'N/A') AS Inventory_Item_ID, IFNULL(Description,'N/A') AS Product_Name,IFNULL(Item_Code,'N/A') AS Product_Code, IFNULL(Primary_UOM_Code,'N/A') AS Primary_UOM,IFNULL(Brand_Code,'N/A') AS Brand_Code, IFNULL(Agency,'N/A') AS Agency, IFNULL(List_Price,'0') AS List_Price, IFNULL(Net_Price,'0') AS Net_Price from (SELECT B.Qty, M.Media_File_ID, TBL_Product.* FROM TBL_Product LEFT JOIN ((select Lot_Qty AS Qty, Item_ID FROM TBL_Product_Stock where Expiry_Date>datetime('now') group by Item_ID)) AS B ON TBL_Product.Inventory_Item_ID=B.Item_ID inner join TBL_Media_File_Mapping as M ON TBL_Product.Inventory_Item_ID = M.Inventory_Item_ID) where Inventory_Item_ID = '%@' group by Inventory_Item_ID",productID];
    }
    
    NSLog(@"products query is %@", query);
    
    NSMutableArray *productsDataArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:query];
    
    if (productsDataArray.count>0) {
        return productsDataArray;
    }
    else {
        return nil;
    }
}

- (NSMutableArray *)fetchStockWithProductID:(NSString*)productID
{
    //fetch stock only if expiry date is greater then today

    NSString *query = [NSString stringWithFormat:@"select IFNULL(Item_ID,'0') AS Product_ID, IFNULL(Lot_No,'N/A') AS Lot_No ,IFNULL(Expiry_Date,'0') AS Expiry_Date, IFNULL(Lot_Qty,'0') AS Lot_Qty from TBL_Product_Stock where Item_ID = '%@' and Expiry_Date > datetime('now') and  Lot_Qty > '0'",productID];
    
    NSMutableArray *stockArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:query];
    
    if (stockArray.count>0) {
        return stockArray;
    }
    else {
        return nil;
    }
}

- (NSMutableArray *)fetchMediaFilesforProduct:(NSString*)productID
{
    NSMutableArray *contentArray = [[NSMutableArray alloc]init];
    NSString *productQry=[NSString stringWithFormat:@"select DISTINCT IFNULL(Product_Name ,'N/A') AS Product_Name , IFNULL(Product_ID ,'N/A')AS Product_ID,IFNULL(Product_Code ,'N/A')AS Product_Code,IFNULL(A.Media_File_ID,'N/A') AS Media_File_ID ,IFNULL(Media_Type,'N/A') AS Media_Type,IFNULL(Caption,'N/A') AS Caption, IFNULL(Filename,'N/A') AS File_Name, IFNULL(Brand_Code,'N/A') AS Brand_Code, IFNULL(Agency,'N/A') AS Agency from TBL_Media_Files AS A LEFT JOIN (select C.Inventory_Item_ID AS Product_ID, C.Description as Product_Name, C.Item_Code as Product_Code, C.Brand_Code as Brand_Code, C.Agency as Agency, B.Media_File_ID FROM TBL_Media_File_Mapping As B left join TBL_Product as C on B.Inventory_Item_ID = C.Inventory_Item_ID) AS B On CAST(A.Media_File_ID as nvarchar(100)) =CAST(B.Media_File_ID as nvarchar(100)) where Product_ID = '%@' and A.Download_Flag ='N' Order by Caption ASC", productID];
    
    
    NSMutableArray* productMediaFiles=[[SWDatabaseManager retrieveManager]fetchDataForQuery:productQry];
    if (productMediaFiles.count>0) {
        
        for (NSInteger i=0; i<productMediaFiles.count; i++) {
            
            ProductMediaFile* currentProduct = [[ProductMediaFile alloc]init];
            NSMutableDictionary *currentProductDict = [productMediaFiles objectAtIndex:i];
            currentProduct.Media_File_ID=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Media_File_ID"]];
            currentProduct.Product_Code=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Product_Code"]];
            currentProduct.Product_Name=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Product_Name"]];
            currentProduct.Caption=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Caption"]];
            currentProduct.File_Name=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"File_Name"]];
            currentProduct.Media_Type=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Media_Type"]];
            currentProduct.Entity_ID_1=[MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Product_ID"]];
            currentProduct.isDemoed=NO;
            
            NSString* downloadUrl=[[[NSUserDefaults standardUserDefaults]valueForKey:@"ServerAPILink"] stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",currentProduct.Media_File_ID,@"M"]];
            currentProduct.Download_Url=downloadUrl;
            
            
            NSString*documentsDirectory;
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                documentsDirectory=[SWDefaults applicationDocumentsDirectory];
            }
            
            else
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains
                (NSDocumentDirectory, NSUserDomainMask, YES);
                documentsDirectory = [paths objectAtIndex:0];
            }
            
            currentProduct.File_Path = [documentsDirectory stringByAppendingPathComponent:currentProduct.File_Name];
            currentProduct.Brand_Code = [MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Brand_Code"]];
            currentProduct.Agency = [MedRepDefaults getDefaultStringForEmptyString:[currentProductDict valueForKey:@"Agency"]];
            [contentArray addObject:currentProduct];
        }
        return contentArray;
    }
    else {
        return 0;
    }
}

@end
