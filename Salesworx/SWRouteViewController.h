//
//  SWRouteViewController.h
//  SWRoute
//
//  Created by Irfan Bashir on 5/17/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

//#import <UIKit/UIKit.h>
//#import <MapKit/MapKit.h>
//#import "MapView2.h"
//
////#import "RegexKitLite.h"
//#import "Place2.h"
//#import "PlaceMark.h"
//#import <CoreLocation/CoreLocation.h>
//#import "SWFoundation.h"
//#import "SWPlatform.h"
//
//@interface SWRouteViewController : SWViewController < UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate,MKMapViewDelegate,MKAnnotation,CLLocationManagerDelegate,ZBarReaderDelegate,UIImagePickerControllerDelegate> {
//    
//    
//
//    NSArray *routes;
//    UITableView *tableView;
//    NSDate *selectedDate;
//    UIPopoverController *datePickerPopOver;
//    UIButton *codeLabel ;
//    UIView *footerView;
//    MapView2 *mapView;
//    Place2 *place;
//    CSMapAnnotation2 *Annotation;
//    UILabel *visitedPlace;
//    UILabel *remainingPlace;
//    UILabel *coveragePlace;
//    UIButton *minusDate;
//    UIButton *addDate;
//    UIImageView *bottomImage;
//    NSMutableDictionary *customer;
//    UIImageView* routeView;
//    UIColor* lineColor;
//    NSString * forCurrentLat;
//    NSString * forCurrentLong;
//    CLLocationManager * _locManager;
//    CLLocation *myloa;
//    CLLocationManager *locationManager;
//    CLLocationCoordinate2D currentLocation;
//    UIButton *Login_Btn;
//    UIButton *locationButton;
//    UIButton *customerMapButton;
//    ZBarReaderViewController *reader;
//    SWNoResultsView *noResultsView;
//    //
//    BOOL isFurtureDate;
//    int visitedCustomer ;
//    int remainningCustomer ;
//
//    UIView *myBackgroundView;
//
//    
//}
//
//
//@end


#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>
#import "MapView2.h"
#import "Place2.h"
#import "PlaceMark.h"
#import <CoreLocation/CoreLocation.h>
#import "SWFoundation.h"
#import "SWPlatform.h"
#import "ZBarSDK.h"
#import "UIViewController+MJPopupViewController.h"
#import "SalesWorxCashCustomerOnsiteVisitOptionsViewController.h"
#import "SalesWorxCustomClass.h"
#import "SalesWorxVisitOptionsViewController.h"
#import "SalesWorxImageBarButtonItem.h"
@interface SWRouteViewController : SWViewController<UIPopoverControllerDelegate,MKMapViewDelegate,MKAnnotation,CLLocationManagerDelegate,ZBarReaderDelegate,UIImagePickerControllerDelegate>

{
    SalesWorxImageBarButtonItem *btnBarCode;
    
    NSDate *selectedDate;
    NSArray *routes;
    NSMutableDictionary *customer;
    UIPopoverController *datePickerPopOver;
    
  IBOutlet  MapView2 *mapView;
    Place2 *place;
    CSMapAnnotation2 *Annotation;
    ZBarReaderViewController *reader;
    IBOutlet SalesWorxImageView *statusImageView;
    SalesWorxVisit * salesWorxVisit;
    
    NSIndexPath * selectedIndexPath;
    BOOL isFurtureDate;
    NSMutableArray* indexPathArray;
    
    NSString* openVisitID;
    UIAlertView * openVisitAlert;
    
    NSString* openPlannedVisitID;
    
    NSString* openPlannedDetailID;

    IBOutlet MedRepElementTitleLabel *lastVisitedOnHeaderLabel;
    IBOutlet MedRepElementDescriptionLabel *lastVisitedDateLabel;
}
@property (weak, nonatomic) IBOutlet UIView *locationView;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property(strong,nonatomic) Customer * selectedCustomer;

@property (weak, nonatomic) IBOutlet UIButton *btnDate;
@property (weak, nonatomic) IBOutlet UIButton *btnMinusDate;
@property (weak, nonatomic) IBOutlet UIButton *btnPlusDate;
@property (weak, nonatomic) IBOutlet UITableView *customerTableView;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;

@property (weak, nonatomic) IBOutlet UIButton *btnStartVisit;
@property (weak, nonatomic) IBOutlet UIButton *btnCustomerStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblCreditLimit;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailabelBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblPDCDue;
@property (weak, nonatomic) IBOutlet UILabel *lblOverDue;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalOutstandings;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentMonthSales;

@property (weak, nonatomic) IBOutlet UILabel *lblPlannedVisits;
@property (weak, nonatomic) IBOutlet UILabel *lblCompletedVisits;
@property (weak, nonatomic) IBOutlet UILabel *lblOutOfRoute;



@end

