//
//  SalesWorxPaymentCollectionFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 12/1/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SalesWorxPaymentCollectionFilterViewController.h"
#import "SWDefaults.h"
#import "MedRepQueries.h"
#import "MedRepDoctorFilterDescriptionViewController.h"

@interface SalesWorxPaymentCollectionFilterViewController ()

@end

@implementation SalesWorxPaymentCollectionFilterViewController


@synthesize filterPopOverController,customerListArray,customerNumberButton,addressButton,tradeChannelButton,cashCustomerSwitch;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = self.filterTitle;
    self.navigationItem.titleView = label;
    

    titleString=[[NSString alloc]init];
    filterParametersDictionary=[[NSMutableDictionary alloc]init];
    
    if ([_previousFilterParametersDict valueForKey:@"Cash_Cust"]!=nil) {

        
        if ([[_previousFilterParametersDict valueForKey:@"Cash_Cust"] isEqualToString:@"N"]) {
            
            [filterParametersDictionary setValue:@"N" forKey:@"Cash_Cust"];
            
            [cashCustomerSwitch setOn:NO animated:NO];
        }
        
        else if ([[_previousFilterParametersDict valueForKey:@"Cash_Cust"] isEqualToString:@"Y"])
        {
            [filterParametersDictionary setValue:@"Y" forKey:@"Cash_Cust"];
            
            [cashCustomerSwitch setOn:YES animated:NO];
        }
    }
    
    else
    {
        [filterParametersDictionary setValue:@"N" forKey:@"Cash_Cust"];

    }
    
    
    if ([_previousFilterParametersDict valueForKey:@"Customer_No"]!=nil)
    {
        [customerNumberButton setTitle:[_previousFilterParametersDict valueForKey:@"Customer_No"] forState:UIControlStateNormal];
        [filterParametersDictionary setValue:[_previousFilterParametersDict valueForKey:@"Customer_No"] forKey:@"Customer_No"];
        [self.customerNameFilter setText:[NSString stringWithFormat:@"%@",[_previousFilterParametersDict valueForKey:@"Customer_No"]]];
        
    }
    
    
    if ([_previousFilterParametersDict valueForKey:@"Trade_Classification"]!=nil)
    {
        [tradeChannelButton setTitle:[_previousFilterParametersDict valueForKey:@"Trade_Classification"] forState:UIControlStateNormal];
        self.tradeChannelFilter.text=[NSString stringWithFormat:@"%@",[_previousFilterParametersDict valueForKey:@"Trade_Classification"]];
        
        [filterParametersDictionary setValue:[_previousFilterParametersDict valueForKey:@"Trade_Classification"] forKey:@"Trade_Classification"];
    }
        
    NSLog(@"previous filter parameters in filter %@", [_previousFilterParametersDict description]);

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    
    
    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesCollectionFilter];
    }
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    UIBarButtonItem * saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearTapped)];
    
    
    
    [saveBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                           forState:UIControlStateNormal];
    
    saveBtn.tintColor=[UIColor whiteColor];
    
    
    
    UIBarButtonItem * closeBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    
    
    
    [closeBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                            forState:UIControlStateNormal];
    
    closeBtn.tintColor=[UIColor whiteColor];
//
    
    
    //    [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:self]setTitleTextAttributes:barattributes forState:forState:UIControlStateNormal
    
   	
    self.navigationController.navigationBar.topItem.title = @"";
    
    
    self.navigationItem.rightBarButtonItem=saveBtn;
    self.navigationItem.leftBarButtonItem=closeBtn;
    
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
        
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
        
        
    }
}
-(void)closeButtonTapped
{
    if ([self.paymentFilterDelegate respondsToSelector:@selector(productsFilterDidClose)]) {
        [self.paymentFilterDelegate productsFilterDidClose];
        [self.paymentFilterDelegate filteredProductParameters:filterParametersDictionary];
    }
    [filterPopOverController dismissPopoverAnimated:YES];
}

-(void)clearTapped
{
    _customerNameFilter.text=@"";
    _tradeChannelFilter.text=@"";
    [cashCustomerSwitch setOn:NO animated:YES];
    filterParametersDictionary=[[NSMutableDictionary alloc]init];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==self.customerNameFilter) {
        titleString=@"Customer Number";
        
        MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
        filterDescVC.filterPopOverController=self.filterPopOverController;
        filterDescVC.isCreateVisit=YES;
        filterDescVC.selectedFilterDelegate=self;
        filterDescVC.descTitle=@"Customer Number";
        filterDescVC.filterDescArray=[customerListArray valueForKey:@"Customer_No"];
        [self.navigationController pushViewController:filterDescVC animated:YES];
        return NO;
    }
    
    else if (textField==self.tradeChannelFilter)
    {
        
        titleString=@"Trade Channel";
        
        MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
        filterDescVC.filterPopOverController=self.filterPopOverController;
        filterDescVC.isCreateVisit=YES;
        filterDescVC.selectedFilterDelegate=self;
        filterDescVC.descTitle=@"Trade Channel";
        
        
        //fetch trade channel for customer number
        
        
        NSMutableArray* tradeChannelArray=[[NSMutableArray alloc]init];
        
        
        NSLog(@"customer list array is %@", [customerListArray description]);
        
        for (NSInteger i=0; i<customerListArray.count; i++) {
            
            
            NSMutableArray* tempTradeChannelArray=[MedRepQueries fetchTradeChannelforCustomerNumber:[[customerListArray objectAtIndex:i] valueForKey:@"Customer_No"]];
            
            [tradeChannelArray addObject:[[tempTradeChannelArray objectAtIndex:0] valueForKey:@"Trade_Channel"] ];
            
            //adding it to customer list array for filtereing when filter button tapped
            [[customerListArray objectAtIndex:i]setValue:[[tempTradeChannelArray objectAtIndex:0] valueForKey:@"Trade_Channel"] forKey:@"Trade_Channel"];
            
            NSLog(@"trade channel array is %@", [tradeChannelArray description]);
        }
        
        NSArray *refinedTradeChannelArray = [[NSSet setWithArray: tradeChannelArray] allObjects];
        
        // NSLog(@"customer list array is %@", [customerListArray description]);
        
        
        if (refinedTradeChannelArray.count>0) {
            
            filterDescVC.filterDescArray=[refinedTradeChannelArray mutableCopy];
            [self.navigationController pushViewController:filterDescVC animated:YES];
            
        }
        

        return NO;
    }
    return YES;
}

- (IBAction)customerNameButtonTapped:(id)sender {
    
    
    titleString=@"Customer Number";
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.isCreateVisit=YES;
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=@"Customer Number";
    filterDescVC.filterDescArray=[customerListArray valueForKey:@"Customer_No"];
    [self.navigationController pushViewController:filterDescVC animated:YES];

    
}

//create visit delegate being resused
-(void)selectedValueforCreateVisit:(NSIndexPath*)indexPath
{
    if ([titleString isEqualToString:@"Customer Number"]) {
        
        [filterParametersDictionary setValue:[[customerListArray objectAtIndex:indexPath.row] valueForKey:@"Customer_No"] forKey:@"Customer_No"];
        
        
        NSLog(@"selected index is %@" ,[[customerListArray objectAtIndex:indexPath.row] valueForKey:@"Customer_No"]);
        
        
//        [customerNumberButton setTitle:[[customerListArray objectAtIndex:indexPath.row] valueForKey:@"Customer_No"] forState:UIControlStateNormal];
        
        self.customerNameFilter.text=[NSString stringWithFormat:@"%@",[[customerListArray objectAtIndex:indexPath.row] valueForKey:@"Customer_No"]];
        
        
    }
    
//    else if ([titleString isEqualToString:@"Trade Channel"])
//        
//    {
//        [filterParametersDictionary setValue:[[customerListArray objectAtIndex:indexPath.row] valueForKey:@"Trade_Classification"] forKey:@"Trade_Classification"];
//        
//        [tradeChannelButton setTitle:[[customerListArray objectAtIndex:indexPath.row] valueForKey:@"Trade_Classification"] forState:UIControlStateNormal];
//    }
}

- (IBAction)addressButtonTapped:(id)sender {
}

-(void)selectedFilterValue:(NSString*)selectedString
{
    if ([titleString isEqualToString:@"Trade Channel"])
        
    {
        [filterParametersDictionary setValue:selectedString forKey:@"Trade_Classification"];
        
        [tradeChannelButton setTitle:selectedString forState:UIControlStateNormal];
        
        self.tradeChannelFilter.text=[NSString stringWithFormat:@"%@",selectedString];
    }
}

- (IBAction)tradeChannelButtonTapped:(id)sender
{
    titleString=@"Trade Channel";
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.isCreateVisit=YES;
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=@"Trade Channel";
    
    
    //fetch trade channel for customer number
    
    
    NSMutableArray* tradeChannelArray=[[NSMutableArray alloc]init];
    
    
    NSLog(@"customer list array is %@", [customerListArray description]);
    
    for (NSInteger i=0; i<customerListArray.count; i++) {
        
        
       NSMutableArray* tempTradeChannelArray=[MedRepQueries fetchTradeChannelforCustomerNumber:[[customerListArray objectAtIndex:i] valueForKey:@"Customer_No"]];
        
        [tradeChannelArray addObject:[[tempTradeChannelArray objectAtIndex:0] valueForKey:@"Trade_Channel"] ];
        
        //adding it to customer list array for filtereing when filter button tapped
        [[customerListArray objectAtIndex:i]setValue:[[tempTradeChannelArray objectAtIndex:0] valueForKey:@"Trade_Channel"] forKey:@"Trade_Channel"];
        
        NSLog(@"trade channel array is %@", [tradeChannelArray description]);
    }
    
    NSArray *refinedTradeChannelArray = [[NSSet setWithArray: tradeChannelArray] allObjects];

   // NSLog(@"customer list array is %@", [customerListArray description]);
    
    
    if (refinedTradeChannelArray.count>0) {
        
        filterDescVC.filterDescArray=[refinedTradeChannelArray mutableCopy];
        [self.navigationController pushViewController:filterDescVC animated:YES];

    }
}
- (IBAction)cashCustomerSwitch:(UISegmentedControl *)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        
        NSLog(@"This Cash");
        
        //[filterDict setValue:@"OTC" forKey:@"Specification"];
        
        
    }
    else{
        //[filterDict setValue:@"Rx" forKey:@"Specification"];

        
        NSLog(@"This is Not Cash");
    }
    
    
}

- (IBAction)cashCustomerSwitchTapped:(id)sender {
    
    BOOL state = [sender isOn];
    
    NSLog(@"Switch is %hhd", state);
    
    if (state == YES) {
        
        NSLog(@"This Active");
        
        [filterParametersDictionary setValue:@"Y" forKey:@"Cash_Cust"];
    }
    else{
        //[filterDict setValue:@"InActive" forKey:@"Is_Active"];
        [filterParametersDictionary setValue:@"N" forKey:@"Cash_Cust"];

        
        NSLog(@"This is InActive");
        
    }
}
- (IBAction)searchButtonTapped:(id)sender
{
    //test search
    
    NSPredicate *predicateTest = [NSPredicate predicateWithFormat:@"Cash_Cust CONTAINS[cd] %@",[filterParametersDictionary valueForKey:@"Cash_Cust"]];
    NSArray *temp =[customerListArray filteredArrayUsingPredicate:predicateTest];
 
    NSMutableArray * filteredVisitsArray=[[NSMutableArray alloc]init];
    NSString* filter = @"%K == [c] %@";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K LIKE[cd] %@", @"Customer_No", [NSString stringWithFormat:@"*%@*",[filterParametersDictionary valueForKey:@"Customer_No"]]];
    
    //    NSPredicate *tradeChannelPredicate = [NSPredicate predicateWithFormat:@"%K LIKE[cd] %@", @"Customer_No", [NSString stringWithFormat:@"*%@*",[filterParametersDictionary valueForKey:@"Customer_No"]]];
    
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSLog(@"filtered parameter dict is %@", [filterParametersDictionary description]);
    
    if([[filterParametersDictionary valueForKey:@"Customer_No"] length]>0)
    {
        [predicateArray addObject:predicate];
    }
    if([[filterParametersDictionary valueForKey:@"Trade_Classification"] length]>0)
    {
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Trade_Channel", [filterParametersDictionary valueForKey:@"Trade_Classification"]]];
    }
    if([[filterParametersDictionary valueForKey:@"Cash_Cust"] length]>0) {
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:filter, @"Cash_Cust", [filterParametersDictionary valueForKey:@"Cash_Cust"]]];
    }
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"compound predicate array is %@", [compoundpred description]);
    
    filteredVisitsArray=[[customerListArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    NSLog(@"filtered array %d", [filteredVisitsArray count]);
    
    if (predicateArray.count==0) {
        
        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Missing Data", nil) message:@"Please select your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [noFilterAlert show];
    }
    else if (filteredVisitsArray.count>0)
    {
        if ([self.paymentFilterDelegate respondsToSelector:@selector(filteredProducts:)]) {
            [self.paymentFilterDelegate filteredProducts:filteredVisitsArray];
            [self.paymentFilterDelegate filteredProductParameters:filterParametersDictionary];
            [self.filterPopOverController dismissPopoverAnimated:YES];
        }
    }
    else
    {
//        UIAlertView* noFilterAlert=[[UIAlertView alloc]initWithTitle:@"No Matches" message:@"please change your filter criteria and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [noFilterAlert show];
        [MedRepDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];

    }
}

- (IBAction)resetButtonTapped:(id)sender
{
    _previousFilterParametersDict=[[NSMutableDictionary alloc]init];
    
    if ([self.paymentFilterDelegate respondsToSelector:@selector(productsFilterdidReset)]) {
        [self.paymentFilterDelegate productsFilterdidReset];
        [self.filterPopOverController dismissPopoverAnimated:YES];
    }
}
@end
