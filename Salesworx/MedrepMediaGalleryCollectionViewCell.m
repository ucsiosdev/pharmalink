//
//  MedrepMediaGalleryCollectionViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/5/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "MedrepMediaGalleryCollectionViewCell.h"

@implementation MedrepMediaGalleryCollectionViewCell
@synthesize mediaImageView,mediaScrollview;
- (void)awakeFromNib {
    // Initialization code
    mediaScrollview.delegate=self;
    
    mediaScrollview.autoresizesSubviews = YES;
    mediaScrollview.multipleTouchEnabled =YES;
    mediaScrollview.maximumZoomScale = 4.0;
    mediaScrollview.minimumZoomScale = 1.0;
    mediaScrollview.clipsToBounds = YES;
    // mediaScrollview.delegate = self;
    mediaScrollview.zoomScale = 1.0;
    
    
    mediaScrollview.contentSize = mediaImageView.frame.size;

    
   // [mediaScrollview setZoomScale:97.0];
    
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"MedrepMediaGalleryCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    
    }
    
    return self;
    
}



@end
