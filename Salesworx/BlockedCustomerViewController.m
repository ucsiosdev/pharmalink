//
//  BlockedCustomerViewController.m
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "BlockedCustomerViewController.h"
#import "MedRepDefaults.h"

@interface BlockedCustomerViewController ()

@end

@implementation BlockedCustomerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:[kBlockedCustomersTitle localizedCapitalizedString]];

    gridView=nil;
    gridView = [[GridView alloc] initWithFrame:CGRectMake(8, 8, 1008, 689)];
    [gridView setDataSource:self];
    [gridView setDelegate:self];
    [gridView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [gridView.layer setBorderWidth:1.0f];
    [self.view addSubview:gridView];
    filterTableView.backgroundView=nil;[filterTableView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [filterTableView.layer setBorderWidth:1.0f];
    NSString *sql = @"Select Customer_ID , Customer_Name , Contact , Phone from TBL_Customer where Cust_Status='N' OR Credit_Hold = 'Y'";
    dataArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:sql];

}
-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"Block Documents");
    // [self viewReportAction];
}
- (void)currencyTypeChanged:(NSDictionary *)customer {
    
    [currencyTypePopOver dismissPopoverAnimated:YES];
    currencyTypeViewController = nil;
    currencyTypePopOver = nil;
    
}
#pragma TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableViews numberOfRowsInSection:(NSInteger)section {
    return 6;
    
}
- (CGFloat)tableView:(UITableView *)tableViews heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, tv.bounds.size.width, 44)]  ;
    UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, headerView.bounds.size.width, headerView.bounds.size.height)]  ;
    [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [companyNameLabel setText:NSLocalizedString(@"Filter", nil)];
    [companyNameLabel setBackgroundColor:UIColorFromRGB(0xF6F7FB)];
    [companyNameLabel setTextColor:[UIColor darkGrayColor]];
    //    [companyNameLabel setShadowColor:[UIColor whiteColor]];
    //    [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
    [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];
    [companyNameLabel setTextAlignment:NSTextAlignmentCenter];
    
    [headerView addSubview:companyNameLabel];
    return headerView;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return NSLocalizedString(@"Filter", nil);
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"CustomerDetailViewCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
    }

    
    
    [cell.textLabel setText:NSLocalizedString(@"Filter", nil)];
    [cell.detailTextLabel setText:@"Value"];
    [cell.textLabel setFont:LightFontOfSize(14.0f)];
    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];

    return cell;
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==1)
    {
        currencyTypeViewController=nil;
        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
        [currencyTypeViewController setTarget:self];
        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
        [currencyTypeViewController setPreferredContentSize:CGSizeMake(300,300)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
        currencyTypePopOver=nil;
        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        currencyTypePopOver.delegate=self;
        [currencyTypePopOver presentPopoverFromRect:CGRectMake(600, 190, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    currencyTypePopOver=nil;
    currencyTypeViewController=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return dataArray.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 4;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    
    if (column == 0) {
        title = [NSString stringWithFormat:@"   %@",NSLocalizedString(@"Code", nil)];
    } else if (column == 1) {
        title = NSLocalizedString(@"Name", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Contact", nil);
    } else if(column == 3) {
        title = NSLocalizedString(@"Phone", nil);
    } 
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";

    NSDictionary *data = [dataArray objectAtIndex:row];
    
    if (column == 0) {
        text = [NSString stringWithFormat:@"     %@", [data stringForKey:@"Customer_ID"]];
    } else if (column == 1) {
        text = [NSString stringWithFormat:@"%@", [data stringForKey:@"Customer_Name"]];
    } else if (column == 2) {
        text = [NSString stringWithFormat:@"%@", [data stringForKey:@"Contact"]];
    } else if (column == 3) {
        text = [NSString stringWithFormat:@"%@", [data stringForKey:@"Phone"]];
    }
    return text;
    
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    if (columnIndex == 0) {
        return 20;
    } else if (columnIndex == 1) {
        return 40;
    } else if (columnIndex == 2) {
        return 20;
    }
    else if (columnIndex == 3) {
        return 20;
    }
    else
        return 0;
}
- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
