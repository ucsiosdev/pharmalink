//
//  SWTextViewCell.h
//  SWFoundation
//
//  Created by Irfan Bashir on 7/18/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "EditableCell.h"

@interface SWTextViewCell : EditableCell < UITextViewDelegate > {
    UITextView *textView;
}

@property (nonatomic, strong) UITextView *textView;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
@end