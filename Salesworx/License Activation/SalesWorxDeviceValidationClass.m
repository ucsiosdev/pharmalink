//
//  SalesWorxDeviceValidationClass.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 11/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxDeviceValidationClass.h"
#import "SWDefaults.h"
#import "DataSyncManager.h"
#import "SWPlatform.h"
#import <GAI.h>
#import <GAITracker.h>



@implementation SalesWorxDeviceValidationClass

-(void)updateFailureDetailstoGoogleAnalytics
{
    NSMutableDictionary * googleAnalyticsDict=[[NSMutableDictionary alloc]init];
    
    [googleAnalyticsDict setValue:[[SWDefaults userProfile]valueForKey:@"User_Name"] forKey:@"username"];
    [googleAnalyticsDict setValue:[SWDefaults licenseIDForInfo] forKey:@"Customer_ID"];
    
    
    
    NSData *SelectedSeverData = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    id SelectedServersDetailsjson = [NSJSONSerialization JSONObjectWithData:SelectedSeverData options:0 error:nil];
    
    [googleAnalyticsDict setValue:[SelectedServersDetailsjson valueForKey:@"name"] forKey:@"Server_Name"];
    
    [googleAnalyticsDict setValue:[SelectedServersDetailsjson valueForKey:@"url"] forKey:@"Server_Url"];
    
    [googleAnalyticsDict setValue:[[DataSyncManager sharedManager]getDeviceID] forKey:@"Device_ID"];
    
    
    [SWDefaults updateDeviceValidationFailureDetailstoGoogleAnalytics:googleAnalyticsDict];
    
    
    NSLog(@"google analytics dict is %@", googleAnalyticsDict);
    
}
-(NSString *)ValidateDevice
{
    return @"Success";
    
    NSString *customerID = [SWDefaults stringBetweenString:@"C" andString:@"K" andMainString:[SWDefaults licenseIDForInfo]];
    NSString *cavId = [[customerID stringByAppendingString:@"A"] stringByAppendingString:@"19"];
    
    NSString *defaultsDeviceID = [[DataSyncManager sharedManager]getDeviceID];
    const char *s = [defaultsDeviceID cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    CC_SHA256(keyData.bytes, keyData.length, digest);
    NSData *out=[NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    NSString *hash=[out debugDescription];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    NSString *did = hash;
    
    NSDate *currentDate = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *ts = [dateFormatter stringFromDate:currentDate];
    
    
    NSString *post = [NSString stringWithFormat:@"cavid=%@&did=%@&ts=%@&usr=%@",cavId,did,ts,[SWDefaults username]];
    NSLog(@"Request Parameter of device-id verification %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.ucssolutions.com/licman/val-dev.php"]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSError* connectionError;
    NSURLResponse * _Nullable response;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&connectionError];
    
    if (data!=nil) {
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                             options:kNilOptions
                                                               error:&error];
        NSLog(@"device validation response %@", json);
        
        if ([[SWDefaults getValidStringValue:[json valueForKey:@"VCODE"]]length] != 0)
        {
            NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            
            NSLog(@"Response %@",str);
            NSLog(@"connectionError  %@",connectionError);
            
            
            NSString *hashInputString=[NSString stringWithFormat:@"%@|%@|%@",cavId,did,ts];
            
            const char *s=[hashInputString cStringUsingEncoding:NSASCIIStringEncoding];
            NSData *keyData=[NSData dataWithBytes:s length:strlen(s)];
            
            uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
            CC_SHA256(keyData.bytes, keyData.length, digest);
            NSData *out=[NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
            NSString *hash=[out debugDescription];
            hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
            hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
            hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
            NSLog(@"out put %@",hash);
            
            if([hash isEqualToString:[SWDefaults getValidStringValue:[json valueForKey:@"VCODE"]]])
            {
                return @"Success";
            }
            else
            {
                NSLog(@"device validation has failed");
                
                [self updateFailureDetailstoGoogleAnalytics];
                
                return @"Device Validation Failed";
                return @"Success";
            }
        }
        else
        {
            [self updateFailureDetailstoGoogleAnalytics];
            
            NSLog(@"device validation has failed");
            
            return [json valueForKey:@"ERROR"];
            return @"Success";
        }
    }
    else {
        return @"Success";
    }
}

@end
