//
//  UIAlertView+MedRepAlertView.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/27/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (MedRepAlertView)
- (void) hide;
@end
