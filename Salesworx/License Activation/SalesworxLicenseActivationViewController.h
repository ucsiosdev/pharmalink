////
////  SalesworxLicenseActivationViewController.h
////  SalesWorx
////
////  Created by Unique Computer Systems on 11/23/15.
////  Copyright © 2015 msaad. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import "MBProgressHUD.h"
//#import "SynchroniseViewController.h"
//#import "RSA.h"
//#import "AES.h"
//#import <NewActivationViewController.h>
//
//@interface SalesworxLicenseActivationViewController : UIViewController<UIAlertViewDelegate,UITextFieldDelegate>
//
//{
//    NSMutableDictionary* licenseTypesDictionary;
//
//    NSString *licenseEvalutationTime,*licenseType;
//
//    SynchroniseViewController *verifyLicenceController;
//
//
//    RSA *rsaObj;
//    AES *aesObj;
//
//    NSString* appVersionID;
//
//    NSString*customerID;
//
//    NSString*deviceID;
//
//    NSString *decryString;
//
//    UIAlertController *myAlertController;
//
//    UIAlertAction* ok;
//
//
//}
//@property(strong,nonatomic) NewActivationViewController *activationManager;
//
//@property (strong, nonatomic) IBOutlet UILabel *versionNumberLbl;
//- (IBAction)activateButtonTapped:(id)sender;
//@property (strong, nonatomic) IBOutlet UISlider *licenseTypeSlider;
//@property (strong, nonatomic) IBOutlet UITextField *customerIDTextField;
//@end


//
//  SalesworxLicenseActivationViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/23/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "SynchroniseViewController.h"
#import "RSA.h"
#import "AES.h"
#import <NewActivationViewController.h>
#import "SalesWorxActivateNewUserViewController.h"
#import <MessageUI/MessageUI.h>


@interface SalesworxLicenseActivationViewController : UIViewController<UIAlertViewDelegate,UITextFieldDelegate,UIPopoverControllerDelegate,MFMailComposeViewControllerDelegate,UIPopoverPresentationControllerDelegate>

{
    NSMutableDictionary* licenseTypesDictionary;
    NSString *licenseEvalutationTime,*licenseType;
    
    SynchroniseViewController *verifyLicenceController;
    
    
    RSA *rsaObj;
    AES *aesObj;
    
    NSString* appVersionID;
    
    NSString*customerID;
    
    NSString*deviceID;
    
    NSString *decryString;
    
    BOOL popOverDismissed;
    
    UIAlertController *alertController;
    
    UIAlertAction* done;
    UIBarButtonItem *btnSetting;
    UITextField *txtURL;
    UIPopoverController *popOver;
    BOOL URLChanged;
    NSString *previousCustomerID;
    NSString *customerIDReturnFromServerAfterChangeURL;
}
@property(strong,nonatomic) NewActivationViewController *activationManager;
@property (strong, nonatomic) IBOutlet UILabel *versionNumberLbl;
@property (weak, nonatomic) IBOutlet UIButton *btnRetry;


@end

