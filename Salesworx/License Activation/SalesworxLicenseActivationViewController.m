////
////  SalesworxLicenseActivationViewController.m
////  SalesWorx
////
////  Created by Unique Computer Systems on 11/23/15.
////  Copyright © 2015 msaad. All rights reserved.
////
//
//#import "SalesworxLicenseActivationViewController.h"
//#import "MedRepDefaults.h"
//#import "SWDefaults.h"
//#import <Security/Security.h>
//#import "NSString+Additions.h"
//#import "SSKeychain.h"
//#import <LoginViewController.h>
//#import <SynchroniseViewController.h>
//#import <Reachability.h>
//#import "SalesWorxUserActivationViewController.h"
//#import <NewActivationViewController.h>
//
//@interface SalesworxLicenseActivationViewController ()
//
//@end
//
//@implementation SalesworxLicenseActivationViewController
//@synthesize licenseTypeSlider,versionNumberLbl;
//- (void)viewDidLoad {
//    [super viewDidLoad];
//
//
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveLicenseActivationMessage:) name:@"License Activated Successfully" object:self];
//
//
//    CGRect frame = CGRectMake(0, 0, 400, 44);
//    UILabel *label = [[UILabel alloc] initWithFrame:frame];
//    label.backgroundColor = [UIColor clearColor];
//    label.font = headerTitleFont;
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor whiteColor];
//    label.text = @"License Activation";
//    self.navigationItem.titleView = label;
//
//
//
//
//
//    licenseTypesDictionary=[[NSMutableDictionary alloc]init];
//
//    NSMutableArray* thirtyDayLicenseArray=[[NSMutableArray alloc]init];
//
//    [thirtyDayLicenseArray setValue:@"30" forKey:@"Evaluation_Time"];
//
//
//
//    // Do any additional setup after loading the view from its nib.
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//
//-(void)viewWillAppear:(BOOL)animated
//{
//
//    licenseEvalutationTime =@"30";
//    licenseType=@"EVAL_TIME";
//
//    customerID=@"C16K82563";
//
//    UINavigationBar *nbar = self.navigationController.navigationBar;
//
//    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
//        //iOS 7
//        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
//        nbar.translucent = NO;
//
//        nbar.tintColor = [UIColor whiteColor]; //bar button item color
//
//    } else {
//        //ios 4,5,6
//        nbar.tintColor = [UIColor whiteColor];
//
//
//    }
//
//
//    NSBundle *bundle = [NSBundle mainBundle];
//    NSString *appVersion = [bundle objectForInfoDictionaryKey:(NSString *)@"CFBundleShortVersionString"];
//    NSString *appBuildNumber = [bundle objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
//
//    versionNumberLbl.text=[NSString stringWithFormat:@"Version : %@(%@)",appVersion,appBuildNumber];
//
//
//}
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/
//- (IBAction)licenseTypeSliderSwiped:(UISlider *)sender
//{
//
//    NSLog(@"check current slider value %0.2f", sender.value);
//
//
//    if(sender.value<=5)
//    {
//        sender.value = 5;
//        licenseEvalutationTime=@"30";
//
//        licenseType=@"EVAL_TIME";
//    }
//
//    else if(sender.value >15 && sender.value<=40)
//    {
//        sender.value = 30;
//
//        licenseEvalutationTime=@"60";
//        licenseType=@"EVAL_TIME";
//
//
//    }
//
//    else if(sender.value >40 && sender.value<=75)
//    {
//        sender.value = 58;
//
//        licenseEvalutationTime=@"90";
//        licenseType=@"EVAL_TIME";
//
//
//    }
//
//    else if(sender.value >75 && sender.value<=100)
//    {
//        sender.value = 90;
//
//        licenseEvalutationTime=@"0";
//        licenseType=@"PERMANENT";
//
//
//    }
//
//    [licenseTypeSlider setValue:sender.value animated:YES];
//
//}
//
//
//- (IBAction)activateButtonTapped:(id)sender {
//
//    NSLog(@"License Evaluation Time is %@",licenseEvalutationTime );
//
//
//
//    [self activateLicense];
//
//
//
//    /*
//    verifyLicenceController=[[SynchroniseViewController alloc]init];
//    isValid=[verifyLicenceController startLicenceingWithCustomerIF:customerIDStr andLicenceType:licenseType andLicenceLimit:licenseEvalutationTime];
//
//    if (isValid)
//
//    {
//        NSLog(@"LICENSE IS VALID");
//    }
//
//    else
//    {
//        NSLog(@"LICENSE IS INVALID");
//    }*/
//
//
//
//}
//
//#pragma mark License Activation Method
//
//-(void)activateLicense
//{
//
//
//    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
//    if (networkStatus == NotReachable) {
//        NSLog(@"There IS NO internet connection");
//
//
//        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
//
//
//
//            //Step 1: Create a UIAlertController
//            UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:@"Internet Connection Unavailable"
//                                                                                       message:@"Please connect to internet and try again"
//                                                                                preferredStyle:UIAlertControllerStyleAlert                   ];
//
//            //Step 2: Create a UIAlertAction that can be added to the alert
//            UIAlertAction* ok = [UIAlertAction
//                                 actionWithTitle:@"OK"
//                                 style:UIAlertActionStyleDefault
//                                 handler:^(UIAlertAction * action)
//                                 {
//                                     //Do some thing here, eg dismiss the alertwindow
//                                     [myAlertController dismissViewControllerAnimated:YES completion:nil];
//
//                                 }];
//
//            //Step 3: Add the UIAlertAction ok that we just created to our AlertController
//            [myAlertController addAction: ok];
//
//            [self presentViewController:myAlertController animated:YES completion:^{
//
//            }];
//
//        }
//
//        else
//        {
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wdeprecated-declarations"
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Unavailable"
//                                                            message:@"Please connect to internet and try again"
//                                                           delegate:self
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
//#pragma clang diagnostic pop
//        }
//
//
//
//
//
//    }
//    else
//    {
//        NSLog(@"There IS internet connection");
//
//
//
//    NSString* missingString;
//    if (licenseType.length==0) {
//
//        missingString=@"licenseType";
//    }
//
//    else if (licenseEvalutationTime.length==0)
//    {
//        missingString=@"Evaluation Time";
//    }
//
//    if (missingString.length>0) {
//
//
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
//
//
//
//    //Step 1: Create a UIAlertController
//    UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:@"Missing Data"
//                                                                               message:[NSString stringWithFormat:@"Please enter %@",missingString]
//                                                                        preferredStyle:UIAlertControllerStyleAlert                   ];
//
//    //Step 2: Create a UIAlertAction that can be added to the alert
//    UIAlertAction* ok = [UIAlertAction
//                         actionWithTitle:@"OK"
//                         style:UIAlertActionStyleDefault
//                         handler:^(UIAlertAction * action)
//                         {
//                             //Do some thing here, eg dismiss the alertwindow
//                             [myAlertController dismissViewControllerAnimated:YES completion:nil];
//
//                         }];
//
//    //Step 3: Add the UIAlertAction ok that we just created to our AlertController
//    [myAlertController addAction: ok];
//
//        [self presentViewController:myAlertController animated:YES completion:^{
//
//        }];
//
//    }
//
//    else
//    {
//    #pragma clang diagnostic push
//    #pragma clang diagnostic ignored "-Wdeprecated-declarations"
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Data"
//                                                        message:[NSString stringWithFormat:@"Please enter %@",missingString]
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//    #pragma clang diagnostic pop
//    }
//    }
//
//
//    else
//    {
//
//
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//
//
//
//
//        NSString* refinedCustomerID=[[NSString alloc]init];
//
//
//        __block BOOL isValid = NO;
//        rsaObj = [[RSA alloc] init];
//        NSString *guidString = [NSString createGuid];
//        NSLog(@"String Guid %@",guidString);
//
//        guidString = [guidString stringByReplacingOccurrencesOfString: @"-" withString:@""];
//        //    //NSLog(@"String Guid with out %@",guidString);
//
//
//        NSString *encryptString=[[rsaObj encryptToString:guidString]urlencode];
//
//
//
//        //avid    = @"8";
//         NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
//         //appVersionID  = [infoDict objectForKey:@"CFBundleVersion"];
//
//        //this is not app version ID, its the version ID of licensing system
//        appVersionID = @"19";
//        [SWDefaults setLicenseIDForInfo:customerID];
//        refinedCustomerID     = [SWDefaults stringBetweenString:@"C" andString:@"K" andMainString:customerID] ;
//        [SWDefaults setLicenseCustomerID:refinedCustomerID];
//
//
//        NSLog(@" customer ID in new screen  %@", customerID);
//
//
//        deviceID     = [[DataSyncManager sharedManager]getDeviceID];
//
//
//
//
//        NSString *myRequestString =[NSString stringWithFormat:@"avid=%@&cid=%@&sid=%@&lt=%@&ll=%@&k=%@",appVersionID,customerID,deviceID,licenseType,licenseEvalutationTime,encryptString];
//        NSLog(@"my req str %@", myRequestString);
//
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//        NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
//        [request setURL:[NSURL URLWithString:kLicenseActivationURL]];
//        [request setHTTPMethod:@"POST"];
//        [request setHTTPBody:myRequestData];
//
//        NSLog(@"license activation request string is %@", request);
//
//
//        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
//
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//
//
//            NSError* error;
//            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//            if([data length] >0 && error == nil && [httpResponse statusCode] == 200) /*sucess*/
//            {
//                NSString *responseJsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//
//                NSLog(@"Response %@",responseJsonString);
//                aesObj = [[AES alloc] init];
//                if([responseJsonString hasPrefix:@"<License>"])
//                {
//                    NSString *licenseString = [SWDefaults stringBetweenString:@"<License>" andString:@"</License>" andMainString:responseJsonString];
//                    NSLog(@"license Key %@", licenseString);
//
//
//
//                    //now check if this license is valid
//
//                    decryString =  [aesObj decrypt:licenseString withKey:guidString];
//
//                    NSLog(@"decrypted license %@", decryString);
//
//                    NSArray *list = [decryString componentsSeparatedByString:@""];
//
//                    NSLog(@"license list array %@", [list description]);
//
//
//                    [SWDefaults setLicenseKey:[NSDictionary dictionaryWithObjects:list forKeys:[NSArray arrayWithObjects:@"cid",@"avid",@"sid",@"lt",@"ll",@"date", nil]]] ;
//
//
//
//
//                    NSDictionary* licenseDict=[SWDefaults licenseKey];
//
//                    NSLog(@"license limit dict is %@",[licenseDict description]);
//
//
//                    NSString* licenseTime=[licenseDict valueForKey:@"lt"];
//
//
//
//                    NSArray* tempEvalucationTime=[decryString componentsSeparatedByString:@"EVAL_TIME"];
//
//                    NSString* licEvalTimeStr=[[NSString alloc]init];
//
//
//                    if (tempEvalucationTime.count>0) {
//
//                        if([licenseTime isEqualToString:@"PERMANENT"])
//
//                        {
//
//                            NSLog(@"this is a permanent license");
//
//
//
//                            [SWDefaults setIsActivationDone:@"NO"];
//
//
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                [[NSNotificationCenter defaultCenter]postNotificationName:@"License Activated Successfully" object:self];
//
//                            });
//
//
//
//
//
//
//
//                        }
//
//                        else
//                        {
//
//
//
//
//                            licEvalTimeStr =licenseTime;
//
//
//
//                    NSLog(@"eval time string  %@", [licEvalTimeStr description]);
//
//
//                    NSString *licenseActivatedDate = [licenseDict valueForKey:@"date"];
//
//
//                    NSLog(@"license activated date %@", licenseActivatedDate);
//
//                    //now set this date in device keychain to verify license during login
//
//
//                    [SSKeychain setPassword:licenseActivatedDate forService:kLicenseActivationDateKey account:@"user"];
//
//
//                    NSString* keyChainDate=[SSKeychain passwordForService:kLicenseActivationDateKey account:@"user"];
//                    NSLog(@"key chain date is %@", keyChainDate);
//
//
//                isValid=[SWDefaults validateSalesWorxLicense:[licenseDict valueForKey:@"ll"]];
//
//                    if (isValid==YES) {
//
//                        //now move to activation screen
//
//                         [[NSNotificationCenter defaultCenter]postNotificationName:@"License Activated Successfully" object:self];
//
//                    }
//                    else
//                    {
//
//                    }
//
//                }
//                    }
//
//
//
//                }
//
//
//                else if ([responseJsonString hasPrefix:@"<Error>"])
//                {
//                    NSString *errorString = [SWDefaults stringBetweenString:@"<Error>" andString:@"</Error>" andMainString:responseJsonString] ;
//
//                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
//
//
//
//                        //Step 1: Create a UIAlertController
//        UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:errorString
//                                                                                                   message:@"please contact your system administrator"
//                                                                                            preferredStyle:UIAlertControllerStyleAlert                   ];
//
//                        //Step 2: Create a UIAlertAction that can be added to the alert
//                        UIAlertAction* ok = [UIAlertAction
//                                             actionWithTitle:@"OK"
//                                             style:UIAlertActionStyleDefault
//                                             handler:^(UIAlertAction * action)
//                                             {
//                                                 //Do some thing here, eg dismiss the alertwindow
//                                                 [myAlertController dismissViewControllerAnimated:YES completion:nil];
//
//                                             }];
//
//                        //Step 3: Add the UIAlertAction ok that we just created to our AlertController
//                        [myAlertController addAction: ok];
//
//                        [[[[UIApplication sharedApplication]keyWindow]rootViewController]presentViewController:myAlertController animated:YES completion:^{
//
//                        }];
//
//
//
//
//                    }
//
//                    else
//                    {
//        #pragma clang diagnostic push
//        #pragma clang diagnostic ignored "-Wdeprecated-declarations"
//                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:errorString
//                                                                        message:@"please contact your system administrator"
//                                                                       delegate:self
//                                                              cancelButtonTitle:@"OK"
//                                                              otherButtonTitles:nil];
//                        [alert show];
//        #pragma clang diagnostic pop
//                    }
//
//
//
//                }
//
//
//
//            }
//
//            else
//            {
//                NSLog(@"LICENSE ACTIVATION FAILED %@", connectionError.localizedDescription);
//
//
//                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
//
//
//
//                    //Step 1: Create a UIAlertController
//                    UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:@"License Activation Failed"
//                                                                                               message:[NSString stringWithFormat:@"%@ please try again",connectionError.localizedDescription]
//                                                                                        preferredStyle:UIAlertControllerStyleAlert                   ];
//
//                    //Step 2: Create a UIAlertAction that can be added to the alert
//                    UIAlertAction* ok = [UIAlertAction
//                                         actionWithTitle:@"OK"
//                                         style:UIAlertActionStyleDefault
//                                         handler:^(UIAlertAction * action)
//                                         {
//                                             //Do some thing here, eg dismiss the alertwindow
//                                             [myAlertController dismissViewControllerAnimated:YES completion:nil];
//
//
//
//                                         }];
//
//                    //Step 3: Add the UIAlertAction ok that we just created to our AlertController
//                    [myAlertController addAction: ok];
//
//                    [[[[UIApplication sharedApplication]keyWindow]rootViewController]presentViewController:myAlertController animated:YES completion:^{
//
//                    }];
//
//
//
//
//                }
//
//                else
//                {
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wdeprecated-declarations"
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"License Activation Failed"
//                                                                    message:[NSString stringWithFormat:@"%@ please try again",connectionError.localizedDescription]
//                                                                   delegate:self
//                                                          cancelButtonTitle:@"OK"
//                                                          otherButtonTitles:nil];
//                    [alert show];
//#pragma clang diagnostic pop
//                }
//
//
//
//            }
//
//
//
//
//        }];
//
//    }
//}
//}
//
//#pragma mark UIAlertView Delegate Methods
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (alertView.tag==100) {
//
//
//
//
//        [SWDefaults setSalesWorxLicenseActivationStatus:YES];
//
//
//
//        NewActivationViewController *loginVC = [[NewActivationViewController alloc] initWithNoDB];
//
//       // [loginVC showActivate];
//
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
//
//        [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
//
//
//    }
//
//    else if (alertView.tag==1000)
//
//    {
//
//        [SWDefaults setSalesWorxLicenseActivationStatus:YES];
//
//        //updATE NEW ACTIVATION SCREEN HERE
//        //                SalesWorxUserActivationViewController * userActivationVC=[[SalesWorxUserActivationViewController alloc]init];
//
//
//        NewActivationViewController *loginVC = [[NewActivationViewController alloc] initWithNoDB];
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
//
//        [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
//
//
//
//    }
//}
//
//#pragma mark License Activated Notification Method
//
//-(void)receiveLicenseActivationMessage:(NSNotification*)licenseNotification
//{
//
//
//    if ([[licenseNotification name]isEqualToString:@"License Activated Successfully"]) {
//
//        NSString* licenseAlertTitle =@"License Activated Successfully";
//
//
//        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
//
//
//
////            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations"
////                                                            message:licenseAlertTitle
////                                                           delegate:self
////                                                  cancelButtonTitle:@"OK"
////                                                  otherButtonTitles:nil];
////            alert.tag=1000;
////            [alert show];
//
//
//
//            //Step 1: Create a UIAlertController
//            myAlertController = [UIAlertController alertControllerWithTitle:@"Congratulations"
//                                                                                       message:licenseAlertTitle
//                                                                                preferredStyle:UIAlertControllerStyleAlert                   ];
//
//            //Step 2: Create a UIAlertAction that can be added to the alert
//             ok = [UIAlertAction
//                                 actionWithTitle:@"OK"
//                                 style:UIAlertActionStyleDefault
//                                 handler:^(UIAlertAction * action)
//                                 {
//                                     //Do some thing here, eg dismiss the alertwindow
//                                     //[myAlertController dismissViewControllerAnimated:YES completion:nil];
//
//                                     [myAlertController dismissViewControllerAnimated:YES completion:nil];
//
//
//                                 }];
//
//            //Step 3: Add the UIAlertAction ok that we just created to our AlertController
//            [myAlertController addAction: ok];
//
//            [[[[UIApplication sharedApplication]keyWindow]rootViewController]presentViewController:myAlertController animated:YES completion:^{
//
//
//
//
//                [SWDefaults setSalesWorxLicenseActivationStatus:YES];
//
//          //updATE NEW ACTIVATION SCREEN HERE
////                SalesWorxUserActivationViewController * userActivationVC=[[SalesWorxUserActivationViewController alloc]init];
//
//
//                NewActivationViewController *loginVC = [[NewActivationViewController alloc] initWithNoDB];
//                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
//
//                [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
//
//
//
//            }];
////
//
//
//
//        }
//
//        else
//        {
//    #pragma clang diagnostic push
//    #pragma clang diagnostic ignored "-Wdeprecated-declarations"
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations"
//                                                            message:licenseAlertTitle
//                                                           delegate:self
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            alert.tag=100;
//            [alert show];
//    #pragma clang diagnostic pop
//        }
//
//
//    }
//
//
//
//
//
//}
//
//
//-(void)viewWillDisappear:(BOOL)animated
//{
//    ok=nil;
//    myAlertController=nil;
//
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//
//#pragma mark UITextField Delegate Methods
//
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    if (textField==self.customerIDTextField) {
//
//        NSString *trimmedString = [textField.text stringByTrimmingCharactersInSet:
//                                   [NSCharacterSet whitespaceCharacterSet]];
//
//        self.customerIDTextField.text=trimmedString;
//        customerID=trimmedString;
//
//        [[NSUserDefaults standardUserDefaults]setObject:customerID forKey:@"Customer_ID"];
//
//
//    }
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//
//    [textField resignFirstResponder];
//
//    return YES;
//
//}
//@end
//
//
//
//
//

//
//  SalesworxLicenseActivationViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/23/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SalesworxLicenseActivationViewController.h"
#import "MedRepDefaults.h"
#import "MedRepTextField.h"
#import "SWDefaults.h"
#import <Security/Security.h>
#import "NSString+Additions.h"
#import "SSKeychain.h"
#import <LoginViewController.h>
#import <SynchroniseViewController.h>
#import <Reachability.h>
#import "SalesWorxUserActivationViewController.h"
#import <NewActivationViewController.h>
#import "UIDeviceHardware.h"

@interface SalesworxLicenseActivationViewController ()

@end

@implementation SalesworxLicenseActivationViewController

@synthesize versionNumberLbl;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveLicenseActivationMessage:) name:@"License Activated Successfully" object:self];
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Initializing SalesWorx";
    //self.navigationItem.titleView = label;
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Initializing SalesWorx", nil)];

    
    
    licenseTypesDictionary=[[NSMutableDictionary alloc]init];
    NSMutableArray* thirtyDayLicenseArray=[[NSMutableArray alloc]init];
    [thirtyDayLicenseArray setValue:@"30" forKey:@"Evaluation_Time"];

    licenseEvalutationTime=@"0";
    licenseType=@"PERMANENT";
    _btnRetry.hidden = YES;
    customerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
    
    if (customerID == nil) {
        customerID=@"C16K82563";
    }
    previousCustomerID = customerID;
    if ([[SWDefaults licenseKey] count] == 0)
    {
        NSLog(@"activate license called from did load");
        popOverDismissed=YES;
        [self activateLicense];
    }
    else
    {
        popOverDismissed=YES;
        [self performSelector:@selector(loadingNextView) withObject:nil afterDelay:5.0f];
    }
}


- (void)loadingNextView
{
    
    
    //check if popover is visible, if popover is visible dont load next view untill user taps on save or cancel
    
    
//    if (popOver.popoverVisible==YES) {
//        NSLog(@"POPOVER IS VISIBLE SO NOT LOADING NEXT VIEW");
//    }
//    else{
    
    
    if (popOverDismissed==NO) {
        NSLog(@"trying to load next view but not allowed because popover is visible");
    }
    else{
    
    if ([[SWDefaults licenseKey] count] != 0)
    {
        //-------Check for user activation
        NSLog(@"%@",[SWDefaults isActivationDone]);
        
        
        BOOL  databaseExists=[[SWDatabaseManager retrieveManager]databaseExists];
        
        
        NSMutableArray* tblUserArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"Select * from TBL_User"];
        
        NSLog(@"user array count is %d and database exists? %hhd",tblUserArray.count,databaseExists);
        
        if (databaseExists==NO || tblUserArray.count==0) {
            
            
        }
        
        if ( URLChanged || databaseExists==NO || tblUserArray.count==0)
        {
            NSLog(@"Not Activated");
            
            //-------For no activation, push activation controller
           
            /*
            NewActivationViewController *loginVC = [[NewActivationViewController alloc] initWithNoDB];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
            [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];*/
            
            
         

            
            
            SalesWorxActivateNewUserViewController * newUserVC=[[SalesWorxActivateNewUserViewController alloc]init];
            
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newUserVC] ;
            
            [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
            

            
        }
        else
        {
            LoginViewController *loginVC = [[LoginViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
            [[[UIApplication sharedApplication]keyWindow]setRootViewController:navigationController];
        }
    }
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    UINavigationBar *nbar = self.navigationController.navigationBar;
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
    
    btnSetting = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"SettingIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnSetting)];
    self.navigationItem.rightBarButtonItem=btnSetting;
    
    versionNumberLbl.text=[NSString stringWithFormat:@"Version  %@",[[[SWDefaults alloc]init] getAppVersionWithBuild]];
}

#pragma mark Button Action
-(void)btnSetting
{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadingNextView) object: nil];
    popOverDismissed=NO;
    
    UINavigationBar *nbar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0, 0, 300, 40)];
    nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
    nbar.translucent = NO;
    nbar.tintColor = [UIColor whiteColor]; //bar button item color
    
    UIViewController *viewController = [[UIViewController alloc]init];
    UIView *viewForSetting = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 250)];
    [viewForSetting addSubview:nbar];
    
    UILabel *lblURL = [[UILabel alloc]initWithFrame:CGRectMake(10, 40, 280, 40)];
    lblURL.text = @"Change URL";
    lblURL.font = MedRepElementTitleLabelFont;
    lblURL.textColor = MedRepElementTitleLabelFontColor;
    [viewForSetting addSubview:lblURL];
    
    txtURL = [[MedRepTextField alloc]initWithFrame:CGRectMake(10, 90, 280, 40)];
    if ([customerID isEqualToString:@"C16K82563"])
    {
        txtURL.text = @"www.salesworx.ae/demo";
    }
    else
    {
        txtURL.text = [NSString stringWithFormat:@"www.salesworx.ae/%@",customerID];
    }
    
    txtURL.font = MedRepSingleLineLabelFont;
    txtURL.textColor = MedRepDescriptionLabelFontColor;
    txtURL.layer.borderWidth=1.0f;
    txtURL.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
    txtURL.layer.masksToBounds = NO;
    txtURL.layer.cornerRadius=5.0;
    txtURL.delegate = self;
    txtURL.returnKeyType = UIReturnKeyDefault;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 40)];
    txtURL.leftView = paddingView;
    txtURL.leftViewMode = UITextFieldViewModeAlways;
    [viewForSetting addSubview:txtURL];
    
    MedRepButton *btnSave = [[MedRepButton alloc]initWithFrame:CGRectMake(20, 150, 120, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave addTarget:self action:@selector(btnChangeURL) forControlEvents:UIControlEventTouchUpInside];
    [btnSave.titleLabel setFont:MedRepButtonFont];
    [btnSave.titleLabel setTextColor:[UIColor whiteColor]];
    btnSave.backgroundColor=[UIColor colorWithRed:(87.0/255.0) green:(190.0/255.0) blue:(135.0/255.0) alpha:1];
    btnSave.layer.cornerRadius=5.0f;
    CALayer *bottomBorderSave = [CALayer layer];
    bottomBorderSave.frame = CGRectMake(0.0f, btnSave.frame.size.height, btnSave.frame.size.width, 4.0f);
    bottomBorderSave.cornerRadius=5.0f;
    bottomBorderSave.backgroundColor = [UIColor yellowColor].CGColor;
    btnSave.layer.shadowColor = [[UIColor colorWithRed:(13.0/255.0) green:(136.0/255.0) blue:(96.0/255.0) alpha:1] CGColor];
    btnSave.layer.shadowOffset = CGSizeMake(0.0, 3.0);
    btnSave.layer.shadowOpacity = 1.0;
    btnSave.layer.shadowRadius = 0.0;

    [viewForSetting addSubview:btnSave];
    
    
    
    
    
    MedRepButton *btnDefaultURl = [[MedRepButton alloc]initWithFrame:CGRectMake(160, 150, 120, 40)];
    [btnDefaultURl setTitle:@"Use Default" forState:UIControlStateNormal];
    [btnDefaultURl addTarget:self action:@selector(btnDefaultURL) forControlEvents:UIControlEventTouchUpInside];
    
    [btnDefaultURl.titleLabel setFont:MedRepButtonFont];
    [btnDefaultURl.titleLabel setTextColor:[UIColor whiteColor]];
    btnDefaultURl.backgroundColor=[UIColor colorWithRed:(87.0/255.0) green:(190.0/255.0) blue:(135.0/255.0) alpha:1];
    btnDefaultURl.layer.cornerRadius=5.0f;
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, btnDefaultURl.frame.size.height, btnDefaultURl.frame.size.width, 4.0f);
    bottomBorder.cornerRadius=5.0f;
    bottomBorder.backgroundColor = [UIColor yellowColor].CGColor;
    btnDefaultURl.layer.shadowColor = [[UIColor colorWithRed:(13.0/255.0) green:(136.0/255.0) blue:(96.0/255.0) alpha:1] CGColor];
    btnDefaultURl.layer.shadowOffset = CGSizeMake(0.0, 3.0);
    btnDefaultURl.layer.shadowOpacity = 1.0;
    btnDefaultURl.layer.shadowRadius = 0.0;
    [viewForSetting addSubview:btnDefaultURl];

    
    
    
    
    
    
    
    
    
    SalesWorxNavigationButton *btnCancel = [[SalesWorxNavigationButton alloc]initWithFrame:CGRectMake(0, 0, 60, 40)];
    [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    btnCancel.titleLabel.font=NavigationBarButtonItemFont;
    btnCancel.titleLabel.textColor=[UIColor whiteColor];
    [btnCancel addTarget:self action:@selector(btnCancel) forControlEvents:UIControlEventTouchUpInside];
    btnCancel.titleLabel.textColor = [UIColor whiteColor];
    [nbar addSubview:btnCancel];
    
    [viewController.view addSubview:viewForSetting];
    
    popOver = [[UIPopoverController alloc]initWithContentViewController:viewController];
    popOver.delegate = self;
    [popOver setPopoverContentSize:CGSizeMake(300, 200)];

    [popOver presentPopoverFromBarButtonItem:btnSetting permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
- (IBAction)btnRetry:(id)sender
{
    NSLog(@"activate license called from btn retry");

    [self activateLicense];
}
-(void)btnDefaultURL
{
    [popOver dismissPopoverAnimated:YES];
    popOverDismissed=YES;

    URLChanged = true;
    [txtURL resignFirstResponder];
    txtURL.text = @"www.salesworx.ae/demo";
    [self btnChangeURL];
}
-(void)btnCancel
{
    [txtURL resignFirstResponder];
    if ([[SWDefaults licenseKey] count] != 0)
    {
        [self performSelector:@selector(loadingNextView) withObject:nil afterDelay:5.0f];
    }
    [popOver dismissPopoverAnimated:YES];
    popOverDismissed=YES;

}
-(void)btnChangeURL
{
    [popOver dismissPopoverAnimated:YES];
    popOverDismissed=YES;

    [txtURL resignFirstResponder];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KNoNetworkAlertTitle andMessage:KNoNetworkAlertMessage withController:self];
    }
    else
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *myRequestString =[NSString stringWithFormat:@"server=%@",txtURL.text];
        NSLog(@"my req str %@", myRequestString);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
        [request setURL:[NSURL URLWithString:kLicenseActivationValidationURL]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:myRequestData];
        
        NSLog(@"license activation change URL request string is %@", request);
        
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            NSError* error;
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if([data length] >0 && error == nil && [httpResponse statusCode] == 200) /*sucess*/
            {
                NSString *responseJsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSLog(@"Response %@",responseJsonString);
                if([responseJsonString hasPrefix:@"<c>"])
                {
                    customerIDReturnFromServerAfterChangeURL = [SWDefaults stringBetweenString:@"<c>" andString:@"</c>" andMainString:responseJsonString];
                    NSLog(@"Customer ID %@", customerIDReturnFromServerAfterChangeURL);
                    
                    
                    
                    //adding simulator access
                    
                    UIDeviceHardware *hardware=[[UIDeviceHardware alloc] init];
                    NSLog(@"Device is %@",[hardware platformString]);
                    
                    if ([[hardware platformString] isEqualToString:@"Simulator"]) {
                        customerID=@"C15K82659";
                        
                        [[NSUserDefaults standardUserDefaults]setValue:customerID forKey:@"Customer_ID"];
                        

                    }
                  
                    
                    if ([customerID isEqualToString:customerIDReturnFromServerAfterChangeURL]|| [[hardware platformString] isEqualToString:@"Simulator"])
                    {
                        [popOver dismissPopoverAnimated:YES];
                        popOverDismissed=YES;

                        /***** Move to next view after 1 seconds *****/
                        [self performSelector:@selector(loadingNextView) withObject:nil afterDelay:1.0f];
                    } else
                    {
                        
                        //check if database exists
                        
                        
                     BOOL databaseExists=[[SWDatabaseManager retrieveManager]databaseExists];
                        
                        
                    NSMutableArray*    tblUserArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"Select * from TBL_User"];
                        
                        if (databaseExistAlert==YES || tblUserArray.count>0) {
                            
                            UIAlertAction* okAction = [UIAlertAction
                                                       actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                                       style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                                       {
                                                           [SWDefaults setIsActivationDone:@"NO"];
                                                           [SWDefaults setSalesWorxLicenseActivationStatus:YES];
                                                           URLChanged = true;
                                                           customerID = customerIDReturnFromServerAfterChangeURL;
                                                           [popOver dismissPopoverAnimated:YES];
                                                           popOverDismissed=YES;

                                                           NSLog(@"activate license called from btn url change");

                                                           [self activateLicense];
                                                       }];
                            UIAlertAction* CancelAction = [SWDefaults GetDefaultCancelAlertActionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)];
                            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,CancelAction ,nil];
                            [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:KWarningAlertTitleStr andMessage:KApplicationDataResetAlertMessage andActions:actionsArray withController:self];
                        
                        }
                        else
                        {
                            //dont show the alert if there is no database.
                            [SWDefaults setIsActivationDone:@"NO"];
                            [SWDefaults setSalesWorxLicenseActivationStatus:YES];
                            URLChanged = true;
                            customerID = customerIDReturnFromServerAfterChangeURL;
                            NSLog(@"activate license called from btn url change action 2");

                            
                            if ([popOver isPopoverVisible]) {
                                NSLog(@"dismissing popover in btn url chnage action 2");
                                [popOver dismissPopoverAnimated:YES];
                            }
                            popOverDismissed=YES;
                            [self activateLicense];
                            
                        }
                        
//                        URLChanged = true;
//                        customerID = custID;
//                        [self activateLicense];
                    }
                }
                else if ([responseJsonString hasPrefix:@"<Error>"])
                {
                    NSString *errorString = [SWDefaults stringBetweenString:@"<Error>" andString:@"</Error>" andMainString:responseJsonString] ;
                    [SWDefaults showAlertAfterHidingKeyBoard:errorString andMessage:KLicenseActivationScreenURLChangeWebServiceErrorAlertMessage withController:self];
                }
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:KLicenseActivationScreenURLChangeWebServiceServerErrorAlertTitle andMessage:[NSString stringWithFormat:@"%@ %@",connectionError.localizedDescription,KPleaseTryAgainStr] withController:self];
            }
        }];
    }
}

#pragma mark License Activation Method .
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    NSLog(@"popover dismissmed delegate called");
    popOverDismissed=YES;
}

-(void)activateLicense
{
    
    if (popOverDismissed ==NO) {
        
        NSLog(@"PopOver is visible so not activating license");
        
    }
    else
    {
    
    [txtURL resignFirstResponder];
    _btnRetry.hidden = YES;
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"There IS NO internet connection");
        _btnRetry.hidden = NO;
        [SWDefaults showAlertAfterHidingKeyBoard:KNoNetworkAlertTitle andMessage:KNoNetworkAlertMessage withController:self];
    }
    else
    {
        NSLog(@"There IS internet connection");
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString* refinedCustomerID=[[NSString alloc]init];
        
        
        __block BOOL isValid = NO;
        rsaObj = [[RSA alloc] init];
        NSString *guidString = [NSString createGuid];
        NSLog(@"String Guid %@",guidString);
        
        guidString = [guidString stringByReplacingOccurrencesOfString: @"-" withString:@""];
        //    //NSLog(@"String Guid with out %@",guidString);
        
        NSLog(@"guid used for encryption %@", guidString);
        
        NSString *encryptString=[[rsaObj encryptToString:guidString]urlencode];
        
        
        
        //avid    = @"8";
        NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
        //appVersionID  = [infoDict objectForKey:@"CFBundleVersion"];
        
        //this is not app version ID, its the version ID of licensing system
        appVersionID = @"19";
        [SWDefaults setLicenseIDForInfo:customerID];
        refinedCustomerID     = [SWDefaults stringBetweenString:@"C" andString:@"K" andMainString:customerID] ;
        [SWDefaults setLicenseCustomerID:refinedCustomerID];
        
        
        NSLog(@" customer ID in new screen  %@", customerID);
        deviceID     = [[DataSyncManager sharedManager]getDeviceID];
        
        
        NSString *myRequestString =[NSString stringWithFormat:@"avid=%@&cid=%@&sid=%@&lt=%@&ll=%@&k=%@",appVersionID,customerID,deviceID,licenseType,licenseEvalutationTime,encryptString];
        NSLog(@"my req str %@", myRequestString);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
        [request setURL:[NSURL URLWithString:kLicenseActivationURL]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:myRequestData];
        
        NSLog(@"license activation request string is %@", request);
        
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            
            NSError* error;
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if([data length] >0 && error == nil && [httpResponse statusCode] == 200) /*sucess*/
            {
                NSString *responseJsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSLog(@"Response %@",responseJsonString);
                aesObj = [[AES alloc] init];
                if([responseJsonString hasPrefix:@"<License>"])
                {
                    NSString *licenseString = [SWDefaults stringBetweenString:@"<License>" andString:@"</License>" andMainString:responseJsonString];
                    
                    NSLog(@"license Key in license activation %@", licenseString);
                    
                    NSLog(@"guid being used to decrypt license %@", guidString);
                    
                    
                    @try {
                        decryString =  [aesObj decrypt:licenseString withKey:guidString];

                    } @catch (NSException *exception) {
                        
                        NSLog(@"decrypting license error %@", error.debugDescription);
                        
                    } @finally {
                        
                    }
                    
                    if ([NSString isEmpty:decryString]) {
                        NSLog(@"decrypted string is empty");
                        
                        [SWDefaults showAlertAfterHidingKeyBoard:@"License activation decryption failed sending logs automatically" andMessage:@"" withController:self];
                        
                       // [self sendEmail];
                        
                        
                    }
                    
                    
                    
                    //now check if this license is valid
                    
                    
                    NSLog(@"decrypted license %@", decryString);
                    
                    NSArray *list = [decryString componentsSeparatedByString:@""];
                    
                    NSLog(@"license list array %@", [list description]);
                    
                    
                    [SWDefaults setLicenseKey:[NSDictionary dictionaryWithObjects:list forKeys:[NSArray arrayWithObjects:@"cid",@"avid",@"sid",@"lt",@"ll",@"date", nil]]] ;
                    
                    
                    
                    
                    NSDictionary* licenseDict=[SWDefaults licenseKey];
                    
                    NSLog(@"license limit dict is %@",[licenseDict description]);
                    
                    
                    NSString* licenseTime=[licenseDict valueForKey:@"lt"];
                    
                    
                    
                    NSArray* tempEvalucationTime=[decryString componentsSeparatedByString:@"EVAL_TIME"];
                    NSString* licEvalTimeStr=[[NSString alloc]init];
                    
                    
                    if (tempEvalucationTime.count>0)
                    {
                        if([licenseTime isEqualToString:@"PERMANENT"])
                        {
                            NSLog(@"this is a permanent license");
                            
                            //[SWDefaults setIsActivationDone:@"NO"];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [[NSUserDefaults standardUserDefaults]setObject:customerID forKey:@"Customer_ID"];
                                if (URLChanged)
                                {
                                    [popOver dismissPopoverAnimated:YES];
                                    popOverDismissed=YES;

                                    /***** Move to next view after 1 seconds *****/
                                    [self performSelector:@selector(loadingNextView) withObject:nil afterDelay:1.0f];
                                    
//                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:@"Url changed successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                                    alert.tag = 1;
//                                    [alert show];
                                } else {
                                    [[NSNotificationCenter defaultCenter]postNotificationName:@"License Activated Successfully" object:self];
                                }
                            });
                        }
                        else
                        {
                            licEvalTimeStr =licenseTime;
                            
                            NSLog(@"eval time string  %@", [licEvalTimeStr description]);
                            
                            NSString *licenseActivatedDate = [licenseDict valueForKey:@"date"];
                            
                            NSLog(@"license activated date %@", licenseActivatedDate);
                            
                            //now set this date in device keychain to verify license during login
                            
                            [SSKeychain setPassword:licenseActivatedDate forService:kLicenseActivationDateKey account:@"user"];
                            
                            NSString* keyChainDate=[SSKeychain passwordForService:kLicenseActivationDateKey account:@"user"];
                            
                            NSLog(@"key chain date is %@", keyChainDate);
                            
                            
                            isValid=[SWDefaults validateSalesWorxLicense:[licenseDict valueForKey:@"ll"]];
                            if (isValid==YES)
                            {
                                //now move to activation screen
                                [[NSUserDefaults standardUserDefaults]setObject:customerID forKey:@"Customer_ID"];
                                if (URLChanged)
                                {
                                    [popOver dismissPopoverAnimated:YES];
                                    popOverDismissed=YES;

                                    /***** Move to next view after 1 seconds *****/
                                    [self performSelector:@selector(loadingNextView) withObject:nil afterDelay:1.0f];
                                    
//                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:@"Url changed successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                                    alert.tag = 1;
//                                    [alert show];
                                } else {
                                    [[NSNotificationCenter defaultCenter]postNotificationName:@"License Activated Successfully" object:self];
                                }
                            }
                            else
                            {
                                
                            }
                        }
                    }
                }
                else if ([responseJsonString hasPrefix:@"<Error>"])
                {
                    //NSString *errorString = [SWDefaults stringBetweenString:@"<Error>" andString:@"</Error>" andMainString:responseJsonString]
                    
                    NSLog(@"response json string with error is %@", responseJsonString);


                    NSString* testMessage= [KApplicationInitializationErrorAlertMessage stringByAppendingString:responseJsonString];
                   // [self sendEmail];

                    
                    if (URLChanged)
                    {
                        customerID = previousCustomerID;
                        [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:testMessage withController:self];

                    }
                    else
                    {
                        _btnRetry.hidden = NO;
                        [SWDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:testMessage withController:self];

                    }
                    
                    
                }
            }
            
            else
            {
                NSLog(@"LICENSE ACTIVATION FAILED %@ \n HTTP Response %@", connectionError.localizedDescription,httpResponse);
                if (URLChanged)
                {
                    customerID = previousCustomerID;

                    [SWDefaults showAlertAfterHidingKeyBoard:KLicenseFailureErrorTitle andMessage:[NSString stringWithFormat:@"%@ %@",connectionError.localizedDescription,KPleaseTryAgainStr] withController:self];

                }
                else
                {
                    
                    
                    
                     _btnRetry.hidden = NO;
                    [SWDefaults showAlertAfterHidingKeyBoard:KLicenseFailureErrorTitle andMessage:[NSString stringWithFormat:@"%@ %@",connectionError.localizedDescription,KPleaseTryAgainStr] withController:self];

                }
            }
        }];
        
    }
    }
}


#pragma mark UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 2)
    {
        if (buttonIndex == 0)
        {
            [SWDefaults setIsActivationDone:@"NO"];
            [SWDefaults setSalesWorxLicenseActivationStatus:YES];
            URLChanged = true;
            customerID = customerIDReturnFromServerAfterChangeURL;
            NSLog(@"activate license called from alert action");
            [self activateLicense];
        }
        else
        {
            NSLog(@"Cancel");
        }
    }
//    if (alertView.tag == 1)
//    {
//        [popOver dismissPopoverAnimated:YES];
//        /***** Move to next view after 1 seconds *****/
//        [self performSelector:@selector(loadingNextView) withObject:nil afterDelay:1.0f];
//    }
//    if (alertView.tag==100)
//    {
//        /***** Move to next view after 3 seconds *****/
//        [self performSelector:@selector(loadingNextView) withObject:nil afterDelay:3.0f];
//    }
}

#pragma mark License Activated Notification Method

-(void)receiveLicenseActivationMessage:(NSNotification*)licenseNotification
{
    if ([[licenseNotification name]isEqualToString:@"License Activated Successfully"])
    {
        [SWDefaults setSalesWorxLicenseActivationStatus:YES];
        
//        NSString* licenseAlertTitle =@"License Activated Successfully";
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:licenseAlertTitle delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        alert.tag = 100;
//        [alert show];
        
        [self performSelector:@selector(loadingNextView) withObject:nil afterDelay:5.0f];
    }
}
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark
-(void)viewWillDisappear:(BOOL)animated
{
    done=nil;
    alertController=nil;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}
#pragma mark
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sendEmail
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        
        [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
        
        
        [mailController setSubject:@"SalesWorx Device Logs"];
        [mailController setMessageBody:@"Hi,\n Please find the attached device logs for SalesWorx." isHTML:NO];
        [mailController setToRecipients:@[@"ucssupport@ucssolutions.com"]];
        
        
        NSDate *currentDate = [NSDate date];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        
        NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate: currentDate];
        NSString *documentPathComponent=[NSString stringWithFormat:@"DeviceLogs-%ld.txt",(long)[components month]];
        
        
        
        NSString *fileNameString;
        fileNameString=[[MedRepDefaults fetchDocumentsDirectory] stringByAppendingPathComponent:documentPathComponent];
        NSData *noteData = [NSData dataWithContentsOfFile:fileNameString];
        
        [mailController addAttachmentData:noteData mimeType:@"text/plain" fileName:[NSString stringWithFormat:@"DeviceLogs-%ld.txt",(long)[components month]]];
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KFailureAlertTitle andMessage:KUNSupportMailComposer withController:self];
        
    }
    
    
}

#pragma mark Mail Composer Delegate


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"Log email sent.");
            
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end




