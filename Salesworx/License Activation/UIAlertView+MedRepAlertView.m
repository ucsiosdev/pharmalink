//
//  UIAlertView+MedRepAlertView.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/27/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "UIAlertView+MedRepAlertView.h"

@implementation UIAlertView (MedRepAlertView)

- (void) hide {
    [self dismissWithClickedButtonIndex:0 animated:YES];
}

@end
