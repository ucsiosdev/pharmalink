//
//  BackgroundDocumentsDownload.m
//  Total
//
//  Created by Pavan Kumar Singamsetti on 11/23/15.
//  Copyright © 2015 Total Marketing Middle East. All rights reserved.
//

#import "BackgroundDocumentsDownload.h"
#import "SWAppDelegate.h"
#import "FMDatabase.h"
#import "FileDownloadInfo.h"
#import "MedRepDefaults.h"
#import "MedRepQueries.h"
#import "FileDownloadInfo.h"
#import "SDWebImageManager.h"
@implementation BackgroundDocumentsDownload

@synthesize downLoadSession;

-(void)downloadDocumentsInBackground
{
    app=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    app.isBackgroundProcessRunning=YES;
    
    NSURLSessionConfiguration * sessionConfiguration=[NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:[NSString stringWithFormat:@"com.BGTransferDemo%@",[app getUTCFormateDate:[NSDate date]]]];
    
    // NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfiguration:@"com.BGTransferDemo"];
    sessionConfiguration.HTTPMaximumConnectionsPerHost = 1;
    
    //sessionConfiguration.timeoutIntervalForResource=5;
    downLoadSession = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                    delegate:self
                                               delegateQueue:nil];
    //add movies to background task
    
    arrFileDownloadData = [[NSMutableArray alloc] init];
    videoMappingArray=[[NSMutableArray alloc] init];
    NSMutableArray *tempUrlsArray=[[NSMutableArray alloc] init];
    // [self moveUrlFilesToDatabse];
    // NSPredicate *filesFileterPredicate =[NSPredicate predicateWithFormat:@"fileType contains[cd] %@",@"1"];
    NSMutableArray *documentsTobeDownloaded=[app.currentDownloadingDocumentsArray mutableCopy];
    
    NSLog(@"databse actions started");
    NSFileManager *filemanager=[NSFileManager defaultManager];
    
    [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%lu",(unsigned long)documentsTobeDownloaded.count] forKey:@"totalfilesCount"];
    [[NSUserDefaults standardUserDefaults]setValue:@"Checking Downloaded Content" forKey:@"currentDownloadingTask"];
    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"DownloadedMediaCount"];

    for (NSInteger i=0; i<documentsTobeDownloaded.count; i++) {
        MediaFile *document=[documentsTobeDownloaded objectAtIndex:i];
        
        [document.Filename lastPathComponent];
        NSString *documentLocation=[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",documentsFolderName,[document.Filename lastPathComponent]]];
        
        NSLog(@"document name being downloaded %@", documentLocation);
        
        
        if([filemanager fileExistsAtPath:documentLocation] && [document.Download_Media_Type isEqualToString:kDefaultDownloadMediaType])
        {
            
            //to be done
            //file already exists just update
            NSString *updateMediaFile =[NSString stringWithFormat:@"UPDATE TBL_Media_Files SET Download_Flag='%@' WHERE Media_File_ID='%@'",@"N",document.Media_File_ID];
            
        
            NSLog(@"media file already exists just updating the flag : %@ \n with id : %@", updateMediaFile,document.Media_File_ID);
            
            
            
            BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
            if (status) {
                
                NSLog(@"db updated for media file id %@", document.Media_File_ID);
                
            }

            NSInteger downloadedMediaCount;
            
            if ([[NSUserDefaults standardUserDefaults]valueForKey:@"DownloadedMediaCount"]==nil) {
                downloadedMediaCount=0;
            }
            
            else
            {
                downloadedMediaCount=[[[NSUserDefaults standardUserDefaults]valueForKey:@"DownloadedMediaCount"] integerValue];
            }
            
            downloadedMediaCount++;
            
            
            [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%ld",(long)downloadedMediaCount] forKey:@"DownloadedMediaCount"];
        }
        else
        {
            
            if(![tempUrlsArray containsObject:document.Filename])
            {
                
                if ([document.Download_Media_Type isEqualToString:kMerchandisingPOSMDownloadMediaType]||[document.Download_Media_Type isEqualToString:kOrgLogoDownloadMediaType]) {
                    
                    NSString* existingFileURL = [[MedRepDefaults fetchDocumentsDirectory]stringByAppendingPathComponent:document.Filename];
                    
                    if (![[NSFileManager defaultManager]fileExistsAtPath:existingFileURL]) {
                        
                        NSString* serVerAPILink =[[NSUserDefaults standardUserDefaults]valueForKey:@"ServerAPILink"];
                        
                        NSString *strUrlForImage =[serVerAPILink stringByAppendingString:[NSString stringWithFormat:@"File/%@?FileType=%@",document.Filename,document.Download_Media_Type]];

                        [arrFileDownloadData addObject:[[FileDownloadInfo alloc] initWithFileTitle:document andDownloadSource:strUrlForImage]];
                        [tempUrlsArray addObject:strUrlForImage];

                    }
                    
                }
                else{
                
                
                NSString* serVerAPILink =[[NSUserDefaults standardUserDefaults]valueForKey:@"ServerAPILink"];
                
                 NSString *strUrlForImage =[serVerAPILink stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",document.Media_File_ID,@"M"]];
                
                
                [arrFileDownloadData addObject:[[FileDownloadInfo alloc] initWithFileTitle:document andDownloadSource:strUrlForImage]];
                [tempUrlsArray addObject:strUrlForImage];
                }
            }
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"FailedFilesCountOfCurrentDownloadTask"];

    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%lu",(unsigned long)documentsTobeDownloaded.count] forKey:@"getTotalCount"];
    
    
    if(arrFileDownloadData.count>0)
    {
        [self startVideoDownloadsinBackground];
    }
    else
    {
        app.isBackgroundProcessRunning=NO;
        [self updateLastMediaSyncDate];
    }
    
}
+(NSString*)fetchTodaysDateWithTime
{
    NSString* dateString=[[NSMutableString alloc]init];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    dateString = [df stringFromDate:[NSDate date]];
    
    NSLog(@"Date is %@", dateString);
    return dateString;
    
}


#pragma BackgroundDownloadInit
-(void)startVideoDownloadsinBackground
{
    // Access all FileDownloadInfo objects using a loop.
    
    if (arrFileDownloadData.count>0) {
        
        for (NSInteger i=0; i<[arrFileDownloadData count]; i++) {
            
            
            FileDownloadInfo *fdi = [arrFileDownloadData objectAtIndex:i];
            
            // Check if a file is already being downloaded or not.
            if (!fdi.isDownloading) {
                // Check if should create a new download task using a URL, or using resume data.
                if (fdi.taskIdentifier == -1) {
                    fdi.downloadTask = [self.downLoadSession downloadTaskWithURL:[NSURL URLWithString:fdi.downloadSource]];
                }
                else{
                    fdi.downloadTask = [self.downLoadSession downloadTaskWithResumeData:fdi.taskResumeData];
                }
                
                NSLog(@"file being downloaded is %@",fdi.fileTitle);
                
                
                // Keep the new taskIdentifier.
                fdi.taskIdentifier = fdi.downloadTask.taskIdentifier;
                
                // Start the download.
                [fdi.downloadTask resume];
                
                // Indicate for each file that is being downloaded.
                fdi.isDownloading = YES;
            }
        }
    }
}




#pragma mark - NSURLSession Delegate method implementation
-(int)getFileDownloadInfoIndexWithTaskIdentifier:(unsigned long)taskIdentifier{
    int index = 0;
    if (arrFileDownloadData.count>0) {
        for (int i=0; i<[arrFileDownloadData count]; i++) {
            FileDownloadInfo *fdi = [arrFileDownloadData objectAtIndex:i];
            if (fdi.taskIdentifier == taskIdentifier) {
                index = i;
                break;
            }
        }
    }
    return index;
}


-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) downloadTask.response;
    NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSLog(@"file download location %@",location);
    
    int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];

    if([httpResponse statusCode]==200 && arrFileDownloadData.count>0)
    {
        FileDownloadInfo *fdi = [arrFileDownloadData objectAtIndex:index];
        NSError *error;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        MediaFile * currentMediaFile=fdi.fileTitle;
        
        NSString *destinationFilename = [NSString stringWithFormat:@"%@",currentMediaFile.Filename];
        // NSURL *destinationURL = [NSURL URLWithString:[NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",documentsFolderName,destinationFilename]]];
        
        
        NSLog(@"file name being downloaded %@",destinationFilename);
        
        
        NSArray *URLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
        NSURL * docDirectoryURL = [URLs objectAtIndex:0];
        
        NSURL *destinationURL = [docDirectoryURL URLByAppendingPathComponent:destinationFilename];
        
        
        if ([fileManager fileExistsAtPath:[destinationURL path]]) {
            [fileManager removeItemAtURL:destinationURL error:nil];
        }
        
        BOOL success = [fileManager copyItemAtURL:location
                                            toURL:destinationURL
                                            error:&error];
        
        if (success) {
            
            
            //copy the image to thumbnail folder, create the folder if not exists
           
            if ([[destinationFilename.pathExtension uppercaseString] isEqualToString:@"JPG"]||[[destinationFilename.pathExtension uppercaseString] isEqualToString:@"JPEG"]|| [[destinationFilename.pathExtension uppercaseString] isEqualToString:@"PNG"])
            {
                NSError * thumbnailError;
                NSString * thumbnailImageDirectoryPath =[[SWDefaults applicationDocumentsDirectory]stringByAppendingPathComponent:kThumbnailsFolderName];
                
                if (![[NSFileManager defaultManager] fileExistsAtPath:thumbnailImageDirectoryPath]) {
                    [[NSFileManager defaultManager] createDirectoryAtPath:thumbnailImageDirectoryPath withIntermediateDirectories:NO attributes:nil error:&thumbnailError];
                                NSLog(@"Thumbnail images directory created");
                    }
                
                NSString *savedImagePath = [[SWDefaults applicationDocumentsDirectory]  stringByAppendingPathComponent:destinationFilename];
                
                UIImage* savedImage = [UIImage imageWithContentsOfFile:savedImagePath];
                
                //145X109
                UIImage * resizedImage=[SWDefaults imageResize:savedImage andResizeTo:CGSizeMake(400, 300)];
                
                NSData *imageData = UIImageJPEGRepresentation(resizedImage, 0.7);
                
                NSLog(@"writing thumbnail file to path %@",[thumbnailImageDirectoryPath stringByAppendingPathComponent:destinationFilename]);
                
                
               bool thumbanailSaved= [imageData writeToFile:[thumbnailImageDirectoryPath stringByAppendingPathComponent:destinationFilename] atomically:NO];
                
                NSLog(@"thumbnail saved status %hhd",thumbanailSaved);
                
                
            }
            

//
            

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            // Change the flag values of the respective FileDownloadInfo object.
            
            if (arrFileDownloadData.count>0) {
                fdi.isDownloading = NO;
                fdi.downloadComplete = YES;
                
                
                NSInteger downloadedMediaCount;
                
                if ([[NSUserDefaults standardUserDefaults]valueForKey:@"DownloadedMediaCount"]==nil) {
                    
                    downloadedMediaCount=0;
                }
                
                else
                {
                    downloadedMediaCount=[[[NSUserDefaults standardUserDefaults]valueForKey:@"DownloadedMediaCount"] integerValue];
                    
                }
                
                downloadedMediaCount++;
                
                
                [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%ld",(long)downloadedMediaCount] forKey:@"DownloadedMediaCount"];
                

                
                NSString *updateMediaFile =[NSString stringWithFormat:@"UPDATE TBL_Media_Files SET Download_Flag='%@' WHERE Media_File_ID='%@'",@"N",currentMediaFile.Media_File_ID];
                BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
                if (status) {
                    
                    NSLog(@"db updated for media file id %@", fdi.fileTitle);
                    
                }
                
                else
                {
                    NSLog(@"error updating database");
                    
                }
                
                
                
                NSPredicate *similadocumentsarray=[NSPredicate predicateWithFormat:@"Filename contains [cd] %@",fdi.downloadSource ];
                
                NSMutableArray *filteredDosuments=[app.currentDownloadingDocumentsArray mutableCopy];
                [filteredDosuments filterUsingPredicate:similadocumentsarray];
                
                NSLog(@"%lu",(unsigned long)filteredDosuments.count);
               
                //to be done
                //[self insertMultipleDocuments:filteredDosuments];
               
                // Set the initial value to the taskIdentifier property of the fdi object,
                // so when the start button gets tapped again to start over the file download.
                fdi.taskIdentifier = -1;
                
                // In case there is any resume data stored in the fdi object, just make it nil.
                fdi.taskResumeData = nil;
                
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Reload the respective table view row using the main thread.
                    //            [self.tblFiles reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]
                    //                                 withRowAnimation:UITableViewRowAnimationNone];
                    
                }];
            }
        }
        else{
            NSLog(@"Unable to copy temp file. Error: %@", [error localizedDescription]);
        }
    }
    else
    {
        NSInteger failedCount;
        
        int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
        FileDownloadInfo *fdi;
        if (arrFileDownloadData.count>0) {
            fdi = [arrFileDownloadData objectAtIndex:index];
        }
        
        MediaFile * currentMediaFile=fdi.fileTitle;

        NSLog(@"Unable To download. Error: %ld \n %@",(long)[httpResponse statusCode], currentMediaFile.Filename);
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"FailedFilesCountOfCurrentDownloadTask"]==nil)
        {
            failedCount=0;
        }
        else
        {
            failedCount=[[[NSUserDefaults standardUserDefaults] valueForKey:@"FailedFilesCountOfCurrentDownloadTask"]integerValue];
        }
        failedCount++;
        [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%ld",failedCount] forKey:@"FailedFilesCountOfCurrentDownloadTask"];
    }
    
    
    
    

}


-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    
    NSLog(@"session %@ --- %@",session,self.downLoadSession);
    
    NSLog(@"didCompleteWithError is %@", error.localizedDescription);
    
    
    int index = [self getFileDownloadInfoIndexWithTaskIdentifier:task.taskIdentifier];
    FileDownloadInfo *fdi;
    if (arrFileDownloadData.count>0) {
        fdi = [arrFileDownloadData objectAtIndex:index];
    }
    
    
    MediaFile * currentMediaFile=fdi.fileTitle;

    if (error != nil ) {
        
        NSInteger failedCount;
        NSLog(@"Unable To download. Error: %ld %@ (%ld)",(long)error.description,error.localizedDescription,(long)error.code);
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"FailedFilesCountOfCurrentDownloadTask"]==nil)
        {
            failedCount=0;
        }
        else
        {
            failedCount=[[[NSUserDefaults standardUserDefaults] valueForKey:@"FailedFilesCountOfCurrentDownloadTask"]integerValue];
        }
        failedCount++;
        [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%ld",(long)failedCount] forKey:@"FailedFilesCountOfCurrentDownloadTask"];
        
        NSLog(@"failedCount %ld",(long)failedCount);
    }
    else
    {
        
        
        
    }
    
    //check if all files downloaded
    NSInteger totalFilestoDownload=[[[NSUserDefaults standardUserDefaults]valueForKey:@"getTotalCount"] integerValue];
    NSInteger failedFilesCount=[[[NSUserDefaults standardUserDefaults]valueForKey:@"FailedFilesCountOfCurrentDownloadTask"] integerValue];
    NSInteger downloadedMediaCount=[[[NSUserDefaults standardUserDefaults]valueForKey:@"DownloadedMediaCount"] integerValue];
    if (totalFilestoDownload==downloadedMediaCount+failedFilesCount) {
        
        [downLoadSession invalidateAndCancel];
        app.isBackgroundProcessRunning=NO;
        SynViewController * synVC=[[SynViewController alloc]init];
        NSMutableArray* filestoDownload=[synVC fetchMediaFilesFromDocuments];
        if (filestoDownload.count>0) {
            NSLog(@"Restarting background task for failed docs");
            //[self downloadDocumentsInBackground];
        }
        [self updateLastMediaSyncDate];

    }
    NSLog(@"media download count in background documents download total:%ld failed:%ld downloaded:%ld",totalFilestoDownload,failedFilesCount,downloadedMediaCount);
}

//
-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    
    if (totalBytesExpectedToWrite == NSURLSessionTransferSizeUnknown) {
        NSLog(@"Unknown transfer size");
    }
    else{
        // Locate the FileDownloadInfo object among all based on the taskIdentifier property of the task.
        int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
        [[NSUserDefaults standardUserDefaults]setValue:@"Downloading Content" forKey:@"currentDownloadingTask"];
        
        if (arrFileDownloadData.count>0) {
            
            FileDownloadInfo *fdi = [arrFileDownloadData objectAtIndex:index];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                // Calculate the progress.
                fdi.downloadProgress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
                
                NSLog(@"%@ progress %f", fdi.downloadSource,fdi.downloadProgress);
                
                app=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
                
                app.isBackgroundProcessRunning=YES;
                
                NSLog(@"totalBytesExpectedToWrite --> %lld",totalBytesExpectedToWrite);
                if([MedRepDefaults isDeviceAvailableSpaceMoreThanBufferSizeAfterThisFileDownload:[NSString stringWithFormat:@"%lld",totalBytesExpectedToWrite/1024]])
                {
                    
                    
                }
                else
                {
                    [downloadTask cancel];
                    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"MoviesDownLoadStoppedAlertShown"] isEqualToString:@"Y"])
                    {
                    }
                    else
                    {
                        UIAlertView *deviceSpaceAlert=[[UIAlertView alloc]initWithTitle:@"download stopped" message:@"Insufficient space to download the documents on your iPad." delegate:nil cancelButtonTitle:KAlertOkButtonTitle otherButtonTitles:nil];
                        [deviceSpaceAlert show];
                        [[NSUserDefaults standardUserDefaults]setValue:@"Y" forKey:@"MoviesDownLoadStoppedAlertShown"];
                        
                    }
                }
            }];
        }
    }
    
    
    //check if all files downloaded
    NSInteger totalFilestoDownload=[[[NSUserDefaults standardUserDefaults]valueForKey:@"getTotalCount"] integerValue];
    NSInteger failedFilesCount=[[[NSUserDefaults standardUserDefaults]valueForKey:@"FailedFilesCountOfCurrentDownloadTask"] integerValue];
    NSInteger downloadedMediaCount=[[[NSUserDefaults standardUserDefaults]valueForKey:@"DownloadedMediaCount"] integerValue];
    if (totalFilestoDownload==downloadedMediaCount+failedFilesCount) {
        
        [downLoadSession invalidateAndCancel];
        app=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
        app.isBackgroundProcessRunning=NO;
        SynViewController * synVC=[[SynViewController alloc]init];
        NSMutableArray* filestoDownload=[synVC fetchMediaFilesFromDocuments];
        if (filestoDownload.count>0) {
            NSLog(@"Restarting background task for failed docs");
           // [self downloadDocumentsInBackground];
        }
        [self updateLastMediaSyncDate];

    }
}


-(void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{
    
    SWAppDelegate *appDelegate = (SWAppDelegate*)[UIApplication sharedApplication].delegate;
    [session invalidateAndCancel];
    // Check if all download tasks have been finished.
    [self.downLoadSession getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        
        if ([downloadTasks count] == 0) {
            if (appDelegate.backgroundTransferCompletionHandler != nil) {
                // Copy locally the completion handler.
                void(^completionHandler)() = appDelegate.backgroundTransferCompletionHandler;
                
                // Make nil the backgroundTransferCompletionHandler.
                appDelegate.backgroundTransferCompletionHandler = nil;
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Call the completion handler to tell the system that there are no other background transfers.
                    completionHandler();
                    
                    // Show a local notification when all downloads are over.
                   
                    NSLog(@"ALL FILES DOWNLOADED SUCCESSFULLY");
                    
                    
//                    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//                    localNotification.alertBody = @"All files have been downloaded!";
//                    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                }];
            }
        }
    }];
}

-(void)updateLastMediaSyncDate
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults]setValue:dateString forKey:@"LastMediaSyncDate"];
}

-(NSString *)getLastMediaSyncDate
{
    if([[NSUserDefaults standardUserDefaults]valueForKey:@"LastMediaSyncDate"]==nil)
        return @"N/A";
    
    return [[NSUserDefaults standardUserDefaults]valueForKey:@"LastMediaSyncDate"];
}

-(void)downloadMerchandisingPOSMImages
{
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString* serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/File/",[testServerDict stringForKey:@"url"]];
    
    //POSM Images
    NSMutableArray * posmArray = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"select IFNULL(Custom_Attribute_1,'')AS Custom_Attribute_1 from TBL_App_Codes where code_type = 'POSM_TITLE'"]];
    if (posmArray.count>0) {
        for (NSMutableDictionary* currentDict in posmArray) {
            NSString* fileName=[NSString getValidStringValue:[currentDict valueForKey:@"Custom_Attribute_1"]];
            NSString* fileURL =[serverAPI stringByAppendingString:[NSString stringWithFormat:@"%@?FileType=POSM_IMG",fileName]];
            
            NSString* existingFileURLPath=[[MedRepDefaults fetchDocumentsDirectory] stringByAppendingPathComponent:fileName];
            if (![[NSFileManager defaultManager]fileExistsAtPath:existingFileURLPath]) {
                [self DownloadImageFromURL:fileURL AndSaveInDocumentsDirectoryWithName:fileName];
            }
        }
    }

}

-(void)DownloadOrgLogoImages{
    
    NSMutableArray * orgLogosArray = [[NSMutableArray alloc] init];
    orgLogosArray = [[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Value_2 AS FileURL,Value_1 AS FileName from TBL_Custom_Info where Value_2 IS NOT NULL AND Info_Type = 'ORG_LOGO'"];
    
    for (NSInteger i=0; i<orgLogosArray.count; i++) {
        NSDictionary * imagefileDic = [orgLogosArray objectAtIndex:i];
        [self DownloadImageFromURL:[imagefileDic valueForKey:@"FileURL"] AndSaveInDocumentsDirectoryWithName:[imagefileDic valueForKey:@"FileName"]];
        NSLog(@"Downloading ORG Logo %@ -- From URL %@",[imagefileDic valueForKey:@"FileName"],[imagefileDic valueForKey:@"FileURL"]);
    }
}
-(void)DownloadImageFromURL:(NSString *)imageURL AndSaveInDocumentsDirectoryWithName:(NSString *)name{
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [[manager imageDownloader] downloadImageWithURL:[NSURL URLWithString:imageURL] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
        NSData *pngData = UIImagePNGRepresentation(image);
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
        [pngData writeToFile:getImagePath atomically:YES];
        NSLog(@"Downloaded image %@ -- From URL %@",name,imageURL);
    }];
}
@end
