//
//  BackgroundDocumentsDownload.h
//  Total
//
//  Created by Pavan Kumar Singamsetti on 11/23/15.
//  Copyright © 2015 Total Marketing Middle East. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWAppDelegate.h"
#import "DataType.h"
@class SWAppDelegate;


@interface BackgroundDocumentsDownload : NSObject<NSURLSessionDelegate>
{
    NSMutableArray *arrFileDownloadData;
    NSMutableArray *videoMappingArray;
    SWAppDelegate *app;
    NSInteger skippedDocCount;
}
@property (nonatomic, strong) NSURLSession *downLoadSession;
@property (strong,nonatomic) NSMutableArray *documentsarray;
-(void)downloadDocumentsInBackground;
-(void)updateLastMediaSyncDate;
-(NSString *)getLastMediaSyncDate;
-(void)DownloadImageFromURL:(NSString *)imageURL AndSaveInDocumentsDirectoryWithName:(NSString *)name;
-(void)DownloadOrgLogoImages;
-(void)downloadMerchandisingPOSMImages;

@end
