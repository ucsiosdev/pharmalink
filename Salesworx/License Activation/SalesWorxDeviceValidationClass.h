//
//  SalesWorxDeviceValidationClass.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 11/8/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
/*@protocol SalesWorxDeviceValidationClassDelegate <NSObject>
-(void)DeviceValidationServiceStatus:(NSString *)response;
@end*/

@interface SalesWorxDeviceValidationClass : NSObject
-(NSString *)ValidateDevice;
-(NSMutableArray *)fetchWhiteListedCustomerIds;

//@property (strong,nonatomic) id <SalesWorxDeviceValidationClassDelegate>deviceValidationClassDelegate;

@end
