//
//  PlainSectionHeader.h
//  SWPlatform
//
//  Created by Irfan Bashir on 7/5/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlainSectionHeader : UIView {
    UIImageView *headerImage;
    UILabel *sectionText;
}

@property (nonatomic, strong) UIImageView *headerImage;
@property (nonatomic, strong) UILabel *sectionText;

- (id)initWithWidth:(CGFloat)width text:(NSString *)text;

@end