//
//  SalesWorxReviewDocumentsViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 10/28/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepElementDescriptionLabel.h"
#import "SalesWorxCustomClass.h"
#import "StringPopOverViewController_ReturnRow.h"
#import "XYPieChart/XYPieChart.h"
#import "MedRepDateRangeTextfield.h"
#import "SalesWorxReportsDateSelectorViewController.h"

@interface SalesWorxReviewDocumentsViewController : UIViewController<StringPopOverViewController_ReturnRow,UIPopoverControllerDelegate,XYPieChartDataSource,XYPieChartDelegate, UIPopoverPresentationControllerDelegate,SalesWorxReportsDateSelectorViewControllerDelegate>
{
    NSString * popString, *datePopOverTitle;
    NSMutableArray *unfilteredReviewDocumentsArray, *arrCustomerName, *arrCustomersDetails, *arrDocumentType ,*arrCollectionDocuments, *arrOrderAndReturnDocuments;
    
    IBOutlet UITableView *tblReviewDocuments;
    IBOutlet MedRepTextField *txtCustomer;
    IBOutlet MedRepTextField *txtDocumentType;
    IBOutlet MedRepDateRangeTextfield *DateRangeTextField;
    IBOutlet UILabel *lblTotalAmount;
    IBOutlet UIView *viewFooter;
    IBOutlet XYPieChart *customerPieChart;
    IBOutlet UIImageView *pieChartPlaceholderImage;
    IBOutlet UIView *parentViewOfPieChart;
}

@property(strong,nonatomic) NSMutableArray *arrReviewDocuments;
@property(nonatomic, strong) NSMutableArray *slicesForCustomerPotential;
@property(nonatomic, strong) NSArray        *sliceColorsForCustomerPotential;
@end
