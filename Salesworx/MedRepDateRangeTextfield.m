//
//  MedRepDateRangeTextfield.m
//  MedRep
//
//  Created by Pavan Kumar Singamsetti on 1/28/18.
//  Copyright © 2018 msaad. All rights reserved.
//

#import "MedRepDateRangeTextfield.h"
#import "MedRepQueries.h"
@implementation MedRepDateRangeTextfield
@synthesize StartDate,EndDate,SelecetdCustomDateRangeText;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(NSString *)getDateRangeFormatText{
    if(SelecetdCustomDateRangeText == nil){
        return [NSString stringWithFormat:@"%@ - %@",[MedRepQueries ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:StartDate],[MedRepQueries ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:EndDate]];
    }else{
        return [NSString stringWithFormat:@"%@ (%@ - %@)",SelecetdCustomDateRangeText,[MedRepQueries ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:StartDate],[MedRepQueries ConvertDateIntoDeviceDateWithOutTimeDisplayFormat:EndDate]];
    }

}
-(void)SetCustomDateRangeText{
    self.text =  [self getDateRangeFormatText];
}
@end
