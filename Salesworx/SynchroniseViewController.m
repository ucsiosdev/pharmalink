     
//
//  SynchroniseViewController.m
//  SWPlatform
//
//  Created by msaad on 1/23/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
// 5G+2PAHMjWMrcsgI1sCjFUaE/SSuEGaBpdT/lVdHKQMS3tElyLlgRtNATzKw5Ok8F1qMisRig++QYYLN1BI+Pg==

//5015033D543642EABD871E6A91487B9D

//58519223cef9cee4df999436c5e8f3e96aEVAL_TIME602013-03-21
#import "SynchroniseViewController.h"
#import <Security/Security.h>

@implementation NSString (NSString_Extended)

- (NSString *)urlencode {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[self UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}
@end

@implementation SynchroniseViewController

- (BOOL)startLicenceingWithCustomerIF:(NSString *)custID andLicenceType:(NSString *)licenceT andLicenceLimit:(NSString *)LicenceL
{
    
    
    
    BOOL isValid = NO;
    rsaObj = [[RSA alloc] init];
    NSString *guidString = [NSString createGuid];
    NSLog(@"String Guid %@",guidString);
    
    guidString = [guidString stringByReplacingOccurrencesOfString: @"-" withString:@""];
//    //NSLog(@"String Guid with out %@",guidString);

    
    NSString *encryptString=[rsaObj encryptToString:guidString];
    NSLog(@"Encrypted Guid %@",[encryptString urlencode]);
    
    //avid    = @"8";
   // NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
   // avid  = [infoDict objectForKey:@"CFBundleVersion"];
    
    avid = @"19";
    [SWDefaults setLicenseIDForInfo:custID];
    cid     = [self stringBetweenString:@"C" andString:@"K" andMainString:custID] ;
    [SWDefaults setLicenseCustomerID:cid];

  NSLog(@"customer ID %@",cid);
  NSLog(@"customer ID %@",[SWDefaults licenseCustomerID]);
    sid     = [[DataSyncManager sharedManager]getDeviceID];
    lt      =  licenceT;
    ll      = LicenceL ;
    NSLog(@"sid : %@ %@ %@",sid,lt,ll );
    
    
    NSString *myRequestString =[NSString stringWithFormat:@"avid=%@&cid=%@&sid=%@&lt=%@&ll=%@&k=%@",avid,custID,sid,lt,ll,[encryptString urlencode]];
    NSLog(@"my req str %@", myRequestString);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [request setURL:[NSURL URLWithString:@"http://www.ucssolutions.com/licman/gen-lic.jsp"]];
    [request setHTTPMethod:@"POST"];
	[request setHTTPBody:myRequestData];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSLog(@"error description %@", err.debugDescription);
    
    NSLog(@"resp data %@", responseData);

    //no unsed licenses available here
    NSString *json_string2 = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
  
    NSLog(@"Response %@",json_string2);
    aesObj = [[AES alloc] init];
    if([json_string2 hasPrefix:@"<License>"])
    {
        NSString *finalString = [self stringBetweenString:@"<License>" andString:@"</License>" andMainString:json_string2];
        NSString *decryString =  [aesObj decrypt:finalString withKey:guidString];
        //NSLog(@"%@",decryString);
        
        isValid=[self licenceValidation:decryString];
        if (!isValid)
        {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"License validation error", nil) message:NSLocalizedString(@"Your license has been expired or invalid.Please contact your administrator for further assistance ", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]  show];

//            [ILAlertView showWithTitle:@"License validation error"
//                               message:@"Your license has been expired or invalid.Please contact your administrator for further assistance "
//                      closeButtonTitle:NSLocalizedString(@"OK", nil)
//                     secondButtonTitle:nil
//                   tappedButtonAtIndex:^(NSInteger buttonIndex)
//             {
//                 // [[NSNotificationCenter defaultCenter] postNotificationName:kSessionActivationErrorNotification object:nil];
//                 
//             }];
        }
    }
    else
    {
        NSString *finalString = [self stringBetweenString:@"<Error>" andString:@"</Error>" andMainString:json_string2];
        
          [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Request timed out please try again", nil) message:finalString delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]  show];
        
//        [ILAlertView showWithTitle:NSLocalizedString(@"Error", nil)
//                           message:finalString
//                  closeButtonTitle:NSLocalizedString(@"OK", nil)
//                 secondButtonTitle:nil
//               tappedButtonAtIndex:nil];
        isValid = NO;
    }
    return isValid;
    
    
}

-(BOOL)licenceValidation:(NSString *)lstring
{
    BOOL isValid = YES;
    NSArray *list = [lstring componentsSeparatedByString:@""];
   // if([[SWDefaults licenseKey] count]==0)
   // {
        [SWDefaults setLicenseKey:[NSDictionary dictionaryWithObjects:list forKeys:[NSArray arrayWithObjects:@"cid",@"avid",@"sid",@"lt",@"ll",@"date", nil]]] ;
        isValid=[self licenseVerification];
   // }
   // else
   // {
     //  isValid=[self licenseVerification];
   // }
    return  isValid;
}




-(BOOL)licenseVerification
{
    BOOL isValid = YES;
    
    //NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    //avid  = [infoDict objectForKey:@"CFBundleVersion"];
    
    if(![[[SWDefaults licenseKey] stringForKey:@"avid"] isEqualToString:@"19"])
    {
        isValid=NO;
    }
   
    if(![[[SWDefaults licenseKey] stringForKey:@"cid"] isEqualToString:[SWDefaults licenseCustomerID]])
    {
        isValid=NO;
    }
    
    if(![[[SWDefaults licenseKey] stringForKey:@"sid"] isEqualToString:[[DataSyncManager sharedManager]getDeviceID]])
    {
        isValid=NO;
    }
    
    if([[[SWDefaults licenseKey] stringForKey:@"lt"] isEqualToString:@"EVAL_TIME"])
    {
        NSString *lDateString= [[SWDefaults licenseKey] stringForKey:@"date"];
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        NSDate *lDate = [formatter dateFromString:lDateString];
        int lLimit = [[[SWDefaults licenseKey] stringForKey:@"ll"] intValue];
        lDate = [lDate dateByAddingTimeInterval:60*60*24*lLimit];
        NSDate *now =  [self   getServerDate];
        
        
        //sample date
        
        NSString* sampleDate;
        
        NSDate * testDate=[NSDate date];
        
        
        
        
        NSComparisonResult result = [now compare:lDate];
        switch (result)
        {
            case NSOrderedAscending:
                ////NSLog(@"Future Date");
                break;
            case NSOrderedDescending:
                ////NSLog(@"Earlier Date");
                isValid=NO;
                break;
            case NSOrderedSame:
                ////NSLog(@"Today/Null Date Passed");
                break;
            default:
                ////NSLog(@"Error Comparing Dates");
                break;
        }
        formatter=nil;
        usLocale=nil;
    }
    
    return isValid;
}
-(NSString*)stringBetweenString:(NSString*)start andString:(NSString*)end andMainString:(NSString *)mainString
{
    NSScanner* scanner = [NSScanner scannerWithString:mainString];
    [scanner setCharactersToBeSkipped:nil];
    [scanner scanUpToString:start intoString:NULL];
    if ([scanner scanString:start intoString:NULL])
    {
        NSString* result = nil;
        if ([scanner scanUpToString:end intoString:&result])
        {
            return result;
        }
    }
    return nil;
}

- (NSDate *)getServerDate
{
    //http://www.ucssolutions.com/licman/get-time.jsp
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://www.ucssolutions.com/licman/get-time.jsp"]];
    [request setHTTPMethod:@"GET"];

    NSURLResponse *response;
    NSError *err;
    NSData *responseData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *json_string2 = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSString *finalString = [self stringBetweenString:@"<DATE>" andString:@"</DATE>" andMainString:json_string2];
    
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSDate *lDate = [formatter dateFromString:finalString];
    formatter=nil;
    usLocale=nil;
    return lDate;
}

@end
