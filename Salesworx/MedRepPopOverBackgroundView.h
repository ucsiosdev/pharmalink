//
//  MedRepPopOverBackgroundView.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/24/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepPopOverBackgroundView : UIPopoverBackgroundView

@property (nonatomic, strong) UIImageView *arrowImageView;
@end
