//
//  SalesworxProductSamplesGivenViewController.m
//  MedRep
//
//  Created by USHYAKU-IOS on 11/16/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesworxProductSamplesGivenViewController.h"
#import "SalesworxProductSamplesGivenTableViewCell.h"

@interface SalesworxProductSamplesGivenViewController ()

@end

@implementation SalesworxProductSamplesGivenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)updateSamplesGivenData:(NSMutableArray *)arraySamplesGiven
{
    arrSamplesGiven = arraySamplesGiven;
    [self.tblSamplesGiven reloadData];
}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSamplesGiven.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"samplesGiven";
    SalesworxProductSamplesGivenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesworxProductSamplesGivenTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:kUITableViewHeaderBackgroundColor];
    cell.isHeader=YES;
    
    cell.lblProductName.isHeader=YES;
    cell.lblSamplesGiven.isHeader=YES;
    
    cell.lblProductName.text = NSLocalizedString(@"Product Name", nil) ;
    cell.lblSamplesGiven.text = NSLocalizedString(@"Samples Given", nil);
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"samplesGiven";
    SalesworxProductSamplesGivenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesworxProductSamplesGivenTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.lblProductName.text = [[arrSamplesGiven objectAtIndex:indexPath.row]valueForKey:@"Product_Name"];
    cell.lblSamplesGiven.text = [[arrSamplesGiven objectAtIndex:indexPath.row]valueForKey:@"Issue_Qty"];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
