//
//  MedRepCreateLocationViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 7/1/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepCreateLocationViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "MedRepQueries.h"
#import "MedRepPlacesSearchViewController.h"
#import "MedRepDefaults.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "Shared/SharedUtils.h"
@interface MedRepCreateLocationViewController ()

@end

@implementation MedRepCreateLocationViewController


@synthesize addressTxtView,locationLbl,typeLbl,telephoneLbl,doctorLbl,locationMapView,locationCoordinates,selectedLocationLatitude,SelectedLocationLongitude,contactTitleLbl,contactTitleBtn,contactNameHeader,contactEmailHeader,contactTelephoneHeader,contactTitleHeader;

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
//    CGRect frame = CGRectMake(0, 0, 400, 44);
//    UILabel *label = [[UILabel alloc] initWithFrame:frame];
//    label.backgroundColor = [UIColor clearColor];
//    label.font = headerTitleFont;
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor whiteColor];
//    label.text = NSLocalizedString(@"Create Location", nil);
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Create Location", nil)];
    // Do any additional setup after loading the view from its nib.
    
    

    NSLog(@"cities data is %@", citiesArray);

    
    //[self.navigationItem setHidesBackButton:YES];
     UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_btn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed:)];
    [newBackButton setTintColor:[UIColor whiteColor]];
   // [self.navigationItem setLeftBarButtonItem:newBackButton];
}
- (IBAction)backButtonPressed:(id)sender {
    contactEmailAddressTxtFld.text=@"";
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillDisappear:(BOOL)animated {
 
}


#pragma mark - keyboard movements


-(BOOL)validateContactData
{
    BOOL status=YES;
    
    
    if ( contactTitleTxtFld.text.length==0 || contactNameTxtFld.text.length==0 ||contactEmailAddressTxtFld.text.length==0 || contactTelephoneTxtFld.text.length==0) {
        
        
        status=NO;
            }
    
    return status;
    
}


-(void)saveLocation

{
    capturedDataDict=[[NSMutableDictionary alloc]init];
    
    
    
//    CLLocation *locationCoord = [[CLLocation alloc] initWithLatitude:csAnnotation.coordinate.latitude longitude:csAnnotation.coordinate.longitude];

    
    
    
    CLLocation *locationCoord = [[CLLocation alloc] initWithLatitude:[selectedLocationLatitude doubleValue]longitude:[SelectedLocationLongitude doubleValue]];
    
    
//    BOOL contactDetailsAvailable;
//    
//    
//    if (isPharmacyLocation==YES || self.contactTitleLbl.text.length==0 || contactNameTxtFld.text.length==0 ||contactEmailAddressTxtFld.text.length==0 || contactTelephoneTxtFld.text.length==0) {
//        
//        
//        contactDetailsAvailable=NO;
//        
//    }
    
    
    
    
    
    BOOL internetAvailable=[SharedUtils isConnectedToInternet];
    
    
    if (internetAvailable==YES) {
        
        
    }
    
    else
    {
        double tempvalue=0.00;
        
        
        locationCoord=[[CLLocation alloc] initWithLatitude:tempvalue longitude:tempvalue];
        
        selectedLocationLatitude=@"0.00";
        SelectedLocationLongitude=@"0.00";
        
    }
    if ( isPharmacyLocation==YES)
    {
        BOOL checkInternetConnectivity= [SharedUtils isConnectedToInternet];
        if (locationCoord==nil || locationNameTxtFld.text.length==0 || typeTextField.text.length==0 || telephoneNumberTxtFld.text.length==0 || cityTextField.text.length==0 /*|| contactNameTxtFld.text.length==0*/)
        {
            NSString* missingString;
            if (locationCoord==nil)
            {
                missingString=@"Please enter Location Coordinates";
            }
            else if (locationNameTxtFld.text.length==0)
            {
                missingString=@"Please enter Location Name";
            }
            else if (typeTextField.text.length==0)
            {
                missingString=@"Please enter Location Type";
            }
            else if (cityTextField.text.length==0)
            {
                missingString=@"Please enter City";
            }
            else if (telephoneNumberTxtFld.text.length==0)
            {
                missingString=@"Please enter Telephone Number";
            }
            NSString* alertString;
            alertString=missingString;
            
            
            
            
            
            [MedRepDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:NSLocalizedString(alertString, nil) withController:self];
        }
        else if([contactNameTxtFld.text trimString].length>0 && (contactTitleTxtFld.text.length==0 || contactEmailAddressTxtFld.text.length==0 || contactEmailAddressTxtFld.text.length==0 || ![SharedUtils validateEmailAddress:contactEmailAddressTxtFld.text] || contactTelephoneTxtFld.text.length==0) && (contactEmailAddressTxtFld.text.length==0 && contactTelephoneTxtFld.text.length==0))
        {
            NSString* missingString;

             if (contactTitleTxtFld.text.length==0)
            {
                missingString=@"Contact Title";
            }

           else if (contactEmailAddressTxtFld.text.length==0)
            {
                missingString=@"Contact Email Address";
            }
            else if (![SharedUtils validateEmailAddress:contactEmailAddressTxtFld.text])

            {
                missingString=@"Valid Contact Email Address";
            }
            else if (contactTelephoneTxtFld.text.length==0 )
            {
                missingString=@"Contact Telephone";
            }

            NSString* alertString;
            alertString=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Please enter", nil),NSLocalizedString(missingString, nil)];
            
            [MedRepDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:NSLocalizedString(alertString, nil) withController:self];
        }
        else
        {
            if (selectedLocationLatitude.length>0 && selectedLocationLatitude.length>0 && checkInternetConnectivity)
            {
                [capturedDataDict setValue:selectedLocationLatitude forKey:@"Latitude"];
                [capturedDataDict setValue:SelectedLocationLongitude forKey:@"Longitude"];
                NSLog(@"lat longs set");
            }
            else
            {
                [capturedDataDict setValue:@"0.0" forKey:@"Latitude"];
                [capturedDataDict setValue:@"0.0" forKey:@"Longitude"];
            }
            
            [capturedDataDict setValue:locationNameTxtFld.text forKey:@"Location_Name"];
            
            [capturedDataDict setValue:typeTextField.text forKey:@"Location_Type"];
            
            [capturedDataDict setValue:telephoneNumberTxtFld.text forKey:@"Telephone"];
            
            [capturedDataDict setValue:doctorLbl.text forKey:@"Doctor"];
            
            [capturedDataDict setValue:cityTextField.text forKey:@"City"];
            
            [capturedDataDict setValue:addressTxtView.text forKey:@"Address"];
            NSString* locationID = [NSString createGuid];
            
            [capturedDataDict setValue:locationID forKey:@"Location_ID"];

            //add contact Details
            
            if([contactNameTxtFld.text trimString].length>0 )
            {
                NSString* contactID=[NSString createGuid];
                
                [capturedDataDict setValue:contactID forKey:@"Contact_ID"];
                
                [capturedDataDict setValue:contactNameTxtFld.text forKey:@"ContactName"];
                
                [capturedDataDict setValue:contactEmailAddressTxtFld.text forKey:@"ContactEmail"];
                
                [capturedDataDict setValue:contactTelephoneTxtFld.text forKey:@"ContactTelephone"];
                
                [capturedDataDict setValue:contactTitleTxtFld.text forKey:@"ContactTitle"];

            }
            else
            {
                [capturedDataDict setValue:@"" forKey:@"Contact_ID"];
                
                [capturedDataDict setValue:@"" forKey:@"ContactName"];
                
                [capturedDataDict setValue:@"" forKey:@"ContactEmail"];
                
                [capturedDataDict setValue:@"" forKey:@"ContactTelephone"];
                
                [capturedDataDict setValue:@"" forKey:@"ContactTitle"];
            }
            
            NSLog(@"captured data dict is %@", [capturedDataDict description]);
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                               if([self.locationDetailsDelegate respondsToSelector:@selector(addLocationData:)])
                                               {
                                                   //save data to table
                                                   //even if new new location is created there might be a possibility thst user might need to add doctor as well with out choosing from the list of available doctors so add location and doctor while creating visit
                                                   
                                                   //added contact fields in UI, title,name,city,phone etc
                                                   BOOL status=[MedRepQueries createLocationWithDetails:capturedDataDict];
                                                   if (status==YES) {
                                                       NSLog(@"location Created Successfully");
                                                       [self.locationDetailsDelegate addLocationData:capturedDataDict];
                                                       [self.navigationController popViewControllerAnimated:YES];
                                                   }
                                                   
                                                   else
                                                   {

                                                       [MedRepDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:KLocationCreationErrorAlertMessageStr withController:self];
                                                   }
                                               }
                                       }];
            UIAlertAction* CancelAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action)
                                           {
                                               
                                           }];
            
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,CancelAction ,nil];
            [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(KLocationCreationConfirmationAlertTitleStr, nil) andMessage:NSLocalizedString(KLocationCreationConfirmationAlertMessageStr, nil) andActions:actionsArray withController:self];
        }
    }
    else  if (locationCoord==nil || locationNameTxtFld.text.length==0 || typeTextField.text.length==0 || telephoneNumberTxtFld.text.length==0 || cityTextField.text.length==0 ||doctorTextField.text.length==0 )
    {
        NSString* missingString;
        
        //NSString* missingMapLocation=@"Please tap on location map and choose location";
        
        if (locationCoord==nil)
        {
            missingString=@"Location Coordinates";
        }
        else if (locationNameTxtFld.text.length==0)
        {
            missingString=@"Location Name";
        }
        else if (typeTextField.text.length==0)
        {
            missingString=@"Location Type";
        }
        else if (doctorTextField.text.length==0)
        {
            missingString=@"Doctor";
        }
        else if (cityTextField.text.length==0)
        {
            missingString=@"City";
        }
        else if (telephoneNumberTxtFld.text.length==0)
        {
            missingString=@"Telephone Number";
        }
        
        //      else if (addressTxtView.text.length==0)
        //
        //      {
        //          missingString=@"Address";
        //      }
        //      else if (selectedLocationLatitude.length==0)
        //
        //      {
        //          missingString=missingMapLocation;
        //      }
        //
        //      else if (SelectedLocationLongitude.length==0)
        //
        //      {
        //          missingString=missingMapLocation;
        //      }
        
        NSString* alertString;
        
        //      if ([missingString isEqualToString:missingMapLocation]) {
        //
        //          alertString=[NSString stringWithFormat:@"%@",missingMapLocation];
        //      }
        //
        //      else
        //      {
        alertString=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Please enter", nil),NSLocalizedString(missingString, nil)];
        //      }
        [MedRepDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:NSLocalizedString(alertString, nil) withController:self];
    }
    
    else
    {
        if (selectedLocationLatitude.length>0 && selectedLocationLatitude.length>0)
        {
            [capturedDataDict setValue:selectedLocationLatitude forKey:@"Latitude"];
            [capturedDataDict setValue:SelectedLocationLongitude forKey:@"Longitude"];
            NSLog(@"lat longs set");
        }
        if (doctorIDforSelectedDoctor.length>0)
        {
            [capturedDataDict setValue:doctorIDforSelectedDoctor forKey:@"Doctor_ID"];
        }
        
        [capturedDataDict setValue:locationNameTxtFld.text forKey:@"Location_Name"];
        
        [capturedDataDict setValue:typeTextField.text forKey:@"Location_Type"];
        
        [capturedDataDict setValue:telephoneNumberTxtFld.text forKey:@"Telephone"];
        
        [capturedDataDict setValue:doctorTextField.text forKey:@"Doctor"];
        
        [capturedDataDict setValue:cityTextField.text forKey:@"City"];
        
        [capturedDataDict setValue:addressTxtView.text forKey:@"Address"];
        
        //add contact Details
        
        //no separate contact details for doctor, because for a hospital location doctro is contact
        
                NSString* contactID=[NSString createGuid];
                
                [capturedDataDict setValue:contactID forKey:@"Contact_ID"];
                
                [capturedDataDict setValue:contactNameTxtFld.text forKey:@"ContactName"];
                
                [capturedDataDict setValue:contactEmailAddressTxtFld.text forKey:@"ContactEmail"];
        
                [capturedDataDict setValue:contactTelephoneTxtFld.text forKey:@"ContactTelephone"];
        
                [capturedDataDict setValue:self.contactTitleLbl.text forKey:@"ContactTitle"];
        
        
        NSString* locationID = [NSString createGuid];
        
        [capturedDataDict setValue:locationID forKey:@"Location_ID"];
        
        
        NSLog(@"Pharmacy captured data dict bewfore saving is %@", [capturedDataDict description]);
        
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       if([self.locationDetailsDelegate respondsToSelector:@selector(addLocationData:)])
                                       {
                                           //save data to table
                                           //even if new new location is created there might be a possibility thst user might need to add doctor as well with out choosing from the list of available doctors so add location and doctor while creating visit
                                           
                                           //added contact fields in UI, title,name,city,phone etc
                                           BOOL status=[MedRepQueries createLocationWithDetails:capturedDataDict];
                                           if (status==YES) {
                                               NSLog(@"location Created Successfully");
                                               [self.locationDetailsDelegate addLocationData:capturedDataDict];
                                               [self.navigationController popViewControllerAnimated:YES];
                                           }
                                           
                                           else
                                           {
                                               
                                               [MedRepDefaults showAlertAfterHidingKeyBoard:KErrorAlertTitlestr andMessage:KLocationCreationErrorAlertMessageStr withController:self];
                                           }
                                       }
                                   }];
        UIAlertAction* CancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertCancelButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           
                                       }];
        
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction,CancelAction ,nil];
        [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:KLocationCreationConfirmationAlertTitleStr andMessage:KLocationCreationConfirmationAlertMessageStr andActions:actionsArray withController:self];
        
    }
}

-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.preferredContentSize=CGSizeMake(700, 600);
    if (self.navigationController){
        self.navigationController.preferredContentSize = self.preferredContentSize;
    }

    if (self.isMovingToParentViewController) {
        [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldMarketingCreateLocation];
    }
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];

    
    addressTxtView.clipsToBounds=YES;
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
    }
    
    [locationMapView setShowsUserLocation:YES];
    
    contactTitlesArray=[[NSMutableArray alloc]initWithObjects:@"Dr",@"Mr",@"Ms", nil];
    
    UIBarButtonItem* saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveLocation)];
    
    [saveBtn setTitleTextAttributes:[MedRepDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem=saveBtn;
    
    
    
    //current location
    
    appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    
    MKCoordinateSpan span = MKCoordinateSpanMake(52.0f,80.0001f);
    CLLocationCoordinate2D coordinate = appDelegate.currentLocation.coordinate;
    MKCoordinateRegion region = {coordinate, span};
    
    
    NSMutableArray* unsortedEmiratesArray=[[NSMutableArray alloc]init];
    
    

    
    unsortedEmiratesArray=[MedRepQueries fetchCitiesData];
    
    NSSortDescriptor *valueDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"description" ascending:YES];
    emiratesArray = [[unsortedEmiratesArray sortedArrayUsingDescriptors:@[valueDescriptor]] mutableCopy];
    
    NSLog(@"sorted emirates array is %@", emiratesArray);
    
//    [emiratesArray addObject:@"Abu Dhabi"];
//    [emiratesArray addObject:@"Dubai"];
//    [emiratesArray addObject:@"Sharjah"];
//    [emiratesArray addObject:@"Ajman"];
//    [emiratesArray addObject:@"Umm Al Quwain"];
//    [emiratesArray addObject:@"Ras Al Khaimah"];
//    [emiratesArray addObject:@"Fujairah"];
    
    
    //[emiratesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSLog(@"sorted emirates array %@", [emiratesArray description]);
    
    
    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (
                                                                          coordinate, 50000, 50000);


    
    MKCoordinateRegion regionThatFits = [locationMapView regionThatFits:region];
    NSLog(@"Fit Region %f %f", regionThatFits.center.latitude, regionThatFits.center.longitude);
    
    [locationMapView setRegion:regionCustom animated:YES];
    
    [locationMapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];

    

    
    
    
    
    
    
    doctorsArray=[MedRepQueries fetchDoctorsforCreateLocation];
    
    refinedDoctorsArray=[[NSMutableArray alloc]init];
    
    
    
    //NSLog(@"doctors are %@", [doctorsArray description]);
    
    
    
    for (NSMutableDictionary * currentDict in doctorsArray) {
        
        if ([currentDict objectForKey:@"Doctor_Name"]) {
            
            [refinedDoctorsArray addObject:[currentDict objectForKey:@"Doctor_Name"]];
            
        }
        
        
    }
    
    
    locationsarray=[[NSMutableArray alloc]initWithObjects:@"Clinic",@"Hospital",@"Pharmacy", nil];
    
    
    
    for (UIView * currentLbl in self.view.subviews) {
        
        if ([currentLbl isKindOfClass:[UILabel class]]) {
            
            
            if (currentLbl.tag==4||currentLbl.tag==1||currentLbl.tag==2||currentLbl.tag==3|| currentLbl.tag==5) {
                
                
//                CALayer *bottomBorder = [CALayer layer];
//                bottomBorder.frame = CGRectMake(0.0f, currentLbl.frame.size.height - 1, currentLbl.frame.size.width, 1.0f);
//                bottomBorder.backgroundColor = [UIColor colorWithRed:(201.0/255.0) green:(209.0/255.0) blue:(222.0/0/255.0) alpha:1].CGColor;
//                [currentLbl.layer addSublayer:bottomBorder];
//                
                
//                CALayer* layer = currentLbl.layer;
//                
//                CALayer *bottomBorder = [CALayer layer];
//                bottomBorder.borderColor = [UIColor darkGrayColor].CGColor;
//                bottomBorder.borderWidth = 1;
//                bottomBorder.frame = CGRectMake(-1, layer.frame.size.height-1, layer.frame.size.width, 1);
//                [bottomBorder setBorderColor:[UIColor blackColor].CGColor];
//                [layer addSublayer:bottomBorder];
              
            }
            
//            
        }
        
    }
    
    
//    
//    addressTxtView.layer.borderColor = UITextViewBorderColor.CGColor;
//    addressTxtView.layer.borderWidth = 1.0;
//    
//
//    
//    
//    locationMapView.layer.borderColor = [UIColor colorWithRed:(201.0/255.0) green:(209.0/255.0) blue:(222.0/0/255.0) alpha:1].CGColor;
//    locationMapView.layer.borderWidth = 1.0;
    

    
    
   // NSLog(@" refined doctors are %@", [refinedDoctorsArray description]);
    
    
    //add annotation for map
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 2.0; //user needs to press for 2 seconds
    [locationMapView addGestureRecognizer:lpgr];
    
    UITapGestureRecognizer * tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired=1;
    [locationMapView addGestureRecognizer:tapGesture];
    
    

    
//    if (selectedLocationLatitude.length>0 && SelectedLocationLongitude.length>0) {
//        
//        
//        NSLog(@"lat longs set in delegate");
//        
//        place = [[Place alloc] init] ;
//        place.latitude= [selectedLocationLatitude doubleValue];
//        place.longitude = [SelectedLocationLongitude doubleValue];
//        place.name= locationLbl.text;
//        place.otherDetail = @"";
//        
//        [locationMapView removeAnnotation:csAnnotation];
//        
//        
//        csAnnotation=[[CSMapAnnotation alloc]initWithPlace:place] ;
//        
//        
//        
//        [locationMapView addAnnotation:csAnnotation];
//        
//
//    }

    
    
    for (UITextField* currentTxtFld in self.view.subviews) {
        
        
        if ([currentTxtFld isKindOfClass:[UITextField class]]) {
            
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            currentTxtFld.leftView = paddingView;
            currentTxtFld.leftViewMode = UITextFieldViewModeAlways;
        }
        
    }
    
    
    
    
    for (UIView * currentButton in self.view.subviews) {
        
        if ([currentButton isKindOfClass:[UIButton class]]) {
            
            
            if (currentButton.tag==1006||currentButton.tag==1007||currentButton.tag==1008) {
                
                currentButton.layer.borderWidth=1.0f;
                currentButton.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
                currentButton.layer.shadowColor = [UIColor blackColor].CGColor;
                currentButton.layer.shadowOffset = CGSizeMake(2, 2);
                currentButton.layer.shadowOpacity = 0.1;
                currentButton.layer.shadowRadius = 1.0;
                
                
            }
        }
    }
    
    
    
//    addressTxtView.layer.borderWidth=1.0f;
//    addressTxtView.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
//    addressTxtView.layer.masksToBounds = NO;
//    addressTxtView.layer.shadowColor = [UIColor blackColor].CGColor;
//    addressTxtView.layer.shadowOpacity = 0.1f;
//    addressTxtView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
//    addressTxtView.layer.shadowRadius = 1.0f;
//   // addressTxtView.layer.shouldRasterize = YES;
    
    
    
    locationMapView.layer.borderWidth=1.0f;
    locationMapView.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
    locationMapView.layer.masksToBounds = NO;
    locationMapView.layer.shadowColor = [UIColor blackColor].CGColor;
    locationMapView.layer.shadowOpacity = 0.1f;
    locationMapView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    locationMapView.layer.shadowRadius = 1.0f;
    locationMapView.layer.shouldRasterize = YES;
    
    
    
    for (UIView * currentTextField in self.view.subviews) {
        
        if ([currentTextField isKindOfClass:[UITextField class]]) {
            
            
            if (currentTextField.tag==1000||currentTextField.tag==1001||currentTextField.tag==1003||currentTextField.tag==1004 || currentTextField.tag==1005) {
                
                
                //this drop shadow wont work so adding a uiview behind it
                
                
//                UIView* shadowView = [[UIView alloc] initWithFrame:currentTextField.frame];
//                
//                shadowView.layer.borderWidth=1.0f;
//                shadowView.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
//                shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
//                shadowView.layer.shadowOffset = CGSizeMake(2, 2);
//                shadowView.layer.shadowOpacity = 0.1;
//                shadowView.layer.shadowRadius = 1.0;
//                
                
                
                /*
                currentTextField.layer.borderWidth=1.0f;
                currentTextField.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
                currentTextField.layer.masksToBounds = NO;
                currentTextField.layer.shadowColor = [UIColor blackColor].CGColor;
                currentTextField.layer.shadowOpacity = 0.1f;
                currentTextField.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
                currentTextField.layer.shadowRadius = 1.0f;
                currentTextField.layer.shouldRasterize = YES;*/
                
                
                
                //shadowView.layer.shouldRasterize = YES;
                //[self.view addSubview:shadowView];
                
                
                //[self.view insertSubview:shadowView belowSubview:currentTextField];
                

                //[currentTextField sendSubviewToBack:shadowView];
                
//                currentButton.layer.borderWidth=1.0f;
//                currentButton.layer.borderColor=[UITextFieldDarkBorderColor CGColor];
//                currentButton.layer.shadowColor = [UIColor blackColor].CGColor;
//                currentButton.layer.shadowOffset = CGSizeMake(1, 1);
//                currentButton.layer.shadowOpacity = 0.1;
//                currentButton.layer.shadowRadius = 1.0;
                
                
            }
        }
    }
    
   
    
}


-(void)disabledButtonTapped:(id)sender{

}
-(void)handleTapGesture:(UIGestureRecognizer*)gesture

{
    NSLog(@"location Tapped");
    MedRepPlacesSearchViewController* placesSearch=[[MedRepPlacesSearchViewController alloc]init];
    placesSearch.delegate=self;
    
    //[self presentPopupViewController:placesSearch animationType:MJPopupViewAnimationSlideBottomTop];
    
    
    
    //[self presentViewController:placesSearch animated:YES completion:nil];
    
    
    [self.navigationController pushViewController:placesSearch animated:YES];
    
}

- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:locationMapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [locationMapView convertPoint:touchPoint toCoordinateFromView:locationMapView];
    
    
    place = [[Place alloc] init] ;
    place.latitude= touchMapCoordinate.latitude;
    place.longitude =  touchMapCoordinate.longitude;
    place.name= locationLbl.text;
    place.otherDetail = @"";
    
    [locationMapView removeAnnotation:csAnnotation];
    
    
    csAnnotation=[[CSMapAnnotation alloc]initWithPlace:place] ;
    
    

    [locationMapView addAnnotation:csAnnotation];
    
    
    
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = @"cafe";
    request.region = MKCoordinateRegionMakeWithDistance(touchMapCoordinate, 1000, 1000);
    MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
    [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        for (MKMapItem *item in response.mapItems) {
            NSLog(@"location results%@", item.name);
        }
    }];
    
    
    
}

//- (IBAction)emirateButtonTapped:(id)sender {
  -(void)cityDropDownTapped
{
    

    popUpString=@"City";
    
    
    //fetch cities
    
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=popUpString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=emiratesArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];
    

    
    /*
    UIButton* locationBtn=(UIButton*)sender;
    
    valuePopOverController=nil;
    
    locationPopOver = nil;
    locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    
    
    
    locationPopOver.colorNames = emiratesArray;
    
    locationPopOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
        [valuePopOverController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        //The color picker popover is showing. Hide it.
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }*/

    
}


- (IBAction)closeButtonTapped:(id)sender {
    
    [self.dismissDelegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}

//- (IBAction)typeButtonTapped:(id)sender {
 -(void)typeDropDownTapped
{
    popUpString=@"Location";
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"Visit";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=popUpString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=locationsarray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];

    
    /*
    UIButton* locationBtn=(UIButton*)sender;
    
    valuePopOverController=nil;
    
    locationPopOver = nil;
    locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    
    
    
    locationPopOver.colorNames = locationsarray;
    
    locationPopOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
        [valuePopOverController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        //The color picker popover is showing. Hide it.
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }*/

    
}

//- (IBAction)doctorButtonTapped:(id)sender {
  -(void)doctorDropDownTapped
{
    
    if([typeTextField.text.lowercaseString isEqualToString:@"Pharmacy".lowercaseString])
    {
        doctorTextField.text=@"";
        UIAlertView * pharmacyAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Pharmacy Selected", nil) message:NSLocalizedString(@"Doctor not required for visiting a pharmacy", nil) delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
        pharmacyAlert.tag=1;
        [pharmacyAlert show];
        
        return;
    }
    popUpString=@"Doctors";
    
    
    valuePopOverController=nil;
    
    
    locationPopOver = nil;
    locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    
    
    refinedDoctorsArray=doctorsArray;
    
   // NSLog(@"refined doctors are %@", refinedDoctorsArray);
    
    if ([refinedDoctorsArray containsObject:@"New Doctor"]) {
        
    
    }
    else if (![[AppControl retrieveSingleton].DISABLE_FM_CREATE_NEW_DOCTOR isEqualToString:KAppControlsYESCode])
    {
        NSMutableDictionary * doctorDict=[[NSMutableDictionary alloc]init];
        
        [doctorDict setObject:@"New Doctor" forKey:@"Contact_Name"];
        
        
        [refinedDoctorsArray insertObject:doctorDict atIndex:0];
    }
    
    
    MedRepDoctorFilterDescriptionViewController * filterDescVC=[[MedRepDoctorFilterDescriptionViewController alloc]init];
    //filterDescVC.filterPopOverController=self.filterPopOverController;
    filterDescVC.filterType=@"New Location";
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=popUpString;
    CGRect typeFrame=self.view.frame;
    
    //typeFrame.size.width=typeFrame.size.width/2;
    
    filterDescVC.customRect=typeFrame;
    NSLog(@"current frame %@", NSStringFromCGRect(typeFrame));
    filterDescVC.isCreateVisit=YES;
    filterDescVC.filterDescArray=refinedDoctorsArray;
    
    [self.navigationController pushViewController:filterDescVC animated:YES];

}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==100000000) {
        [contactEmailAddressTxtFld becomeFirstResponder];
        
    }
}

//#pragma mark UIPopOver Controller delegate
//
//- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
//{
//    return NO;
//}

#pragma mark UIAlertView Delegate Method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"clicked index %d", buttonIndex);
    
    if (alertView.tag==1 || alertView.tag==2) {
        
    }
    
    
    else
    {
        
    }
    
}


#pragma mark String Popover delegate


-(void)selectedValueforCreateVisit:(NSIndexPath*)indexPath
{
    
    //NSInteger selectedIndex=indexPath.row-1;
    
   // NSLog(@"SELECTED INDEX FOR DOCTOR IS %d",selectedIndex);

    if ([popUpString isEqualToString:@"Location"]) {
        
        typeTextField.text=[NSString stringWithFormat:@"%@", [locationsarray objectAtIndex:indexPath.row]];
        if([[NSString stringWithFormat:@"%@", [locationsarray objectAtIndex:indexPath.row]].lowercaseString isEqualToString:@"Pharmacy".lowercaseString])
        {
            
            
            isPharmacyLocation=YES;
            
            
            doctorTextField.text=@"";
            doctorIDforSelectedDoctor=nil;
            
            
            
//            contactTitleTxtFld.enabled=YES;
//            contactEmailAddressTxtFld.enabled=YES;
//            contactTelephoneTxtFld.enabled=YES;
//            contactNameTxtFld.enabled=YES;
//            contactTitleBtn.enabled=YES;
            
            
//            contactTitleLbl.hidden=NO;
//            contactEmailAddressTxtFld.hidden=NO;
//            contactTelephoneTxtFld.hidden=NO;
//            contactNameTxtFld.hidden=NO;
//            contactTitleBtn.hidden=NO;
            
            
            
            
//            contactTitleHeader.hidden=NO;
//            contactNameHeader.hidden=NO;
//            contactEmailHeader.hidden=NO;
//            contactTelephoneHeader.hidden=NO;

            
        }
        
        else
        {
            isPharmacyLocation=NO;
            
//            contactTitleTxtFld.enabled=NO;
//            contactEmailAddressTxtFld.enabled=NO;
//            contactTelephoneTxtFld.enabled=NO;
//            contactNameTxtFld.enabled=NO;
//            contactTitleBtn.enabled=NO;
            
//            contactTitleLbl.hidden=YES;
//            contactEmailAddressTxtFld.hidden=YES;
//            contactTelephoneTxtFld.hidden=YES;
//            contactNameTxtFld.hidden=YES;
//            contactTitleBtn.hidden=YES;
            
//            contactTitleHeader.hidden=YES;
//            contactNameHeader.hidden=YES;
//            contactEmailHeader.hidden=YES;
//            contactTelephoneHeader.hidden=YES;
            
            contactNameTxtFld.text=@"";
            contactTitleTxtFld.text=@"";
            contactEmailAddressTxtFld.text=@"";
            contactTelephoneTxtFld.text=@"";

        }
        
    }
    
    else if ([popUpString isEqualToString:@"Doctors"])
    {
        NSLog(@"selected doctor %@", [refinedDoctorsArray objectAtIndex:indexPath.row]);
        
        
        doctorTextField.text=[NSString stringWithFormat:@"%@", [[refinedDoctorsArray objectAtIndex:indexPath.row] valueForKey:@"Contact_Name"]];
        
       if(indexPath.row>0)
        {
            
            //because an object New Doctor added in array being sent
            
            doctorIDforSelectedDoctor=[NSString stringWithFormat:@"%@", [[doctorsArray objectAtIndex:indexPath.row]valueForKey:@"Doctor_ID"]];
            
            NSLog(@"selected doctor is %@", [doctorsArray objectAtIndex:indexPath.row]);
            
            
        }
        else
        {
            doctorIDforSelectedDoctor=@"";
        }
        NSLog(@"DOCTOR ID FOR SELECTED DOCTOR IS %@", doctorIDforSelectedDoctor);
        
    }
    
    else if ([popUpString isEqualToString:@"City"])
        
    {
        cityTextField.text=[NSString stringWithFormat:@"%@", [emiratesArray objectAtIndex:indexPath.row]];
        
    }


}

-(void)selectedStringValue:(NSIndexPath *)indexPath

{
    
    //adding a new doctor cell so increment indexpath here
    NSInteger selectedIndex=indexPath.row-1;
    
    if ([popUpString isEqualToString:@"Location"]) {
        
        typeTextField.text=[NSString stringWithFormat:@"%@", [locationsarray objectAtIndex:indexPath.row]];
        
        NSLog(@"selected type is %@",[locationsarray objectAtIndex:indexPath.row] );
        
        if([[NSString stringWithFormat:@"%@", [locationsarray objectAtIndex:indexPath.row]].lowercaseString isEqualToString:@"Pharmacy".lowercaseString])
        {
            
            
            isPharmacyLocation=YES;

            
            doctorTextField.text=@"";
            doctorIDforSelectedDoctor=nil;
            
            
            contactTitleTxtFld.hidden=NO;
            contactEmailAddressTxtFld.hidden=NO;
            contactTelephoneTxtFld.hidden=NO;
            contactNameTxtFld.hidden=NO;
            contactTitleBtn.hidden=NO;
            
            contactTitleHeader.hidden=NO;
            contactNameHeader.hidden=NO;
            contactEmailHeader.hidden=NO;
            contactTelephoneHeader.hidden=NO;
        }
        
        else
        {
            isPharmacyLocation=NO;

            contactTitleTxtFld.hidden=YES;
            contactEmailAddressTxtFld.hidden=YES;
            contactTelephoneTxtFld.hidden=YES;
            contactNameTxtFld.hidden=YES;
            contactTitleBtn.hidden=YES;
            
            contactTitleHeader.hidden=YES;
            contactNameHeader.hidden=YES;
            contactEmailHeader.hidden=YES;
            contactTelephoneHeader.hidden=YES;
            
            
        }
        
    }
    
    else if ([popUpString isEqualToString:@"Doctors"])
    {
        doctorTextField.text=[NSString stringWithFormat:@"%@", [[refinedDoctorsArray  objectAtIndex:indexPath.row]valueForKey:@"Contact_Name"]];
        if(selectedIndex>0)
        {
            doctorIDforSelectedDoctor=[NSString stringWithFormat:@"%@", [[doctorsArray objectAtIndex:selectedIndex]valueForKey:@"Doctor_ID"]];
                                   
        }
        
    }
    
    else if ([popUpString isEqualToString:@"City"])
        
    {
        cityTextField.text=[NSString stringWithFormat:@"%@", [emiratesArray objectAtIndex:indexPath.row]];
        
    }
    
    else if ([popUpString isEqualToString:@"ContactTitle"])
    {
        self.contactTitleLbl.text=[NSString stringWithFormat:@"%@", [contactTitlesArray objectAtIndex:indexPath.row]];
    }
    
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    locationPopOver.delegate=nil;
    locationPopOver=nil;
}


#pragma mark UITextView Delegate Methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if([newString length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    
    return (newString.length<AddressTextFieldLimit);

}



#pragma mark UITextField Methods


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField ==telephoneNumberTxtFld) {
        
        [telephoneNumberTxtFld resignFirstResponder];
        [addressTxtView becomeFirstResponder];
        

    }
    
    else if (textField==contactEmailAddressTxtFld)
        
    {
        BOOL emailValid=[MedRepDefaults validateEmail:contactEmailAddressTxtFld.text];
        
        if (emailValid==YES || contactEmailAddressTxtFld.text.length==0) {

        }
        else
        {
            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           //[contactEmailAddressTxtFld becomeFirstResponder];
                                       }];
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
            [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(KInvalidEmailAlertTitleStr, nil) andMessage:NSLocalizedString(KInvalidEmailAlertMessageStr, nil) andActions:actionsArray withController:self];
        }

    }
}





- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField==contactTitleTxtFld) {
        [contactTitleTxtFld resignFirstResponder];
        [contactNameTxtFld becomeFirstResponder];
        return YES;

        
    }
    
   else if (textField==locationNameTxtFld) {
        [locationNameTxtFld resignFirstResponder];
        //[telephoneNumberTxtFld becomeFirstResponder];
       return YES;


    }
    
    else if (textField ==telephoneNumberTxtFld)
    {
        [telephoneNumberTxtFld resignFirstResponder];
        [addressTxtView becomeFirstResponder];
        
        return NO;
        
    }
    
   else if (textField == contactNameTxtFld) {
        [contactNameTxtFld resignFirstResponder];
        [contactEmailAddressTxtFld becomeFirstResponder];
       return YES;

       
    }
    
    
    else if (textField == contactEmailAddressTxtFld) {
        
        BOOL emailValid=[MedRepDefaults validateEmail:contactEmailAddressTxtFld.text];
        
        if (emailValid==YES) {
            [contactEmailAddressTxtFld resignFirstResponder];
            [contactTelephoneTxtFld becomeFirstResponder];
        }
        else
        {

            UIAlertAction* okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           //[contactEmailAddressTxtFld becomeFirstResponder];
                                       }];
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:okAction ,nil];
            [MedRepDefaults ShowConfirmationAlertAfterHidingKeyBoard:KInvalidEmailAlertTitleStr andMessage:KInvalidEmailAlertMessageStr andActions:actionsArray withController:self];
        }
        
      
        
        
        return YES;

        
        
    }
    
    else if (textField == contactTelephoneTxtFld) {
        [contactTelephoneTxtFld resignFirstResponder];
        return YES;

        
    }
    else
    {
        return YES;
    }

    
}



- (void) animateUITextField: (UITextField*) textField up: (BOOL) up
{
    
    
}






#pragma mark MapView Delegate methods
//- (MKAnnotationView *) mapView: (MKMapView *) mapView viewForAnnotation:(id<MKAnnotation>) annotation
//{
//    if (annotation == mapView.userLocation)
//    {
//        return nil;
//    }
//    else
//    {
//        MKAnnotationView *pin = (MKAnnotationView *) [locationMapView dequeueReusableAnnotationViewWithIdentifier: @"VoteSpotPin"];
//        if (pin == nil)
//        {
//            pin = [[MKAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"TestPin"];
//        }
//        else
//        {
//            pin.annotation = annotation;
//        }
//        
//        [pin setImage:[UIImage imageNamed:@"TestPin.png"]];
//        pin.canShowCallout = YES;
//        pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//        return pin;
//    }
//}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
 
    CSMapAnnotation * currentAnnotation=[[CSMapAnnotation alloc]init];
    currentAnnotation=[mapView.annotations objectAtIndex:0];
    
    NSLog(@"check pin location %f %f ", currentAnnotation.coordinate.latitude,currentAnnotation.coordinate.longitude);
    
    
    
    NSLog(@"pin tapped ");
    

}


#pragma mark Location Delegate

-(void)selectedLocation:(CLLocationCoordinate2D)location
{
    NSLog(@"location through delegate is %f %f ", location.latitude, location.longitude);
    
    NSString *latitude = [[NSString alloc] initWithFormat:@"%f", location.latitude];

    
    NSString *longitude = [[NSString alloc] initWithFormat:@"%f", location.longitude];

    
   
    selectedLocationLatitude=latitude;
    SelectedLocationLongitude=longitude;
    
    
    
    
//    [capturedDataDict setValue:latitude forKey:@"Latitude"];
////
//    [capturedDataDict setValue:longitude forKey:@"Longitude"];

    
    
    
}


#pragma maek UITextField Delegate Methods
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
//  
////    typeLbl.layer.sublayerTransform= CATransform3DMakeTranslation(5, 0, 0);
////    textField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
//    
//    NSUInteger newLength = [textField.text length] + [string length] - range.length;
//    
//    if(textField == telephoneNumberTxtFld && newLength>12)
//    {
//        return NO;
//    }
//    else if(textField==locationNameTxtFld && newLength>50)
//    {
//        return NO;
//    }
//    
// if (textField == telephoneNumberTxtFld) {
//        
//        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
//        
//        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
//        
//        return [string isEqualToString:filtered];
//    }
//    
//    else if (textField ==contactEmailAddressTxtFld) {
//        
//        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:EMAIL_ACCEPTABLE_CHARECTERS] invertedSet];
//        
//        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
//        
//        return [string isEqualToString:filtered];
//    }
//    else
//    {
//        return YES;
//    }
//    
//    
//}


- (IBAction)contactTitleTapped:(id)sender {
    
    
    popUpString=[[NSString alloc]init];
    
    
    popUpString=@"ContactTitle";
    
    UIButton* locationBtn=(UIButton*)sender;
    
    valuePopOverController=nil;
    
    locationPopOver = nil;
    locationPopOver = [[StringPopOverViewController_ReturnRow alloc] initWithStyle:UITableViewStylePlain withWidth:300];
    
    
    
    locationPopOver.colorNames = contactTitlesArray;
    
    locationPopOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:locationPopOver];
        [valuePopOverController presentPopoverFromRect:locationBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        //The color picker popover is showing. Hide it.
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    
    

    
}

#pragma mark Disbaled Contact Buttons

- (IBAction)contactTitleDisabledEventButton:(id)sender {
    
    NSString* tappedButtonName;
    
    
    
    UIButton * currentButton=(UIButton*)sender;
    
    if (currentButton .tag==1001 && contactNameTxtFld.enabled==NO) {
        
        tappedButtonName=@"Contact Name not required";
    }
    else if (currentButton .tag==1002  && contactTitleTxtFld.enabled==NO) {
        
        tappedButtonName=@"Contact Title not required";
    }
    else if (currentButton .tag==1003 && contactEmailAddressTxtFld.enabled==NO) {
        
        tappedButtonName=@"Contact Email not required";
    }
    else if (currentButton .tag==1004 && contactTelephoneTxtFld.enabled==NO) {
        
        tappedButtonName=@"Contact Telephone not required";
    }
    
    if (tappedButtonName.length>0) {
        
        
        
        
        UIAlertView* disabledContactAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Hospital Location Selected", nil) message:NSLocalizedString(tappedButtonName, nil) delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
        disabledContactAlert.tag=2;
        [disabledContactAlert show];
        
    }

    
    

}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.view endEditing:YES];

    NSString* tappedButtonName;

    if(typeTextField.text.length>0 &&![typeTextField.text.lowercaseString isEqualToString:@"pharmacy"])
    {
        if ( textField==contactNameTxtFld) {
            
            tappedButtonName=@"Contact Name not required";
        }
        else if ( textField==contactTitleTxtFld) {
            
            tappedButtonName=@"Contact Title not required";
        }
        else if ( textField==contactEmailAddressTxtFld) {
            
            tappedButtonName=@"Contact Email not required";
        }
        else if (textField== contactTelephoneTxtFld) {
            
            tappedButtonName=@"Contact Telephone not required";
        }
        
        if (tappedButtonName.length>0) {
            
            
            UIAlertView* disabledContactAlert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Hospital Location Selected", nil) message:NSLocalizedString(tappedButtonName, nil) delegate:self cancelButtonTitle:(NSLocalizedString(@"OK", nil)) otherButtonTitles: nil];
            disabledContactAlert.tag=2;
            [disabledContactAlert show];
            return NO;
        }

    }
    
    if(textField==typeTextField)
    {
        [self typeDropDownTapped];
        return NO;
    }
    else if(textField==doctorTextField)
    {
        [self doctorDropDownTapped];
        return NO;
    }
    else if(textField==cityTextField)
    {
        [self cityDropDownTapped];
        return NO;
    }

    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if([newString length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    
    
    if(textField==contactTitleTxtFld)
    {
        NSString *expression = @"^[a-zA-Z.]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<ContactTitleTextFieldLimit);
    }
    else  if(textField==contactNameTxtFld)
    {
        NSString *expression = @"^[a-zA-Z. ]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<ContactNameTextFieldLimit);
    }
    else  if(textField==contactEmailAddressTxtFld)
    {
        
        return  (newString.length<ContactEmailAddressTextFieldLimit);

    }
    if(textField==locationNameTxtFld)
    {
        NSString *expression = @"^[a-zA-Z0-9. ]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<LocationNameTextFieldLimit);
    }
    if(textField==telephoneNumberTxtFld || textField==contactTelephoneTxtFld)
    {
        NSString *expression = @"^[0-9+ ]+$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return (numberOfMatches != 0 && newString.length<ContactTelephoneTextFieldLimit);


    }
    return YES;

}


@end
