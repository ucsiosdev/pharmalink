//
//  MedRepDoctorsLastVisitDetailsPopOverViewController.h
//  MedRep
//
//  Created by Unique Computer Systems on 12/7/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "NSString+Additions.h"
#import "MedRepElementDescriptionLabel.h"
#import "HMSegmentedControl.h"
#import "MedRepQueries.h"

@interface MedRepDoctorsLastVisitDetailsPopOverViewController : UIViewController
{
    NSString* nextVisitObjectiveTitle;
}
@property (strong, nonatomic) IBOutlet UITextView *previousVisitNotesTextView;
@property (strong,nonatomic)NSString *notesString;
@property (strong,nonatomic)NSMutableDictionary * lastVisitDetailsDict;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *lastVisitNotesLabel;
@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *nextVisitObjectiveLabel;
@property (strong, nonatomic) IBOutlet HMSegmentedControl *lastVisitDetailsSegmentController;
@property (strong, nonatomic) IBOutlet UITextView *contentTextView;
@property (nonatomic)BOOL isVisitScreen;



@end
