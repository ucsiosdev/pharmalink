//
//  ProductBrandFilterViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 7/9/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWTableViewcontroller.h"


#import "SWPlatform.h"

@interface ProductBrandFilterViewController : SWTableViewController <EditableCellDelegate> {
    NSArray *filters;
//       id target;
    SEL action;
    UIView *myBackgroundView;
}

@property (nonatomic, strong) NSArray *filters;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;
- (void)selectionDone:(id)sender;
@end