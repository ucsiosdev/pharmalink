//
//  MedRepSearchPopOverViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/3/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol DismissPopoverDelegate <NSObject>


-(void)medRepPopOverControllerSearchString:(NSString*)searchText;



@end

@interface MedRepSearchPopOverViewController : UIViewController

{
    id searchPopoverDelegate;
}

@property(strong,nonatomic) IBOutlet UISearchBar *popoverControllerSearchBar;

@property(nonatomic) id searchPopoverDelegate;


@property(strong,nonatomic) UIPopoverController * popOverController;

@property(strong,nonatomic) UINavigationController * navController;

@end
