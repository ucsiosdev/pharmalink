////
////  PaymentReportViewController.m
////  Salesworx
////
////  Created by msaad on 6/13/13.
////  Copyright (c) 2013 msaad. All rights reserved.
////
//
//#import "PaymentReportViewController.h"
//
//@interface PaymentReportViewController ()
//
//@end
//
//@implementation PaymentReportViewController
//
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
////- (void)dealloc
////{
////    NSLog(@"Dealloc %@",self.title);
////    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
////}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//        self.view.backgroundColor = [UIColor whiteColor];
//    gridView = [[GridView alloc] initWithFrame:CGRectMake(302, 27, 695, 600)];
//    [gridView setDataSource:self];
//    [gridView setDelegate:self];
//    [gridView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    [gridView.layer setBorderWidth:1.0f];
//    [self.view addSubview:gridView];
//    
//    filterTableView.backgroundView=nil;[filterTableView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    [filterTableView.layer setBorderWidth:1.0f];
//    
//    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
//    datePickerViewController.isRoute=NO;
//    datePickerViewController.forReports=YES;
//    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
//    navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    navigationController.navigationBar.translucent = NO;
//    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
//    datePickerPopOver.delegate=self;
//    
//    NSDateFormatter *formatter = [NSDateFormatter new]  ;
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    
//    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//    [offsetComponents setMonth:-1]; // note that I'm setting it to -1
//    fromDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
//    toDate=[formatter stringFromDate:[NSDate date]];
//    custType = @"All";
//    [self viewReportAction];
//    formatter=nil;
//    usLocale=nil;
//
//    // Do any additional setup after loading the view.
//}
//-(void)viewWillAppear:(BOOL)animated
//{
//    //NSLog(@"Payment Documents");
//     [self viewReportAction];
//}
//- (void)typeChanged:(NSString *)sender
//{
//    [customPopOver dismissPopoverAnimated:YES];
//    if(isCustType)
//    {
//        custType=sender;
//    }
//    else
//    {
//        DocType = sender;
//    }
//    [filterTableView reloadData];
//    [self viewReportAction];
//}
//- (void)dateChanged:(SWDatePickerViewController *)sender
//{
//    [datePickerPopOver dismissPopoverAnimated:YES];
//    NSDateFormatter *formatter = [NSDateFormatter new]  ;
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatter setLocale:usLocale];
//    if(isFromDate)
//    {
//        fromDate=[formatter stringFromDate:sender.selectedDate];
//    }
//    else
//    {
//        toDate = [formatter stringFromDate:sender.selectedDate];
//    }
//    [filterTableView reloadData];
//    formatter=nil;
//    usLocale=nil;
//    [self viewReportAction];
//
//}
//- (void)currencyTypeChanged:(NSDictionary *)customer {
//    [currencyTypePopOver dismissPopoverAnimated:YES];
//    customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
//    [filterTableView reloadData];
//    [self viewReportAction];
//
//}
//
//#pragma TableView Delegate
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 44;
//}
//- (NSInteger)tableView:(UITableView *)tableViews numberOfRowsInSection:(NSInteger)section {
//    return 4;
//    
//}
//- (CGFloat)tableView:(UITableView *)tableViews heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 44;
//}
//- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
//{
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, tv.bounds.size.width, 44)]  ;
//    UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, headerView.bounds.size.width, headerView.bounds.size.height)]  ;
//    [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
//    [companyNameLabel setText:NSLocalizedString(@"Filter", nil)];
//    [companyNameLabel setBackgroundColor:UIColorFromRGB(0xF6F7FB)];
//    [companyNameLabel setTextColor:[UIColor darkGrayColor]];
//    //    [companyNameLabel setShadowColor:[UIColor whiteColor]];
//    //    [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
//    [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];
//    [companyNameLabel setTextAlignment:NSTextAlignmentCenter];
//    
//    [headerView addSubview:companyNameLabel];
//    return headerView;
//    
//}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//	return NSLocalizedString(@"Filter", nil);
//}
//- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    @autoreleasepool
//    {
//        NSString *CellIdentifier = @"CustomerDetailViewCell";
//        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
//        if (!cell)
//        {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
//        }
//
//        if(indexPath.row==0)
//        {
//            if(customerDict.count!=0)
//            {
//                cell.textLabel.text = [customerDict stringForKey:@"Customer_Name"];
//            }
//            else
//            {
//                cell.textLabel.text = NSLocalizedString(@"Select Customer", nil);
//            }
//        }
//        else if(indexPath.row==1)
//        {
//            [cell.textLabel setText:NSLocalizedString(@"From Date", nil)];
//            [cell.detailTextLabel setText:fromDate];
//        }
//        else if(indexPath.row==2)
//        {
//            [cell.textLabel setText:NSLocalizedString(@"To Date", nil)];
//            [cell.detailTextLabel setText:toDate];
//        }
//        else if(indexPath.row==3)
//        {
//            [cell.textLabel setText:NSLocalizedString(@"Payment Type", nil)];
//            [cell.detailTextLabel setText:custType];
//        }
////        else if(indexPath.row==4)
////        {
////            [cell.textLabel setText:NSLocalizedString(@"Document Type", nil)];
////            [cell.detailTextLabel setText:DocType];
////        }
//        else
//        {
//            [cell.textLabel setText:@"View Report"];
//            
//        }
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        cell.textLabel.font=LightFontOfSize(14.0f);
//        cell.detailTextLabel.font=LightFontOfSize(14.0f);
//
//        return cell;
//    }
//}
//#pragma mark UITableView Delegate
//- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.row==0)
//    {
//        currencyTypeViewController=nil;
//        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
//        [currencyTypeViewController setTarget:self];
//        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
//        [currencyTypeViewController setContentSizeForViewInPopover:CGSizeMake(400,600)];
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
//        navigationController.navigationBar.barStyle = UIBarStyleBlack;
//        navigationController.navigationBar.translucent = NO;
//        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
//        currencyTypePopOver.delegate=self;
//        [currencyTypePopOver presentPopoverFromRect:CGRectMake(280, 95, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
//    }
//    else if(indexPath.row==1)
//    {
//        isFromDate=YES;
//        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,135, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
//    else if(indexPath.row==2)
//    {
//        isFromDate=NO;
//        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,165, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
//    else if(indexPath.row==3)
//    {
//        customVC=nil;
//        customVC = [[CustomerPopOverViewController alloc] init] ;
//        [customVC setTarget:self];
//        [customVC setAction:@selector(typeChanged:)];
//         [customVC setContentSizeForViewInPopover:CGSizeMake(200,250)];
//        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
//        customPopOver.delegate=self;
//        isCustType=YES;
//        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Cash",@"Current Cheque",@"PDC" , nil]];
//        [customPopOver presentPopoverFromRect:CGRectMake(280,195, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
////    else if(indexPath.row==4)
////    {
////        customVC = [[CustomerPopOverViewController alloc] init] ;
////        [customVC setTarget:self];
////        [customVC setAction:@selector(typeChanged:)];
////        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
////        isCustType=NO;
////        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Invoice",@"Credit Note" , nil]];
////        [customPopOver presentPopoverFromRect:CGRectMake(280,235, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
////    }
//    else
//    {
//            }
//    [tv deselectRowAtIndexPath:indexPath animated:YES];
//    
//}
//
//#pragma GridView Delegate
//- (int)numberOfRowsInGrid:(GridView *)gridView {
//    return dataArray.count;
//}
//
//- (int)numberOfColumnsInGrid:(GridView *)gridView {
//    return 5;
//}
//
//- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
//    NSString *title = @"";
//    
//    if (column == 0) {
//        title = [NSString stringWithFormat:@"    %@",NSLocalizedString(@"Document No", nil)];
//    } else if (column == 1) {
//        title = NSLocalizedString(@"Date", nil);
//    } else if (column == 2) {
//        title = NSLocalizedString(@"Name", nil);
//    } else if(column == 3) {
//        title = NSLocalizedString(@"Type", nil);
//    } else if(column == 4){
//        title = NSLocalizedString(@"Amount", nil);
//    }
//    else if(column == 4){
//        title = NSLocalizedString(@"Date", nil);
//    }
//    return title;
//}
//
//- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
//    NSDictionary *data = [dataArray objectAtIndex:row];
//    
//    NSString *value = @"";
//    if (column == 0) {
//        value = [NSString stringWithFormat:@"     %@", [data stringForKey:@"Collection_Ref_No"]];
//    } else if (column == 1) {
//        value = [data dateStringForKey:@"Collected_On" withDateFormat:NSDateFormatterMediumStyle];
//    } else if (column == 2) {
//        value = [data stringForKey:@"Customer_Name"];
//    } else if (column == 3) {
//        value = [data stringForKey:@"Collection_Type"];
//    } else if (column == 4) {
//        value = [data currencyStringForKey:@"Amount"];
//    }else if (column == 5) {
//    }
//    return value;
//}
//- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
//    if (columnIndex == 0) {
//        return 22;
//    } else if (columnIndex == 2) {
//        return 30;
//    }
//    else if (columnIndex == 1) {
//        return 15;
//    }
//    else if (columnIndex == 3) {
//        return 15;
//    }else
//        return 20;
//}
//-(IBAction)viewReportAction
//{
//    NSString *q = @"SELECT A.Collection_Ref_No,A.Collected_On, C.Customer_No, C.Customer_Name, A.Collection_Type,A.Collection_Type As PayType, A.Cheque_No, A.Cheque_Date, A.Bank_Name, A.Amount, A.Bank_Branch FROM TBL_Collection AS A INNER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  WHERE A.Collected_On >='{0}'  AND  A.Collected_On <='{1}' ";
//    
//    if(customerDict.count!=0)
//    {
//        q = [q stringByAppendingString:@" AND A.Customer_Id='{2}' AND A.Site_Use_Id='{3}'"];
//    }
//    if(![custType isEqualToString:@"All"])
//    {
//        q = [q stringByAppendingString:@" AND A.Collection_Type='{4}'"];
//    }
//    
//    q = [q stringByAppendingString:@" ORDER BY  A.Collection_Type ASC, A.Collection_Ref_No"];
//    
//    q = [q stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
//    q = [q stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
//    if(customerDict.count!=0)
//    {
//        q = [q stringByReplacingOccurrencesOfString:@"{2}" withString:[customerDict stringForKey:@"Customer_ID"]];
//        q = [q stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Site_Use_ID"]];
//    }
//    
//    
//    if(![custType isEqualToString:@"All"])
//    {
//        if([custType isEqualToString:@"Cash"])
//        {
//            q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:@"CASH"];
//        }
//        else if([custType isEqualToString:@"Current Cheque"])
//        {
//            q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:@"CURR-CHQ"];
//        }
//        else if([custType isEqualToString:@"PDC"])
//        {
//            q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:@"POST-CHQ"];
//        }
//    }
//    else
//    {
//        q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:custType];
//    }
//    
//    dataArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:q];
//    [gridView reloadData];
//
//}
//- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
//{
//    currencyTypePopOver=nil;
//    //datePickerPopOver=nil;
//    customPopOver=nil;
//    
//    currencyTypeViewController=nil;
//    customVC=nil;
//
//}
//
//- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
//{
//    
//    return YES;
//}
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    if ([self isViewLoaded] && self.view.window == nil)
//        self.view = nil;
//    // Dispose of any resources that can be recreated.
//}
//
//
//@end



//
//  PaymentReportViewController.m
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "PaymentReportViewController.h"
#import "MedRepDefaults.h"


@interface PaymentReportViewController ()

@end

@implementation PaymentReportViewController
@synthesize datePickerViewControllerDate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    

    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:[kPaymentSummaryTitle localizedCapitalizedString]];

    gridView = [[GridView alloc] initWithFrame:CGRectMake(283, 8, 733, 689)];
    [gridView setDataSource:self];
    [gridView setDelegate:self];
    [gridView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [gridView.layer setBorderWidth:1.0f];
    [self.view addSubview:gridView];
    
    filterTableView.backgroundView=nil;[filterTableView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [filterTableView.layer setBorderWidth:1.0f];
    
    filterTableView.backgroundColor=[UIColor whiteColor];

    
    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
    datePickerViewController.isRoute=NO;
    datePickerViewController.forReports=YES;
    
     self.datePickerViewControllerDate=datePickerViewController;
    
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
    navigationController.navigationBar.barStyle = UIBarStyleBlack;
    navigationController.navigationBar.translucent = NO;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:-1]; // note that I'm setting it to -1
    fromDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
    toDate=[formatter stringFromDate:[NSDate date]];
    custType = @"All";
    [self viewReportAction];
    formatter=nil;
    usLocale=nil;
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"Payment Documents");
    [self viewReportAction];
}
- (void)typeChanged:(NSString *)sender
{
    [customPopOver dismissPopoverAnimated:YES];
    if(isCustType)
    {
        custType=sender;
    }
    else
    {
        DocType = sender;
    }
    [filterTableView reloadData];
    [self viewReportAction];
}
- (void)dateChanged:(SWDatePickerViewController *)sender
{
    [datePickerPopOver dismissPopoverAnimated:YES];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    if(isFromDate)
    {
        NSString * sampleDate=fromDate;
        
        if ([formatter stringFromDate:sender.selectedDate]==nil) {
            fromDate=sampleDate;
        }
        else
        {
            fromDate=[formatter stringFromDate:sender.selectedDate];
            
        }
        
        
    }
    else
    {
        NSString* sampleToDate=toDate;
        
        if ([formatter stringFromDate:sender.selectedDate]==nil) {
            toDate=sampleToDate;
        }
        else
        {
            toDate=[formatter stringFromDate:sender.selectedDate];
            
            
        }
        
        
    }
    [filterTableView reloadData];
    
    if ([formatter stringFromDate:sender.selectedDate]==nil) {
        
    }
    else
    {
        [self viewReportAction];
        
    }
    formatter=nil;
    usLocale=nil;
    
}
- (void)currencyTypeChanged:(NSDictionary *)customer {
    [currencyTypePopOver dismissPopoverAnimated:YES];
    customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    [filterTableView reloadData];
    [self viewReportAction];
    
}

#pragma TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableViews numberOfRowsInSection:(NSInteger)section {
    return 4;
    
}
- (CGFloat)tableView:(UITableView *)tableViews heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, tv.bounds.size.width, 44)]  ;
    UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, headerView.bounds.size.width, headerView.bounds.size.height)]  ;
    [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [companyNameLabel setText:NSLocalizedString(@"Filter", nil)];
    [companyNameLabel setBackgroundColor:UIColorFromRGB(0xF6F7FB)];
    [companyNameLabel setTextColor:[UIColor darkGrayColor]];
    //    [companyNameLabel setShadowColor:[UIColor whiteColor]];
    //    [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
    [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];
    [companyNameLabel setTextAlignment:NSTextAlignmentCenter];
    
    [headerView addSubview:companyNameLabel];
    return headerView;
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return NSLocalizedString(@"Filter", nil);
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool
    {
        NSString *CellIdentifier = @"CustomerDetailViewCell";
        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
        }
        
        if(indexPath.row==0)
        {
            if(customerDict.count!=0)
            {
                cell.textLabel.text = [customerDict stringForKey:@"Customer_Name"];
            }
            else
            {
                cell.textLabel.text = NSLocalizedString(@"Select Customer", nil);
            }
        }
        else if(indexPath.row==1)
        {
            [cell.textLabel setText:NSLocalizedString(@"From Date", nil)];
            [cell.detailTextLabel setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"dd/MM/yyyy" scrString:fromDate]];
        }
        else if(indexPath.row==2)
        {
            [cell.textLabel setText:NSLocalizedString(@"To Date", nil)];
            [cell.detailTextLabel setText:[MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"dd/MM/yyyy" scrString:toDate]];
        }
        else if(indexPath.row==3)
        {
            [cell.textLabel setText:NSLocalizedString(@"Payment Type", nil)];
            [cell.detailTextLabel setText:custType];
        }
        //        else if(indexPath.row==4)
        //        {
        //            [cell.textLabel setText:NSLocalizedString(@"Document Type", nil)];
        //            [cell.detailTextLabel setText:DocType];
        //        }
        else
        {
            [cell.textLabel setText:@"View Report"];
            
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font=LightFontOfSize(14.0f);
        cell.detailTextLabel.font=LightFontOfSize(14.0f);
        
        return cell;
    }
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0)
    {
        currencyTypeViewController=nil;
        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
        [currencyTypeViewController setTarget:self];
        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
        [currencyTypeViewController setPreferredContentSize:CGSizeMake(400,600)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.translucent = NO;
        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        currencyTypePopOver.delegate=self;
        [currencyTypePopOver presentPopoverFromRect:CGRectMake(280, 95, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    else if(indexPath.row==1)
    {
        isFromDate=YES;
        
        
        NSLog(@"From Date is %@", fromDate);
        
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        
        [fromater setDateFormat:@"yyyy-MM-dd"];
        
        NSDate * finalDate=[fromater dateFromString:fromDate];
        
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        
        datePickerViewControllerDate.picker.date=finalDate;

        
        
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,135, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==2)
    {
        isFromDate=NO;
        
        
        NSLog(@"From Date is %@", fromDate);
        
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        
        [fromater setDateFormat:@"yyyy-MM-dd"];
        
        NSDate * finalDate=[fromater dateFromString:toDate];
        
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        
        datePickerViewControllerDate.picker.date=finalDate;

        
        
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,165, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==3)
    {
        customVC=nil;
        customVC = [[CustomerPopOverViewController alloc] init] ;
        [customVC setTarget:self];
        [customVC setAction:@selector(typeChanged:)];
        [customVC setPreferredContentSize:CGSizeMake(200,250)];
        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
        customPopOver.delegate=self;
        isCustType=YES;
        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Cash",@"Current Cheque",@"PDC" , nil]];
        [customPopOver presentPopoverFromRect:CGRectMake(280,195, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    //    else if(indexPath.row==4)
    //    {
    //        customVC = [[CustomerPopOverViewController alloc] init] ;
    //        [customVC setTarget:self];
    //        [customVC setAction:@selector(typeChanged:)];
    //        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
    //        isCustType=NO;
    //        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Invoice",@"Credit Note" , nil]];
    //        [customPopOver presentPopoverFromRect:CGRectMake(280,235, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    //    }
    else
    {
    }
    [tv deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return dataArray.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 5;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    
    if (column == 0) {
        title = [NSString stringWithFormat:@"    %@",NSLocalizedString(@"Document No", nil)];
    } else if (column == 1) {
        title = NSLocalizedString(@"Date", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Name", nil);
    } else if(column == 3) {
        title = NSLocalizedString(@"Type", nil);
    } else if(column == 4){
        title = NSLocalizedString(@"Amount", nil);
    }
    else if(column == 4){
        title = NSLocalizedString(@"Date", nil);
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSDictionary *data = [dataArray objectAtIndex:row];
    
    NSLog(@"customer name is %@", [data description]);
    
    
    NSString *value = @"";
    if (column == 0) {
        value = [NSString stringWithFormat:@"     %@", [data stringForKey:@"Collection_Ref_No"]];
    } else if (column == 1) {
        value = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"dd/MM/yyyy" scrString:[data stringForKey:@"Collected_On"]];
    } else if (column == 2) {
        
        
        //fetch customer name
        
        NSString* custNameQry=[NSString stringWithFormat:@"select Customer_Name from tbl_Customer where Customer_ID='%@' ",[data valueForKey:@"Customer_ID"] ];
        NSMutableArray* custNameArry=[[SWDatabaseManager retrieveManager]fetchDataForQuery:custNameQry];
        
        if (custNameArry.count>0) {
            
            value=[[custNameArry valueForKey:@"Customer_Name"] objectAtIndex:0];
        }
        
        else
        {
             value = [data valueForKey:@"Customer_Name"];
        }
        //value = [data stringForKey:@"Customer_Name"];
    } else if (column == 3) {
        
        NSString * collectionType=[data stringForKey:@"Collection_Type"];
        if ([collectionType isEqualToString:@"POST-CHQ"]) {
            collectionType =@"PDC";
        }
        
        value = collectionType;
    } else if (column == 4) {
        value = [data currencyStringForKey:@"Collected_Amount"];
    }else if (column == 5) {
    }
    return value;
}
- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    if (columnIndex == 0) {
        return 22;
    } else if (columnIndex == 2) {
        return 30;
    }
    else if (columnIndex == 1) {
        return 15;
    }
    else if (columnIndex == 3) {
        return 15;
    }else
        return 20;
}
-(IBAction)viewReportAction
{
    
    //Payment report redundancy fixed
    
//    NSString *q = @"SELECT   A.Collection_Ref_No,A.Collected_On, C.Customer_No, C.Customer_Name, A.Collection_Type,A.Collection_Type As PayType, A.Cheque_No, A.Cheque_Date, A.Bank_Name, A.Amount, A.Bank_Branch FROM TBL_Collection AS A INNER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  WHERE A.Collected_On >='{0}'  AND  A.Collected_On <='{1}' ";
    
   
//      NSString *q = @"select TBL_Collection.Collection_Ref_NO ,TBL_Collection.Collection_Type,TBL_Collection.Amount,TBL_Collection.Collected_On,TBL_Collection.Customer_ID, (TBL_Doc_Addl_Info.Custom_Attribute_5 *TBL_Collection.Amount ) AS Amount    from TBL_Collection inner join TBL_Doc_Addl_Info on TBL_Collection.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO where TBL_Collection.Collected_On>='{0}' AND TBL_Collection.Collected_On<='{1}' ";
//    
    
    
    
   NSString *q =@" SELECT IFNULL(I.Amount,'0')  AS Collected_Amount, A.Collected_On AS Start_Time, A.Collection_Ref_No AS Collection_Ref_No, A.Collected_On as DocDate,A.Collection_Type,A.Collected_On,A.Customer_ID,(IFNULL(TBL_Doc_Addl_Info.Custom_Attribute_5,'1') *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype  FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Collection_Invoices AS I on A.Collection_ID=I.Collection_ID  LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{0}' AND A.Collected_On<='{1}' ";
    
    
    
//    NSString *q = @"SELECT  A.Collection_Ref_No,A.Collected_On, C.Customer_No,C.Customer_Name, A.Collection_Type,A.Collection_Type As PayType, A.Cheque_No, A.Cheque_Date, A.Bank_Name,sum(A.Amount) as Amount, A.Bank_Branch FROM TBL_Collection AS A INNER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  WHERE A.Collected_On >='{0}'  AND  A.Collected_On <='{1}'   ";
    
    if(customerDict.count!=0)
    {
        q = [q stringByAppendingString:@" AND A.Customer_Id='{2}' AND A.Site_Use_Id='{3}'"];
    }
    if(![custType isEqualToString:@"All"])
    {
        q = [q stringByAppendingString:@" AND A.Collection_Type='{4}'"];
    }
    
//    q = [q stringByAppendingString:@" ORDER BY  TBL_Collection.Collection_Type ASC, TBL_Collection.Collection_Ref_No"];//GROUP BY C.Customer_Name
//    

    q = [q stringByAppendingString:@" ORDER BY  Start_Time DESC"];//GROUP BY C.Customer_Name
    

    
    
    
    q = [q stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
    q = [q stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    if(customerDict.count!=0)
    {
        q = [q stringByReplacingOccurrencesOfString:@"{2}" withString:[customerDict stringForKey:@"Customer_ID"]];
        q = [q stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Site_Use_ID"]];
    }
    
    NSLog(@"query for payment is %@", q);
    
    NSLog(@"cust type is %@", custType);
    
    if(![custType isEqualToString:@"All"])
    {
        if([custType isEqualToString:@"Cash"])
        {
            q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:@"CASH"];
        }
        else if([custType isEqualToString:@"Current Cheque"])
        {
            q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:@"CURR-CHQ"];
        }
        else if([custType isEqualToString:@"PDC"])
        {
//            q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:@"POST-CHQ"];
            q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:@"PDC"];
            
            
            
            q = [q stringByReplacingOccurrencesOfString:@"A.Collection_Type='PDC'" withString:@"(A.Collection_Type = 'PDC' OR A.Collection_Type='POST-CHQ')"];
            

            NSLog(@"PDC updated qry %@",q);
            
  
            
        }
    }
    else
    {
        q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:custType];
    }
    
    
   // q= [q stringByAppendingString:@" ORDER BY A.Collected_On DESC"];
    
    
    NSString * finalQueryStr=q;
    
    NSLog(@"payment report  final query is %@", finalQueryStr);
    
    NSLog(@"payment report qry %@", q);
    
    
    dataArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:q];
    [gridView reloadData];
    
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    currencyTypePopOver=nil;
    //datePickerPopOver=nil;
    customPopOver=nil;
    
    currencyTypeViewController=nil;
    customVC=nil;
    
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}


@end

