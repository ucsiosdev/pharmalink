//
//  MedRepDefaults.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepDefaults.h"
#import "MedRepQueries.h"




#import "SystemUtilitiesViewController.h"


@implementation MedRepDefaults

+(NSMutableArray*)filterArray:(NSMutableArray*)contentArray searchText:(NSString*)searchText

{
    
    
    

    NSPredicate *bPredicate =
    [NSPredicate predicateWithFormat:@"SELF contains[c] '%@'", searchText];
    NSArray *beginWithB =
    [contentArray filteredArrayUsingPredicate:bPredicate];
    
    return [beginWithB mutableCopy];
    
    
}

+(BOOL)isDeviceAvailableSpaceMoreThanBufferSizeAfterThisFileDownload:(NSString *)fileSize
{
    NSString* freeSpace=[SystemUtilitiesViewController freeDiskSpace];
    
    float freespaceInKiloBytes=([freeSpace floatValue]*1024)*1024;
    float freeSpaceAfterDownload=(freespaceInKiloBytes-[fileSize floatValue]);
    if( freeSpaceAfterDownload >(500*1024))
    {
        return YES;
    }
    return NO;
}



+(BOOL) validateEmail:(NSString*) emailString
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%i", regExMatches);
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}


+ (BOOL)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate
{
    
    
    

    
    
    
    NSDate* enddate = checkEndDate;
    NSDate* currentdate = [NSDate date];
    
    
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;

    
    
    BOOL today = [[NSCalendar currentCalendar] isDateInToday:checkEndDate];

    if (today==YES)
        return NO;

    
   else if (secondsBetweenDates < 0)

    
        return YES;
    else
        return NO;
}

+(BOOL)isValidEmail
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}


+(UIColor*)generateRandomColor
{
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    return color;
}

+(NSString*)fetchInitials:(NSString*)name{
    
    
    
    //test
    
    
//    NSMutableString * firstCharacters = [NSMutableString string];
//    NSArray * words = [name componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    
//        for (NSString * word in words) {
//            if ([word length] > 0) {
//                NSString * firstLetter = [word substringToIndex:1];
//                [firstCharacters appendString:[firstLetter uppercaseString]];
//                
//                if (firstCharacters.length>2) {
//                    
//                    firstCharacters =[firstCharacters substringFromIndex:1];
//                }
//            }
//        }
//    
//    
//    
//       NSLog(@"test initial %@", [firstCharacters description]);
    
    
    
    
    NSLog(@"name string is %@", [name description]);
    
    
    NSArray *tempArray = [name componentsSeparatedByString:@" "];
    
    NSLog(@"temp array  header is %@", [tempArray description]);

  NSString*  str1 = [tempArray objectAtIndex:0];
    NSLog(@"%@", str1);
    
    NSString* initials;
    NSRange whiteSpaceRange = [name rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        NSLog(@"Found whitespace");
        
        
        if ([[tempArray objectAtIndex:1]length]==0) {
            
            //empty string only one word name
            
            NSLog(@"temp array  with white spaces is %@", [[tempArray objectAtIndex:0] description]);
            
            
            initials=[[[tempArray objectAtIndex:0] substringToIndex:1]stringByAppendingString:[[tempArray objectAtIndex:0] substringToIndex:2]];

                //initials=[[tempArray objectAtIndex:0] substringToIndex:2];
            NSLog(@"SINGLE WORD NAME INITIALS ARE %@", initials);
            
        }
        
        else
        {
        
          //  NSLog(@"temp array is %@",[tempArray description]);
            
           // NSLog(@"temp array object is %@",[[tempArray objectAtIndex:0]description]);
            
            
        initials=[[[tempArray objectAtIndex:0] substringToIndex:1]stringByAppendingString:[[tempArray objectAtIndex:1] substringToIndex:1]];
        }
        NSLog(@"initials are %@", initials);
    }
    
    else
    {
        
        if (name.length>=2) {
            initials=[name substringToIndex:2];
        }
        
        
        initials=[[name substringToIndex:1] stringByAppendingString:[name substringToIndex:2]];
        NSLog(@"initials are %@", initials);
   
    
    }
    
    
    return [initials uppercaseString];
    
    
}




+(NSString*)refineDateFormat:(NSString *)srcFormat destFormat:(NSString *)destFormat scrString:(NSString *)srcString

{
    
    /**
     cheat sheet
     
     Tuesday, Aug 9, 2016
     EEEE, MMM d, yyyy
     
     08/09/2016
     MM/dd/yyyy
     
     08-09-2016 06:09
     MM-dd-yyyy HH:mm
     
     Aug 9, 6:09 AM
     MMM d, H:mm a
     
     August 2016
     MMMM yyyy
     
     Aug 9, 2016
     MMM d, yyyy
     
     Tue, 9 Aug 2016 06:09:29 +0000
     E, d MMM yyyy HH:mm:ss Z
     
     2016-08-09T06:09:29+0000
     yyyy-MM-dd'T'HH:mm:ssZ
     
     09.08.16	
     dd.MM.yy
     
     **/
 
    srcString=[SWDefaults getValidStringValue:srcString];
    
    if(srcString.length==0)
    {
        return nil;
    }
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone systemTimeZone];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];

    [dateFormatter setDateFormat:srcFormat];
    NSDate* myDate = [dateFormatter dateFromString:srcString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone systemTimeZone];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    [formatter setDateFormat:destFormat];
    NSString *stringFromDate = [formatter stringFromDate:myDate];
    
    if (stringFromDate.length>0) {
        
        return stringFromDate;
    }
    
    else
    {
        return nil;
    }

}

+ (UIViewController *)backViewController:(UINavigationController*)navController
{
    NSInteger numberOfViewControllers = navController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [navController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}


+ (NSMutableArray*)fetchFirstandLastDatesofCurrentMonth:(NSDate *)date

{
    
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit;
    NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
    
    
    NSDate * firstDateOfMonth = [MedRepDefaults returnDateForMonth:comps.month year:comps.year day:1];
    NSDate * lastDateOfMonth = [MedRepDefaults returnDateForMonth:comps.month year:comps.year day:0];
    
    NSLog(@"date %@", date);              // date 2013-06-20
    NSLog(@"First day of month %@", firstDateOfMonth); // firstDateOfMonth 2013-06-01
    NSLog(@"Last day of month %@", lastDateOfMonth);   // lastDateOfMonth  2013-06-30
    
    
    NSMutableArray * dateArray=[[NSMutableArray alloc]init];
    
    [dateArray addObject:firstDateOfMonth];
    [dateArray addObject:lastDateOfMonth];
    
    
    return dateArray;
}


+ (NSDate *)returnDateForMonth:(NSInteger)month year:(NSInteger)year day:(NSInteger)day {
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    [components setDay:day];
    [components setMonth:month];
    [components setYear:year];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    return [gregorian dateFromComponents:components];
}

+(NSString*)fetchTodaysDate
{
    NSString* dateString=[[NSMutableString alloc]init];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];

    [df setDateFormat:@"yyyy-MM-dd"];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];

    dateString = [df stringFromDate:[NSDate date]];
    
    NSLog(@"Date is %@", dateString);
    return dateString;
    
}

//+(NSDate *) startOfMonth
//{
////    NSCalendar * calendar = [NSCalendar currentCalendar];
////    


////    NSDate * startOfMonth = [calendar dateFromComponents: currentDateComponents];
////    
////    return startOfMonth;
//    
//    
//    
//    
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
//                                                                   fromDate:[NSDate date]];
//    components.day = 1;
//    
//    
//    
//    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
//    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    [formatterTime setLocale:usLocaleq];
//    [formatterTime setTimeZone:[NSTimeZone systemTimeZone]];
//
//
//    
//    
//    NSDate *firstDayOfMonthDate = [[NSCalendar currentCalendar] dateFromComponents: components];
//    NSLog(@"First day of month: %@", [formatterTime stringFromDate:firstDayOfMonthDate]);
//    NSString* dateStr=[formatterTime stringFromDate:firstDayOfMonthDate];
//    
//    
//    
//    
////    NSDateFormatter *formatterTime1 = [NSDateFormatter new] ;
////    NSLocale *usLocaleq1 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
////    [formatterTime1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
////    [formatterTime1 setLocale:usLocaleq1];
////    //[formatterTime1 setTimeZone:[NSTimeZone systemTimeZone]];
////    formatterTime1.timeZone = [NSTimeZone localTimeZone];
////    
//    
////    NSLog(@"Date for locale %@: %@",
////          [[formatterTime1 locale] localeIdentifier], [formatterTime1 dateFromString:dateStr]);
//
//    
//    NSDate * test=[formatterTime dateFromString:dateStr];
//    
//    NSDate * firstDate=[MedRepDefaults convertToDateFrom:dateStr];
//    
//    
//    NSLog(@"test date %@", test);
//    
//    //return firstDate;
//    
//    
//    
//    
//    
//    NSString *dateString = @"2015-08-05 00:00:00";
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    
//    NSDate *date = [MedRepDefaults toLocalTime];
//    NSLog(@"first date is %@", date);
//
//    
//}


+(NSDate *) startOfMonth
{
    
    
    NSCalendar *anotherCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    anotherCalendar.timeZone = [NSTimeZone timeZoneWithName:NSGregorianCalendar];
    
//    NSDateComponents *anotherComponents = [anotherCalendar components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitNanosecond) fromDate:[NSDate date]];
    
    
    
    
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                                   fromDate:[NSDate date]];
    components.day = 1;
    
    
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    [formatterTime setTimeZone:[NSTimeZone systemTimeZone]];
    
    
    
    
    NSDate *firstDayOfMonthDate = [[NSCalendar currentCalendar] dateFromComponents: components];
    
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: firstDayOfMonthDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: firstDayOfMonthDate];
}



+(NSDate *) convertToDateFrom:(NSString *) dateString
{
    if (dateString == nil || [dateString isEqual:@""]) return nil; //return nil if dateString is empty
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
     NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [df setLocale:usLocaleq];
    [df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC+4:00"]];
    
    
    NSDate *date = [df dateFromString:dateString];
    
    return date;
}

+ (NSDate *) endOfMonth
{
    NSCalendar * calendar = [NSCalendar currentCalendar];
    
    NSDate * plusOneMonthDate = [MedRepDefaults dateByAddingMonths: 1];
    NSDateComponents * plusOneMonthDateComponents = [calendar components: NSYearCalendarUnit | NSMonthCalendarUnit fromDate: plusOneMonthDate];
    NSDate * endOfMonth = [[calendar dateFromComponents: plusOneMonthDateComponents] dateByAddingTimeInterval: -1]; // One second before the start of next month
    
    return endOfMonth;
    
    
    
}


+ (NSDate *) dateByAddingMonths: (NSInteger) monthsToAdd
{
    NSCalendar * calendar = [NSCalendar currentCalendar];
    
    NSDateComponents * months = [[NSDateComponents alloc] init];
    [months setMonth: monthsToAdd];
    
    return [calendar dateByAddingComponents: months toDate: [NSDate date] options: 0];
}


+ (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate {
    return [date compare:firstDate] == NSOrderedDescending &&
    [date compare:lastDate]  == NSOrderedAscending;
}



+(BOOL)isDemoPlanAvailableforDate:(NSDate*)selectedDate
{
    
    if (selectedDate!=nil) {
        
    
    
    NSDateFormatter* systemDateFormat=[[NSDateFormatter alloc]init];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [systemDateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [systemDateFormat setLocale:usLocaleq];
    
    [systemDateFormat setTimeZone:[NSTimeZone timeZoneWithName: @"UTC"]];
    
    
    //fetch valid from and valid to dates from tbl_Demo_Plan
    
    NSString* validFrom=[MedRepQueries fetchDateStringofDemoPlan:@"Valid_From"];
    
    NSString* validTo=[MedRepQueries fetchDateStringofDemoPlan:@"Valid_To"];
    
    NSLog(@"valid from and valid to from database are %@ %@", validFrom,validTo);
    
    
    
    NSDate* validFromDate=[systemDateFormat dateFromString:validFrom];
    
    
    NSDate* validToDate=[systemDateFormat dateFromString:validTo];
    
    NSLog(@"valid from and valid to Dates are %@ %@", validFromDate,validToDate);
    
    
    
    BOOL status=[MedRepDefaults isDate:selectedDate inRangeFirstDate:validFromDate lastDate:validToDate];
    
    if (status ==YES) {
        
        NSLog(@"DEMO PLAN AVAILABLE FOR DATE");
        
        return YES;
        
    }
    
        else
        {
            return NO;
        }
        
    }
    
    else
    {
        return NO;
    }

}


#pragma mark Signature Delegate Methods


+(void)setDoctorSignature:(id)signatureData

{
    [[NSUserDefaults standardUserDefaults]setValue:signatureData forKey:@"doctorSignature"];
}

+(void)clearDoctorSignature

{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"doctorSignature"];
    
    
}

+(id)fetchDoctorSignature
{
    id signature=[[NSUserDefaults standardUserDefaults]valueForKey:@"doctorVisitSignature"];
    return signature;
    
}

+(void)setSignatureCapturedForCurrentCall:(BOOL)isSignatureCaptured
{
    [[NSUserDefaults standardUserDefaults]setBool:isSignatureCaptured forKey:@"isSignatureCaptured"];
}
+(BOOL)isSignatureCapturedForCurrentCall
{
    return [[NSUserDefaults standardUserDefaults]boolForKey:@"isSignatureCaptured"];
}



+(void)setVisitStartTime:(NSString*)startTime

{
    [[NSUserDefaults standardUserDefaults]setObject:startTime forKey:@"VisitStartTime"];
}

+(void)clearVisitStartTime

{
    
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"VisitStartTime"];

}

+(NSString*)fetchVisitStartTime
{
    NSString* visitStartTime=[[NSUserDefaults standardUserDefaults]objectForKey:@"VisitStartTime"];
    return visitStartTime;
    
}



+ (NSString *)lastIssueNoteReference {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastIssueNoteRef"];
}

+ (void)setIssueNoteReference:(NSString *)ref {
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastIssueNoteRef"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)initializeMedRepDefaults {
    
    NSString *lastOrderReference = [MedRepDefaults lastIssueNoteReference];
    if (!lastOrderReference || lastOrderReference.length == 0) {
    [MedRepDefaults setIssueNoteReference:@"M000I0000000000"];
    
}
}

+(NSString*)fetchDocumentsDirectory

{
    NSString*documentsDirectory;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    

    return documentsDirectory;
    
}

+(NSString *)getDefaultStringForEmptyString:(NSString *)str
{
    if([[[SWDefaults getValidStringValue:str] trimString] length]==0)
        return KNotApplicable;
    else
        return str;
    
}
#pragma mark Blur effect image rendering methods

+ (UIImage *)takeSnapshotOfView:(UIView *)view
{
    CGFloat reductionFactor = 1;
    UIGraphicsBeginImageContext(CGSizeMake(view.frame.size.width/reductionFactor, view.frame.size.height/reductionFactor));
    [view drawViewHierarchyInRect:CGRectMake(0, 0, view.frame.size.width/reductionFactor, view.frame.size.height/reductionFactor) afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)blurWithImageEffects:(UIImage *)image
{
    
    //    UIImageView * temp =[[UIImageView alloc]initWithFrame:CGRectMake(0, 64, image.size.width, image.size.height)];
    //
    //    temp.image=image;
    //
    //    [self.view addSubview:temp];
    
    return [image applyBlurWithRadius:2 tintColor:[UIColor colorWithWhite:0 alpha:0] saturationDeltaFactor:1 maskImage:nil];
}


+(UIImage*)generateThumbImageforPdf:(NSString*)filePath

{
    NSURL* pdfFileUrl = [NSURL fileURLWithPath:filePath];
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfFileUrl);
    CGPDFPageRef page;
    
    CGRect aRect = CGRectMake(0, 0, 160, 160); // thumbnail size
    UIGraphicsBeginImageContext(aRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIImage* thumbnailImage;
    
    
    NSUInteger totalNum = CGPDFDocumentGetNumberOfPages(pdf);
    
    for(int i = 0; i < totalNum; i++ ) {
        
        
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 0.0, aRect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        
        CGContextSetGrayFillColor(context, 1.0, 1.0);
        CGContextFillRect(context, aRect);
        
        
        // Grab the first PDF page
        page = CGPDFDocumentGetPage(pdf, i + 1);
        CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(page, kCGPDFMediaBox, aRect, 0, true);
        // And apply the transform.
        CGContextConcatCTM(context, pdfTransform);
        
        CGContextDrawPDFPage(context, page);
        
        // Create the new UIImage from the context
        thumbnailImage = UIGraphicsGetImageFromCurrentImageContext();
        
        //Use thumbnailImage (e.g. drawing, saving it to a file, etc)
        
        CGContextRestoreGState(context);
        
    }
    
    
    UIGraphicsEndImageContext();
    CGPDFDocumentRelease(pdf);
    
    return thumbnailImage;
}



+(NSDictionary*)fetchBarAttributes
{
    NSDictionary *barattributes = [NSDictionary dictionaryWithObject:NavigationBarButtonItemFont
                                                              forKey:NSFontAttributeName];
    
    return barattributes;
    
}

+(NSString *)getValidStringValue:(id)inputstr
{
    if ([inputstr isEqual:(id)[NSNull null]] || inputstr==nil)
    {
        return [[NSString alloc]init];
    }
    if([inputstr isKindOfClass:[NSString class]] && ([inputstr isEqualToString:@"<null>"] || [inputstr isEqualToString:@"<Null>"]) )
    {
        return [[NSString alloc]init];
    }
    
    return [NSString stringWithFormat:@"%@",inputstr];
}


+(VisitDetails*)fetchVisitDetailsObject:(NSMutableDictionary*)selectedVisitDetailsDict
{
   VisitDetails* visitDetails=[[VisitDetails alloc]init];
    visitDetails.ActualPlannedVisitID=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"ActualPlannedVisitID"]];
    visitDetails.Actual_Visit_ID=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Actual_Visit_ID"]];
    visitDetails.Address=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Address"]];
    visitDetails.Class=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Class"]];
    visitDetails.Contact_ID=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Contact_ID"]];
    visitDetails.Created_At=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Created_At"]];
    visitDetails.Created_By=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Created_By"]];
    visitDetails.Demo_Plan_ID=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Demo_Plan_ID"]];
    visitDetails.Doctor_Class=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Doctor_Class"]];
    visitDetails.Doctor_ID=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Doctor_ID"]];
    visitDetails.Doctor_Name=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Doctor_Name"]];
    visitDetails.EMP_ID=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"EMP_ID"]];
    visitDetails.Last_Visited_At=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Last_Visited_At"]];
    visitDetails.Latitude=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Latitude"]];
    visitDetails.Location_ID=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Location_ID"]];
    visitDetails.Location_Name=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Location_Name"]];
    visitDetails.Location_Type=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Location_Type"]];
    visitDetails.Longitude=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Longitude"]];
    visitDetails.OTC=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"OTC"]];
    visitDetails.Objective=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Objective"]];
    visitDetails.Pharmacy_Contact_Name=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Pharmacy_Contact_Name"]];
    visitDetails.Planned_Visit_ID=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Planned_Visit_ID"]];
    visitDetails.Selected_Index=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Selected_Index"]];
    visitDetails.Specialization=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Specialization"]];
    visitDetails.Trade_Channel=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Trade_Channel"]];
    visitDetails.Visit_Date=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Visit_Date"]];
    visitDetails.Visit_Status=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Visit_Status"]];
    visitDetails.Visit_Type=[SWDefaults getValidStringValue:[selectedVisitDetailsDict valueForKey:@"Visit_Type"]];
    visitDetails.presentationArray=[[NSMutableArray alloc]init];
    return visitDetails;

}

#pragma mark Visit FaceTime
+(NSString*) timeBetweenSatrtTime:(NSString *)startTime andEndTime:(NSString*)endTime {
    NSTimeInterval elapsedTimeDisplayingPoints =  [[MedRepDefaults convertToDateFrom:endTime] timeIntervalSinceDate:[MedRepDefaults convertToDateFrom:startTime]];

    NSLog(@"start and end time while converting are Start Time:%@, End Time:%@",startTime,endTime);
    NSInteger ti = (NSInteger)elapsedTimeDisplayingPoints;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

#pragma mark UIAlertController Methods
+(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)viewController{
    [UIView animateWithDuration:0 animations: ^{
        [viewController.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [viewController presentViewController:alert animated:YES completion:nil];
    }];
    
}
+(void) ShowConfirmationAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage andActions:(NSMutableArray *)actionsArray withController:(UIViewController *)viewController{
    [UIView animateWithDuration:0 animations: ^{
        [viewController.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                      message:NSLocalizedString(alertMessage,nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        for (NSInteger i=0; i<actionsArray.count; i++) {
            [alert addAction:[actionsArray objectAtIndex:i]];
        }
        [viewController presentViewController:alert animated:YES completion:nil];
    }];
    
}
+(UIAlertAction*)GetDefaultCancelAlertActionWithTitle:(NSString*)title
{
    UIAlertAction* CancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(title,nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                   }];
    return CancelAction;
}
-(NSMutableDictionary *)DeleteEmptyStringKeysInDic:(NSMutableDictionary *)dic
{
    NSMutableDictionary *tempDic=[dic mutableCopy];
    

    
    for (NSString *key in [tempDic allKeys]) {
        if([[[SWDefaults getValidStringValue:[dic valueForKey:key]] trimString]isEqualToString:@""] )
            [dic removeObjectForKey:key];
    }

    
    
    
    return dic;
}

+(NSMutableArray*)fetchDaystoDisable:(NSMutableArray*)weekDayStringsArray
{
    NSMutableArray * refinedArray=[[NSMutableArray alloc]init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter weekdaySymbols];
    
    NSMutableArray * weekDaySymbolsArray=[[dateFormatter weekdaySymbols] mutableCopy];
    if (weekDayStringsArray.count>0) {
        for (NSInteger i=0; i<weekDayStringsArray.count; i++) {
            NSString * currentDay = [SWDefaults getValidStringValue:[weekDayStringsArray objectAtIndex:i]];
            if ([weekDaySymbolsArray containsObject:currentDay]) {
                [refinedArray addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[weekDaySymbolsArray indexOfObject:currentDay]]];
            }
        }
    }
    //NSLog(@"days to enable %@", refinedArray);
    //now we got the number of the day (sunday -0) etc not get the days which are not in this array and block those days
    
    //NSLog(@"refined array is %@",refinedArray);
    
    NSMutableArray * daysToBlockArray=[[NSMutableArray alloc]init];
    
    for (NSInteger i=0; i<weekDaySymbolsArray.count; i++) {
        
        if (![refinedArray containsObject:[NSString stringWithFormat:@"%ld",(long)i]]) {
            [daysToBlockArray addObject:[NSString stringWithFormat:@"%ld",(long)i+1]];
        }
        
    }
    return refinedArray;
}


@end


