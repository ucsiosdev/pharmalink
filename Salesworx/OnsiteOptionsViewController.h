//
//  OnsiteOptionsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/7/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "SalesWorxCustomClass.h"
#import "SWAppDelegate.h"
#import "NSString+Additions.h"
@protocol updateVisitOptionButtons <NSObject>

-(void)selectedVisitOption:(NSString*)visitOption;

@end

@interface OnsiteOptionsViewController : UIViewController<UIAlertViewDelegate>

{
    id delegate;
}

- (IBAction)segmentSwitch:(UISegmentedControl *)sender ;

@property (nonatomic, assign) id delegate;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property(strong,nonatomic) SalesWorxVisit * currentVisit;
@end
